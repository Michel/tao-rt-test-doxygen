# User visible changes in TAO library API

## Versions 0.8

* New utility program `andor-features`.

* New naming conventions.  Most functions are named following the template
  `tao_$TYPE_$VERB` or `tao_$TYPE_$VERB_$NOUN` with `$TYPE` the type of the
  main argument, `$VERB` a verb indication the action, and `$NOUN` a noun.
  This follows conventions in many large C libraries and helps to quickly
  figure out the methods associated with a given object type (there are almost
  500 functions in the main TAO library and almost 700 functions in all TAO
  libraries).

* Simplify and speed-up error management.  In the previous versions, error
  stack almost never had a depth greater than one which means that it would be
  sufficient to have each thread just memorize its last error.  The information
  about the last error is now stored in a per-thread static data of small size
  with qualifier `static _Thread_local` (or `static thread_local` with
  `<threads.h>`).

  Simplifications brought by the new error handling system:

  - Implementation is much more simple.

  - No needs to perform any cleanup on exit and no risks of memory leaks.

  - Bindings are much easier to write.

  Expected speed-up:

  - Only 3ns to retrieve the per-thread data instead of 13ns in former code.

  - No needs to allocate and free memory for each error.


## Versions 0.7

* Deformable mirror client/server.

* Standardization of naming conventions (standard C imposes that names prefixed
  by `_` are reserved):

  - Public symbols and macros are prefixed by `tao_` and `TAO_`.

  - Private/helper symbols and macros are prefixed by `tao_` or `TAO_` and
    suffixed by `_`.

  - Types are no longer suffixed by `_t` (which is reserved for the C).

  - `struct`/`enum`/`union` have the same name as their `typedef` counterpart
    with a final `_` (to avoid confusion in Doxygen documentaion of opaque
    structures).

  - Headers define a macro `TAO_..._H_` to avoid being included more than once.

* Provide thread-pools.

* Image server for cameras connected to a Phoenix frame-grabber.

* Provide a unified camera model.

* Implement general means to identify pixel encoding.

* Shared objects now have an *owner* name which is used to identify the
  owner/creator of the resource.  This is used to provide the XPA access point
  of the image servers.

* TAO provide servers using the [XPA Messaging
  System](https://github.com/ericmandel/xpa) and implemented on top of XPA
  servers.

* TAO is split in several parts with separate libraries (`libtao.so`,
  `libtao-xpa.so`, `libtao-fits.so`, `libtao-andor.so`, `libtao-phoenix.so`,
  ...) and headers to restrict dependencies with other libraries.

* Add some tests.  Type `make check` to run the tests, look for `*.log` files
  for the detailed results.

* Interface to r/w locks.

* Timed operations now return a `tao_status` value which may be `TAO_TIMEOUT`.
  Function `tao_get_absolute_timeout()` returns a `tao_timeout` value which
  reflects the different possible cases: errors, timeout alread expired,
  timeout is occuring now, in the future or never.  Function
  `tao_is_finite_absolute_time()` has been suppressed as the value returned by
  `tao_get_absolute_timeout()` is sufficient to figure out the kind of timeout.

* Allow for process shared mutexes and condition variables.

* Use `tao_time`  for storing the time with fields of known type (unlike `struct timespec`
  whose field types depend on the implementation).

* Manage to have '-Wall -Werror -O2' the default compilation flags.

* Include `stdbool.h` and use the macros provided by this header: `bool` for
  the type of a boolean and `true` or `false` values.

* Shared images may have associated weights.

* Implement the policy for sharing images allowing no readers if there is a
  writer and at most one writer.

* C type `long` is used for array dimensions, number of elements and indices.

* Error messages are correctly printed in Julia interface.

* Anonymous semaphores are used to signal new images.
