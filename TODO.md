## Important

* Choosing a sub-image with ActiveSilicon Phoenix frame grabber is not yet
  fully working (chosen ROI must respect hardware constraints: horizontal
  offset and width must be multiples of 16, vertical offset and height must be
  multiples of 2).

* Setting connection speed with ActiveSilicon Phoenix frame grabber is not yet
  fully working (but will be possible).

* When some lengths of the region to copy are equal to one they can be
  suppressed to speed-up copy.  The only difficulty is to properly collapse the
  corresponding array dimensions of the source and of the destination.  This
  would lead to very efficient slice extraction (possibly with conversion).

* Shared camera members corresponding to a dimension should be `long` not `int`.

* Weighted images implemented as a cube of 2 images do preserve alignment.

* Hitting Ctrl-C at a server terminal kills the server but never calls `atexit`
  cleanup functions.

* Quitting a process does not release its locks.  This is intended because
  having a (write) lock means that the protected ressources are potentially in
  an intermediate state and, obviously, releasing the lock automatically is a
  bad idea because the state cannot be fixed.  Perhaps, read-only locks can be
  (and are?) released automatically?  If locks are used to wait on a condition,
  it is unlikely that the process dies in the middle (i.e., between acquiring
  the lock and starting to wait).

* Perhaps it is a better idea to use r/w locks of the POSIX Threads Library
  instead of my own implementation (whose motivation was to combine the of
  exclusive lock, r/w lock and condition variable with one mutex and one
  condition variable).

* See https://www.drdobbs.com/soft-real-time-programming-with-linux/184402031
  for soft-real-time programming hints.


## Things to do

* Compile `copy.c`, preprocessing, etc. with good optimization flags by
  default: `-DNDEBUG -O3 -fomit-frame-pointer -mavx2 -mfma`.

* Use per-thread data to simplify error management.  `tao_report_errors` shall
  use the error handler.

* Use enum constants `TAO_READ_ONLY` and `TAO_READ_WRITE` to lock r/w locks.
  This may yield more readable code.

* Use `inline` which is an allowed keyword in standard C99 (see
  http://www.drdobbs.com/the-new-c-inline-functions/184401540 for usage).
  For instance: `tao_strlen`, `tao_time_to_...`, etc.

* All members of shared objects must have a specific size (e.g. use `int32_t`
  or `int64_t` instead of `int` or `long`).

* Align memory for Andor buffers.

* Simplify management of acquisition buffers.  Only the first pending buffer is
  accessible (via `tao_get_acquisition_buffer(cam)` and can be dropped by
  `tao_release_acquisition_buffer()`.  Call
  `tao_number_of_pending_acquisition_buffers()` to figure out how many buffers
  are available and `tao_wait_acquisition_buffer()` to acquire a new buffer.

* Change high level prefix `phnx_` to `phoenix_` in TAO-Phoenix library.

* Have a `tao_print_camera_settings` which takes a camera argument and calls
  `tao_print_camera_information`.  Have a more specific version
  `andor_print_camera_settings`, `phoenix_print_camera_settings`, etc.  Other
  possibility: add a virtual method to print specific camera settings.
  Provide `tao_update_camera_settings` or `tao_update_camera_configuration`.

* Locking the shared camera for updating the state, the frame counter, etc. is
  probably overkilling.  It should be sufficient to use atomic operations.  I
  do not know how this behaves through shared memory.  Atomic operations can
  also be assumed for setting variables such as `quitting`, `debuglevel`, etc.

* Make acquisition buffers available after stopping acquisition.

* It is stupid to allocate `buffers+1` acquisition buffers.  In principle,
  the code is written so as to avoid overruns.

* Retrieve camera model.

* Use `cam->pending` instead of event bit to detect that there are pending
  frames in Phoenix server.

* In TAO Phoenix code, we could just call `PHX_StreamRead` with
  `PHX_BUFFER_GET` whenever an acquisition buffer (always the first pending
  one) is returned by `tao_wait_acquisition_buffer()` but this function just
  returns the first pending buffer if there are any pending buffers.  So, in
  the current code, we have to update the address of the acquisition buffer
  whenever possible, that is right after releasing a buffer if there are at
  least one pending buffer and right after receiving a
  `PHX_INTRPT_BUFFER_READY` event if there are no other pending buffers.  This
  is a bit ugly and intricated (although it amounts to calling `PHX_StreamRead`
  with `PHX_BUFFER_GET` the minimum number of times) and there is always the
  risk of loosing the synchronization (e.g., in case of errors).  A possibility
  would be to have another virtual method being called if the buffer address is
  `NULL`.  Another possibility would be to have the low level API completely
  handles the `drop` argument of `tao_wait_acquisition_buffer()`.  This latter
  has my preference because it results in more consistent code with the logic
  not spread accross several functions.  The high-level API just makes sure
  arguments are correct.

* Make `tao_normalize_time` takes 2 arguments and provide inline version.

* Rename `tao_shared_camera_t` as `tao_remote_camera_t`?

* Call `AC_HEADER_STDBOOL()` inteads of `AC_CHECK_HEADERS([stdbool.h])` and use
  `HAVE__BOOL` instead of `HAVE_STDBOOL_H`.  See macro `AC_HEADER_STDBOOL` in
  Autoconf doc. and write a `tao-config.h` file with specific macros and
  definition which are system dependent but are needed to compile other
  software with TAO.

* Fix types of fields in structures in `tao-private.h`.

* `size_t` -> `long` in most cases.

* Use r/w locks instead of mutexes to lock (most) shared resources.

* Clarify the distinction between shared camera, camera, hardware camera, etc.

* Instead of immediately attempt to reflect the configuration of the camera
  after any parameter change, it may be better to defer that until starting the
  acquisition.  Optionally a function, say `tao_apply_camera_configuration`,
  may be called before to trigger device configuration before.  Other
  functions, say `tao_check_camera_configuration` and
  `tao_reset_camera_configuration`, may be called to check the current settings
  (before applying them) or to revert to actual camera settings.

* Specialize `attach(::Type{TAO.SharedArray{T}},id)` and
  `attach(::Type{TAO.SharedArray{T,N}},id)` to have a more specific returned
  value.

* Use 64-bit signed integers for storing the time (since the Epoch) in
  microseconds.  This is what is done in Apache Portable Runtime library (APR).
  This is OK for times in the range ±292,271 yr which is largely sufficient for
  now (and the future!).  The resolution of 1 µs is about what is provided by
  current real-time Linux kernel and is sufficient for adaptive optics systems.

* Use a more flexible error reporting system so that a more detailed error
  message can be given (for instance, for a function changing a parameter, the
  function name and the parameter name).  To that end, we may use a dynamic
  buffer to build up the error message.  For efficiency reasons, we do not want
  to do that when error is registered but only for reporting errors.

* Use `LD_LIBRARY_PATH` (or equivalent) to locate libaries.  Perhaps there is
  already some `AC_...` macro which does this.

* Require that a shared object be locked before being detached.

* Rename `_finalize_` methods as `_destroy_`.

* Rename `tao_destroy_camera` methods as `tao_close_camera`.

* Measure and compare the times taken to trigger some event via a Unix domain
  or internet socket (with the peer waiting the socket file descriptor to be
  ready for reading via `select` or equivalent function), via a process-shared
  monitor or via a semaphore.  A process-shared monitor is a shared mutex and
  condtion variable (like in TAO shared objects).  Semaphores and
  process-shared monitors should be the fastest.  Unix domain sockets may be as
  fast and, at least, should be faster than internet sockets. For internet
  sockets, TCP/IP may be slower than UDP.

* Benchmark reading/writing shared arrays to check whether they are as fast
  as regular Julia arrays (using @simd @inbounds for i in eachindex(A) ...).

* Check whether the same process can attach the same shared memory segment
  more than once.

## Julia code

* File `themis/utils.jl` is more general than just Themis (except the
  definitions of the pupil).

* `fit_wfs_grid` etc. should be in a common module/file.

* It may be sufficient to get rid of the type assertion and use
  `AbstractCamera` for the type of argument in `timewait`, `wait`, `trywait`,
  etc.  to share the same code for `SharedCamera` and an `ImageProvider`.  Type
  stability being provided by `get_last_image`.

* Methods `wait`, `timewait` and `trywait` on an `SharedCamera` shall only
  return an identifier.  Then `wait(SharedArray, cam::SharedCamera)` can
  return a shared array.  Or do the opposite change:
  `wait(Int,cam::SharedCamera)` just yield the identifier.

* Strategy for the reference image:
  - Use nearest pixel for fast diagonal LHS matrix.
  - Fix the sub-pixel shift of individual sub-images.
  - Integrate data part of the normal equations with a time scale.
  - Solve the normal equations starting at the previous reference image.
  - Use a diagonal preconditioner.
  - Account for the reweighting of the precision.
  - Set the regularization level so as to maximize the likelihood.

* Use a global vector of `Errors` (one for each thread) to simplify
  management of errors.

  ```julia
  const __errs__ = Vector{Errors}(undef, 1)
  function __init__()
      if Threads.nthreads() != length(__errs__)
          resize!(__errs__, Threads.nthreads())
      end
      for i in eachindex(__errs__)
          __errs__[i] = Errors()
      end
  end
  function any_errors(id::Int = Threads.threadid())
      __errs__[id].ptr != C_NULL
  end
  ```

## Alpao

* Make a server to control the deformable mirror.  Advantages: more than one
  process can use the deformable mirror, the mirror shape can be maintained
  with the last or loaded configuration.  Drawbacks: we may loose a bit of
  responsiveness but this can be measured and it may be negligible (e.g. a few
  µs) compared to other delays.

* For fast sending of commands, the commands could be stored in shared memory.
  Two sets of commands: the ones that the client wants to apply and the current
  ones.  A process-shared monitor can be used to quickly trigger the sending
  of commands.


## Phoenix part

* Setting connection speed is not fully functional (it works sometimes).

* Implement connection-reset.

* Set trigger mode (even though defaults are OK).

* The way the end of acquisition is signaled makes the "stop" command behaves
  as the "abort" one.

* Some operations (mostly playing with the configuration) should be forbidden
  while acquisition is running.

* phx_start should apply the user chosen configuration?


## Vectorization and real-time computations

* Make sure shared array values are optimally aligned to benefit from
  vectorization.  [in principle: OK]

* Compile several versions of the same critical functions (e.g. image
  preprocessing) with different SIMD options and choose the fastest one at
  runtime (like https://liboil.freedesktop.org/wiki/).

* Add stride term in `tao_preprocess_image_*` methods.  Manage to compile this
  code with vectorization.

* It may be faster (cf., CPng camera) to process the raw image line by line
  (possibly using a few threads):

  - extract/unpack one line of raw pixels in a suitably aligned buffer
    (perhaps do type conversion at this moment);
  - pre-process the pixels from the aligned buffer;
  - move to next line;

  This would also solve some issues like having strided raw images (e.g.,
  Andor camera or when ROI is smaller than the captured image because of
  constraints), having packed pixel values in the captured image, ...


## Some benchmark results

```julia
using TAO
TAO.loadclib()
cam = attach(TAO.SharedCamera, 13533202)
TAO.LibTAO.get_last_image_counter(cam)
TAO.LibTAO.get_last_image_shmid(cam)
using BenchmarkTools
@benchmark TAO.LibTAO.get_last_image_counter(cam) # takes ~ 11.2 ns/call
@benchmark TAO.LibTAO.get_last_image_shmid(cam)   # takes ~ 11.2 ns/call
```

as a comparison:

```julia
unsafe_get_state(cam::TAO.SharedCamera)::Cint =
    unsafe_load(Ptr{Cint}(cam.ptr + 64))
unsafe_set_state!(cam::TAO.SharedCamera,val::Integer) =
    unsafe_store!(Ptr{Cint}(cam.ptr + 64), val)
get_state(cam::TAO.SharedCamera)::Cint =
    (cam.ptr == C_NULL ? -1 : unsafe_load(Ptr{Cint}(cam.ptr + 64)))
set_state!(cam::TAO.SharedCamera,val::Integer) =
    cam.ptr != C_NULL && unsafe_store!(Ptr{Cint}(cam.ptr + 64), val)
@benchmark unsafe_get_state(cam)     # takes ~  3.4 ns/call
@benchmark get_state(cam)            # takes ~  3.9 ns/call
@benchmark unsafe_set_state!(cam, 3) # takes ~  5.2 ns/call
@benchmark set_state!(cam, 3)        # takes ~ 11.2 ns/call
```

same functions but with `@noinline`:

```julia
@noinline unsafe_get_state(cam::TAO.SharedCamera)::Cint =
    unsafe_load(Ptr{Cint}(cam.ptr + 64))
@noinline unsafe_set_state!(cam::TAO.SharedCamera,val::Integer) =
    unsafe_store!(Ptr{Cint}(cam.ptr + 64), val)
@noinline get_state(cam::TAO.SharedCamera)::Cint =
    (cam.ptr == C_NULL ? -1 : unsafe_load(Ptr{Cint}(cam.ptr + 64)))
@noinline set_state!(cam::TAO.SharedCamera,val::Integer) =
    cam.ptr != C_NULL && unsafe_store!(Ptr{Cint}(cam.ptr + 64), val)
@benchmark unsafe_get_state(cam)     # takes ~  4.5 ns/call
@benchmark get_state(cam)            # takes ~  4.5 ns/call
@benchmark unsafe_set_state!(cam, 3) # takes ~  6.0 ns/call
@benchmark set_state!(cam, 3)        # takes ~ 12.5 ns/call
```

Thus the extra time taken to check argument is not significant when getters are
used to retrieve members of shared structures.  As a matter of fact, the
timings of the new getters (which accept a `NULL` pointer and use branch
prediction) are the same as before: about 11.2 ns/call on the same machine.

With the new version of the callers (which check the validity of the obejct
address and after removed that checking from the
`unsafe_convert(::Type{Ptr{T}},obj)` method) the new timings are impressive:

```julia
using TAO
TAO.loadclib()
cam = attach(TAO.SharedCamera, 13533202)
cam0 = TAO.SharedCamera()  # has a NULL pointer
TAO.LibTAO.get_last_image_counter(cam)
TAO.LibTAO.get_last_image_shmid(cam)
using BenchmarkTools
@benchmark TAO.LibTAO.get_last_image_counter(cam)  # takes ~ 4.5 ns/call
@benchmark TAO.LibTAO.get_gain(cam)                # takes ~ 4.2 ns/call
@benchmark TAO.LibTAO.get_last_image_counter(cam0) # takes ~ 5.0 ns/call
@benchmark TAO.LibTAO.get_gain(cam0)               # takes ~ 5.0 ns/call
```

Thus these versions are much faster (perhaps the Julia check can be made faster
by making the `unsafe_convert` method inlined of by having it call another
method in case of error instead of embedding the throwing of the error).  Note
that branch prediction saves about 0.5 ns/call.

Specifying argument types via `Union{...}` has a cost!  For instance:

```julia
get_type(obj::AnySharedObject) =
   ccall((:tao_get_shared_object_type, taolib), Cint, (Ptr{Cvoid},), obj)
```

takes ~ 20 ns/call, while:

```julia
get_type(obj::SharedCamera) =
   ccall((:tao_get_shared_object_type, taolib), Cint, (Ptr{Cvoid},), obj)
```

takes ~ 4.5 ns/call.

Other benchmarks:

* Calling `lock(obj)` then `unlock(obj)` takes ~ 60 ns.

* Attach + detach of a shared memory segment takes a few µs (3.7µs on average
  on my laptop).

* The `ping` command takes about 2.5 milliseconds with the shell command
  `xpaget` but only 400 µs when using a persistent connection.
