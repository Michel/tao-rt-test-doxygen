// alpao-mirror-server.c -
//
// Implementation of a remote ALPAO deformable mirror server.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2021-2022, Éric Thiébaut.

#include "tao-alpao.h"
#include "tao-errors.h"
#include "tao-generic.h"
#include "tao-remote-mirrors-private.h"

#include <math.h>
#include <string.h>

// Private data needed for the cleanup callback.
static tao_remote_mirror* dm = NULL;
static long* inds = NULL;
static uint8_t* msk = NULL;
static alpao_mirror* dev = NULL;

// Send the requested command.
static tao_status on_send(
    tao_remote_mirror* dm,
    void* ctx)
{
    // The context is the mirror device.
    alpao_mirror* dev = ctx;

    // Copy requested commands as actual commands and send them (which also updates
    // the actual commands).
    double* act_cmds = tao_remote_mirror_get_actual_commands(dm);
    const double* req_cmds = tao_remote_mirror_get_requested_commands(dm);
    const double* refs =  tao_remote_mirror_get_reference(dm);
    long nacts = dm->nacts;
    for (long i = 0; i < nacts; ++i) {
        act_cmds[i] = req_cmds[i];
    }
    return alpao_send_commands(dev, act_cmds, refs, nacts);
}

// Reset the deformable mirror.  This amounts to sending zero commands.
static tao_status on_reset(
    tao_remote_mirror* dm,
    void* ctx)
{
    double* req_cmds = tao_remote_mirror_get_requested_commands(dm);
    long nacts = tao_remote_mirror_get_nacts(dm);
    for (long i = 0; i < nacts; ++i) {
        req_cmds[i] = 0.0;
    }
    return on_send(dm, ctx);
}

// Release ressources.  This function is automatically called on normal exit.
static void cleanup(void)
{
    if (dm != NULL) {
        dm->base.command = TAO_COMMAND_NONE;
        dm->base.state = TAO_STATE_KILLED;
        tao_remote_mirror_detach(dm);
        dm = NULL;
    }
    if (msk != NULL) {
        tao_free(msk);
        msk = NULL;
    }
    if (inds != NULL) {
        tao_free(inds);
        inds = NULL;
    }
    if (dev != NULL) {
        alpao_close_mirror(dev);
        dev = NULL;
    }
}

int main(
    int argc,
    char* argv[])
{
    // Determine program name.
    const char* progname = tao_basename(argv[0]);

    // Install function to free all allocated resources.
    if (atexit(cleanup) != 0) {
        fprintf(stderr, "%s: failed to install cleanup handler\n",
                progname);
        return EXIT_FAILURE;
    }

    // Parse arguments.
    char const* ident = NULL;
    bool debug = false;
    long nbufs = 10000;
    unsigned int orient = 0;
    unsigned int perms = 0077;
    const char* config = NULL;
    const char* usage = "Usage: %s [OPTIONS ...] [--] CONFIG NAME\n";
    bool opt = true;
    for (int iarg = 1; iarg < argc; ++iarg) {
        char dummy;
        if (opt) {
            // Argument may be an option.
            if (argv[iarg][0] != '-') {
                opt = false;
            } else if (argv[iarg][1] == '-' && argv[iarg][2] == '\0') {
                opt = false;
                continue;
            }
        }
        if (opt) {
            // Argument is an option.
            if (strcmp(argv[iarg], "-h") == 0 || strcmp(argv[iarg], "-help") == 0 ||
                strcmp(argv[iarg], "--help") == 0) {
                printf(usage, progname);
                printf("\n");
                printf("Arguments:\n");
                printf("  CONFIG               Configuration file.\n");
                printf("  NAME                 Name of server.\n");
                printf("\n");
                printf("Options:\n");
                printf("  -orient BITS         Orientation of layout [%u].\n",
                       orient);
                printf("  -nbufs NBUFS         Number of frame buffers [%ld].\n",
                       nbufs);
                printf("  -perms BITS          Bitwise mask of permissions [0%o].\n",
                       perms);
                printf("  -debug               Debug mode [%s].\n",
                       (debug ? "true" : "false"));
                printf("  -h, -help, --help    Print this help.\n");
                return EXIT_SUCCESS;
            }
            if (strcmp(argv[iarg], "-orient") == 0) {
                if (iarg + 1 >= argc) {
                    fprintf(stderr, "%s: missing argument for option %s\n",
                            progname, argv[iarg]);
                    return EXIT_FAILURE;
                }
                if (sscanf(argv[iarg+1], "%iarg %c", (int*)&orient, &dummy) != 1) {
                    fprintf(stderr, "%s: invalid value \"%s\" for option %s\n",
                            progname, argv[iarg+1], argv[iarg]);
                    return EXIT_FAILURE;
                }
                orient &= 5;
                ++iarg;
                continue;
            }
            if (strcmp(argv[iarg], "-nbufs") == 0) {
                if (iarg + 1 >= argc) {
                    fprintf(stderr, "%s: missing argument for option %s\n",
                            progname, argv[iarg]);
                    return EXIT_FAILURE;
                }
                if (sscanf(argv[iarg+1], "%ld %c", &nbufs, &dummy) != 1 || nbufs < 2) {
                    fprintf(stderr, "%s: invalid value \"%s\" for option %s\n",
                            progname, argv[iarg+1], argv[iarg]);
                    return EXIT_FAILURE;
                }
                ++iarg;
                continue;
            }
            if (strcmp(argv[iarg], "-perms") == 0) {
                if (iarg + 1 >= argc) {
                    fprintf(stderr, "%s: missing argument for option %s\n",
                            progname, argv[iarg]);
                    return EXIT_FAILURE;
                }
                if (sscanf(argv[iarg+1], "%iarg %c", (int*)&perms, &dummy) != 1) {
                    fprintf(stderr, "%s: invalid value \"%s\" for option %s\n",
                            progname, argv[iarg+1], argv[iarg]);
                    return EXIT_FAILURE;
                }
                perms &= 0777;
                ++iarg;
                continue;
            }
            if (strcmp(argv[iarg], "-debug") == 0) {
                debug = true;
                continue;
            }
            fprintf(stderr, "%s: unknown option %s\n", progname, argv[iarg]);
            return EXIT_FAILURE;
        } else {
            // Positional argument.
            if (config == NULL) {
                config = argv[iarg];
            } else if (ident == NULL) {
                ident = argv[iarg];
            } else {
                fprintf(stderr, "%s: too many arguments\n", progname);
            bad_usage:
                fprintf(stderr, usage, progname);
                return EXIT_FAILURE;
            }
        }
    }
    if (config == NULL) {
        fprintf(stderr, "%s: missing configuration name\n", progname);
        goto bad_usage;
    }
    if (ident == NULL) {
        fprintf(stderr, "%s: missing server name\n", progname);
        goto bad_usage;
    }

    // Open the deformable mirror device.
    dev = alpao_open_mirror(config);
    if (dev == NULL) {
        fprintf(stderr, "%s: failed to open deformable mirror device\n",
                progname);
        tao_report_error();
        return EXIT_FAILURE;
    }

    // Build mirror mask and layout indices.
    long nacts = dev->nacts;
    long dims[2];
    msk = alpao_mirror_mask_create_by_nacts(nacts, dims);
    if (msk == NULL) {
        fprintf(stderr, "%s: cannot create mirror mask for %ld actuators\n",
                progname, nacts);
        return EXIT_FAILURE;
    }
    inds = tao_malloc(dims[0]*dims[1]*sizeof(*inds));
    if (inds == NULL) {
        fprintf(stderr, "%s: cannot allocate %ld×%ld array for layout\n",
                progname, dims[0], dims[1]);
        return EXIT_FAILURE;
    }
    nacts = tao_indexed_layout_build(inds, msk, dims[0], dims[1], orient);
    if (nacts < 1) {
        fprintf(stderr, "%s: failed to build layout indices\n", progname);
        tao_report_error();
        return EXIT_FAILURE;
    }
    if (nacts != dev->nacts) {
        fprintf(stderr, "%s: device has %ld actuators while built layout "
                "has %ld actuators\n", progname, dev->nacts, nacts);
        tao_report_error();
        return EXIT_FAILURE;
    }

    // Allocate the remote mirror instance.
    dm = tao_remote_mirror_create(ident, nbufs, inds, dims[0], dims[1], perms);
    if (dm == NULL) {
        fprintf(stderr, "%s: failed to create remote mirror instance\n",
                progname);
        tao_report_error();
        return EXIT_FAILURE;
    }

    // Run loop (on entry of the loop we own the lock on the remote mirror
    // instance).
    tao_remote_mirror_operations ops = {
        .on_send = on_send,
        .on_reset = on_reset,
        .name = ident,
        .debug = debug
    };
    tao_status status = tao_remote_mirror_run_loop(dm, &ops, NULL);
    if (status != TAO_OK) {
        tao_report_error();
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
