// private.c -
//
// Implementation of the low level (private) interface to ActiveSilicon Phoenix
// frame grabber.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2017-2022, Éric Thiébaut.
// Copyright (C) 2016, Éric Thiébaut & Jonathan Léger.

#include "tao-phoenix-private.h"
#include "tao-errors.h"

#include <math.h>
#include <string.h>

#ifdef _PHX_POSIX
#  include <termios.h>
#endif

//-----------------------------------------------------------------------------
// ERROR MANAGEMENT

// Mutex to protect error handler settings.
static tao_mutex error_handler_mutex = TAO_MUTEX_INITIALIZER;

// Verbosity of error handler.
static int error_handler_level = 1;

int phx_set_error_handler_verbosity(
    int level)
{
    int previous;
    if (pthread_mutex_lock(&error_handler_mutex) == 0) {
        previous = error_handler_level;
        error_handler_level = level;
        (void)pthread_mutex_unlock(&error_handler_mutex);
    } else {
        previous = -1;
    }
    return previous;
}

int phx_get_error_handler_verbosity(
    void)
{
    int level = -1;
    if (pthread_mutex_lock(&error_handler_mutex) == 0) {
        level = error_handler_level;
        (void)pthread_mutex_unlock(&error_handler_mutex);
    }
    return level;
}

void phx_default_error_handler(
    const char* funcname,
    etStat errcode,
    const char* reason)
{
    if (errcode != PHX_OK) {
        int level = phx_get_error_handler_verbosity();
        if (level >= 2) {
            if (reason != NULL && reason[0] != '\0') {
                fprintf(stderr, "Function %s failed with code 0x%08x.\n%s\n",
                        funcname, (unsigned int)errcode, reason);
            } else {
                fprintf(stderr, "Function %s failed with code 0x%08x.\n",
                        funcname, (unsigned int)errcode);
            }
        }
    }
}

// Callback for TAO error management system.
static void get_error_details(
    int code,
    const char** reason,
    const char** info)
{
    if (reason != NULL) {
        *reason = phx_status_description(code);
    }
    if (info != NULL) {
        *info = phx_status_identifier(code);
        if (*info != NULL && (*info)[0] == '\0') {
            *info = NULL;
        }
    }
}

void phx_push_error(
    const char* func,
    int code)
{
    tao_store_other_error(func, code, get_error_details);
}

const char* phx_status_identifier(
    etStat code)
{
    switch (code) {
#define CASE(x) case x: return #x;
        CASE(PHX_OK);
        CASE(PHX_ERROR_BAD_HANDLE);
        CASE(PHX_ERROR_BAD_PARAM);
        CASE(PHX_ERROR_BAD_PARAM_VALUE);
        CASE(PHX_ERROR_READ_ONLY_PARAM);
        CASE(PHX_ERROR_OPEN_FAILED);
        CASE(PHX_ERROR_INCOMPATIBLE);
        CASE(PHX_ERROR_HANDSHAKE);
        CASE(PHX_ERROR_INTERNAL_ERROR);
        CASE(PHX_ERROR_OVERFLOW);
        CASE(PHX_ERROR_NOT_IMPLEMENTED);
        CASE(PHX_ERROR_HW_PROBLEM);
        CASE(PHX_ERROR_NOT_SUPPORTED);
        CASE(PHX_ERROR_OUT_OF_RANGE);
        CASE(PHX_ERROR_MALLOC_FAILED);
        CASE(PHX_ERROR_SYSTEM_CALL_FAILED);
        CASE(PHX_ERROR_FILE_OPEN_FAILED);
        CASE(PHX_ERROR_FILE_CLOSE_FAILED);
        CASE(PHX_ERROR_FILE_INVALID);
        CASE(PHX_ERROR_BAD_MEMBER);
        CASE(PHX_ERROR_HW_NOT_CONFIGURED);
        CASE(PHX_ERROR_INVALID_FLASH_PROPERTIES);
        CASE(PHX_ERROR_ACQUISITION_STARTED);
        CASE(PHX_ERROR_INVALID_POINTER);
        CASE(PHX_ERROR_LIB_INCOMPATIBLE);
        CASE(PHX_ERROR_SLAVE_MODE);
        CASE(PHX_ERROR_DISPLAY_CREATE_FAILED);
        CASE(PHX_ERROR_DISPLAY_DESTROY_FAILED);
        CASE(PHX_ERROR_DDRAW_INIT_FAILED);
        CASE(PHX_ERROR_DISPLAY_BUFF_CREATE_FAILED);
        CASE(PHX_ERROR_DISPLAY_BUFF_DESTROY_FAILED);
        CASE(PHX_ERROR_DDRAW_OPERATION_FAILED);
        CASE(PHX_ERROR_WIN32_REGISTRY_ERROR);
        CASE(PHX_ERROR_PROTOCOL_FAILURE);
        CASE(PHX_WARNING_TIMEOUT);
        CASE(PHX_WARNING_FLASH_RECONFIG);
        CASE(PHX_WARNING_ZBT_RECONFIG);
        CASE(PHX_WARNING_NOT_PHX_COM);
        CASE(PHX_WARNING_NO_PHX_BOARD_REGISTERED);
        CASE(PHX_WARNING_TIMEOUT_EXTENDED);
#undef CASE
    default:
        return "";
    }
}

//-----------------------------------------------------------------------------
// PIXEL ENCODING

tao_encoding phx_camsrc_to_encoding(
    etParamValue cam_src_col,
    int cam_src_depth)
{
    if (0 < cam_src_depth && cam_src_depth <= 255) {
        switch (cam_src_col) {
        case PHX_CAM_SRC_MONO:
            return TAO_ENCODING_MONO(cam_src_depth);

        case PHX_CAM_SRC_RGB:
            return TAO_ENCODING_RGB(cam_src_depth);

        case PHX_CAM_SRC_BAY_RGGB:
            return TAO_ENCODING_BAYER_RGGB(cam_src_depth);

        case PHX_CAM_SRC_BAY_GRBG:
            return TAO_ENCODING_BAYER_GRBG(cam_src_depth);

        case PHX_CAM_SRC_BAY_GBRG:
            return TAO_ENCODING_BAYER_GBRG(cam_src_depth);

        case PHX_CAM_SRC_BAY_BGGR:
            return TAO_ENCODING_BAYER_BGGR(cam_src_depth);

        case PHX_CAM_SRC_YUV422:
            if (cam_src_depth == 16) {
                return TAO_ENCODING_YUV422;
            }
            break;

        default:
            break;
        }
    }
    return TAO_ENCODING_UNKNOWN;
}

tao_status phx_define_camera_encoding(
    phnx_device* dev,
    tao_encoding enc)
{
    etParamValue cam_src_col;
    etParamValue cam_src_depth;
    tao_encoding col = TAO_ENCODING_COLORANT(enc);
    tao_encoding pxl = TAO_ENCODING_BITS_PER_PIXEL(enc);
    tao_encoding pkt = TAO_ENCODING_BITS_PER_PACKET(enc);
    tao_encoding flg = TAO_ENCODING_FLAGS(enc);

    if (flg != 0 || pkt != pxl) {
    error:
        tao_store_error(__func__, TAO_BAD_ENCODING);
        return TAO_ERROR;
    }
    switch (col) {
    case TAO_COLORANT_MONO:
        cam_src_col = PHX_CAM_SRC_MONO;
        break;
    case TAO_COLORANT_RGB:
        cam_src_col = PHX_CAM_SRC_RGB;
        break;
    case TAO_COLORANT_BAYER_RGGB:
        cam_src_col = PHX_CAM_SRC_BAY_RGGB;
        break;
    case TAO_COLORANT_BAYER_GRBG:
        cam_src_col = PHX_CAM_SRC_BAY_GRBG;
        break;
    case TAO_COLORANT_BAYER_GBRG:
        cam_src_col = PHX_CAM_SRC_BAY_GBRG;
        break;
    case TAO_COLORANT_BAYER_BGGR:
        cam_src_col = PHX_CAM_SRC_BAY_BGGR;
        break;
    case TAO_COLORANT_YUV422:
        cam_src_col = PHX_CAM_SRC_YUV422;
        break;
    default:
        goto error;
    }
    cam_src_depth = pxl;
    if (phx_set(dev, PHX_CAM_SRC_COL,   cam_src_col  ) != TAO_OK ||
        phx_set(dev, PHX_CAM_SRC_DEPTH, cam_src_depth) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

// Table of correspondnaces between PHX_DST_FORMAT and TAO pixel encoding.
static struct {
    const char*   name; // textual parameter value
    etParamValue   val; // parameter value
    tao_encoding enc; // encoding
} buffer_encodings[] = {
#define ENCDEF(val, enc) {#val, val, enc}
    ENCDEF(PHX_DST_FORMAT_Y8,     TAO_ENCODING_MONO(8)),
    ENCDEF(PHX_DST_FORMAT_Y10,    TAO_ENCODING_MONO(10)),
    ENCDEF(PHX_DST_FORMAT_Y12,    TAO_ENCODING_MONO(12)),
    ENCDEF(PHX_DST_FORMAT_Y12B,   TAO_ENCODING_MONO_PKT(12,32)),
    ENCDEF(PHX_DST_FORMAT_Y14,    TAO_ENCODING_MONO(14)),
    ENCDEF(PHX_DST_FORMAT_Y16,    TAO_ENCODING_MONO(16)),
    ENCDEF(PHX_DST_FORMAT_Y32,    TAO_ENCODING_MONO(32)),
    ENCDEF(PHX_DST_FORMAT_Y36,    TAO_ENCODING_MONO(36)),
    ENCDEF(PHX_DST_FORMAT_2Y12,   TAO_ENCODING_MONO_PKT(12,24)),
    ENCDEF(PHX_DST_FORMAT_BAY8,   TAO_ENCODING_BAYER_RGGB(8)),
    ENCDEF(PHX_DST_FORMAT_BAY10,  TAO_ENCODING_BAYER_RGGB(10)),
    ENCDEF(PHX_DST_FORMAT_BAY12,  TAO_ENCODING_BAYER_RGGB(12)),
    ENCDEF(PHX_DST_FORMAT_BAY14,  TAO_ENCODING_BAYER_RGGB(14)),
    ENCDEF(PHX_DST_FORMAT_BAY16,  TAO_ENCODING_BAYER_RGGB(16)),
    ENCDEF(PHX_DST_FORMAT_RGB15,  TAO_ENCODING_RGB(15)),
    ENCDEF(PHX_DST_FORMAT_RGB16,  TAO_ENCODING_RGB(16)),
    ENCDEF(PHX_DST_FORMAT_RGB24,  TAO_ENCODING_RGB(24)),
    ENCDEF(PHX_DST_FORMAT_RGB32,  TAO_ENCODING_RGB(32)),
    ENCDEF(PHX_DST_FORMAT_RGB36,  TAO_ENCODING_RGB(36)),
    ENCDEF(PHX_DST_FORMAT_RGBX32, TAO_ENCODING_RGBA(32)),
    ENCDEF(PHX_DST_FORMAT_XRGB32, TAO_ENCODING_ARGB(32)),
    ENCDEF(PHX_DST_FORMAT_RGB48,  TAO_ENCODING_RGB(48)),
    ENCDEF(PHX_DST_FORMAT_BGR15,  TAO_ENCODING_BGR(15)),
    ENCDEF(PHX_DST_FORMAT_BGR16,  TAO_ENCODING_BGR(16)),
    ENCDEF(PHX_DST_FORMAT_BGR24,  TAO_ENCODING_BGR(24)),
    ENCDEF(PHX_DST_FORMAT_BGR32,  TAO_ENCODING_BGR(32)),
    ENCDEF(PHX_DST_FORMAT_BGR36,  TAO_ENCODING_BGR(36)),
    ENCDEF(PHX_DST_FORMAT_BGR48,  TAO_ENCODING_BGR(48)),
    ENCDEF(PHX_DST_FORMAT_BGRX32, TAO_ENCODING_BGRA(32)),
    ENCDEF(PHX_DST_FORMAT_XBGR32, TAO_ENCODING_ABGR(32)),
    ENCDEF(PHX_DST_FORMAT_YUV422, TAO_ENCODING_YUV422),
#undef ENCDEF
    { NULL, 0, TAO_ENCODING_UNKNOWN },
};

tao_encoding phx_dstformat_to_encoding(
    etParamValue dst_format)
{
    for (int i = 0; buffer_encodings[i].name != NULL; ++i) {
        if (buffer_encodings[i].val == dst_format) {
            return buffer_encodings[i].enc;
        }
    }
    return TAO_ENCODING_UNKNOWN;
}

tao_status phx_define_buffer_encoding(
    phnx_device* dev,
    tao_encoding enc)
{
    for (int i = 0; buffer_encodings[i].name != NULL; ++i) {
        if (buffer_encodings[i].enc == enc) {
            etParamValue dst_format = buffer_encodings[i].val;
            return phx_set(dev, PHX_DST_FORMAT, dst_format);
        }
    }
    tao_store_error(__func__, TAO_BAD_ENCODING);
    return TAO_ERROR;
}

//-----------------------------------------------------------------------------
// BYTE SWAPPING ROUTINES

#if 0
static inline void swap2(
    void* ptr)
{
    uint8_t* buf = ptr;
    uint8_t c0 = buf[0];
    uint8_t c1 = buf[1];
    buf[0] = c1;
    buf[1] = c0;
}
#endif

static inline void swap4(
    void* ptr)
{
    uint8_t* buf = ptr;
    uint8_t c0 = buf[0];
    uint8_t c1 = buf[1];
    uint8_t c2 = buf[2];
    uint8_t c3 = buf[3];
    buf[0] = c3;
    buf[1] = c2;
    buf[2] = c1;
    buf[3] = c0;
}

static inline void swap8(
    void* ptr)
{
    uint8_t* buf = ptr;
    uint8_t c0 = buf[0];
    uint8_t c1 = buf[1];
    uint8_t c2 = buf[2];
    uint8_t c3 = buf[3];
    uint8_t c4 = buf[4];
    uint8_t c5 = buf[5];
    uint8_t c6 = buf[6];
    uint8_t c7 = buf[7];
    buf[0] = c7;
    buf[1] = c6;
    buf[2] = c5;
    buf[3] = c4;
    buf[4] = c3;
    buf[5] = c2;
    buf[6] = c1;
    buf[7] = c0;
}

//-----------------------------------------------------------------------------
// COAXPRESS CAMERAS

tao_status phx_detect_coaxpress(
    phnx_device* dev)
{
    etParamValue info;
    uint32_t magic, width, height, pixelformat;

    if (phx_get_parameter(dev, PHX_CXP_INFO, &info) != TAO_OK) {
        return TAO_ERROR;
    }
    dev->coaxpress = false;
    if ((info & PHX_CXP_CAMERA_DISCOVERED) != 0) {
        // The frame grabber thinks that we have a CoaXPress camera.  We check
        // the magic number and set the byte order for CoaXPress communication.
        if (cxp_get(dev, STANDARD, &magic) != TAO_OK) {
        bad_magic:
            tao_store_error(__func__, TAO_BAD_MAGIC);
            return TAO_ERROR;
        }
        if (magic != CXP_MAGIC) {
            swap4(&magic);
            if (magic != CXP_MAGIC) {
                goto bad_magic;
            }
            dev->swap = ! dev->swap;
        }
        dev->coaxpress = true;

        // Get pixel format, current image size (full width and full height must
        // be correctly set later), device vendor name and device model name.
        if (cxp_get(dev, PIXEL_FORMAT_ADDRESS, &pixelformat) != TAO_OK || // FIXME: unused
            cxp_get(dev, WIDTH_ADDRESS,              &width) != TAO_OK ||
            cxp_get(dev, HEIGHT_ADDRESS,            &height) != TAO_OK ||
            cxp_get(dev, DEVICE_VENDOR_NAME,    dev->vendor) != TAO_OK ||
            cxp_get(dev, DEVICE_MODEL_NAME,      dev->model) != TAO_OK) {
            return TAO_ERROR;
        }
        dev->base.info.sensorwidth = width;
        dev->base.info.sensorheight = height;
        tao_camera_roi_define(&dev->roi, 1, 1, 0, 0, width, height);
        dev->base.info.config.roi = dev->roi;
    }
    return TAO_OK;
}

tao_status phx_define_coaxpress_connection(
    phnx_device* dev,
    const phnx_connection* con)
{
    etParamValue bitrate, discovery;
    switch (con->speed) {
    case    0: bitrate = PHX_CXP_BITRATE_MODE_AUTO; break;
    case 1250: bitrate = PHX_CXP_BITRATE_MODE_CXP1; break;
    case 2500: bitrate = PHX_CXP_BITRATE_MODE_CXP2; break;
    case 3125: bitrate = PHX_CXP_BITRATE_MODE_CXP3; break;
    case 5000: bitrate = PHX_CXP_BITRATE_MODE_CXP5; break;
    case 6250: bitrate = PHX_CXP_BITRATE_MODE_CXP6; break;
    default:
        tao_store_error(__func__, TAO_BAD_SPEED);
        return TAO_ERROR;
    }
    switch (con->channels) {
    case 0: discovery = PHX_CXP_DISCOVERY_MODE_AUTO; break;
    case 1: discovery = PHX_CXP_DISCOVERY_MODE_1X;   break;
    case 2: discovery = PHX_CXP_DISCOVERY_MODE_2X;   break;
    case 4: discovery = PHX_CXP_DISCOVERY_MODE_4X;   break;
    default:
        tao_store_error(__func__, TAO_BAD_CHANNELS);
        return TAO_ERROR;
    }
    if (bitrate != 0) {
        if (phx_set(dev, PHX_CACHE_FLUSH,
                    (etParamValue)PHX_DUMMY_PARAM) != TAO_OK) {
            return TAO_ERROR;
        }
        if (discovery != 0 &&
            phx_set(dev, PHX_CXP_DISCOVERY_MODE, discovery) != TAO_OK) {
            return TAO_ERROR;
        }
        if (phx_set(dev, PHX_CXP_BITRATE_MODE|PHX_CACHE_FLUSH,
                    bitrate) != TAO_OK) {
            return TAO_ERROR;
        }
    }
    return TAO_OK;
}

//-----------------------------------------------------------------------------
// WRAPPERS FOR PHOENIX ROUTINES

tao_status phx_read_stream(
    phnx_device* dev,
    etAcq command,
    void* addr)
{
    etStat code = PHX_StreamRead(dev->handle, command, addr);
    if (code != PHX_OK) {
        if (command == PHX_UNLOCK && code == PHX_ERROR_NOT_IMPLEMENTED) {
            // Ignore this error.
            return TAO_OK;
        }
        phx_push_error("PHX_StreamRead", code);
        return TAO_ERROR;
    }
    return TAO_OK;
}

//-----------------------------------------------------------------------------
// ROUTINES TO QUERY/SET FRAME GRABBER PARAMETERS

tao_status phx_get_parameter(
    phnx_device* dev,
    etParam param,
    void* addr)
{
    etStat code = PHX_ParameterGet(dev->handle, param, addr);
    if (code != PHX_OK) {
        phx_push_error("PHX_ParameterGet", code);
        return TAO_ERROR;
    }
    return TAO_OK;
}

tao_status phx_set_parameter(
    phnx_device* dev,
    etParam param,
    void* addr)
{
    etStat code = PHX_ParameterSet(dev->handle, param, addr);
    if (code != PHX_OK) {
        phx_push_error("PHX_ParameterSet", code);
        return TAO_ERROR;
    }
    return TAO_OK;
}

tao_status phx_get(
    phnx_device* dev,
    etParam param,
    etParamValue* valptr)
{
    return phx_get_parameter(dev, param, valptr);
}

tao_status phx_set(
    phnx_device* dev,
    etParam param,
    etParamValue value)
{
    return phx_set_parameter(dev, param, &value);
}

//-----------------------------------------------------------------------------
// ROUTINES TO READ/WRITE COAXPRESS REGISTERS

tao_status cxp_read(
    phnx_device* dev,
    uint32_t addr,
    uint8_t* data,
    uint32_t* size)
{
    etStat code = PHX_ControlRead(dev->handle, PHX_REGISTER_DEVICE,
                                  &addr, data, size, dev->timeout);
    if (code != PHX_OK) {
        phx_push_error("PHX_ControlRead", code);
        return TAO_ERROR;
    }
    return TAO_OK;
}

// Unfortunately, setting some parameters (as the pixel format or the gamma
// correction) returns an error with an absurd code (PHX_ERROR_MALLOC_FAILED)
// even if the value has been correctly set.  The error cannot be just ignored
// as further reads of registers yield wrong values.  The strategy is to close
// and re-open the camera when such an error occurs which solves the problem in
// practice to the cost of the time spent to close and re-open (0.4 sec.).  To
// avoid alarming the user, printing of error messages is disabled during this
// process.
//
// Other (unsuccessful) strategies have been tried:
//
// - Calling PHX_ControlReset yields a PHX_ERROR_BAD_HANDLE error.
//
// - Re-reading the register until its value is OK (this does not work for
//   write-only registers).

tao_status cxp_write(
    phnx_device* dev,
    uint32_t addr,
    uint8_t* data,
    uint32_t* size)
{
    tao_status status = TAO_OK;
    int level = phx_set_error_handler_verbosity(0);
    etStat code = PHX_ControlWrite(dev->handle, PHX_REGISTER_DEVICE,
                                     &addr, data, size, dev->timeout);
    if (code == PHX_ERROR_MALLOC_FAILED) {
        // This is probably a bogus register, try to reset the camera.
        tao_camera* cam = &dev->base;
        cam->runlevel = 3; // to force using reset
        status = tao_camera_reset(cam);
        phx_set_error_handler_verbosity(level);
    } else if (code != PHX_OK) {
        phx_push_error("PHX_ControlWrite", code);
        status = TAO_ERROR;
    }
    return status;
}

tao_status cxp_reset(
    phnx_device* dev,
    uint32_t addr)
{
    etStat code = PHX_ControlReset(dev->handle, PHX_REGISTER_DEVICE,
                                     &addr, dev->timeout);
    if (code != PHX_OK) {
        phx_push_error("PHX_ControlReset", code);
        return TAO_ERROR;
    }
    return TAO_OK;
}

#define ENCODE(S,T,N)                                                          \
    tao_status cxp_read_##S(                                                   \
        phnx_device* dev,                                                      \
        uint32_t addr,                                                         \
        T* value)                                                              \
    {                                                                          \
        uint32_t size = sizeof(T);                                             \
        tao_status status = cxp_read(dev, addr, (uint8_t*)value, &size);       \
        if (status == TAO_OK && dev->swap) {                                   \
            swap##N(value);                                                    \
        }                                                                      \
        return status;                                                         \
    }                                                                          \
                                                                               \
    tao_status cxp_write_##S(                                                  \
        phnx_device* dev,                                                      \
        uint32_t addr,                                                         \
        T value)                                                               \
    {                                                                          \
        uint32_t size = sizeof(T);                                             \
        if (dev->swap) {                                                       \
            swap##N(&value);                                                   \
        }                                                                      \
        return cxp_write(dev, addr, (uint8_t*)&value, &size);                  \
    }
ENCODE(uint32,  uint32_t,  4)
ENCODE(uint64,  uint64_t,  8)
ENCODE(float32, float32_t, 4)
ENCODE(float64, float64_t, 8)
#undef ENCODE

tao_status cxp_read_string(
    phnx_device* dev,
    uint32_t addr,
    uint32_t len,
    char* buf)
{
    uint32_t count = len;
    if (cxp_read(dev, addr, (uint8_t*)buf, &count) != TAO_OK) {
        return TAO_ERROR;
    }
    buf[TAO_MIN(len, count)] = '\0';
    return TAO_OK;
}

tao_status cxp_read_indirect_uint32(
    phnx_device* dev,
    uint32_t addr,
    uint32_t* value)
{
    uint32_t regaddr;

    // Get the address of the register.
    if (cxp_read_uint32(dev, addr, &regaddr) != TAO_OK) {
        return TAO_ERROR;
    }

    // Get the value at that address.
    return cxp_read_uint32(dev, regaddr, value);
}

//-----------------------------------------------------------------------------
// KEYBOARD

#ifdef _PHX_POSIX
static int              peek_character = -1;
static struct termios   initial_settings, new_settings;
#endif
#ifdef _PHX_VXWORKS
volatile int gnKbHitCount;
volatile int gnKbHitCountMax = 100;
#endif

void phx_keyboard_init(
    void)
{
#ifdef _PHX_POSIX
    tcgetattr(0, &initial_settings);
    new_settings = initial_settings;
    new_settings.c_lflag    &= ~ICANON;
    new_settings.c_lflag    &= ~ECHO;
    new_settings.c_lflag    &= ~ISIG;
    new_settings.c_cc[VMIN]  = 1;
    new_settings.c_cc[VTIME] = 0;
    tcsetattr(0, TCSANOW, &new_settings);
#elif defined _PHX_VXWORKS
    gnKbHitCount = 0;
#else
    // Nothing to do
#endif
}

bool phx_keyboard_hit(
    void)
{
#if defined _PHX_POSIX
    char  ch;
    int   nread;

    if (peek_character != -1) {
        return true;
    }
    new_settings.c_cc[VMIN] = 0;
    tcsetattr(0, TCSANOW, &new_settings);
    nread = read(0, &ch, 1);
    new_settings.c_cc[VMIN] = 1;
    tcsetattr(0, TCSANOW, &new_settings);

    if (nread == 1) {
        peek_character = ch;
        return true;
    }
    return false;
#elif defined _PHX_VXWORKS
    return (gnKbHitCount++ > gnKbHitCountMax);
#elif defined _MSC_VER
    return (_kbhit() != 0);
#else
    return (kbhit() != 0);
#endif
}

void phx_keyboard_final(
    void)
{
#ifdef _PHX_POSIX
    tcsetattr(0, TCSANOW, &initial_settings);
#elif defined _PHX_VXWORKS
    // TODO
#else
    // Nothing to do
#endif
}

int phx_keyboard_read(
    void)
{
#ifdef _PHX_POSIX
    char ch;
    int nBytes;

    if (peek_character != -1) {
        ch = peek_character;
        peek_character = -1;
        return ch;
    }
    nBytes = read(0, &ch, 1);
    return (nBytes == 1 ? ch : '\0');
#elif defined _PHX_VXWORKS
    // TODO
#elif defined _MSC_VER
    return _getch();
#else
    return getch();
#endif
}
