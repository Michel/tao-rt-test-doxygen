// phoenix-server.c -
//
// Program to run image server for a camera connected to an ActiveSilicon
// Phoenix frame grabber.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2018-2022, Éric Thiébaut.

// FIXME: account for sensorencoding, bufferencoding options
// FIXME: drop frames, do not wait if there are pending frames

#include "tao-config.h"
#include "tao-errors.h"
#include "tao-generic.h"
#include "tao-shared-arrays-private.h"
#include "tao-shared-cameras-private.h"
#include "tao-servers-private.h"
#include "tao-phoenix-private.h"
#include "tao-phoenix-options.h"

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <strings.h>
#include <signal.h>
#include <pthread.h>
#include <math.h>

#define ERROR_DEBUGLEVEL   -1 // debug-level for errors
#define COMMAND_DEBUGLEVEL  1 // debug-level for commands
#define CONTROL_DEBUGLEVEL  2 // debug-level for control
#define MONITOR_DEBUGLEVEL  3 // debug-level for monitor
#define TICKING_DEBUGLEVEL  4 // debug-level for checking whether
                              // application is alive

// Global variables shared between the server and the processor threads.  These
// resources are "private" to the threads of this process.  The shared camera
// in the virtual frame-grabber is said to be "public" as it is shared with
// other processes.  Access to the "private" resources (including the camera
// `cam`) is controlled by locking/unlocking the camera `cam`, access to the
// "public" resources is controlled by locking/unlocking the shared camera
// `shmcam = tao_framegrabber_get_shared_camera(vfg)`.

static const char* accesspoint = "TAO:Phoenix0";
static const char* ident = NULL; // Server name.
static tao_camera* cam = NULL;
static tao_framegrabber* vfg = NULL;
static tao_shared_camera* shcam = NULL;
static volatile int debuglevel = 0;
static volatile bool drop = true;
static volatile bool quitting = false;
static volatile bool suspended = false;
static int (*on_processing)(const tao_acquisition_buffer* buf) = NULL;
static long errors = 0; // number of errors
static const char* progname = "phoenix_server";

static tao_status init_callback(
    tao_server* srv, void* ctx);

static int process_frame(
    const tao_acquisition_buffer* buf);

static void reflect_settings(
    void);

// In this server implementation, there are 3 threads, one thread is controlled
// by the Phoenix library and receives camera events, the two others (a
// "worker" and a "server" threads) are created by the application.  The
// "worker" thread executes the `run_worker` function and receives events from
// the two other threads.  The "server" thread handles requests from the
// clients and sends commands to the "worker" thread.
//
// Commands and clients requests are managed in a synchronous way: at any time
// there is at most one pending command/request and all commands/requests have
// a reply.  This means that, although the clients may notice transient states
// ("starting", "stopping" or "aborting") of the shared camera, the server only
// sees stable states ("sleeping", "acquiring" or "finished").
//
// Life cycle of the "worker" thread (commands are in the left column, states
// in the rightmost ones):
//
//                      INITIALIZING ===> SLEEPING
//
//  +----> NONE    :::> SLEEPING
//  |       |
//  |       V
//  |     START    :::> STARTING ===> ACQUIRING
//  |       |
//  |       V
//  +-- STOP/ABORT :::> STOPPING/ABORTING ===> SLEEPING
//
//  NONE/START/STOP/ABORT
//          |
//          V           SLEEPING ===> FINISHED
//         QUIT    :::> STARTING ===> ABORTING ===> FINISHED
//                      ACQUIRING ===> ABORTING ===> FINISHED
//
// Legend: ---> allowed "moves" triggered by commands
//         :::> consequence of a scheduled task
//         ===> temporal evolution of the processing thread
//
// Note that the only difference between "stop" and "abort" is that "abort"
// stops acquisition as soon as possible while "stop" process the current
// image, if any, before stopping acquisition.
//
// Below are the commands that can be scheduled.  The policy is as follows: the
// master must lock the private resources to schedule a new command, and waits
// that command goes back to CMD_NONE.  QUIT is irreversible, after a START,
// any of STOP, ABORT and QUIT are possible.
typedef enum command_ {
    CMD_NONE = 0, // No commands
    CMD_START,    // Start acquisition
    CMD_STOP ,    // Stop acquisition
    CMD_ABORT,    // Abort acquisition
    CMD_QUIT,     // Acquisition thread must quit
} command;

// Other "private" resources used to monitor the camera state
// (without having to lock the camera) and to send commands ("start',
// "stop", "abort" or "quit") to the worker and to wait for their results.
//
// The "worker" thread always lock/unlock the monitored information while
// owning the camera lock.
//
// When the "server" thread schedule a command, it waits for monitor.cmd to
// becomes CMD_NONE.
static struct {
    tao_mutex        mutex; // Mutex to lock this structure.
    tao_cond        notify; // Condition variable for the server to wait for
                            // the pending command being taken into account.
    tao_state state; // Current camera state. FIXME: not needed?
    command            cmd; // Pending command.
} monitor = {
    TAO_MUTEX_INITIALIZER,
    TAO_COND_INITIALIZER,
    TAO_STATE_INITIALIZING,
    CMD_NONE,
};

static void lock_monitor(
    void);

static void unlock_monitor(
    void);

static void signal_monitor(
    void);

static void wait_monitor(
    void);

#ifdef ABSOLUTE_TIMED_WAIT_MONITOR
static tao_status timed_wait_monitor(
    const tao_time* abstime);
#else
static tao_status timed_wait_monitor(
    double secs);
#endif

// Global variables for the on_return() callback.
static tao_server* srv = NULL;
static pthread_t worker = 0;

static void* run_worker(void* arg);

// Set commands.
static tao_send_callback on_get_accesspoint;
static tao_send_callback on_get_bias;
static tao_send_callback on_get_bufferencoding;
static tao_send_callback on_get_connection;
static tao_send_callback on_get_debuglevel;
static tao_send_callback on_get_drop;
static tao_send_callback on_get_exposuretime;
static tao_send_callback on_get_framerate;
static tao_send_callback on_get_frames;
static tao_send_callback on_get_gain;
static tao_send_callback on_get_height;
static tao_send_callback on_get_lostframes;
static tao_send_callback on_get_lostsyncs;
static tao_send_callback on_get_nbufs;
static tao_send_callback on_get_origin;
static tao_send_callback on_get_overflows;
static tao_send_callback on_get_overruns;
static tao_send_callback on_get_ping;
static tao_send_callback on_get_pixeltype;
static tao_send_callback on_get_roi;
static tao_send_callback on_get_sensorencoding;
static tao_send_callback on_get_sensorheight;
static tao_send_callback on_get_sensorwidth;
static tao_send_callback on_get_shmid;
static tao_send_callback on_get_state;
static tao_send_callback on_get_suspended;
static tao_send_callback on_get_temperature;
static tao_send_callback on_get_timeouts;
static tao_send_callback on_get_width;
static tao_send_callback on_get_xbin;
static tao_send_callback on_get_xoff;
static tao_send_callback on_get_ybin;
static tao_send_callback on_get_yoff;
static tao_server_command get_commands[] = {
    {"accesspoint",    on_get_accesspoint,    NULL, NULL, NULL},
    {"bias",           on_get_bias,           NULL, NULL, NULL},
    {"bufferencoding", on_get_bufferencoding, NULL, NULL, NULL},
    {"connection",     on_get_connection,     NULL, NULL, NULL},
    {"debuglevel",     on_get_debuglevel,     NULL, NULL, NULL},
    {"drop",           on_get_drop,           NULL, NULL, NULL},
    {"exposuretime",   on_get_exposuretime,   NULL, NULL, NULL},
    {"framerate",      on_get_framerate,      NULL, NULL, NULL},
    {"frames",         on_get_frames,         NULL, NULL, NULL},
    {"gain",           on_get_gain,           NULL, NULL, NULL},
    {"height",         on_get_height,         NULL, NULL, NULL},
    {"lostframes",     on_get_lostframes,     NULL, NULL, NULL},
    {"lostsyncs",      on_get_lostsyncs,      NULL, NULL, NULL},
    {"nbufs",          on_get_nbufs,          NULL, NULL, NULL},
    {"origin",         on_get_origin,         NULL, NULL, NULL},
    {"overflows",      on_get_overflows,      NULL, NULL, NULL},
    {"overruns",       on_get_overruns,       NULL, NULL, NULL},
    {"ping",           on_get_ping,           NULL, NULL, NULL},
    {"pixeltype",      on_get_pixeltype,      NULL, NULL, NULL},
    {"roi",            on_get_roi,            NULL, NULL, NULL},
    {"sensorencoding", on_get_sensorencoding, NULL, NULL, NULL},
    {"sensorheight",   on_get_sensorheight,   NULL, NULL, NULL},
    {"sensorwidth",    on_get_sensorwidth,    NULL, NULL, NULL},
    {"shmid",          on_get_shmid,          NULL, NULL, NULL},
    {"state",          on_get_state,          NULL, NULL, NULL},
    {"suspended",      on_get_suspended,      NULL, NULL, NULL},
    {"temperature",    on_get_temperature,    NULL, NULL, NULL},
    {"timeouts",       on_get_timeouts,       NULL, NULL, NULL},
    {"width",          on_get_width,          NULL, NULL, NULL},
    {"xbin",           on_get_xbin,           NULL, NULL, NULL},
    {"xoff",           on_get_xoff,           NULL, NULL, NULL},
    {"ybin",           on_get_ybin,           NULL, NULL, NULL},
    {"yoff",           on_get_yoff,           NULL, NULL, NULL},
    {NULL,             NULL,                  NULL, NULL, NULL},
};

// Get commands.
static tao_recv_callback on_set_abort;
static tao_recv_callback on_set_config;
static tao_recv_callback on_set_debuglevel;
static tao_recv_callback on_set_drop;
static tao_recv_callback on_set_quit;
static tao_recv_callback on_set_start;
static tao_recv_callback on_set_stop;
static tao_recv_callback on_set_suspend_or_resume;
static tao_server_command set_commands[] = {
    {"abort",      NULL, NULL, on_set_abort,             NULL},
    {"config",     NULL, NULL, on_set_config,            NULL},
    {"debuglevel", NULL, NULL, on_set_debuglevel,        NULL},
    {"drop",       NULL, NULL, on_set_drop,              NULL},
    {"quit",       NULL, NULL, on_set_quit,              NULL},
    {"resume",     NULL, NULL, on_set_suspend_or_resume, NULL},
    {"start",      NULL, NULL, on_set_start,             NULL},
    {"stop",       NULL, NULL, on_set_stop,              NULL},
    {"suspend",    NULL, NULL, on_set_suspend_or_resume, NULL},
    {NULL,         NULL, NULL, NULL,                     NULL},
};

// Other callbacks.
static void on_signal(
    int);

static void on_return(
    void);

int main(
    int argc,
    char* argv[])
{
    // Options (all related variables must be static).
    static int nframes = 20;
    static phnx_config cfg;
    static bool quiet = false;
    static bool debug = false;
    static tao_option options[] = {
        PHNX_OPTION_LOAD,
        PHNX_OPTION_SAVE,
        PHNX_OPTION_CAMERA_ROI(cfg),
        PHNX_OPTION_SENSORENCODING(cfg),
        PHNX_OPTION_BUFFERENCODING(cfg),
        PHNX_OPTION_FRAMERATE(cfg),
        PHNX_OPTION_EXPOSURETIME(cfg),
        PHNX_OPTION_BIAS(cfg),
        PHNX_OPTION_GAIN(cfg),
        PHNX_OPTION_CONNECTION(cfg),
        PHNX_OPTION_ACQUISITION_BUFFERS(cfg),
        TAO_OPTION_STRING(2, "apt", "CLASS:NAME",
                          "Access Point of the server", &accesspoint),
        TAO_OPTION_NONNEGATIVE_INT(2, "nframes", "NUMBER",
                                   "Number of frames to memorize", &nframes),
        PHNX_OPTION_QUIET(quiet),
        PHNX_OPTION_DEBUG(debug),
        PHNX_OPTION_HELP_AND_EXIT(0),
        PHNX_OPTION_LAST_ENTRY,
    };

    // Other variables.
    unsigned int perms = 0660;
    int code;
    tao_status status = TAO_OK;

    // Get name of program.
    progname = tao_basename(argv[0]);

    // Install exit and signal handlers.
    if (signal(SIGQUIT, on_signal) == SIG_ERR) {
        tao_store_system_error("signal(SIGQUIT, ...)");
        goto error;
    }
    if (atexit(on_return) != 0) {
        tao_store_system_error("atexit");
        goto error;
    }

    // Parse the command line options and configure the camera.  The camera
    // state is set to "initializing" until the worker thread has started.
    argc = phnx_start_program(&cam, &cfg, argc, argv, options,
                              "Run image server for a camera connected to an "
                              "ActiveSilicon\nPhoenix frame grabber", "");
    if (argc < 0) {
        goto error;
    }
    if (argc != 1) {
        fprintf(stderr, "%s: too many arguments\n", progname);
        goto error;
    }
    debuglevel = (debug?1:0);
    if (debuglevel > 0 || ! quiet) {
        // Print informations about the camera.
        if (phnx_print_camera_info(cam, stdout) != TAO_OK) {
            goto error;
        }
    }
    cam->info.state = TAO_STATE_INITIALIZING;

    // Get server name.
    if (strncmp(accesspoint, "TAO:", 4) != 0) {
        fprintf(stderr,
                "%s: accesspoint must start with \"TAO:\", got \"%s\"\n",
                progname, accesspoint);
        goto error;
    }
    if (TAO_STRLEN(accesspoint) < 5) {
        fprintf(stderr, "%s: no identifier in accesspoint, got \"%s\"\n",
                progname, accesspoint);
        goto error;
    }
    ident = accesspoint + 4;

    // Create the virtual frame-grabber and reflect the camera settings in the
    // "public" resources.
    vfg = tao_framegrabber_create(ident, nframes, perms);
    if (vfg == NULL) {
        fprintf(stderr, "%s: Failed to create TAO frame-grabber\n", progname);
        goto error;
    }
    shcam = tao_framegrabber_get_shared_camera(vfg);
    reflect_settings();

    // Publish shmid of shared camera.
    if (tao_config_write_long(ident, tao_get_shmid(shcam) )!= TAO_OK) {
        fprintf(stderr, "%s: Failed to publish my shmid\n", progname);
        goto error;
    }

    // Create the server.
    srv = tao_create_server(accesspoint, "help!",
                            init_callback, NULL,
                            tao_serve_send_command, get_commands,
                            tao_serve_recv_command, set_commands);
    if (srv == NULL) {
        fprintf(stderr, "%s: Failed to start TAO server at accesspoint \"%s\"\n",
                progname, accesspoint);
        goto error;
    }

    // Start the image processing thread and wait until it starts (not longer
    // than 10 seconds).
    on_processing = process_frame;
    code = pthread_create(&worker, NULL, run_worker, NULL);
    if (code != 0) {
        tao_store_error("pthread_create", code);
        goto error;
    }
    code = pthread_detach(worker);
    if (code != 0) {
        tao_store_error("pthread_detach", code);
        goto error;
    }
    lock_monitor();
    double timeout = 10.0; // seconds
    if (monitor.state == TAO_STATE_INITIALIZING) {
        status = timed_wait_monitor(timeout);
        if (status != TAO_OK) {
            if (status == TAO_TIMEOUT) {
                fprintf(stderr, "%s: worker did not start after "
                        "%.1f seconds.\n", progname, timeout);
            } else  {
                fprintf(stderr, "%s: some error occurred while waiting "
                        "for worker thread to start.\n", progname);
                goto error;
            }
        }
    }
    if (monitor.state != TAO_STATE_WAITING) {
        status = TAO_ERROR;
    }
    if (debuglevel >= COMMAND_DEBUGLEVEL) {
        fprintf(stderr, "server: camera state is \"%s\"...\n",
                tao_state_get_name(monitor.state));
    }
    unlock_monitor();
    if (status != TAO_OK) {
        goto error;
    }

    // Finally run the server.
    while (! quitting && status == TAO_OK) {
        double secs = 0.5;
        tao_poll_requests(secs, 1);
        if (tao_any_errors(NULL)) {
            status = TAO_ERROR;
        }
    }

    // Return status.  The "on_return" callback will take care of releasing
    // resources.
    if (status == TAO_OK && errors == 0) {
        return EXIT_SUCCESS;
    }

 error:
    if (ident != NULL) {
        tao_config_write_long(ident, -1L);
    }
    tao_report_error();
    return EXIT_FAILURE;
}

static void on_return(
    void)
{
    // Free all resources (more or less in reverse order compared to creation).
    // This function should only be called once.  But in case of, we make sure
    // to mark resources as released to avoid errors such as freeing more than
    // once.
    if (debuglevel >= COMMAND_DEBUGLEVEL) {
        fprintf(stderr, "Freeing resources...\n");
    }
    quitting = true;
    if (srv != NULL) {
        // First kill the server.
        tao_abort_server(srv);
        tao_destroy_server(srv);
        srv = NULL;
    }
    if (worker != 0) {
        // Wait for worker thread to finish before releasing other resources.
        pthread_cancel(worker);
        pthread_join(worker, NULL);
        worker = 0;
    }
    if (vfg != NULL) {
        // Destroy shared ressources after having indicating that the camera is
        // no longer reachable.
        (void)tao_shared_camera_wrlock(shcam);
        shcam->info.state = TAO_STATE_KILLED;
        (void)tao_shared_camera_unlock(shcam);
        (void)tao_framegrabber_destroy(vfg);
        vfg = NULL;
    }
    if (cam != NULL) {
        // Finally close camera device.
        tao_camera_destroy(cam);
        cam = NULL;
    }
}

static void on_signal(
    int sig)
{
    char buf[100];
    char* signam;
    switch (sig) {
    case SIGINT:
        signam = "SIGINT";
        break;
    default:
        sprintf(buf, "%d", sig);
        signam = buf;
    }
    fprintf(stderr, "Signal %s received.  Exiting...\n", signam);
    exit(EXIT_FAILURE);
}

// WORKER ROUTINES ----------------------------------------------------------

// Macros for dealing with bits.  Argument `lval` is an L-value (a variable).
#define SET_BITS(lval, bits)    (lval) |= (bits)
#define CLEAR_BITS(lval, bits)  (lval) &= ~(bits)
#define CHECK_BITS(a, b)        (((a) & (b)) != 0)

// A few aliases to improve code readability.
#define STATE_INITIALIZING TAO_STATE_INITIALIZING
#define STATE_SLEEPING     TAO_STATE_WAITING
#define STATE_STARTING     TAO_STATE_STARTING
#define STATE_ACQUIRING    TAO_STATE_WORKING
#define STATE_STOPPING     TAO_STATE_STOPPING
#define STATE_ABORTING     TAO_STATE_ABORTING
#define STATE_FINISHED     TAO_STATE_KILLED

// Aliases for events.
#define EVENT_COMMAND  TAO_EVENT_COMMAND
#define EVENT_FRAME    TAO_EVENT_FRAME
#define EVENT_ERROR    TAO_EVENT_ERROR
#define EVENTS        (EVENT_COMMAND | EVENT_FRAME | \
                       EVENT_ERROR) // all expected events

#define ASSERT(expr)                                                    \
    do {                                                                \
        if (!(expr)) {                                                  \
            fprintf(stderr,                                             \
                    "{ERROR} Assertion `%s' failed in %s (%s:%d).\n",   \
                    #expr, __func__, __FILE__, __LINE__);               \
            tao_store_error(__func__, TAO_ASSERTION_FAILED);            \
            abort();                                                    \
        }                                                               \
    } while (false)

static void lock_monitor(
    void)
{
    tao_mutex_lock(&monitor.mutex);
}

static void unlock_monitor(
    void)
{
    tao_mutex_unlock(&monitor.mutex);
}

static void signal_monitor(
    void)
{
    tao_condition_signal(&monitor.notify);
}

static void wait_monitor(
    void)
{
    tao_condition_wait(&monitor.notify, &monitor.mutex);
}

#ifdef ABSOLUTE_TIMED_WAIT_MONITOR
static tao_status timed_wait_monitor(
    const tao_time* abstime)
{
    return tao_condition_abstimed_wait(&monitor.notify,
                                       &monitor.mutex, abstime);
}
#else
static tao_status timed_wait_monitor(
    double secs)
{
    return tao_condition_timed_wait(&monitor.notify,
                                    &monitor.mutex, secs);
}
#endif

#define ENCODE(PROC,DTYPE,STYPE)                                        \
    static void                                                         \
    PROC(void* dptr, const void* aptr, const void* bptr,                \
         const uint8_t* srcbuf, long width, long height, long stride)   \
    {                                                                   \
        DTYPE* dst = dptr;                                              \
        const DTYPE* a = aptr;                                          \
        const DTYPE* b = bptr;                                          \
        if (a == NULL && b == NULL) {                                   \
            for (long y = 0; y < height; ++y) {                         \
                const STYPE* src = (const STYPE*)(srcbuf + stride*y);   \
                long off = width*y;                                     \
                for (long x = 0; x < width; ++x) {                      \
                    dst[off + x] = (DTYPE)src[x];                       \
                }                                                       \
            }                                                           \
        } else if (a == NULL) {                                         \
            for (long y = 0; y < height; ++y) {                         \
                const STYPE* src = (const STYPE*)(srcbuf + stride*y);   \
                long off = width*y;                                     \
                for (long x = 0; x < width; ++x) {                      \
                    dst[off + x] = (DTYPE)src[x] - b[off + x];          \
                }                                                       \
            }                                                           \
        } else if (b == NULL) {                                         \
            for (long y = 0; y < height; ++y) {                         \
                const STYPE* src = (const STYPE*)(srcbuf + stride*y);   \
                long off = width*y;                                     \
                for (long x = 0; x < width; ++x) {                      \
                    dst[off + x] = a[off + x]*(DTYPE)src[x];            \
                }                                                       \
            }                                                           \
        } else {                                                        \
            for (long y = 0; y < height; ++y) {                         \
                const STYPE* src = (const STYPE*)(srcbuf + stride*y);   \
                long off = width*y;                                     \
                for (long x = 0; x < width; ++x) {                      \
                    dst[off + x] =                                      \
                        a[off + x]*((DTYPE)src[x] -  b[off + x]);       \
                }                                                       \
            }                                                           \
        }                                                               \
    }
ENCODE(process_flt_u8,  float,  uint8_t)
ENCODE(process_flt_u16, float,  uint16_t)
ENCODE(process_flt_flt, float,  float)
ENCODE(process_flt_dbl, float,  double)
ENCODE(process_dbl_u8,  double, uint8_t)
ENCODE(process_dbl_u16, double, uint16_t)
ENCODE(process_dbl_flt, double, float)
ENCODE(process_dbl_dbl, double, double)
#undef ENCODE

static int process_frame(
    const tao_acquisition_buffer* buf)
{
    int status = 0;

    // It is guaranteed that the configuration will not change while processing
    // the buffer, so we do not have to lock the private data while processing.
    // FIXME: not completely exact...
    long width  = buf->width;
    long height = buf->height;
    long stride = buf->stride;
    tao_shared_array* arr = tao_framegrabber_get_buffer(vfg);
    if (arr != NULL
        && TAO_SHARED_ARRAY_NDIMS(arr) == 2
        && TAO_SHARED_ARRAY_DIM(arr, 1) == width
        && TAO_SHARED_ARRAY_DIM(arr, 2) == height) {
        void* dst = TAO_SHARED_ARRAY_DATA(arr);
        tao_encoding bufenc = buf->encoding;
        const void* src = (unsigned char*)buf->data + buf->offset;
        const void* a = NULL; // gain correction
        const void* b = NULL; // bias correction

        switch (TAO_SHARED_ARRAY_ELTYPE(arr)) {
        case TAO_FLOAT:
            switch (bufenc) {
            case TAO_ENCODING_MONO(8):
                process_flt_u8(dst, a, b, src, width, height, stride);
                break;
            case TAO_ENCODING_MONO(16):
                process_flt_u16(dst, a, b, src, width, height, stride);
                break;
            case TAO_ENCODING_FLOAT(32):
                process_flt_flt(dst, a, b, src, width, height, stride);
                break;
            case TAO_ENCODING_FLOAT(64):
                process_flt_dbl(dst, a, b, src, width, height, stride);
                break;
            default:
                status = -1;
            }
            break;
        case TAO_DOUBLE:
            switch (bufenc) {
            case TAO_ENCODING_MONO(8):
                process_dbl_u8(dst, a, b, src, width, height, stride);
                break;
            case TAO_ENCODING_MONO(16):
                process_dbl_u16(dst, a, b, src, width, height, stride);
                break;
            case TAO_ENCODING_FLOAT(32):
                process_dbl_flt(dst, a, b, src, width, height, stride);
                break;
            case TAO_ENCODING_FLOAT(64):
                process_dbl_dbl(dst, a, b, src, width, height, stride);
                break;
            default:
                status = -1;
            }
            break;
        default:
            status = -1;
        }
        if (status != 0) {
            tao_camera_lock(cam);
            tao_store_error(__func__, TAO_BAD_ENCODING);
            tao_camera_unlock(cam);
        }

        // Copy the time stamp and publish the array.
        arr->ts[0] = buf->frame_start;
        arr->ts[1] = buf->frame_end;
        arr->ts[2] = buf->buffer_ready;
        (void)tao_shared_camera_wrlock(shcam);
        (void)tao_framegrabber_post_buffer(vfg);
        (void)tao_shared_camera_unlock(shcam);
    }

    return status;
}

// This function is called by the "worker" thread to change the state of the
// acquisition task, reflect it in the "public" resources and notify this
// change to the "server" thread.  It is assumed that the camera and the
// monitor are locked by the caller (i.e. the "worker") but not the "public"
// resources.
static void set_state(
    tao_state state)
{
    if (debuglevel >= COMMAND_DEBUGLEVEL) {
        fprintf(stderr, "worker: setting camera state to \"%s\"...\n",
                tao_state_get_name(state));
    }

    // Update "private" information and notify these changes.  Note that it
    // does no hurts to notify even though there are no changes, so we do not
    // check whether the state is different.
    cam->info.state = state;
    monitor.state = state;
    signal_monitor();

    // Update "public" information.
    tao_shared_camera_wrlock(shcam);
    shcam->info.state = state;
    tao_shared_camera_unlock(shcam);
}

// Reflect the "private" configuration into the "public" configuration.
static void reflect_settings(
    void)
{
    tao_shared_camera_wrlock(shcam);
    shcam->info = cam->info;
    tao_shared_camera_unlock(shcam);
}

// This function is called by the "worker" thread to execute a specific
// command.
//
// During the execution of the command, transient camera state is updated to be
// one of: "starting", "aborting", "stopping" or "finished".  After execution
// of the command, the "public" and "private" camera states are set to
// "sleeping", "acquiring" or "finished".
//
// After execution of the command, the camera event mask is cleared and the
// monitor is updated and the server is notified of the execution of the
// command.
//
// If camera is already in the correct acquisition mode, nothing is done except
// that the server is notified of the execution of the command.
//
// It is assumed that the camera device has been locked by the caller (i.e. the
// "worker") but neither the "public" resources nor the "monitor".
static void execute_command(
    command cmd)
{
    // If any changes are needed, determine the (transient) state for the
    // command to be executed and the action to perform.
    int action = 0; // 0: do nothing,
                    // 1: stop acquisition,
                    // 2: start acquisition
    tao_state state = cam->info.state;
    if (state != STATE_FINISHED) {
        switch (cmd) {
        case CMD_START:
            if (cam->runlevel == 1) {
                state = STATE_STARTING;
                action = 2;
            }
            break;
        case CMD_STOP:
            if (cam->runlevel == 2) {
                state = STATE_STOPPING;
                action = 1;
            }
            break;
        case CMD_ABORT:
            if (cam->runlevel == 2) {
                state = STATE_ABORTING;
                action = 1;
            }
            break;
        case CMD_QUIT:
            if (cam->runlevel == 2) {
                state = STATE_ABORTING;
                action = 1;
            } else {
                state = STATE_FINISHED;
            }
            break;
        default:
            ;
        }
    }
    if (action != 0) {
        // Notify others of transient state.
        lock_monitor();
        {
            set_state(state);
        }
        unlock_monitor();
        // Start or stop acquisition as needed.
        tao_status status = TAO_OK;
        if (action == 2) {
            if (debuglevel >= COMMAND_DEBUGLEVEL) {
                fprintf(stderr, "Starting acquisition...");
                fflush(stderr);
            }
            status = tao_camera_start_acquisition(cam);
            if (debuglevel >= COMMAND_DEBUGLEVEL) {
                fprintf(stderr, " (runlevel=%d, status=%s)\n",
                        (int)cam->runlevel,
                        (status == TAO_OK ? "OK" : "ERROR"));
                fflush(stderr);
            }
            (void)tao_shared_camera_wrlock(shcam);
            (void)tao_framegrabber_start_acquisition(vfg);
            (void)tao_shared_camera_unlock(shcam);
        } else {
            if (debuglevel >= COMMAND_DEBUGLEVEL) {
                fprintf(stderr, "Stopping acquisition...");
                fflush(stderr);
            }
            status = tao_camera_stop_acquisition(cam);
            if (debuglevel >= COMMAND_DEBUGLEVEL) {
                fprintf(stderr, " (runlevel=%d, status=%s)\n",
                        (int)cam->runlevel,
                        (status == TAO_OK ? "OK" : "ERROR"));
                fflush(stderr);
            }
            (void)tao_shared_camera_wrlock(shcam);
            (void)tao_framegrabber_stop_acquisition(vfg);
            (void)tao_shared_camera_unlock(shcam);
       }
        if (status != TAO_OK) {
            tao_report_error();
            ++errors;
        }
        if (state != STATE_FINISHED) {
            // Determine the new state of the camera on the basis of the
            // expected run-levels after the command and which are: 1
            // ("sleeping") or 2 ("acquiring").  For any other cases, we
            // attempt to reset the camera (once).
            bool retry = false;
            while (true) {
                if (cam->runlevel == 1) {
                    state = STATE_SLEEPING;
                    break;
                } else if (cam->runlevel == 2) {
                    state = STATE_ACQUIRING;
                    break;
                } else if (retry) {
                    if (tao_camera_reset(cam) != TAO_OK) {
                        tao_report_error();
                        ++errors;
                    }
                    retry = false;
                } else {
                    fprintf(stderr, "%s: Failed to reset the camera...\n",
                            progname);
                    state = STATE_FINISHED;
                    break;
                }
            }
        }
    }
    // Inform server of the command execution.
    lock_monitor();
    {
        CLEAR_BITS(cam->events, EVENT_COMMAND);
        monitor.cmd = CMD_NONE;
        set_state(state);
    }
    unlock_monitor();
}

static const char* command_name(
    command cmd)
{
    switch (cmd) {
    case CMD_START: return "start";
    case CMD_STOP:  return "stop";
    case CMD_ABORT: return "abort";
    case CMD_QUIT:  return "quit";
    default:        return "unknown";
    }
}

// This is the function executed by the "worker" thread.
static void* run_worker(
    void* arg)
{
    command cmd = CMD_NONE; // pending command or CMD_NONE

    // Lock camera and change its state to indicate that initialization has
    // been done.
    if (debuglevel >= COMMAND_DEBUGLEVEL) {
        fprintf(stderr, "worker: locking camera...\n");
    }
    tao_camera_lock(cam);
    ASSERT(cam->info.state == STATE_INITIALIZING);
    lock_monitor();
    {
        set_state(STATE_SLEEPING);
    }
    unlock_monitor();

    // Run the event loop.  Only the worker thread is allowed to change the
    // camera state, so we just process the events.
    while (true) {
        // Wait for some events taking care of spurious wake-ups.
        while (cam->pending < 1 && !CHECK_BITS(cam->events, EVENTS)) {
            if (debuglevel > MONITOR_DEBUGLEVEL) {
                fprintf(stderr, "worker: waiting for camera events...\n");
            }
            if (tao_camera_wait(cam) != TAO_OK) {
                tao_report_error();
                ++errors;
                execute_command(CMD_QUIT); // force finished
                goto done;
            }
            if (debuglevel >= MONITOR_DEBUGLEVEL) {
#define PRT_BIT(cam, id)                                                \
                do {                                                    \
                    if (((cam)->events & TAO_EVENT_##id) != 0) {        \
                        if (flag) {                                     \
                            fputs("|", stderr);                         \
                        }                                               \
                        fputs(#id, stderr);                             \
                        flag = true;                                    \
                    }                                                   \
                } while (false)
                bool flag = false;
                fprintf(stderr, "worker: camera events: 0x%08x (",
                        (unsigned int)cam->events);
                PRT_BIT(cam, ERROR);
                PRT_BIT(cam, FRAME);
                PRT_BIT(cam, COMMAND);
                fputs(")\n", stderr);
                fflush(stderr);
#undef PRT_BIT
            }
        }

        // Process events and commands in priority order.  The bits
        // corresponding to events taken into account are cleared.  For
        // commands ("start", "stop", "abort" or "quit"), clearing the bits is
        // done *after* processing the event and the server thread is notified.
        // The "stop" command is executed after processing a possible pending
        // frame, other commands are executed before.
        if (CHECK_BITS(cam->events, EVENT_ERROR)) {
            // A new error has occurred and the corresponding "private" camera
            // counter has been incremented.  We just reflect this change in
            // the "public" resources.
            reflect_settings();
            CLEAR_BITS(cam->events, EVENT_ERROR);
        }

        // Fetch pending command if any.  The command is executed when
        // appropriate.
        if (CHECK_BITS(cam->events, EVENT_COMMAND)) {
            lock_monitor();
            {
                if (debuglevel >= COMMAND_DEBUGLEVEL) {
                    fprintf(stderr, "worker: command \"%s\" received\n",
                            command_name(monitor.cmd));
                }
                switch (monitor.cmd) {
                case CMD_START:
                case CMD_STOP:
                case CMD_ABORT:
                case CMD_QUIT:
                    break;
                default:
                    // Any other command is ignored.  The server is however
                    // notified.
                    CLEAR_BITS(cam->events, EVENT_COMMAND);
                    monitor.state = cam->info.state;
                    monitor.cmd = CMD_NONE;
                    signal_monitor();
                }
                cmd = monitor.cmd;
            }
            unlock_monitor();
        }

        // Execute pending "start" or "abort" command before processing frame.
        if (cmd == CMD_START || cmd == CMD_ABORT) {
            execute_command(cmd);
            cmd = CMD_NONE;
            if (cam->info.state == STATE_FINISHED) {
                goto done;
            }
        }

        // We do not use the "frame" event to determine whether a new frame is
        // available, we just consider the number of pending buffers.  We
        // however clear the event bit to avoid not waiting for the next frame.
        CLEAR_BITS(cam->events, EVENT_FRAME);

        // If there are any pending frames, deal with them.
        if (cam->pending > 0) {
            phnx_device* dev = (phnx_device*)cam;
            if (drop) {
                // Just keep the more recent buffer.
                if (phnx_release_buffers(dev, 1) != TAO_OK) {
                    tao_report_error(); // FIXME: should quit?
                }
            }

            // Get the image processing to apply from the current settings.  If
            // any processing has to be done, get the acquisition buffer and
            // apply the processing.
            static int (*process)(const tao_acquisition_buffer* buf);
            if (cam->runlevel == 2 && ! suspended) {
                process = on_processing;
            } else {
                process = NULL;
            }
            if (process != NULL) {
                // Get index and address of first pending acquisition
                // buffer.
                int index = TAO_FIRST_PENDING_ACQUISITION_BUFFER(cam);
                tao_acquisition_buffer* buf = &cam->bufs[index];
                buf->data = phnx_get_buffer(dev);
                if (buf->data == NULL) {
                    tao_report_error(); // FIXME: should quit?
                } else {
                    // Do apply the processing, unlocking the camera during
                    // the operation to let others signal events. */
                    if (debuglevel >= MONITOR_DEBUGLEVEL) {
                        fputs("worker: Processing frame...\n", stderr);
                    }
                    tao_camera_unlock(cam);
                    if (process(buf) != 0) {
                        fprintf(stderr, "Error occurred while processing "
                                "frame number %lld\n", (long long)buf->serial);
                        ++errors;
                    }
                    tao_camera_lock(cam);
                }
            }

            // In any case, the buffer should be considered as having been
            // consumed so release the buffer.
            if (phnx_release_buffers(dev, cam->pending - 1) != TAO_OK) {
                tao_report_error(); // FIXME: should quit?
            }
        }

        // Execute any other pending commands.
        if (cmd != CMD_NONE) {
            execute_command(cmd);
            cmd = CMD_NONE;
            if (cam->info.state == STATE_FINISHED) {
                goto done;
            }
        }
    } // end of events loop

    // Unlock the camera and return.
 done:
    tao_camera_unlock(cam);
    return NULL;
}

// SERVER ROUTINES -----------------------------------------------------------

static tao_status init_callback(
    tao_server* srv,
    void* ctx)
{
    // FIXME: tao_shared_camera_wrlock(shcam);
    // FIXME: tao__set_shared_object_accesspoint(
    // FIXME:     &shcam->base, tao_get_server_accesspoint(srv));
    // FIXME: tao_shared_camera_unlock(shcam);
    return TAO_OK;
}

// GET CALLBACK -------------------------------------------------------------

// Set the data returned by an xpaxget request with a formatted text.
static tao_status format_result(
    tao_server* srv,
    void** bufptr,
    size_t* sizptr,
    const char* format,
    ...) TAO_FORMAT_PRINTF(4,5);

// Set the data returned by an xpaxget request with a static string.
static tao_status set_static_result(
    tao_server* srv,
    void** bufptr,
    size_t* sizptr,
    const char* str);

// Set an "usage" error message in reply to an xpaget command.
static void send_usage(
    tao_server* srv,
    const char* argv0,
    const char* args)
{
    // We assume that the usage message is short enough and the format
    // specification valid so that no errors occur below.  This should be the
    // case as the arguments are controlled by the caller.  */
    if (args == NULL) {
        tao_format_reply_message(srv, "Usage: xpaget %s", argv0);
    } else {
        tao_format_reply_message(srv, "Usage: xpaget %s %s", argv0, args);
    }
}

static tao_status format_result(
    tao_server* srv,
    void** bufptr,
    size_t* sizptr,
    const char* format,
    ...)
{
    // Print the formated message into the data buffer of the server, then set
    // the reply data (*bufptr and *sizptr) to reference the contents of the
    // server data buffer.
    va_list args;
    va_start(args, format);
    tao_status status = tao_buffer_vprintf(&srv->databuf, format, args);
    va_end(args);
    if (status == TAO_OK) {
        status = tao_set_reply_data_from_buffer(srv, bufptr, sizptr, false);
    }
    return status;
}

static tao_status set_static_result(
    tao_server* srv,
    void** bufptr,
    size_t* sizptr,
    const char* str)
{
    *sizptr = tao_strlen(str);
    *bufptr = (void*)str;
    return TAO_OK;
}

static tao_status on_get_accesspoint(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void** bufptr,
    size_t* sizptr)
{
    if (argc != 1) {
        send_usage(srv, argv[0], NULL);
        return TAO_ERROR;
    }
    return format_result(srv, bufptr, sizptr, "%s\n",
                         tao_get_server_accesspoint(srv));
}

static tao_status on_get_origin(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void** bufptr,
    size_t* sizptr)
{
    if (argc != 1) {
        send_usage(srv, argv[0], NULL);
        return TAO_ERROR;
    }
    char buffer[32];
    tao_camera_lock(cam);
    tao_time val = cam->info.origin;
    tao_camera_unlock(cam);
    return format_result(srv, bufptr, sizptr, "%s\n",
                         tao_sprintf_time(buffer, &val));
}

#define INT64_FMT TAO_INT64_FORMAT(,d)

#define GETTER(type, format, name, memb)                                \
    static tao_status on_get_##name(                                  \
        tao_server* srv,                                              \
        void* ctx,                                                      \
        int argc,                                                       \
        const char* argv[],                                             \
        void** bufptr,                                                  \
        size_t* sizptr)                                                 \
    {                                                                   \
        if (argc != 1) {                                                \
            send_usage(srv, argv[0], NULL);                             \
            return TAO_ERROR;                                           \
        }                                                               \
        tao_camera_lock(cam);                                           \
        type val = cam->info.memb;                                      \
        tao_camera_unlock(cam);                                         \
        return format_result(srv, bufptr, sizptr, format"\n", val);     \
    }
GETTER(long,   "%ld",      sensorwidth,  sensorwidth)
GETTER(long,   "%ld",      sensorheight, sensorheight)
GETTER(long,   "%ld",      nbufs,        config.nbufs)
GETTER(long,   "%ld",      xbin,         config.roi.xbin)
GETTER(long,   "%ld",      ybin,         config.roi.ybin)
GETTER(long,   "%ld",      xoff,         config.roi.xoff)
GETTER(long,   "%ld",      yoff,         config.roi.yoff)
GETTER(long,   "%ld",      width,        config.roi.width)
GETTER(long,   "%ld",      height,       config.roi.height)
GETTER(double, "%#g",      exposuretime, config.exposuretime)
GETTER(double, "%#g",      framerate,    config.framerate)
GETTER(double, "%#.1f",    temperature,  temperature)
GETTER(int64_t, INT64_FMT, frames,       frames)
GETTER(int64_t, INT64_FMT, overruns,     overruns)
GETTER(int64_t, INT64_FMT, lostframes,   lostframes)
GETTER(int64_t, INT64_FMT, overflows,    overflows)
GETTER(int64_t, INT64_FMT, lostsyncs,    lostsyncs)
GETTER(int64_t, INT64_FMT, timeouts,     timeouts)
#undef GETTER

static tao_status on_get_bias(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void** bufptr,
    size_t* sizptr)
{
    if (argc != 1) {
        send_usage(srv, argv[0], NULL);
        return TAO_ERROR;
    }
    tao_camera_lock(cam);
    double val = ((phnx_device*)cam)->extra.bias;
    tao_camera_unlock(cam);
    return format_result(srv, bufptr, sizptr, "%#g\n", val);
}

static tao_status on_get_gain(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void** bufptr,
    size_t* sizptr)
{
    if (argc != 1) {
        send_usage(srv, argv[0], NULL);
        return TAO_ERROR;
    }
    tao_camera_lock(cam);
    double val = ((phnx_device*)cam)->extra.gain;
    tao_camera_unlock(cam);
    return format_result(srv, bufptr, sizptr, "%#g\n", val);
}

static tao_status on_get_connection(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void** bufptr,
    size_t* sizptr)
{
    if (argc != 1) {
        send_usage(srv, argv[0], NULL);
        return TAO_ERROR;
    }
    tao_camera_lock(cam);
    phnx_connection val = ((phnx_device*)cam)->extra.connection;
    tao_camera_unlock(cam);
    char buf[PHNX_CONNECTION_STRING_SIZE];
    (void)phnx_format_connection_settings(buf, &val);
    return format_result(srv, bufptr, sizptr, "%s\n", buf);
}

static tao_status on_get_debuglevel(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void** bufptr,
    size_t* sizptr)
{
    if (argc != 1) {
        send_usage(srv, argv[0], NULL);
        return TAO_ERROR;
    }
    tao_camera_lock(cam);
    int val = debuglevel;
    tao_camera_unlock(cam);
    return format_result(srv, bufptr, sizptr, "%d\n", val);
}

static tao_status on_get_drop(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void** bufptr,
    size_t* sizptr)
{
    if (argc != 1) {
        send_usage(srv, argv[0], NULL);
        return TAO_ERROR;
    }
    tao_camera_lock(cam);
    const char* str = (drop ? "yes\n" : "no\n");
    tao_camera_unlock(cam);
    return set_static_result(srv, bufptr, sizptr, str);
}

static tao_status on_get_ping(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void** bufptr,
    size_t* sizptr)
{
    if (argc != 1) {
        send_usage(srv, argv[0], NULL);
        return TAO_ERROR;
    }
    char buf[32];
    tao_time t;
    (void)tao_get_monotonic_time(&t);
    tao_sprintf_time(buf, &t);
    return format_result(srv, bufptr, sizptr, "%s\n", buf);
}

static tao_status on_get_roi(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void** bufptr,
    size_t* sizptr)
{
    if (argc != 1) {
        send_usage(srv, argv[0], NULL);
        return TAO_ERROR;
    }
    tao_camera_lock(cam);
    tao_camera_roi roi = cam->info.config.roi;
    tao_camera_unlock(cam);
    return format_result(srv, bufptr, sizptr, "%ld %ld %ld %ld %ld %ld\n",
                         roi.xbin, roi.ybin, roi.xoff, roi.yoff,
                         roi.width, roi.height);
}

static tao_status on_get_shmid(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void** bufptr,
    size_t* sizptr)
{
    // No needs to lock resources to get shmid as its value is imutable.
    if (argc != 1) {
        send_usage(srv, argv[0], NULL);
        return TAO_ERROR;
    }
    int id = tao_shared_camera_get_shmid(shcam);
    return format_result(srv, bufptr, sizptr, "%d\n", id);
}

static tao_status on_get_state(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void** bufptr,
    size_t* sizptr)
{
    if (argc != 1) {
        send_usage(srv, argv[0], NULL);
        return TAO_ERROR;
    }
    tao_camera_lock(cam);
    tao_state state = cam->info.state;
    tao_camera_unlock(cam);
    return format_result(srv, bufptr, sizptr, "%s\n",
                         tao_state_get_name(state));
}

static tao_status on_get_suspended(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void** bufptr,
    size_t* sizptr)
{
    if (argc != 1) {
        send_usage(srv, argv[0], NULL);
        return TAO_ERROR;
    }
    tao_camera_lock(cam);
    const char* str = suspended ? "yes\n" : "no\n";
    tao_camera_unlock(cam);
    return set_static_result(srv, bufptr, sizptr, str);
}

static tao_status get_encoding(
    tao_server* srv,
    void** bufptr,
    size_t* sizptr,
    tao_encoding enc)
{
    char buf[TAO_ENCODING_STRING_SIZE];
    if (tao_format_encoding(buf, enc) != TAO_OK) {
        strcpy(buf, "Unknown");
    }
    return format_result(srv, bufptr, sizptr, "%s\n", buf);
}

static tao_status on_get_pixeltype(
    tao_server* srv, void* ctx,
    int argc, const char* argv[],
    void** bufptr, size_t* sizptr)
{
    if (argc != 1) {
        send_usage(srv, argv[0], NULL);
        return TAO_ERROR;
    }
    tao_camera_lock(cam);
    tao_encoding enc = tao_encoding_of_eltype(cam->info.config.pixeltype);
    tao_camera_unlock(cam);
    return get_encoding(srv, bufptr, sizptr, enc);
}

static tao_status on_get_sensorencoding(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void** bufptr,
    size_t* sizptr)
{
    if (argc != 1) {
        send_usage(srv, argv[0], NULL);
        return TAO_ERROR;
    }
    tao_camera_lock(cam);
    tao_encoding enc = cam->info.config.sensorencoding;
    tao_camera_unlock(cam);
    return get_encoding(srv, bufptr, sizptr, enc);
}

static tao_status on_get_bufferencoding(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void** bufptr,
    size_t* sizptr)
{
    if (argc != 1) {
        send_usage(srv, argv[0], NULL);
        return TAO_ERROR;
    }
    tao_camera_lock(cam);
    tao_encoding enc = cam->info.config.bufferencoding;
    tao_camera_unlock(cam);
    return get_encoding(srv, bufptr, sizptr, enc);
}

// SET CALLBACK -------------------------------------------------------------

// Set an "usage" error message in reply to an xpaset command.
static void recv_usage(
    tao_server* srv,
    const char* argv0,
    const char* args)
{
    // We assume that the usage message is short enough and the format
    // specification valid so that no errors occur below.  This should be the
    // case as the arguments are controlled by the caller.
    if (args == NULL) {
        tao_format_reply_message(srv, "Usage: xpaset %s", argv0);
    } else {
        tao_format_reply_message(srv, "Usage: xpaset %s %s", argv0, args);
    }
}

static void recv_takes_no_data(
    tao_server* srv,
    const char* name)
{
    tao_format_reply_message(srv, "Expecting no data for xpaset "
                             "command \"%s\"", name);
}

// Send a command to the worker thread and wait for the answer.
static tao_status recv_command(
    tao_server* srv,
    command cmd,
    int argc,
    const char* argv[],
    void* buf,
    size_t siz)
{
    const char* errmsg = NULL; // Non-NULL means an error occurred.
    if (siz > 0) {
        recv_takes_no_data(srv, argv[0]);
        return TAO_ERROR;
    }
    if (argc != 1) {
        recv_usage(srv, argv[0], NULL);
        return TAO_ERROR;
    }
    if (cmd == CMD_QUIT) {
        quitting = true;
        tao_abort_server(srv);
    }
    lock_monitor();
    {
        bool done;
        tao_state state = monitor.state;
        if (cmd == CMD_START) {
            done = (state == STATE_ACQUIRING);
        } else if (cmd == CMD_STOP || cmd == CMD_ABORT) {
            done = (state == STATE_SLEEPING);
        } else if (cmd == CMD_QUIT) {
            done = (state == STATE_FINISHED);
        } else {
            done = true;
            errmsg = "Invalid command";
        }
        if (! done && state == STATE_FINISHED && cmd != CMD_QUIT) {
            errmsg = "Camera has been closed";
        } else if (! done) {
            // Push a "command" event and signal it to the "worker" thread.
            tao_camera_lock(cam);
            {
                cam->events |= EVENT_COMMAND;
                tao_camera_broadcast(cam);
            }
            tao_camera_unlock(cam);

            // Set the pending command, wait for it to be taken into account by
            // the "worker" thread and examine the resulting camera state to
            // determine whether the command was successful or not.
            monitor.cmd = cmd;
            while (monitor.cmd != CMD_NONE &&
                   monitor.state != STATE_FINISHED) {
                wait_monitor();
            }
            if (monitor.state == STATE_FINISHED && cmd != CMD_QUIT) {
                errmsg = "Camera has been closed due to unrecoverable errors";
            } else if (cmd == CMD_START) {
                if (monitor.state != STATE_ACQUIRING) {
                    errmsg = "Failed to start acquisition";
                }
            } else if (cmd == CMD_STOP || cmd == CMD_ABORT) {
                if (monitor.state != STATE_SLEEPING) {
                    errmsg = "Failed to stop acquisition";
                }
            }
        }
    }
    unlock_monitor();
    if (errmsg != NULL) {
        tao_set_static_reply_message(srv, errmsg);
        return TAO_ERROR;
    }
    return TAO_OK;
}

static tao_status on_set_debuglevel(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void* buf,
    size_t siz)
{
    int value;
    if (siz > 0) {
        recv_takes_no_data(srv, argv[0]);
        return TAO_ERROR;
    }
    if (argc != 2) {
        recv_usage(srv, argv[0], "level");
        return TAO_ERROR;
    }
    if (tao_parse_int(argv[1], &value, 0) != TAO_OK || value < 0) {
        tao_set_reply_message(srv, "Invalid debug-level", -1);
        return TAO_ERROR;
    }
    tao_camera_lock(cam);
    debuglevel = value;
    tao_camera_unlock(cam);
    tao_set_server_debuglevel(srv, value);
    return TAO_OK;
}

static tao_status on_set_drop(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void* buf,
    size_t siz)
{
    bool value;
    if (siz > 0) {
        recv_takes_no_data(srv, argv[0]);
        return TAO_ERROR;
    }
    if (argc == 2 && strcmp(argv[1], "yes") == 0) {
        value = true;
    } else if (argc == 2 && strcmp(argv[1], "no") == 0) {
        value = false;
    } else {
        recv_usage(srv, argv[0], "yes|no");
        return TAO_ERROR;
    }
    tao_camera_lock(cam);
    drop = value;
    tao_camera_unlock(cam);
    return TAO_OK;
}

static tao_status on_set_suspend_or_resume(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void* buf,
    size_t siz)
{
    if (siz > 0) {
        recv_takes_no_data(srv, argv[0]);
        return TAO_ERROR;
    }
    if (argc != 1) {
        recv_usage(srv, argv[0], NULL);
        return TAO_ERROR;
    }
    tao_camera_lock(cam);
    suspended = (argv[0][0] == 's');
    tao_camera_unlock(cam);
    return TAO_OK;
}

static tao_status on_set_start(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void* buf,
    size_t siz)
{
    return recv_command(srv, CMD_START, argc, argv, buf, siz);
}

static tao_status on_set_stop(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void* buf,
    size_t siz)
{
    return recv_command(srv, CMD_STOP, argc, argv, buf, siz);
}

static tao_status on_set_abort(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void* buf,
    size_t siz)
{
    return recv_command(srv, CMD_ABORT, argc, argv, buf, siz);
}

static tao_status on_set_quit(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void* buf,
    size_t siz)
{
    return recv_command(srv, CMD_QUIT, argc, argv, buf, siz);
}

// Define some macros to deal with configuration options.
#define MARKED(memb) (*(unsigned char*)&mrk.memb != 0)
#define GETOPT(name, memb, expr)                \
    do {                                        \
        if (strcmp(key, name) == 0) {           \
            if (MARKED(memb)) {                 \
                goto duplicate_key;             \
            } else if (!(expr)) {               \
                goto bad_value;                 \
            } else {                            \
                *(unsigned char*)&mrk.memb = 1; \
                goto next_key;                  \
            }                                   \
        }                                       \
    } while (false)
#define SETOPT(memb)                                    \
    do {                                                \
        if (MARKED(memb) && req.memb != cfg.memb) {     \
            cfg.memb = req.memb;                        \
            change = true;                              \
        }                                               \
    } while (false)

static tao_status on_set_config(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void* buf,
    size_t siz)
{
    const char* key;
    const char* val;
    phnx_config req; // to store requested settings
    phnx_config mrk; // to mark requested settings
    phnx_config cfg; // to store settings

    if (siz > 0) {
        recv_takes_no_data(srv, argv[0]);
        return TAO_ERROR;
    }

    // There must be an odd number of arguments (the command plus any number of
    // key-value pairs).
    if ((argc & 1) != 1) {
        recv_usage(srv, argv[0], "key1 val1 [key2 val2 ...]");
        return TAO_ERROR;
    }

    // Clear the dummy `mrk` configuration which is used to keep track of
    // configured parameters and avoid duplicates.
    memset(&mrk, 0, sizeof(mrk));

    // Parse all settings.
    for (int i = 1; i < argc - 1; i += 2) {
        key = argv[i];
        val = argv[i+1];
        if (tao_get_server_debuglevel(srv) > 0) {
            fprintf(stderr, "key=%s, val=%s\n", key, val);
        }
        int c = key[0];
        if (c == 'b') {
            GETOPT("bias", extra.bias,
                  tao_parse_double(val, &req.extra.bias) == TAO_OK);
            GETOPT("bufferencoding", base.bufferencoding,
                   (req.base.bufferencoding = tao_parse_encoding(val)) !=
                   TAO_ENCODING_UNKNOWN);
            GETOPT("nbufs", base.nbufs,
                   tao_parse_long(val, &req.base.nbufs, 10) == TAO_OK
                   && req.base.nbufs >= 2);
        } else if (c == 'e') {
            GETOPT("exposuretime", base.exposuretime,
                   tao_parse_double(val, &req.base.exposuretime) == TAO_OK
                   && req.base.exposuretime >= 0);
        } else if (c == 'f') {
            GETOPT("framerate", base.framerate,
                   tao_parse_double(val, &req.base.framerate) == TAO_OK
                   && req.base.framerate > 0);
        } else if (c == 'g') {
            GETOPT("gain", extra.gain,
                  tao_parse_double(val, &req.extra.gain) == TAO_OK);
        } else if (c == 'h') {
            GETOPT("height", base.roi.height,
                   tao_parse_long(val, &req.base.roi.height, 10) == TAO_OK
                   && req.base.roi.height >= 1);
        } else if (c == 'p') {
            GETOPT("pixeltype", base.pixeltype,
                   (req.base.pixeltype = tao_parse_encoding(val)) !=
                   TAO_ENCODING_UNKNOWN);
        } else if (c == 's') {
            GETOPT("sensorencoding", base.sensorencoding,
                   (req.base.sensorencoding = tao_parse_encoding(val)) !=
                   TAO_ENCODING_UNKNOWN);
        } else if (c == 'w') {
            GETOPT("width", base.roi.width,
                   tao_parse_long(val, &req.base.roi.width, 10) == TAO_OK
                   && req.base.roi.width >= 1);
        } else if (c == 'x') {
            GETOPT("xbin", base.roi.xbin,
                   tao_parse_long(val, &req.base.roi.xbin, 10) == TAO_OK
                   && req.base.roi.xbin >= 1);
            GETOPT("xoff", base.roi.xoff,
                   tao_parse_long(val, &req.base.roi.xoff, 10) == TAO_OK
                   && req.base.roi.xoff >= 0);
        } else if (c == 'y') {
            GETOPT("ybin", base.roi.ybin,
                   tao_parse_long(val, &req.base.roi.ybin, 10) == TAO_OK
                   && req.base.roi.ybin >= 1);
            GETOPT("yoff", base.roi.yoff,
                   tao_parse_long(val, &req.base.roi.yoff, 10) == TAO_OK
                   && req.base.roi.yoff >= 0);
        }
        tao_format_reply_message(srv, "Unknown key `%s`", key);
        return TAO_ERROR;

    next_key:
        continue;

    duplicate_key:
        tao_format_reply_message(srv, "Duplicate key `%s`", key);
        return TAO_ERROR;

    bad_value:
        tao_format_reply_message(srv, "Invalid value for key `%s`", key);
        return TAO_ERROR;
    }

    // Camera must not be acquiring to be configurable.
    tao_status status = TAO_OK;
    tao_camera_lock(cam); // FIXME: check status
    {
        if (cam->runlevel == 1) {
            // Get current configuration, apply the changes, if any, make sure
            // the configuration is up-to-date and whatever could have been
            // done, reflect the actual configuration into the shared camera
            // data.
            phnx_get_configuration(cam, &cfg);
            bool change = false;
            SETOPT(extra.bias);
            SETOPT(base.bufferencoding);
            SETOPT(base.nbufs);
            SETOPT(base.framerate);
            SETOPT(extra.gain);
            SETOPT(base.exposuretime);
            SETOPT(base.pixeltype);
            SETOPT(base.roi.height);
            SETOPT(base.roi.width);
            SETOPT(base.roi.xbin);
            SETOPT(base.roi.xoff);
            SETOPT(base.roi.ybin);
            SETOPT(base.roi.yoff);
            SETOPT(base.sensorencoding);
            if (change && phnx_set_configuration(cam, &cfg) != TAO_OK) {
                status = TAO_ERROR;
            }
            reflect_settings();
            // FIXME: if (status != TAO_OK) {
            // FIXME:     tao_transfer_errors(&srv->errors, &cam->errs);
            // FIXME: }
        } else {
            // We cannot change the settings now.
            const char* msg;
            if (cam->info.state == STATE_FINISHED) {
                msg = "Camera has been closed";
            } else if (cam->runlevel == 2) {
                msg = "Cannot change settings during acquisition";
            } else {
                msg = "Camera in improper state";
            }
            tao_set_static_reply_message(srv, msg);
            status = TAO_ERROR;
        }
    }
    tao_camera_unlock(cam); // FIXME: update status
    return status;
}

#undef GETOPT
#undef SETOPT
#undef IS_MARKED
