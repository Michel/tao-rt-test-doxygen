// tao-phoenix.h -
//
// Definitions for TAO interface to ActiveSilicon Phoenix frame grabber.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2017-2022, Éric Thiébaut.
// Copyright (C) 2016, Éric Thiébaut & Jonathan Léger.

#ifndef TAO_PHOENIX_H_
#define TAO_PHOENIX_H_ 1

#include <tao-cameras.h>

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <pthread.h>

TAO_BEGIN_DECLS

/**
 * @defgroup PhoenixBasics  Phoenix basics
 *
 * @ingroup PhoenixCameras
 *
 * @brief Basic operations and type definitions for Phoenix cameras.
 *
 * @{
 */

/**
 * Structure describing a camera connected to an ActiveSilicon *Phoenix* frame
 * grabber.
 */
typedef struct phnx_device_ phnx_device;

/**
 * Connection settings for image transmission.
 */
typedef struct phnx_connection_ {
    uint32_t channels; ///< Number of active connection channels
    uint32_t    speed; ///< Bitrate (in Mbps) of each channel
} phnx_connection;

/**
 * Format connection settings into a string.
 *
 * This function format connection settings into a string in the form:
 * `"NCHNS*BITRATE"` where `NCHNS` is the number of active connection channels
 * and `BITRATE` is bitrate (in Mbps) of each channel.
 *
 * @param str   The destination string.  Must have at least
 *              `PHNX_CONNECTION_STRING_SIZE` characters (including the final
 *              null).
 * @param con   Pointer to structure with connection settings.
 *
 * @return The destination string.
 *
 * @see phnx_connection, phnx_parse_connection_settings().
 */
extern char* phnx_format_connection_settings(
    char* str,
    const phnx_connection* con);

#define PHNX_CONNECTION_STRING_SIZE 22

/**
 * Parse connection settings from a string.
 *
 * This function parses connection settings from a string of the form:
 * `"NCHNS*BITRATE"` where `NCHNS` is the number of active connection channels
 * and `BITRATE` is bitrate (in Mbps) of each channel.  No spaces are allowed
 * in the string because this function is intended to deal with command line
 * arguments which are usually split at spaces.
 *
 * @param con   Pointer to structure to set with the parsed settings.
 *              Can be `NULL` to just check the validity of the string.
 * @param str   The string to parse.
 *
 * @return @ref TAO_OK if the string is valid, @ref TAO_ERROR otherwise.
 *
 * @see phnx_connection, phnx_format_connection_settings().
 */
extern tao_status phnx_parse_connection_settings(
    phnx_connection* con,
    const char* str);

/**
 * Configurable parameters specific to "Phoenix" cameras.
 *
 * This structure stores the configurable parameters specific to "Phoenix"
 * cameras not in common camera configurable parameters.
 *
 * @see phnx_config, tao_camera_config.
 */
typedef struct phnx_extra_config_ {
    double                bias;///< Detector bias.
    double                gain;///< Detector gain.
    phnx_connection connection;///< Connection for image transmission.
} phnx_extra_config;

/**
 * Configurable parameters of "Phoenix" cameras.
 */
typedef struct phnx_config_ {
    tao_camera_config  base;///< Configurable parameters common to all
                            ///< cameras.
    phnx_extra_config extra;///< Configurable parameters specific to
                              ///< "Phoenix" cameras.
} phnx_config;

/**
 * Table of virtual methods for a Phoenix camera.
 */
typedef struct phnx_operations_ {
    const char* model;///< Name of camera model.

    bool (*identify)(
        phnx_device*);
    ///< Callback to identify this specific model.  Shall return `true` if the
    ///< camera is identified as this model, `false` otherwise.

    tao_status (*initialize)(
        phnx_device*);
    ///< Callback to (re-)initialize the camera.  Shall return non-zero on
    ///< error.

    tao_status (*start)(
        phnx_device*);
    ///< Start callback.  Shall return non-zero on error.

    tao_status (*stop)(
        phnx_device*);
    ///< Stop callback.  Shall return non-zero on error.

    tao_status (*update_config)(
        phnx_device*, bool all);
    ///< Callback to retrieve the camera current device settings (and update
    ///< dev_cfg).  Shall return non-zero on error.

    tao_status (*check_config)(
        phnx_device*,
        const phnx_config*);
    ///< Callback to check set the camera settings.  Shall return non-zero on
    ///< error.

    tao_status (*set_config)(
        phnx_device*,
        const phnx_config*);
    ///< Callback to set the camera settings according to the configuration
    ///< chosen by the user.  Shall return non-zero on error.

    tao_status (*save_config)(
        phnx_device*,
        int);
    ///< Callback to save the camera settings.  Shall return non-zero on
    ///< error.

    tao_status (*load_config)(
        phnx_device*,
        int);
    ///< Callback to load the camera settings.  Shall return non-zero on
    ///< error.
} phnx_operations;

/**
 * Virtual operations table for the Mikrotron MC408x cameras.
 */
extern const phnx_operations phnx_mikrotron_mc408x;

/**
 * Configuration settings to open a device.
 *
 * These options are used by phnx_create_camera().
 */
typedef struct phnx_create_options_ {
    enum {
        PHNX_BOARD_NUMBER_AUTO,
        PHNX_BOARD_NUMBER_1,
        PHNX_BOARD_NUMBER_2,
        PHNX_BOARD_NUMBER_3,
        PHNX_BOARD_NUMBER_4,
        PHNX_BOARD_NUMBER_5,
        PHNX_BOARD_NUMBER_6,
        PHNX_BOARD_NUMBER_7,
    } boardnumber;
    enum {
        PHNX_CHANNEL_NUMBER_AUTO,
        PHNX_CHANNEL_NUMBER_1,
        PHNX_CHANNEL_NUMBER_2,
    } channelnumber;
    enum {
        PHNX_CONFIG_MODE_NORMAL,
        PHNX_CONFIG_MODE_COMMS_ONLY,
        PHNX_CONFIG_MODE_ACQ_ONLY,
    } configmode;
} phnx_create_options;

#define PHNX_CREATE_OPTIONS_DEFAULT \
    {PHNX_BOARD_NUMBER_AUTO, PHNX_CHANNEL_NUMBER_AUTO, PHNX_CONFIG_MODE_NORMAL}

/**
 * Create a new instance of a frame grabber connection to a camera.
 *
 * This function allocates a camera hanlder, connects to the camera and sets
 * some initial camera parameters.  A pointer to a `tao_camera` structure is
 * returned.  This pointer can be used to query/set camera parameters and
 * acquire images.  When no longer needed, the ressources should be released by
 * calling tao_destroy_camera().
 *
 * @param handler     A user defined handler for errors.  If `NULL`, a
 *                    default handler is used whose behavior can be tuned via
 *                    phx_set_error_handler_verbosity().
 *
 * @param configname  The name of the camera configuration file.  Can be
 *                    `NULL`, to attempt to automatically guess the camera
 *                    settings (for now, this only works for the Mikrotron
 *                    MC408x series cameras).
 *
 * @param boardnumber The frame grabber board number.  It can be automatically
 *                    set by using the value `PHX_BOARD_NUMBER_AUTO`.
 *
 * @return A pointer to the new instance or `NULL` in case of errors.
 *
 * @see tao_destroy_camera().
 */
extern tao_camera* phnx_create_camera(
    void (*handler)(const char*, int, const char*),
    char* configname,
    const phnx_create_options* opts);

/**
 * Print camera description.
 */
extern tao_status phnx_print_camera_info(
    tao_camera* cam, FILE* stream);

/**
 * Get "Phoenix" camera device structure.
 *
 * This function check whether @a cam is a "Phoenix" camera and returns its
 * specific device component.
 *
 * @param cam    Address of unified camera structure.
 *
 * @return Address of specific "Phoenix" camera device or `NULL` if @a cam is
 *         not a "Phoenix" camera.
 */
extern phnx_device* phnx_get_device(
    tao_camera* cam);

/**
 * @}
 */

//-----------------------------------------------------------------------------
// CONFIGURATION

/**
 * @defgroup PhoenixConfiguration  Phoenix configuration
 *
 * @ingroup PhoenixCameras
 *
 * @brief Management of Phoenix camera settings.
 *
 * Configurable camera parameters are the region of interest (ROI), the
 * exposure duration and frame rate, the detector bias and gain, *etc*.  As
 * setting camera parameters sequentially may result in an invalid
 * configuration, all parameters are set at the same time.  A complete camera
 * configuration is stored in a @ref phnx_config structure.
 *
 * There are different possible operations with camera configuration:
 *
 * - **load** camera settings from a preset configuration;
 *
 * - **save** camera settings as a preset configuration;
 *
 * - **get** current configuration parameters;
 *
 * - **set** configuration parameters;
 *
 * - **update** configuration parameters.
 *
 * The *load* and *save* operations operate between the camera settings and a
 * given preset configuration stored in the ROM of the camera (or equivalent).
 * The *get* and *set* operations operate between the camera settings and a
 * user provided @ref phnx_config structure.  For efficiency reasons, the
 * camera configuration is memorized in the @ref tao_camera structure
 * associated with the camera and is synchronized with the hardware settings.
 * It may however happen that errors prevent this synchronization.  In that
 * case, the *update* operation can be applied to synchronize the memorized
 * configuration with the current hardware settings.
 *
 * Due to hardware restrictions, the device ROI may be different than the one
 * chosen.  The device ROI is set to encompass the chosen ROI under these
 * restrictions.  The device ROI does not need to be accessible to the end-user
 * and is stored in the `dev_roi` member of the @ref tao_camera structure
 * while the chosen ROI is stored in the `cfg.roi` member of this structure.
 * After checking the validitity of the user chosen ROI, if an error occurs
 * when setting one the ROI parameter, the `cfg.roi` is reset to the hardware
 * settings.
 *
 * @{
 */

/**
 * Load camera settings.
 */
extern tao_status phnx_load_configuration(
    tao_camera* cam,
    int id);

/**
 * Save camera settings.
 */
extern tao_status phnx_save_configuration(
    tao_camera* cam,
    int id);

/**
 * Retrieve camera settings.
 *
 * This function retrieves the current setings of a camera.  Acquisition may be
 * running but the caller must have locked the camera.  This operation cannot
 * fail.
 *
 * Typical usage:
 *
 * ~~~~~{.c}
 * phnx_config cfg;
 * phnx_lock(cam);
 * phnx_get_configuration(cam, &cfg);
 * tao_unlock_camera(cam);
 * ~~~~~
 * @param cam    Address of camera instance.
 * @param cfg    Pointer to configuration for copying parameters.
 *
 * @see tao_get_camera_configuration(), phnx_set_configuration().
 */
extern void phnx_get_configuration(
    tao_camera* cam,
    phnx_config* cfg);

/**
 * Set camera settings.
 *
 * This function changes some setings of a camera.  Acquisition must ne be
 * running and the caller must have locked the camera.
 *
 * Typical usage:
 *
 * ~~~~~{.c}
 * tao_status status;
 * phnx_config cfg;
 * phnx_lock(cam);
 * {
 *     // Retrieve current settings.
 *     phnx_get_configuration(cam, &cfg);
 *     // Change some settings.
 *     cfg.width = 300;
 *     cfg.height = 400;
 *     status = phnx_set_configuration(cam, &cfg);
 * }
 * tao_unlock_camera(cam);
 * if (status != TAO_OK) {
 *      // Some error occurred.
 *      ....
 * } else {
 *      // Operation was successful.
 *      ....
 * }
 * ~~~~~
 * @param cam    Address of camera instance.
 * @param cfg    Pointer to configuration to apply.
 *
 * @return @ref TAO_OK if the configuration is valid, @ref TAO_ERROR otherwise.
 *
 * @see tao_set_camera_configuration(), phnx_get_configuration().
 */
extern tao_status phnx_set_configuration(
    tao_camera* cam,
    const phnx_config* cfg);

/**
 * Check camera settings.
 *
 * This function checks the validity of the setings for a camera.  The caller
 * must have locked the camera.
 *
 * Typical usage:
 *
 * ~~~~~{.c}
 * tao_status status;
 * phnx_config cfg;
 * status = phnx_lock(cam);
 * if (status == TAO_OK) {
 *     // Retrieve current settings.
 *     phnx_get_configuration(cam, &cfg);
 *     // Change some settings.
 *     cfg.width = 300;
 *     cfg.height = 400;
 *     status = phnx_check_configuration(cam, &cfg);
 *     tao_unlock_camera(cam);
 * }
 * if (status != TAO_OK) {
 *      // Some error occurred.
 *      ....
 * } else {
 *      // Operation was successful.
 *      ....
 * }
 * ~~~~~
 *
 * @param cam    Address of camera instance.
 * @param cfg    Pointer to configuration to check.
 *
 * @return @ref TAO_OK if the configuration is valid, @ref TAO_ERROR otherwise.
 */
extern tao_status phnx_check_configuration(
    tao_camera* cam,
    const phnx_config* cfg);

/**
 * Update camera settings from the hardware.
 *
 * This function updates some or all parameters of a camera from the hardware.
 *
 * Typical usage:
 *
 * ~~~~~{.c}
 * tao_status status;
 * phnx_lock(cam);
 * status = phnx_update_configuration(cam, all);
 * tao_unlock_camera(cam);
 * if (status != 0) {
 *      // Some error occurred.
 *      ....
 * } else {
 *      // Operation was successful.
 *      ....
 * }
 * ~~~~~
 * @param cam    Address of camera instance.
 * @param all    If true, all parameters in @a cam are retrieved from the
 *               hardware; otherwise, only the parameters that can
 *               spontaneously change (like the temperature) are
 *               updated.
 *
 * @return @ref TAO_OK in case of success, @ref TAO_ERROR in case of failure.
 */
extern tao_status phnx_update_configuration(
    tao_camera* cam,
    bool all);

/**
 * Find a good encoding for acquisition buffers.
 *
 * @param enc  The encoding to approximate.
 *
 * @return Some encoding which can represent @b enc without loss and whose
 *         pixel size is a multiple of 8 bits.  If no match can be found, @ref
 *         TAO_ENCODING_UNKNOWN is returned.
 */
extern tao_encoding phnx_best_buffer_encoding(
    tao_encoding enc);

/**
 * @}
 */

//-----------------------------------------------------------------------------
// IMAGE ACQUISITION

/**
 * @defgroup PhoenixAcquisition  Phoenix acquisition
 *
 * @ingroup PhoenixCameras
 *
 * @brief Image acquisition with a Phoenix camera.
 *
 * These are low-level functions.  The high-level TAO API for cameras
 * should be sufficient for most usages.
 *
 * @{
 */

/**
 * Release some acquisition buffer of the Phoenix frame-grabber.
 *
 * This function releases pending acquisition buffers until there are at most a
 * given number.  This low-level function is supposed to work with the
 * acquisition callback and with phnx_get_buffer().  The number of pending
 * buffers of the camera is updated.
 *
 * @param dev    The address of a Phoenix camera device.
 * @param limit  The maximum number of pending buffers to keep.
 *               Must be nonnegative.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR on failure.
 */
extern tao_status phnx_release_buffers(
    phnx_device* dev,
    int limit);

/**
 * Retrieve the address of the first pending acquisition buffer of
 * Phoenix frame-grabber.
 *
 * This low-level function is supposed to work with the acquisition callback
 * and with phnx_release_buffers().
 *
 * @param dev    The address of a Phoenix camera device.
 *
 * @return The address of the first pending acquisition buffer, `NULL` on
 *         on failure.
 */
extern void* phnx_get_buffer(
    phnx_device* dev);

/**
 * @}
 */

/**
 * @addtogroup PhoenixProgram  Phoenix programming
 *
 * @ingroup PhoenixCameras
 *
 * @brief Tools for programs running Phoenix cameras.
 *
 * These routines are helpers to build a program that open and configure a
 * camera connected to Phoenix frame grabber.
 *
 * @{
 */

/**
 * Start a program using a camera connected to Phoenix frame grabber.
 *
 * This function parses command line options, open a camera connected to a
 * Phoenix frame grabber and configure it according to the command line
 * options.
 *
 * In the given option table, the addresses of variables associated with the @b
 * help, @b load and @b save options shall be `NULL` on entry.  They will be
 * set to the addresses of some static variables automatically initialized by
 * phnx_start_program().
 *
 * @param cam    Address to store pointer to open camera.
 * @param cfg    Address to of structure to store camera configuration.
 * @param argc   Number of command line arguments.
 * @param argv   Command line arguments.
 * @param opts   Table of options.
 * @param descr  A description of the program purpose.
 * @param args   Positionnal arguments (for the syntax line).
 *
 * @return The number of remaining arguments.
 */
extern int phnx_start_program(
    tao_camera** cam,
    phnx_config* cfg,
    int argc,
    char* argv[],
    tao_option opts[],
    const char* descr,
    const char* args);

/**
 * @}
 */

TAO_END_DECLS

#endif // TAO_PHOENIX_H_
