// phoenix-info.c -
//
// Simple program to show the configuration of a camera connected to an
// ActiveSilicon Phoenix frame grabber.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2018-2022, Éric Thiébaut.
// Copyright (C) 2016, Éric Thiébaut & Jonathan Léger.

#include "tao-phoenix-private.h"
#include "tao-errors.h"

#include <stdio.h>

int main(
    int argc,
    char** argv)
{
    tao_camera* cam;

    cam = phnx_create_camera(NULL, NULL, NULL);
    if (cam == NULL) {
        fprintf(stderr, "Failed to create the camera.\n");
        tao_report_error();
        return EXIT_FAILURE;
    }
    phnx_print_camera_info(cam, stdout);
    tao_camera_destroy(cam);
    return EXIT_SUCCESS;
}
