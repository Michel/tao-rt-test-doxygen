// tao-phoenix-private.h -
//
// Definitions for low level (private) interface to ActiveSilicon Phoenix frame
// grabber.
//
// The low level interface is intended for writing "drivers" for different
// camera models.  Functions, types and macros in the low level (private)
// interface are prefixed by `phx_`, `PHX_`, `cxp_` or `CXP_` (the 2 latters
// for CoaXPress related functionalities).
//
// The high level (public) interface is intending for writing applications.
// Functions, types and macros in the high level (public) interface are
// prefixed by `phnx_` or `PHNX_`.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2017-2022, Éric Thiébaut.
// Copyright (C) 2016, Éric Thiébaut & Jonathan Léger.

#ifndef TAO_PHOENIX_PRIVATE_H_
#define TAO_PHOENIX_PRIVATE_H_ 1

#include <tao-cameras-private.h>
#include <tao-phoenix.h>
#include <tao-coaxpress.h>

#include <phx_api.h>

TAO_BEGIN_DECLS

typedef float  float32_t;
typedef double float64_t;


/**
 * @defgroup PhoenixPrivate  Phoenix private API
 *
 * @ingroup PhoenixCameras
 *
 * @brief Low level (private) API for ActiveSilicon *Phoenix* frame-grabbers.
 *
 * The low level interface is intended for writing "drivers" for different
 * camera models.  Functions, types and macros in the low level (private)
 * interface are prefixed by `phx_`, `PHX_`, `cxp_` or `CXP_` (the 2 latters
 * for CoaXPress related functionalities).
 *
 * None of the functions in the low level interface attempt lock/unlock the
 * camera, it is the caller's responsibility to do it as needed.
 *
 * @{
 */

/**
 * Device structure for "Phoenix" cameras.
 *
 * This structure is exposed for convenience but should be considered as
 * read-only by the user.
 *
 * The encoding of acquisition buffers and number of acquisition buffers
 * (members `bufferencoding` and `nbufs` of the `phnx_config` structure) are
 * directly managed by the high level functions phnx_check_configuration(),
 * phnx_set_configuration() and phnx_get_configuration() and need not be
 * considered by configuration callbacks.
 */
struct phnx_device_ {
    // The "Phoenix" camera class extends the TAO camera class.
    tao_camera            base;///< Unified TAO camera.

    // Parameters needed by all "Phoenix" cameras.
    const phnx_operations* ops;///< Secondary table of virtual methods.
    uint64_t            handle;///< Camera handle.
    tao_time       frame_start;///< Start time of next frame.
    tao_time         frame_end;///< End time of next frame.
    tao_camera_roi         roi;///< ROI for the device.
    phnx_extra_config    extra;///< Configurable parameters specific to
                               ///< "Phoenix" cameras.

    // Members for CoaXPress cameras.
    bool             coaxpress;///< Camera has CoaXPress connection.
    bool                  swap;///< Byteswapping needed for CoaXPress.
                               ///< connection.
    uint32_t           timeout;///< CoaXPress connection timeout
                               ///< (milliseconds).
    char                vendor[48];
    char                 model[48];
};

//-----------------------------------------------------------------------------
// SUPPORTED CAMERAS

/**
 * @defgroup PhoenixSupportedCameras Phoenix supported cameras
 *
 * @ingroup PhoenixPrivate
 *
 * @brief Cameras supported by the TAO-Phoenix API.
 *
 * Supported cameras are checked by `phnx_check_MODEL()` and initialized
 * by `phnx_initialize_MODEL()` where `MODEL` is the model of the cameras.
 *
 * @{
 */

/**
 * Attempt to detect a CoaXPress camera.
 *
 * @param dev   Reference to a structure describing a freshly connected camera.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of errors.
 *
 * @see phx_create().
 */
extern tao_status phx_detect_coaxpress(
    phnx_device* dev);

/**
 * Define frame-grabber parameters to match CoaXPress connection settings.
 */
extern tao_status phx_define_coaxpress_connection(
    phnx_device* dev,
    const phnx_connection* con);

/**
 * @}
 */

/**
 * @defgroup PhoenixPrivateErrors Phoenix errors
 *
 * @ingroup PhoenixPrivate
 *
 * @brief Management of errors for ActiveSilicon *Phoenix* frame-grabbers.
 *
 * **FIXME:** At low level, tao_store_error() is called to update the last
 * error of the calling thread with a given frame-grabber error code.  Errors
 * occuring in functions of the ActiveSilicon Phoenix library may also
 * immediately print some error message.  The printing of such messages is
 * controlled by phx_set_error_handler_verbosity() and the current settings are
 * given by calling phx_get_error_handler_verbosity().
 *
 * @{
 */

//-----------------------------------------------------------------------------
// GET ERROR INFORMATION

/**
 * Get textual identifier of status code.
 *
 * @param status    Status returned by one of the functions of the
 *                  ActiveSilicon Phoenix library.
 *
 * @return The address of a static string with the textual identifier of
 *         the status.
 */
extern const char* phx_status_identifier(
    etStat status);

/**
 * Get textual description of status code.
 *
 * @param status    Status returned by one of the functions of the
 *                  ActiveSilicon Phoenix library.
 *
 * @return The address of a static string describing the status code.
 */
const char* phx_status_description(
    etStat status);

/**
 * Set the level of verbosity of the default error handler.
 *
 * @param level   The level of verbosity: < 1 to print no messages, 1 to print
 *                brief messages, > 1 to print detailed messages.
 *
 * @return The previous value of the verbosity level; -1 in case of errors.
 */
extern int phx_set_error_handler_verbosity(
    int level);

 /**
 * Get the level of verbosity of the default error handler.
 *
 * @return The current verbosity level for error messages printed by the
 *         default error handler; -1 in case of errors.
 */
extern int phx_get_error_handler_verbosity(
    void);

/**
 * Push a new Phoenix error.
 *
 * @param func   Name of function where error occurred.
 * @param code   Error code, one of the `etStat` constants: `PHX_ERROR_*`.
 */
extern void phx_push_error(
    const char* func,
    int code);

/**
 * Portotype of error handler.
 */
typedef void phx_error_handler(
    const char* funcname,
    etStat errcode,
    const char* reason);

/**
 * Default error handler.
 *
 * @param funcname   Name of function where error occurred.
 * @param errcode    Error code, one of the `etStat` constants: `PHX_ERROR_*`.
 * @param reason     Error message.
 */
extern void phx_default_error_handler(
    const char* funcname,
    etStat errcode,
    const char* reason);

/**
 * @}
 */

//-----------------------------------------------------------------------------
// GET/SET PHOENIX FRAME-GRABBER PARAMETERS

/**
 * @defgroup PhoenixFrameGrabberParameters Phoenix parameters
 *
 * @ingroup PhoenixPrivate
 *
 * @brief Management of parameters for ActiveSilicon *Phoenix* frame-grabbers.
 *
 * These functions are to get or set ActiveSilicon *Phoenix* parameters.
 *
 * @{
 */

/**
 * Execute a frame-grabber command.
 */
extern tao_status phx_read_stream(
    phnx_device* dev,
    etAcq command,
    void* addr);

/**
 * Query a frame-grabber parameter.
 */
extern tao_status phx_get_parameter(
    phnx_device* dev,
    etParam param,
    void* addr);

/**
 * Set a frame-grabber parameter.
 */
extern tao_status phx_set_parameter(
    phnx_device* dev,
    etParam param,
    void* addr);

/**
 * Query the value of a frame-grabber parameter.
 */
extern tao_status phx_get(
    phnx_device* dev,
    etParam param,
    etParamValue* valptr);

/**
 * Set the value of a frame-grabber parameter.
 */
extern tao_status phx_set(
    phnx_device* dev,
    etParam param,
    etParamValue value);

/**
 * @}
 */

//-----------------------------------------------------------------------------
// PIXEL ENCODING

/**
 * @defgroup PhoenixPixelEncoding Phoenix encodings
 *
 * @ingroup PhoenixPrivate
 *
 * @brief Pixel encodings for ActiveSilicon *Phoenix* frame-grabbers.
 *
 * @{
 */

/**
 * Set frame-grabber parameters corresponding to the given encoding
 * for the pixels sent by the camera.
 *
 * This function sets the frame-grabber parameters `PHX_CAM_SRC_DEPTH` and
 * `PHX_CAM_SRC_COL` to match the camera pixel encoding given by @a enc.
 *
 * @param dev   The camera.
 * @param enc   The encoding of pixels sent by the camera.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR on error.
 */
extern tao_status phx_define_camera_encoding(
    phnx_device* dev,
    tao_encoding enc);

/**
 * Set frame-grabber parameter for pixel encoding in acquisition buffers.
 *
 * This function sets the frame-grabber parameter `PHX_DST_FORMAT` to match the
 * encoding of pixels in acquisition buffers given by @a enc.
 *
 * @param dev   The camera.
 * @param enc   The encoding of pixels in acquisition buffers.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR on error.
  */
extern tao_status phx_define_buffer_encoding(
    phnx_device* dev,
    tao_encoding enc);

/**
 * Get camera pixel encoding from frame-grabber parameters.
 *
 * @param col    The value of `PHX_CAM_SRC_COL`, that is the color
 *               type of the pixels sent by the camera.
 * @param depth  The value of `PHX_CAM_SRC_DEPTH`, that is the number
 *               of bits per pixel sent by the camera.
 */
extern tao_encoding phx_camsrc_to_encoding(
    etParamValue col,
    int depth);

/**
 * Get acquisition buffers pixel encoding from frame-grabber parameter.
 *
 * @param dstformat   The value of `PHX_DST_FORMAT`.
 */
extern tao_encoding phx_dstformat_to_encoding(
    etParamValue dstformat);

/**
 * @}
 */

//-----------------------------------------------------------------------------
// READ/WRITE COAXPRESS REGISTERS

/**
 * @defgroup PhoenixCoaXPressRegisters CoaXPress registers
 *
 * @ingroup PhoenixPrivate
 *
 * @brief Low level API for CoaXPress registers.
 *
 * These functions are to read or write CoaXPress registers for ActiveSilicon
 * *Phoenix* frame-grabbers.
 *
 * @{
 */

/**
 * Read data from a CoaXPress register.
 */
extern tao_status cxp_read(
    phnx_device* dev,
    uint32_t addr,
    uint8_t* data,
    uint32_t* size);

/**
 * Write data to a CoaXPress register.
 */
extern tao_status cxp_write(
    phnx_device* dev,
    uint32_t addr,
    uint8_t* data,
    uint32_t* size);

/**
 * Reset a CoaXPress register.
 *
 * @warning This functionality is not described in the ActiveSilicon manuals.
 */
extern tao_status cxp_reset(
    phnx_device* dev, uint32_t addr);

/**
 * Read an unsigned 32-bit integer from a CoaXPress register.
 */
extern tao_status cxp_read_uint32(
    phnx_device* dev,
    uint32_t addr,
    uint32_t* value);

/**
 * Read an unsigned 64-bit integer from a CoaXPress register.
 */
extern tao_status cxp_read_uint64(
    phnx_device* dev,
    uint32_t addr, uint64_t* value);

/**
 * Read a 32-bit floating-point value from a CoaXPress register.
 */
extern tao_status cxp_read_float32(
    phnx_device* dev,
    uint32_t addr,
    float32_t* value);

/**
 * Read a 64-bit floating-point value from a CoaXPress register.
 */
extern tao_status cxp_read_float64(
    phnx_device* dev,
    uint32_t addr,
    float64_t* value);

/**
 * Read a string from a CoaXPress register.
 */
extern tao_status cxp_read_string(
    phnx_device* dev,
    uint32_t addr,
    uint32_t len,
    char* buf);

/**
 * Indirect reading of an unsigned 32-bit integer from a CoaXPress register.
 */
extern tao_status cxp_read_indirect_uint32(
    phnx_device* dev,
    uint32_t addr,
    uint32_t* value);

/**
 * Write an unsigned 32-bit integer to a CoaXPress register.
 */
extern tao_status cxp_write_uint32(
    phnx_device* dev,
    uint32_t addr,
    uint32_t value);

/**
 * Write an unsigned 64-bit integer to a CoaXPress register.
 */
extern tao_status cxp_write_uint64(
    phnx_device* dev,
    uint32_t addr,
    uint64_t value);

/**
 * Write a 32-bit floating-point value to a CoaXPress register.
 */
extern tao_status cxp_write_float32(
    phnx_device* dev,
    uint32_t addr,
    float32_t value);

/**
 * Write a 64-bit floating-point value to a CoaXPress register.
 */
extern tao_status cxp_write_float64(
    phnx_device* dev,
    uint32_t addr,
    float64_t value);

/**
 * @}
 */

//-----------------------------------------------------------------------------
// UTILITIES

/**
 * @defgroup PhoenixPrivateTools  Phoenix tools
 *
 * @ingroup PhoenixPrivate
 *
 * @brief Tools for ActiveSilicon *Phoenix* frame-grabbers.
 *
 * @{
 */

/**
 * Initialize cross-platform keyboard input routines.
 */
extern void phx_keyboard_init(
    void);

/**
 * Cross-platform routine to check whether keyboard hit occurred.
 */
extern bool phx_keyboard_hit(
    void);

/**
 * Finalize cross-platform keyboard input routines.
 */
extern void phx_keyboard_final(
    void);

/**
 * Read a character from the keyboard.
 */
extern int phx_keyboard_read(
    void);

/**
 * @}
 */

/**
 * @}
 */

TAO_END_DECLS

#endif // TAO_PHOENIX_PRIVATE_H_
