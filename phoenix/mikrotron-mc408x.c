// mikrotron-mc408x.c -
//
// Routines for Mikrotron MC408x cameras.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2017-2022, Éric Thiébaut.
// Copyright (C) 2016, Éric Thiébaut & Jonathan Léger.

#include "tao-errors.h"
#include "tao-phoenix-private.h"
#include "tao-coaxpress.h"
#include "tao-mikrotron-mc408x.h"

#define USE_FRAMEGRABBER_FOR_SETTING_CONNECTION 1

// Management of camera settings
// =============================
//
// Since camera imposes that its configuration be always correct, certain
// parameters have to be considered as a whole.
//
// For each single parameter or group of parameters, there are two elementary
// operations:
//
// - Updating: the parameters are read from the camera and reflected in the
//   structure describing the camera.
//
// - Setting: the parameters are set for the camera and, if the operation is
//   successful, reflected in the structure describing the camera.
//
// An additional "check" operation is provided which yields whether the
// settings are valid.
//
// Note: In case of error while changing parameters, it may be safer to
// update the parameters.
//
// When changing the configuration, we attempt to only change the settings that
// need to be changed and in an order such as to avoid clashes. The strategy
// is:
//
// 1. reduce bits per pixel if requested;
// 2. reduce frame rate if requested;
// 3. reduce exposure time if requested;
// 4. change ROI if requested;
// 5. augment exposure time if requested;
// 6. augment frame rate if requested;
// 7. augment bits per pixel if requested;

static tao_status update_black_level(
    phnx_device* dev);

static tao_status check_black_level(
    phnx_device* dev,
    double arg);

static tao_status set_black_level(
    phnx_device* dev,
    double arg);

static tao_status update_gain(
    phnx_device* dev);

static tao_status check_gain(
    phnx_device* dev,
    double arg);

static tao_status set_gain(
    phnx_device* dev,
    double arg);

static tao_status update_exposure_time(
    phnx_device* dev);

static tao_status check_exposure_time(
    phnx_device* dev,
    double arg);

static tao_status set_exposure_time(
    phnx_device* dev,
    double arg);

static tao_status update_frame_rate(
    phnx_device* dev);

static tao_status check_frame_rate(
    phnx_device* dev,
    double arg);

static tao_status set_frame_rate(
    phnx_device* dev,
    double arg);

static tao_status update_region_of_interest(
    phnx_device* dev);

static tao_status check_region_of_interest(
    phnx_device* dev,
    const tao_camera_roi* arg);

static tao_status set_region_of_interest(
    phnx_device* dev,
    const tao_camera_roi* arg);

static tao_status update_connection_settings(
    phnx_device* dev);

static tao_status check_connection_settings(
    phnx_device* dev,
    const phnx_connection* arg);

static tao_status set_connection_settings(
    phnx_device* dev,
    const phnx_connection* arg);

static tao_status update_camera_encoding(
    phnx_device* dev);

static tao_status check_camera_encoding(
    phnx_device* dev,
    const tao_encoding arg);

static tao_status set_camera_encoding(
    phnx_device* dev,
    const tao_encoding arg);

static bool is_monochrome(
    phnx_device* dev);
static bool is_color(
    phnx_device* dev);

static tao_status update_temperature(
    phnx_device* dev);

static tao_status set_pattern_noise_reduction(
    phnx_device* dev,
    bool flag);

static tao_status set_filter_mode(
    phnx_device* dev,
    uint32_t mode);

static tao_status set_pixel_pulse_reset(
    phnx_device* dev,
    bool flag);

static tao_status update_configuration(
    phnx_device* dev,
    bool all);

static tao_status check_configuration(
    phnx_device* dev,
    const phnx_config* cfg);

static tao_status set_configuration(
    phnx_device* dev,
    const phnx_config* cfg);

static tao_status load_configuration(
    phnx_device* dev,
    int id);

static tao_status save_configuration(
    phnx_device* dev,
    int id);

static tao_status start(
    phnx_device* dev);

static tao_status stop(
    phnx_device* dev);


// Update camera configuration according to current device settings.
static tao_status update_configuration(
    phnx_device* dev,
    bool all)
{
    if (all) {
        if (update_black_level(        dev) != TAO_OK ||
            update_gain(               dev) != TAO_OK ||
            update_exposure_time(      dev) != TAO_OK ||
            update_frame_rate(         dev) != TAO_OK ||
            update_region_of_interest( dev) != TAO_OK ||
            update_connection_settings(dev) != TAO_OK ||
            update_camera_encoding(    dev) != TAO_OK) {
            return TAO_ERROR;
        }
    }
    if (update_temperature(dev) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

static tao_status check_configuration(
    phnx_device* dev,
    const phnx_config* cfg)
{
    if (check_black_level(        dev,  cfg->extra.bias         ) != TAO_OK ||
        check_gain(               dev,  cfg->extra.gain         ) != TAO_OK ||
        check_exposure_time(      dev,  cfg->base.exposuretime  ) != TAO_OK ||
        check_frame_rate(         dev,  cfg->base.framerate     ) != TAO_OK ||
        check_region_of_interest( dev, &cfg->base.roi           ) != TAO_OK ||
        check_connection_settings(dev, &cfg->extra.connection   ) != TAO_OK ||
        check_camera_encoding(    dev,  cfg->base.sensorencoding) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

static tao_status set_configuration(
    phnx_device* dev,
    const phnx_config* cfg)
{
    // Check configuration parameters.
    if (check_configuration(dev, cfg) != TAO_OK) {
        return TAO_ERROR;
    }

    // Change black level and gain if requested.
    if (cfg->extra.bias != dev->extra.bias) {
        if (set_black_level(dev, cfg->extra.bias) != TAO_OK) {
            return TAO_ERROR;
        }
    }
    if (cfg->extra.gain != dev->extra.gain) {
        if (set_gain(dev, cfg->extra.gain) != TAO_OK) {
            return TAO_ERROR;
        }
    }

    // Augment connection speed if requested.
    if (cfg->extra.connection.channels*cfg->extra.connection.speed >
        dev->extra.connection.channels*dev->extra.connection.speed) {
        if (set_connection_settings(dev, &cfg->extra.connection) != TAO_OK) {
            return TAO_ERROR;
        }
    }

    // Reduce bits per pixel if requested.
    if (TAO_ENCODING_BITS_PER_PIXEL(cfg->base.sensorencoding) <
        TAO_ENCODING_BITS_PER_PIXEL(dev->base.info.config.sensorencoding)) {
        if (set_camera_encoding(dev, cfg->base.sensorencoding) != TAO_OK) {
            return TAO_ERROR;
        }
    }

    // Before reducing frame rate, reduce exposure time if requested.
    if (cfg->base.exposuretime < dev->base.info.config.exposuretime) {
        if (set_exposure_time(dev, cfg->base.exposuretime) != TAO_OK) {
            return TAO_ERROR;
        }
    }

    // Reduce frame rate if requested.
    if (cfg->base.framerate < dev->base.info.config.framerate) {
        if (set_frame_rate(dev, cfg->base.framerate) != TAO_OK) {
            return TAO_ERROR;
        }
    }

    // Change the ROI if it has to change.
    if (set_region_of_interest(dev, &cfg->base.roi) != TAO_OK) {
        return TAO_ERROR;
    }

    // Augment exposure time if requested.
    if (cfg->base.exposuretime > dev->base.info.config.exposuretime) {
        if (set_exposure_time(dev, cfg->base.exposuretime) != TAO_OK) {
            return TAO_ERROR;
        }
    }

    // Augment frame rate if requested.
    if (cfg->base.framerate > dev->base.info.config.framerate) {
        if (set_frame_rate(dev, cfg->base.framerate) != TAO_OK) {
            return TAO_ERROR;
        }
    }

    // Change encoding of camera pixels.
    if (cfg->base.sensorencoding != dev->base.info.config.sensorencoding) {
        if (set_camera_encoding(dev, cfg->base.sensorencoding) != TAO_OK) {
            return TAO_ERROR;
        }
    }

    // Reduce connection speed if requested.
    if (cfg->extra.connection.channels != dev->extra.connection.channels ||
        cfg->extra.connection.speed  != dev->extra.connection.speed) {
        if (set_connection_settings(dev, &cfg->extra.connection) != TAO_OK) {
            return TAO_ERROR;
        }
    }

    return TAO_OK;
}

static uint32_t get_configuration_number(
    int id)
{
    switch (id) {
    case 0: return CXP_USER_SET_SELECTOR_DEFAULT;
    case 1: return CXP_USER_SET_SELECTOR_USER_SET_1;
    case 2: return CXP_USER_SET_SELECTOR_USER_SET_2;
    case 3: return CXP_USER_SET_SELECTOR_USER_SET_3;
    default: return ~(uint32_t)0;
    }
}

// Load one of the configurations memorized by the camera.
static tao_status load_configuration(
    phnx_device* dev,
    int id)
{
    uint32_t sel = get_configuration_number(id);
    if (sel == ~(uint32_t)0) {
        tao_store_error(__func__, TAO_BAD_ARGUMENT);
        return TAO_ERROR;
    }
    if (cxp_set(dev, USER_SET_DEFAULT_SELECTOR, sel) != TAO_OK ||
        cxp_exec(dev, USER_SET_LOAD) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

// Save current camera settings. My understanding is that the default
// configuration (factory settings) cannot be overwritten.
static tao_status save_configuration(
    phnx_device* dev,
    int id)
{
    uint32_t sel = get_configuration_number(id);
    if (sel == ~(uint32_t)0 || sel == CXP_USER_SET_SELECTOR_DEFAULT) {
        tao_store_error(__func__, TAO_BAD_ARGUMENT);
        return TAO_ERROR;
    }
    if (cxp_set(dev, USER_SET_DEFAULT_SELECTOR, sel) != TAO_OK ||
        cxp_exec(dev, USER_SET_SAVE) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

#define CHECK_DOUBLE(dev, arg, min, max, err)                   \
    do {                                                        \
        double _arg = (arg), _min = (min), _max = (max);        \
        if (isfinite(_arg) && _min < _arg && _arg < _max) {     \
            return TAO_OK;                                      \
        }                                                       \
        tao_store_error(__func__, (err));                       \
        return TAO_ERROR;                                       \
    } while (false)

// Get/check/set detector bias (black level).
static tao_status update_black_level(
    phnx_device* dev)
{
    uint32_t val;
    if (cxp_get(dev, BLACK_LEVEL, &val) != TAO_OK) {
        return TAO_ERROR;
    }
    dev->extra.bias = (double)val;
    return TAO_OK;
}

static tao_status check_black_level(
    phnx_device* dev,
    double arg)
{
    CHECK_DOUBLE(dev, arg,
                 cxp_min(BLACK_LEVEL) - 0.5,
                 cxp_max(BLACK_LEVEL) + 0.5,
                 TAO_BAD_BIAS);
}

static tao_status set_black_level(
    phnx_device* dev,
    double arg)
{
    if (check_black_level(dev, arg) == TAO_OK) {
        uint32_t val = lround(arg);
        if (cxp_set(dev, BLACK_LEVEL, val) == TAO_OK) {
            dev->extra.bias = (double)val;
            return TAO_OK;
        }
    }
    return TAO_ERROR;
}


// Get/check/set detector gain.

static tao_status update_gain(
    phnx_device* dev)
{
    uint32_t val;
    if (cxp_get(dev, GAIN, &val) != TAO_OK) {
        return TAO_ERROR;
    }
    dev->extra.gain = (double)val;
    return TAO_OK;
}

static tao_status check_gain(
    phnx_device* dev,
    double arg)
{
    CHECK_DOUBLE(dev, arg,
                 cxp_min(GAIN) - 0.5,
                 cxp_max(GAIN) + 0.5,
                 TAO_BAD_GAIN);
}

static tao_status set_gain(
    phnx_device* dev, double arg)
{
    if (check_gain(dev, arg) == TAO_OK) {
        uint32_t val = lround(arg);
        if (cxp_set(dev, GAIN, val) == TAO_OK) {
            dev->extra.gain = (double)val;
            return TAO_OK;
        }
    }
    return TAO_ERROR;
}


// Set/get exposure time (device value is in µs).

static tao_status update_exposure_time(
    phnx_device* dev)
{
    uint32_t val;
    if (cxp_get(dev, EXPOSURE_TIME, &val) != TAO_OK) {
        return TAO_ERROR;
    }
    dev->base.info.config.exposuretime = 1e-6*val;
    return TAO_OK;
}

static tao_status check_exposure_time(
    phnx_device* dev,
    double arg)
{
    CHECK_DOUBLE(dev, arg,
                 1e-6*(cxp_min(EXPOSURE_TIME) - 0.5),
                 1e-6*(cxp_max(EXPOSURE_TIME) + 0.5),
                 TAO_BAD_EXPOSURETIME);
}

static tao_status set_exposure_time(
    phnx_device* dev,
    double arg)
{
    if (check_exposure_time(dev, arg) == TAO_OK) {
        uint32_t val = lround(arg*1e6);
        if (cxp_set(dev, EXPOSURE_TIME, val) == TAO_OK) {
            dev->base.info.config.exposuretime = 1e-6*(double)val;
            return TAO_OK;
        }
    }
    return TAO_ERROR;
}


// Get/check/set acquisition frame rate (device value is in Hz).

static tao_status update_frame_rate(
    phnx_device* dev)
{
    uint32_t val;
    if (cxp_get(dev, ACQUISITION_FRAME_RATE, &val) != TAO_OK) {
        return TAO_ERROR;
    }
    dev->base.info.config.framerate = (double)val;
    return TAO_OK;
}

static tao_status check_frame_rate(
    phnx_device* dev,
    double arg)
{
    CHECK_DOUBLE(dev, arg,
                 cxp_min(ACQUISITION_FRAME_RATE) - 0.5,
                 cxp_max(ACQUISITION_FRAME_RATE) + 0.5,
                 TAO_BAD_FRAMERATE);
}

static tao_status set_frame_rate(
    phnx_device* dev,
    double arg)
{
    if (check_frame_rate(dev, arg) == TAO_OK) {
        uint32_t val = lround(arg);
        if (cxp_set(dev, ACQUISITION_FRAME_RATE, val) == TAO_OK) {
            dev->base.info.config.framerate = (double)val;
            return TAO_OK;
        }
    }
    return TAO_ERROR;
}


// Get/check/set region of interest.

static tao_status update_region_of_interest(
    phnx_device* dev)
{
    uint32_t xbin, ybin, xoff, yoff, width, height;

    if (cxp_get(dev, DECIMATION_HORIZONTAL, &xbin) != TAO_OK ||
        cxp_get(dev, DECIMATION_VERTICAL,   &ybin) != TAO_OK ||
        cxp_get(dev, OFFSET_X,              &xoff) != TAO_OK ||
        cxp_get(dev, OFFSET_Y,              &yoff) != TAO_OK ||
        cxp_get(dev, WIDTH,                &width) != TAO_OK ||
        cxp_get(dev, HEIGHT,              &height) != TAO_OK) {
        return TAO_ERROR;
    }
    tao_camera_roi_define(&dev->roi, xbin, ybin,
                          xoff, yoff, width, height);
    dev->base.info.config.roi = dev->roi;
    return TAO_OK;
}

static tao_status check_region_of_interest(
    phnx_device* dev,
    const tao_camera_roi* arg)
{
    if (tao_camera_roi_check(arg,
                             dev->base.info.sensorwidth,
                             dev->base.info.sensorheight) != TAO_OK) {
        tao_store_error(__func__, TAO_BAD_ROI);
        return TAO_ERROR;
    }
    return TAO_OK;
}

static tao_status set_region_of_interest(
    phnx_device* dev,
    const tao_camera_roi* arg)
{
    // Check input ROI.
    if (check_region_of_interest(dev, arg) != TAO_OK) {
        return TAO_ERROR;
    }
    if (arg->xbin != 1 || arg->xbin != 1) {
        // FIXME: Rebinning is not yet supported.
        tao_store_error(__func__, TAO_BAD_ROI);
        return TAO_ERROR;
    }
    // Compute the ROI to set for the device so as to encompass the requested
    // ROI and account for hardware constraints.
    long xbin   = 1;
    long ybin   = 1;
    long xoff   = TAO_ROUND_DOWN(arg->xoff, CXP_HORIZONTAL_INCREMENT);
    long yoff   = TAO_ROUND_DOWN(arg->yoff, CXP_VERTICAL_INCREMENT);
    long width  = TAO_ROUND_UP(arg->xoff + arg->width,
                               CXP_HORIZONTAL_INCREMENT) - xoff;
    long height = TAO_ROUND_UP(arg->yoff + arg->height,
                               CXP_VERTICAL_INCREMENT) - yoff;
#define CFG_SET(m, id)                          \
    do {                                        \
        if (cxp_set(dev, id, m) != TAO_OK) {    \
            goto error;                         \
        } else {                                \
            dev->roi.m = m;                     \
        }                                       \
    } while (false)
    if (xbin != dev->roi.xbin) {
        CFG_SET(xbin, DECIMATION_HORIZONTAL);
    }
    if (ybin != dev->roi.ybin) {
        CFG_SET(ybin, DECIMATION_VERTICAL);
    }
    if (xoff < dev->roi.xoff) {
        CFG_SET(xoff, OFFSET_X);
    }
    if (width != dev->roi.width) {
        CFG_SET(width, WIDTH);
    }
    if (xoff > dev->roi.xoff) {
        CFG_SET(xoff, OFFSET_X);
    }
    if (yoff < dev->roi.yoff) {
        CFG_SET(yoff, OFFSET_Y);
    }
    if (height != dev->roi.height) {
        CFG_SET(height, HEIGHT);
    }
    if (yoff > dev->roi.yoff) {
        CFG_SET(yoff, OFFSET_Y);
    }
#undef CFG_SET

    // All settings were successful, update camera ROI to chosen parameters.
    dev->base.info.config.roi = *arg;
    return TAO_OK;

 error:
    // In case of failure, set camera ROI to current device settings.
    dev->base.info.config.roi = dev->roi;
    return TAO_ERROR;
}


// Set/check/get of pixels sent by the camera.

static tao_status update_camera_encoding(
    phnx_device* dev)
{
    uint32_t val;
    tao_encoding enc = TAO_ENCODING_UNKNOWN;
    if (cxp_get(dev, PIXEL_FORMAT, &val) == TAO_OK) {
        switch (val) {
        case CXP_PIXEL_FORMAT_MONO8:
            enc = TAO_ENCODING_MONO(8);
            break;

        case CXP_PIXEL_FORMAT_MONO10:
            enc = TAO_ENCODING_MONO(10);
            break;

        case CXP_PIXEL_FORMAT_BAYERGR8:
            enc = TAO_ENCODING_BAYER_GRBG(8);
            break;

        case CXP_PIXEL_FORMAT_BAYERGR10:
            enc = TAO_ENCODING_BAYER_GRBG(10);
            break;

        default:
            tao_store_error(__func__, TAO_BAD_ENCODING);
        }
    }
    dev->base.info.config.sensorencoding = enc;
    return (enc == TAO_ENCODING_UNKNOWN) ? TAO_ERROR : TAO_OK;
}

static tao_status check_camera_encoding(
    phnx_device* dev,
    tao_encoding enc)
{
    bool success;

    switch (enc) {
    case TAO_ENCODING_MONO(8):
    case TAO_ENCODING_MONO(10):
        success = is_monochrome(dev);
        break;

    case TAO_ENCODING_BAYER_GRBG(8):
    case TAO_ENCODING_BAYER_GRBG(10):
        success = is_color(dev);
        break;

    default:
        success = false;
    }
    if (! success) {
        tao_store_error(__func__, TAO_BAD_ENCODING);
        return TAO_ERROR;
    }
    return TAO_OK;
}

static tao_status set_camera_encoding(
    phnx_device* dev,
    tao_encoding enc)
{
    uint32_t cur, req;

    if (check_camera_encoding(dev, enc) != TAO_OK) {
        return TAO_ERROR;
    }
    switch (enc) {
    case TAO_ENCODING_MONO(8):
        req = CXP_PIXEL_FORMAT_MONO8;
        break;

    case TAO_ENCODING_MONO(10):
        req = CXP_PIXEL_FORMAT_MONO10;
        break;

    case TAO_ENCODING_BAYER_GRBG(8):
        req = CXP_PIXEL_FORMAT_BAYERGR8;
        break;

    case TAO_ENCODING_BAYER_GRBG(10):
        req = CXP_PIXEL_FORMAT_BAYERGR10;
        break;

    default:
        // This is unexpected...
        tao_store_error(__func__, TAO_ASSERTION_FAILED);
        return TAO_ERROR;
    }

    if (cxp_get(dev, PIXEL_FORMAT, &cur) != TAO_OK ||
        (cur != req && cxp_set(dev, PIXEL_FORMAT, req) != TAO_OK)) {
        return TAO_ERROR;
    }
    dev->base.info.config.sensorencoding = enc;
    return TAO_OK;
}

static bool is_monochrome(
    phnx_device* dev)
{
    tao_encoding enc = dev->base.info.config.sensorencoding;
    return (enc == TAO_ENCODING_MONO(8) ||
            enc == TAO_ENCODING_MONO(10));
}

static bool is_color(
    phnx_device* dev)
{
    tao_encoding enc = dev->base.info.config.sensorencoding;
    return (enc == TAO_ENCODING_BAYER_GRBG(8) ||
            enc == TAO_ENCODING_BAYER_GRBG(10));
}


// Get/check/set connection parameters.

static tao_status update_connection_settings(
    phnx_device* dev)
{
    phnx_connection* conn = &dev->extra.connection;
    const uint32_t msk = (CXP_CONNECTION_CONFIG_CONNECTION_1 |
                          CXP_CONNECTION_CONFIG_CONNECTION_2 |
                          CXP_CONNECTION_CONFIG_CONNECTION_3 |
                          CXP_CONNECTION_CONFIG_CONNECTION_4);
    uint32_t val;
    if (cxp_get(dev, CONNECTION_CONFIG, &val) != TAO_OK) {
        return TAO_ERROR;
    }
    switch (val & msk) {
    case CXP_CONNECTION_CONFIG_CONNECTION_1:
        conn->channels = 1;
        break;
    case CXP_CONNECTION_CONFIG_CONNECTION_2:
        conn->channels = 2;
        break;
    case CXP_CONNECTION_CONFIG_CONNECTION_3:
        conn->channels = 3;
        break;
    case CXP_CONNECTION_CONFIG_CONNECTION_4:
        conn->channels = 4;
        break;
    default:
        tao_store_error(__func__, TAO_ASSERTION_FAILED);
        return TAO_ERROR;
    }
    switch (val & ~msk) {
    case CXP_CONNECTION_CONFIG_SPEED_1250:
        conn->speed = 1250;
        break;
    case CXP_CONNECTION_CONFIG_SPEED_2500:
        conn->speed = 2500;
        break;
    case CXP_CONNECTION_CONFIG_SPEED_3125:
        conn->speed = 3125;
        break;
    case CXP_CONNECTION_CONFIG_SPEED_5000:
        conn->speed = 5000;
        break;
    case CXP_CONNECTION_CONFIG_SPEED_6250:
        conn->speed = 6250;
        break;
    default:
        tao_store_error(__func__, TAO_ASSERTION_FAILED);
        return TAO_ERROR;
    }
    return TAO_OK;
}

static tao_status check_connection_settings(
    phnx_device* dev,
    const phnx_connection* conn)
{
    if (conn->channels < 1 || conn->channels > 4 ||
        conn->speed < 1250 || conn->speed > 6250) {
        tao_store_error(__func__, TAO_BAD_SPEED);
        return TAO_ERROR;
    }
    return TAO_OK;
}

static tao_status set_connection_settings(
    phnx_device* dev,
    const phnx_connection* conn)
{
    if (check_connection_settings(dev, conn) != TAO_OK) {
        return TAO_ERROR;
    }

#if USE_FRAMEGRABBER_FOR_SETTING_CONNECTION
    if (phx_define_coaxpress_connection(dev, conn) != TAO_OK) {
        return TAO_ERROR;
    }
    if (update_connection_settings(dev) != TAO_OK) {
        return TAO_ERROR;
    }
    if (conn->channels != 0 &&
        conn->channels != dev->extra.connection.channels) {
        tao_store_error(__func__, TAO_BAD_CHANNELS);
        return TAO_ERROR;
    }
    if (conn->speed != 0 &&
        conn->speed != dev->extra.connection.speed) {
        tao_store_error(__func__, TAO_BAD_SPEED);
        return TAO_ERROR;
    }
#else
    uint32_t val;
    switch (conn->channels) {
    case 1:
        val = CXP_CONNECTION_CONFIG_CONNECTION_1;
        break;
    case 2:
        val = CXP_CONNECTION_CONFIG_CONNECTION_2;
        break;
    case 3:
        val = CXP_CONNECTION_CONFIG_CONNECTION_3;
        break;
    case 4:
        val = CXP_CONNECTION_CONFIG_CONNECTION_4;
        break;
    default:
        tao_store_error(__func__, TAO_BAD_CHANNELS);
        return TAO_ERROR;
    }
    switch (conn->speed) {
    case 1250:
        val |= CXP_CONNECTION_CONFIG_SPEED_1250;
        break;
    case 2500:
        val |= CXP_CONNECTION_CONFIG_SPEED_2500;
        break;
    case 3125:
        val |= CXP_CONNECTION_CONFIG_SPEED_3125;
        break;
    case 5000:
        val |= CXP_CONNECTION_CONFIG_SPEED_5000;
        break;
    case 6250:
        val |= CXP_CONNECTION_CONFIG_SPEED_6250;
        break;
        tao_store_error(__func__, TAO_BAD_SPEED);
        return TAO_ERROR;
    }
    if (cxp_set(dev, CONNECTION_CONFIG, val) != TAO_OK) {
        return TAO_ERROR;
    }
    dev->extra.connection = *conn;
#endif
    return TAO_OK;
}

// Get/set other parameters.

static tao_status update_temperature(
    phnx_device* dev)
{
    int32_t val; // must be signed
    if (cxp_set(dev, DEVICE_INFORMATION_SELECTOR,
                CXP_DEVICE_INFORMATION_SELECTOR_TEMPERATURE) != TAO_OK ||
        cxp_get(dev, DEVICE_INFORMATION, (uint32_t*)&val) != TAO_OK) {
        dev->base.info.temperature = NAN;
        return TAO_ERROR;
    }
    dev->base.info.temperature = 0.5*val;
    return TAO_OK;
}

static tao_status set_pattern_noise_reduction(
    phnx_device* dev,
    bool flag)
{
    uint32_t cur, req = (flag ? 1 : 0);
    if (cxp_get(dev, FIXED_PATTERN_NOISE_REDUCTION, &cur) != TAO_OK ||
        (cur != req &&
         cxp_set(dev, FIXED_PATTERN_NOISE_REDUCTION, req) != TAO_OK)) {
        return TAO_ERROR;
    }
    // FIXME: update camera parameters
    return TAO_OK;
}

static tao_status set_filter_mode(
    phnx_device* dev,
    uint32_t mode)
{
    uint32_t cur;
    if (cxp_get(dev, FILTER_MODE, &cur) != TAO_OK ||
        (cur != mode &&
         cxp_set(dev, FILTER_MODE, mode) != TAO_OK)) {
        return TAO_ERROR;
    }
    // FIXME: update camera parameters
    return TAO_OK;
}

static tao_status set_pixel_pulse_reset(
    phnx_device* dev,
    bool flag)
{
    uint32_t cur, req = (flag ? 1 : 0);
    if (cxp_get(dev, PRST_ENABLE, &cur) != TAO_OK ||
        (cur != req &&
         cxp_set(dev, PRST_ENABLE, req) != TAO_OK)) {
        return TAO_ERROR;
    }
    // FIXME: update camera parameters
    return TAO_OK;
}

static bool identify_camera(
    phnx_device* dev)
{
    return (dev != NULL && dev->coaxpress &&
            strcmp(dev->vendor, "Mikrotron GmbH") == 0 &&
            strncmp(dev->model, "MC408", 5) == 0 &&
            (dev->model[5] == '2' || dev->model[5] == '3' ||
             dev->model[5] == '6' || dev->model[5] == '7'));
}

static tao_status initialize_camera(
    phnx_device* dev)
{
    // Check camera model.
    if (! identify_camera(dev)) {
        tao_store_error(__func__, TAO_BAD_DEVICE);
        return TAO_ERROR;
    }

    // Disable fixed pattern noise reduction.  FIXME: Should be configurable.
    if (set_pattern_noise_reduction(dev, false) != TAO_OK) {
        return TAO_ERROR;
    }

    // Set image filter mode to "raw".  FIXME: Should be configurable.
    if (set_filter_mode(dev, CXP_FILTER_MODE_RAW) != TAO_OK) {
        return TAO_ERROR;
    }

    // Disable the Pixel Pulse Reset feature (recommanded for frame rates
    // higher or equal to 100 Hz).  FIXME: Should be configurable.
    if (set_pixel_pulse_reset(dev, false) != TAO_OK) {
        return TAO_ERROR;
    }

    // Get sensor size.
    uint32_t sensorwidth, sensorheight;
    if (cxp_get(dev, SENSOR_WIDTH,  &sensorwidth) != TAO_OK ||
        cxp_get(dev, SENSOR_HEIGHT, &sensorheight) != TAO_OK) {
        return TAO_ERROR;
    }
    dev->base.info.sensorwidth = sensorwidth;
    dev->base.info.sensorheight = sensorheight;

    // Update other device information.
    if (update_configuration(dev, true) != TAO_OK) {
        return TAO_ERROR;
    }

    // Reset sub-sampling.
    if (dev->roi.xbin != 1 || dev->roi.ybin != 1) {
        tao_camera_roi roi = dev->roi;
        tao_camera_roi_define(&roi, 1, 1,dev->roi.xoff, dev->roi.yoff,
                              dev->roi.xbin*dev->roi.width,
                              dev->roi.ybin*dev->roi.height);
        if (set_region_of_interest(dev, &roi) != TAO_OK) {
            return TAO_ERROR;
        }
    }

    // The following settings are the same as the contents of the configuration
    // file "Mikrotron_MC4080_CXP.pcf".
    if (phx_set(dev, PHX_BOARD_VARIANT, (etParamValue)PHX_DIGITAL) != TAO_OK ||
        phx_set(dev, PHX_CAM_TYPE,           PHX_CAM_AREASCAN_ROI) != TAO_OK ||
        phx_set(dev, PHX_CAM_FORMAT,       PHX_CAM_NON_INTERLACED) != TAO_OK ||
        phx_set(dev, PHX_CAM_CLOCK_POLARITY,    PHX_CAM_CLOCK_POS) != TAO_OK ||
        phx_set(dev, PHX_CAM_SRC_COL,            PHX_CAM_SRC_MONO) != TAO_OK ||
        phx_set(dev, PHX_CAM_DATA_VALID,              PHX_DISABLE) != TAO_OK ||
        phx_set(dev, PHX_CAM_HTAP_NUM,                          1) != TAO_OK ||
        phx_set(dev, PHX_CAM_HTAP_DIR,          PHX_CAM_HTAP_LEFT) != TAO_OK ||
        phx_set(dev, PHX_CAM_HTAP_TYPE,       PHX_CAM_HTAP_LINEAR) != TAO_OK ||
        phx_set(dev, PHX_CAM_HTAP_ORDER,   PHX_CAM_HTAP_ASCENDING) != TAO_OK ||
        phx_set(dev, PHX_CAM_VTAP_NUM,                          1) != TAO_OK ||
        phx_set(dev, PHX_CAM_VTAP_DIR,           PHX_CAM_VTAP_TOP) != TAO_OK ||
        phx_set(dev, PHX_CAM_VTAP_TYPE,       PHX_CAM_VTAP_LINEAR) != TAO_OK ||
        phx_set(dev, PHX_CAM_VTAP_ORDER,   PHX_CAM_VTAP_ASCENDING) != TAO_OK ||
        phx_set(dev, PHX_COMMS_DATA,             PHX_COMMS_DATA_8) != TAO_OK ||
        phx_set(dev, PHX_COMMS_STOP,             PHX_COMMS_STOP_1) != TAO_OK ||
        phx_set(dev, PHX_COMMS_PARITY,      PHX_COMMS_PARITY_NONE) != TAO_OK ||
        phx_set(dev, PHX_COMMS_SPEED,                        9600) != TAO_OK ||
        phx_set(dev, PHX_COMMS_FLOW,          PHX_COMMS_FLOW_NONE) != TAO_OK) {
        return TAO_ERROR;
    }

    // Set acquisition parameters.  FIXME: should be in "start" virtual method?
    if (phx_set(dev, PHX_DATASTREAM_VALID, PHX_DATASTREAM_ALWAYS) != TAO_OK ||
        phx_set(dev, PHX_TIMEOUT_DMA,    1000 /* milliseconds */) != TAO_OK) {
        return TAO_ERROR;
    }

    // Use native byte order for the destination buffer.  FIXME: should be in
    // "start" virtual method?
    if (TAO_IS_LITTLE_ENDIAN) {
        if (phx_set(dev, PHX_DST_ENDIAN, PHX_DST_LITTLE_ENDIAN) != TAO_OK) {
            return TAO_ERROR;
        }
    } else if (TAO_IS_BIG_ENDIAN) {
        if (phx_set(dev, PHX_DST_ENDIAN, PHX_DST_BIG_ENDIAN) != TAO_OK) {
            return TAO_ERROR;
        }
    }
    return TAO_OK;
}

static tao_status start(
    phnx_device* dev)
{
    return cxp_exec(dev, ACQUISITION_START);
}

static tao_status stop(
    phnx_device* dev)
{
    return cxp_exec(dev, ACQUISITION_STOP);
}

// Table of virtual methods for the Mikrotron-MC408x CoaXPress cameras.
const phnx_operations phnx_mikrotron_mc408x = {
    "Mikrotron-MC408x",
    identify_camera,
    initialize_camera,
    start,
    stop,
    update_configuration,
    check_configuration,
    set_configuration,
    save_configuration,
    load_configuration,
};
