// phoenix-capture.c -
//
// Program to capture images with a camera connected to an ActiveSilicon
// Phoenix frame grabber.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2018-2022, Éric Thiébaut.

#include "tao-errors.h"
#include "tao-fits.h"
#include "tao-phoenix-private.h"
#include "tao-phoenix-options.h"

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <strings.h>
#include <math.h>


int main(
    int argc,
    char* argv[])
{
    // Options (all related variables must be static).
    static phnx_config cfg;
    static double timeout = 5.0;
    static bool quiet = false;
    static bool debug = false;
    static int drop = 2;
    static long count = 1;
    static long skip = 2;
    static tao_option options[] = {
        PHNX_OPTION_LOAD,
        PHNX_OPTION_SAVE,
        PHNX_OPTION_CAMERA_ROI(cfg),
        PHNX_OPTION_SENSORENCODING(cfg),
        PHNX_OPTION_BUFFERENCODING(cfg),
        PHNX_OPTION_FRAMERATE(cfg),
        PHNX_OPTION_EXPOSURETIME(cfg),
        PHNX_OPTION_BIAS(cfg),
        PHNX_OPTION_GAIN(cfg),
        PHNX_OPTION_CONNECTION(cfg),
        PHNX_OPTION_ACQUISITION_BUFFERS(cfg),
        PHNX_OPTION_DROP(drop),
        PHNX_OPTION_NONNEGATIVE_SECONDS(2, "timeout", "SECONDS",
                                        "Timeout for each frame in seconds",
                                        &timeout),
        TAO_OPTION_NONNEGATIVE_LONG(2, "count", "NUMBER",
                                    "Number of frames", &count),
        TAO_OPTION_NONNEGATIVE_LONG(2, "skip", "NUMBER",
                                    "Number of frames to skip", &skip),
        PHNX_OPTION_QUIET(quiet),
        PHNX_OPTION_DEBUG(debug),
        PHNX_OPTION_HELP_AND_EXIT(0),
        PHNX_OPTION_LAST_ENTRY,
    };

    // Other variables.
    tao_camera* cam = NULL;
    const char* progname = tao_basename(argv[0]);
    const char* descr = "Capture images from a camera";
    const char* output = NULL;

    // Parse the command line options and configure the camera.
    argc = phnx_start_program(&cam, &cfg, argc, argv, options, descr,
                              "OUTPUT");
    if (argc < 0) {
        return EXIT_FAILURE;
    }

    // Get the name of the output file.
    if (argc != 2) {
        fprintf(stderr, "%s: %s\n", progname,
                argc < 2 ? "missing OUTPUT argument" : "too many arguments");
        return EXIT_FAILURE;
    }
    output = argv[1];
    if (debug || ! quiet) {
        // Print informations about the camera.
        if (phnx_print_camera_info(cam, stdout) != TAO_OK) {
            tao_report_error();
            return EXIT_FAILURE;
        }
        fprintf(stderr, "%s: output file is %s\n", progname, output);
    }

    if (count > 0) {
        const tao_acquisition_buffer* buf;
        tao_time t0, t1;
        double secs = 0, start_secs = 0;
        long frames = 0;
        long timeouts = 0;
        tao_status status;
        puts("starting...");
        tao_camera_lock(cam); // FIXME: check status
        tao_get_monotonic_time(&t0); // FIXME: check status
        tao_camera_set_origin_of_time(cam, &t0); // FIXME: check status
        status = tao_camera_start_acquisition(cam);
        if (status == TAO_OK) {
            tao_get_monotonic_time(&t1); // FIXME: check status
            start_secs = tao_elapsed_seconds(&t1, &t0);
        }
        while (status == TAO_OK && frames < count) {
            puts("waiting...");
            status = tao_camera_wait_acquisition_buffer(cam, &buf, timeout, drop);
            if (status == TAO_OK) {
                ++frames;
                tao_camera_release_acquisition_buffer(cam);
            } else if (status == TAO_TIMEOUT) {
                ++timeouts;
            } else if (status != TAO_ERROR) {
                tao_store_error("tao_camera_wait_acquisition_buffer",
                                TAO_ASSERTION_FAILED);
                status = TAO_ERROR;
            }
        }
        tao_get_monotonic_time(&t1); // FIXME: check status
        tao_camera_stop_acquisition(cam); // FIXME: check status
        tao_camera_unlock(cam); // FIXME: check status
        if (status != TAO_OK) {
            tao_report_error();
            return EXIT_FAILURE;
        }
        secs = tao_elapsed_seconds(&t1, &t0);
        fprintf(stdout, "Starting time: %.3f µs\n", start_secs*1E6);
        fprintf(stdout, "Processing time: %.6f s\n", secs);
        fprintf(stdout, "Frame rate: %.3f fps\n", frames/secs);
        fprintf(stdout, "Processed frames: %10ld\n", frames);
        fprintf(stdout, "Timeouts:         %10ld\n", timeouts);
        fprintf(stdout, "Overruns:         %10ld\n", (long)cam->info.overruns);
        fprintf(stdout, "Overflows:        %10ld\n", (long)cam->info.overflows);
        fprintf(stdout, "Lost frames:      %10ld\n", (long)cam->info.lostframes);
        fprintf(stdout, "Lost synchronizations: %5ld\n", (long)cam->info.lostsyncs);
    }

    // Report any errors and free all resources.
    int retval;
    if (tao_any_errors(NULL)) {
        tao_report_error();
        retval = EXIT_FAILURE;
    } else {
        retval = EXIT_SUCCESS;
    }
    tao_camera_destroy(cam);
    return retval;
}
