// public.c -
//
// Interface to ActiveSilicon Phoenix frame grabber.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2017-2022, Éric Thiébaut.
// Copyright (C) 2016, Éric Thiébaut & Jonathan Léger.

#include "tao-phoenix-private.h"
#include "tao-errors.h"
#include "tao-locks.h"

#include <math.h>
#include <errno.h>
#include <string.h>
#include <assert.h>

// If macro `BUFFER_FULL_IMAGE` is set, all the pixels sent by the camera are
// stored in the acquisition buffers and any cropping will be done when
// processing images.
#define BUFFER_FULL_IMAGE 1

// If macro `NULLIFY_RELEASED_BUFFERS` is true, the data address of an
// acquisition buffer is set to `NULL` when the buffer is released.
#define NULLIFY_RELEASED_BUFFERS 0

static void invalid_runlevel(
    tao_camera* cam,
    const char* func);

static void acquisition_callback(
    tHandle handle,
    uint32_t events,
    void* data);

static tao_status check_regions_of_interest(
    const tao_camera_roi* img,
    const tao_camera_roi* dev,
    long sensorwidth,
    long sensorheight);

static tao_status stop_acquisition(
    phnx_device* dev,
    bool throwerrors);

static tao_status connect_camera(
    phnx_device* dev);

static tao_status disconnect_camera(
    phnx_device* dev,
    bool throwerrors);

//-----------------------------------------------------------------------------
// TABLE OF KNOWN CAMERAS

static phnx_operations const * const known_cameras[] = {
    &phnx_mikrotron_mc408x,
    NULL
};

//-----------------------------------------------------------------------------
// TABLE OF OPERATIONS

// Early declarations of all virtual methods in to initialize table of
// operations.
static tao_status on_initialize(
    tao_camera* cam);

static void on_finalize(
    tao_camera* cam);

static tao_status on_reset(
    tao_camera* cam);

static tao_status on_update_config(
    tao_camera* cam);

static tao_status on_check_config(
    tao_camera* cam,
    const tao_camera_config* cfg);

static tao_status on_set_config(
    tao_camera* cam,
    const tao_camera_config* cfg);

static tao_status on_start(
    tao_camera* cam);

static tao_status on_stop(
    tao_camera* cam);

static tao_status on_release_buffer(
    tao_camera* cam);

static tao_status on_wait_buffer(
    tao_camera* cam,
    double secs);

// Table of operations of all Phoenix cameras.  It address can be used to
// assert that a given TAO camera belongs to this family.
static tao_camera_ops ops = {
    "Phoenix",
    on_initialize,
    on_finalize,
    on_reset,
    on_update_config,
    on_check_config,
    on_set_config,
    on_start,
    on_stop,
    on_wait_buffer,
    on_release_buffer,
};

//-----------------------------------------------------------------------------
// CREATE PHOENIX CAMERA INSTANCE

#undef USE_CONFIG_MODE

tao_camera* phnx_create_camera(
    void (*handler)(const char*,
                    int,
                    const char*),
    char* configname,
    const phnx_create_options* opts)
{
    // Check assumptions.
    assert(sizeof(ui8)  == 1);
    assert(sizeof(ui16) == 2);
    assert(sizeof(ui32) == 4);
    assert(sizeof(ui64) == 8);
    assert(sizeof(float32_t) == 4);
    assert(sizeof(float64_t) == 8);
    assert(sizeof(etParamValue) == sizeof(int));
    assert(sizeof(etStat) == sizeof(int));
    assert(sizeof(float32_t) == 4);
    assert(sizeof(float64_t) == 8);
    assert(sizeof(((phnx_device*)0)->vendor) > CXP_DEVICE_VENDOR_NAME_LENGTH);
    assert(sizeof(((phnx_device*)0)->model) > CXP_DEVICE_MODEL_NAME_LENGTH);

    // Parse open options.
    etParamValue boardnumber;
    if (opts == NULL) {
        static phnx_create_options defaultoptions =
            PHNX_CREATE_OPTIONS_DEFAULT;
        opts = &defaultoptions;
    }
    switch (opts->boardnumber) {
    case PHNX_BOARD_NUMBER_AUTO:
        boardnumber = PHX_BOARD_NUMBER_AUTO;
        break;
    case PHNX_BOARD_NUMBER_1:
        boardnumber = PHX_BOARD_NUMBER_1;
        break;
    case PHNX_BOARD_NUMBER_2:
        boardnumber = PHX_BOARD_NUMBER_2;
        break;
    case PHNX_BOARD_NUMBER_3:
        boardnumber = PHX_BOARD_NUMBER_3;
        break;
    case PHNX_BOARD_NUMBER_4:
        boardnumber = PHX_BOARD_NUMBER_4;
        break;
    case PHNX_BOARD_NUMBER_5:
        boardnumber = PHX_BOARD_NUMBER_5;
        break;
    case PHNX_BOARD_NUMBER_6:
        boardnumber = PHX_BOARD_NUMBER_6;
        break;
    case PHNX_BOARD_NUMBER_7:
        boardnumber = PHX_BOARD_NUMBER_7;
        break;
    default:
        tao_store_error(__func__, TAO_BAD_ARGUMENT);
        return NULL;
    }
    etParamValue channelnumber;
    switch (opts->channelnumber) {
    case PHNX_CHANNEL_NUMBER_AUTO:
        channelnumber = PHX_CHANNEL_NUMBER_AUTO;
        break;
    case PHNX_CHANNEL_NUMBER_1:
        channelnumber = PHX_CHANNEL_NUMBER_1;
        break;
    case PHNX_CHANNEL_NUMBER_2:
        channelnumber = PHX_CHANNEL_NUMBER_2;
        break;
    default:
        tao_store_error(__func__, TAO_BAD_ARGUMENT);
        return NULL;
    }
    etParamValue configmode;
    switch (opts->configmode) {
    case PHNX_CONFIG_MODE_NORMAL:
        configmode = PHX_CONFIG_NORMAL;
        break;
    case PHNX_CONFIG_MODE_COMMS_ONLY:
        configmode = PHX_CONFIG_COMMS_ONLY;
        break;
    case PHNX_CONFIG_MODE_ACQ_ONLY:
        configmode = PHX_CONFIG_ACQ_ONLY;
        break;
    default:
        tao_store_error(__func__, TAO_BAD_ARGUMENT);
        return NULL;
    }

    // Create a Phoenix handle.
    phx_error_handler *error_handler;
    if (handler != NULL) {
        assert(sizeof(etStat) == sizeof(int));
        error_handler = (phx_error_handler*)handler;
    } else {
        error_handler = phx_default_error_handler;
    }
    tHandle handle;
    etStat code = PHX_Create(&handle, error_handler);
    if (code != PHX_OK) {
        phx_push_error("PHX_Create", code);
        return NULL;
    }

    // Set the configuration file name.
    if (configname != NULL && configname[0] != '\0') {
        code = PHX_ParameterSet(handle, PHX_CONFIG_FILE, &configname);
        if (code != PHX_OK) {
            phx_push_error("PHX_ParameterSet(PHX_CONFIG_FILE)", code);
            goto error;
        }
    }

    // Set the board number.
    code = PHX_ParameterSet(handle, PHX_BOARD_NUMBER, &boardnumber);
    if (code != PHX_OK) {
        phx_push_error("PHX_ParameterSet(PHX_BOARD_NUMBER)", code);
        goto error;
    }

    // Set the channel number.
    code = PHX_ParameterSet(handle, PHX_CHANNEL_NUMBER, &channelnumber);
    if (code != PHX_OK) {
        phx_push_error("PHX_ParameterSet(PHX_CHANNEL_NUMBER)", code);
        goto error;
    }

    // Set the configuration mode.  In our case, setting this parameter was
    // blocking, so we only do that if a non-default option is chosen.
    if (configmode != PHX_CONFIG_NORMAL) {
        code = PHX_ParameterSet(handle, PHX_CONFIG_MODE, &configmode);
        if (code != PHX_OK) {
            phx_push_error("PHX_ParameterSet(PHX_CONFIG_MODE)", code);
            goto error;
        }
    }

    // Allocate camera instance with the configured (but not yet open) handle.
    // The remaining initialization will done by the `on_initialize` callback.
    tao_camera* cam = tao_camera_create(&ops, &handle, sizeof(phnx_device));
    if (cam == NULL) {
        goto error;
    }
    return cam;

 error:
    // Destroy Phoenix handle.
    (void)PHX_Destroy(&handle);
    return NULL;
}

//-----------------------------------------------------------------------------
// VIRTUAL METHODS

// Perform the final initialization of a Phoenix camera.  Device handle has
// been created and connected.  Remaining task is to identifut the camera
// model.  This virtual method is only called once during the lifetime of the
// camera instance.
static tao_status on_initialize(
    tao_camera* cam)
{
    assert(cam->ops == &ops);
    phnx_device* dev = (phnx_device*)cam;
    dev->handle = *(tHandle*)cam->ctx;
    dev->swap = false;
    dev->coaxpress = false;
    return connect_camera(dev);
}

// Finalize a "Phoenix" device.  Stop acquisition, close the Phoenix board and
// destroy the Phoenix handle.  No errors are reported (unless in debug mode).
static void on_finalize(
    tao_camera* cam)
{
    assert(cam != NULL);
    assert(cam->ops == &ops);
    phnx_device* dev = (phnx_device*)cam;
    if (cam->runlevel == 2) {
        (void)stop_acquisition(dev, false);
    }
    if (cam->runlevel >= 1) {
        (void)disconnect_camera(dev, false);
    }
    assert(sizeof(dev->handle) >= sizeof(tHandle));
    tHandle handle = dev->handle;
    etStat code = PHX_Destroy(&handle);
    dev->handle = handle;
#ifdef PHNX_DEBUG
    if (code != PHX_OK) {
        fprintf(stderr,
                "PHX_Destroy failed with status %d in %s (%s:%d)\n",
                (int)code, __func__, __FILE__, __LINE__);
    }
#endif // PHNX_DEBUG
}

// Manage to revert to run-level 1.  Should only be called when run-level is 3
// (recoverable error).
static tao_status on_reset(
    tao_camera* cam)
{
    // Disconnect, then reconnect the camera.
    assert(cam->ops == &ops);
    assert(cam->runlevel == 3);
    phnx_device* dev = (phnx_device*)cam;
    if (disconnect_camera(dev, true) != TAO_OK ||
        connect_camera(dev) != TAO_OK) {
        cam->runlevel = 4;
        return TAO_ERROR;
    }
    return TAO_OK;
}

static tao_status on_update_config(
    tao_camera* cam)
{
    // Camera has to be a "Phoenix" camera and must not be acquiring.
    assert(cam->ops == &ops);
    assert(cam->runlevel == 1);
    phnx_device* dev = (phnx_device*)cam;
    if (dev->ops->update_config == NULL) {
        tao_store_error(__func__, TAO_UNSUPPORTED);
        return TAO_ERROR;
    }
    if (dev->ops->update_config(dev, true) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

// Check the validity of a configuration.  Directly check the pixel encoding
// and the number of acquisition buffers and let the `check_config` callback
// deal with the other settings.  FIXME: The camera must be locked while
// checking the configuration.  Acquisition may however be running because
// checking does not change anything.
static tao_status on_check_config(
    tao_camera* cam,
    const tao_camera_config* cfg)
{
    // Camera has to be a "Phoenix" camera.
    assert(cam->ops == &ops);
    phnx_device* dev = (phnx_device*)cam;

    // Call virtual method with specific settings identical to the current
    // ones.
    if (dev->ops->check_config == NULL) {
        tao_store_error(__func__, TAO_UNSUPPORTED);
        return TAO_ERROR;
    }
    phnx_config fullconf;
    fullconf.base = *cfg;
    fullconf.extra = dev->extra;
    if (dev->ops->check_config(dev, &fullconf) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

// Attempt to apply the new configuration.  Directly set the pixel encoding and
// the number of acquisition buffers and let the `set_config` callback deal
// with the other settings.  Camera must not be acquiring.
static tao_status on_set_config(
    tao_camera* cam,
    const tao_camera_config* cfg)
{
    // Camera has to be a "Phoenix" camera and must not be acquiring.
    assert(cam->ops == &ops);
    assert(cam->runlevel == 1);
    phnx_device* dev = (phnx_device*)cam;

    // Set encoding and number of acquisition buffers.
    cam->info.config.bufferencoding = cfg->bufferencoding;
    if (cfg->nbufs < 2) {
        tao_store_error(__func__, TAO_BAD_BUFFERS);
        return TAO_ERROR;
    }
    cam->info.config.nbufs = cfg->nbufs;

    // Call virtual method with specific settings identical to the current
    // ones.
    if (dev->ops->set_config == NULL) {
        tao_store_error(__func__, TAO_UNSUPPORTED);
        return TAO_ERROR;
    }
    phnx_config fullconf;
    fullconf.base = *cfg;
    fullconf.extra = dev->extra;
    if (dev->ops->set_config(dev, &fullconf) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

// Virtual method to start acquisition.  This method is only called after
// initialization and if the camera is not acquiring.
static tao_status on_start(
    tao_camera* cam)
{
    // Assert that argument is a Phoenix camera at run-level 1 ("waiting") and
    // that a minimal number of acquisition buffers have been allocated.
    assert(cam->ops == &ops);
    assert(cam->runlevel == 1);
    assert(cam->nbufs >= 2);
    phnx_device* dev = (phnx_device*)cam;
    int nbufs = cam->nbufs;

    // Name of the caller for reporting errors.
    const char* func = "tao_camera_start_acquisition";

    // Check the compatibility of images and device regions of interest.
    const tao_camera_config* cfg = &cam->info.config;
    if (check_regions_of_interest(&cfg->roi,
                                  &dev->roi,
                                  cam->info.sensorwidth,
                                  cam->info.sensorheight) != TAO_OK) {
        tao_store_error(func, TAO_BAD_ROI);
        return TAO_ERROR;
    }

    // Check acquisition buffer encoding.
    tao_encoding sensorencoding = cfg->sensorencoding;
    tao_encoding bufferencoding = cfg->bufferencoding;
    if (bufferencoding == TAO_ENCODING_UNKNOWN) {
        bufferencoding = phnx_best_buffer_encoding(sensorencoding);
    }
    long bitsperpixel = TAO_ENCODING_BITS_PER_PIXEL(bufferencoding);
    long pixelsize = (bitsperpixel + 7) >> 3;
    if (bitsperpixel <= 0 || (pixelsize << 3) != bitsperpixel) {
        // We do not know how to deal with pixels whose size are not an integer
        // number of bytes.
        tao_store_error(func, TAO_BAD_ENCODING);
        return TAO_ERROR;
    }

    // Set the parameters describing the encoding of pixels sent by the camera
    // and the encoding of pixels in the acquisition buffers.
    if (phx_define_camera_encoding(dev, sensorencoding) != TAO_OK ||
        phx_define_buffer_encoding(dev, bufferencoding) != TAO_OK) {
        return TAO_ERROR;
    }

    // Set the acquisition buffer parameters.  The value of parameter
    // PHX_BUF_DST_XLENGTH is the number of bytes per line of the destination
    // buffer (it must be larger of equal the width of the ROI times the number
    // of bits per pixel rounded up to a number of bytes), the value of
    // PHX_BUF_DST_YLENGTH is the number of lines in the destination buffer (it
    // must be larger or equal PHX_ROI_DST_YOFFSET plus PHX_ROI_YLENGTH.
#ifdef BUFFER_FULL_IMAGE
    etParamValue roi_src_xoffset = 0;
    etParamValue roi_src_yoffset = 0;
    etParamValue roi_xlength     = dev->roi.width;
    etParamValue roi_ylength     = dev->roi.height;
    etParamValue buf_dst_ylength = roi_ylength;
#else
    etParamValue roi_src_xoffset = cfg->roi.xoff - dev->roi.xoff;
    etParamValue roi_src_yoffset = cfg->roi.yoff - dev->roi.yoff;
    etParamValue roi_xlength     = cfg->roi.width;
    etParamValue roi_ylength     = cfg->roi.height;
    etParamValue buf_dst_ylength = roi_ylength;
#endif
    if (phx_set(dev, PHX_ACQ_IMAGES_PER_BUFFER,               1) != TAO_OK ||
        phx_set(dev, PHX_ACQ_BUFFER_START,                    1) != TAO_OK ||
        phx_set(dev, PHX_ACQ_NUM_BUFFERS,            cam->nbufs) != TAO_OK ||
        phx_set(dev, PHX_CAM_ACTIVE_XOFFSET,                  0) != TAO_OK ||
        phx_set(dev, PHX_CAM_ACTIVE_YOFFSET,                  0) != TAO_OK ||
        phx_set(dev, PHX_CAM_ACTIVE_XLENGTH,     dev->roi.width) != TAO_OK ||
        phx_set(dev, PHX_CAM_ACTIVE_YLENGTH,    dev->roi.height) != TAO_OK ||
        phx_set(dev, PHX_CAM_XBINNING,                        1) != TAO_OK ||
        phx_set(dev, PHX_CAM_YBINNING,                        1) != TAO_OK ||
        phx_set(dev, PHX_ACQ_XSUB,                   PHX_ACQ_X1) != TAO_OK ||
        phx_set(dev, PHX_ACQ_YSUB,                   PHX_ACQ_X1) != TAO_OK ||
        phx_set(dev, PHX_ROI_SRC_XOFFSET,       roi_src_xoffset) != TAO_OK ||
        phx_set(dev, PHX_ROI_SRC_YOFFSET,       roi_src_yoffset) != TAO_OK ||
        phx_set(dev, PHX_ROI_XLENGTH,               roi_xlength) != TAO_OK ||
        phx_set(dev, PHX_ROI_YLENGTH,               roi_ylength) != TAO_OK ||
        phx_set(dev, PHX_BUF_DST_YLENGTH,       buf_dst_ylength) != TAO_OK ||
        phx_set(dev, PHX_ROI_DST_XOFFSET,                     0) != TAO_OK ||
        phx_set(dev, PHX_ROI_DST_YOFFSET,                     0) != TAO_OK ||
        // FIXME: PHX_BIT_SHIFT_ALIGN_LSB not defined for PHX_BIT_SHIFT.
        phx_set(dev, PHX_BIT_SHIFT,                           0) != TAO_OK) {
        return TAO_ERROR;
    }

    // Instruct Phoenix frame grabber to use its own acquisition buffers.
    // FIXME: Following the examples in the documentation, the
    // PHX_FORCE_REWRITE flag is set but its effects are not documented and I
    // am not sure whether this is really needed.
    if (phx_set(dev, (PHX_DST_PTR_TYPE|PHX_CACHE_FLUSH|PHX_FORCE_REWRITE),
                PHX_DST_PTR_INTERNAL) != TAO_OK) {
        return TAO_ERROR;
    }

    // Determine the stride in the acquisition buffers as given by
    // `PHX_BUF_DST_XLENGTH`.
    etParamValue buf_dst_xlength;
    if (phx_get(dev, PHX_BUF_DST_XLENGTH, &buf_dst_xlength) != TAO_OK) {
        return TAO_ERROR;
    }
#ifdef PHNX_DEBUG
    fprintf(stderr, "Bytes per line: %ld (min. is %ld)\n",
            (long)buf_dst_xlength, cfg->roi.width*pixelsize);
#endif

    // Instanciate the acquisition buffers information.
    long stride = buf_dst_xlength;
#ifdef BUFFER_FULL_IMAGE
    long offset = ((cfg->roi.xoff - dev->roi.xoff)*pixelsize +
                   (cfg->roi.yoff - dev->roi.yoff)*stride);
#else
    long offset = 0;
#endif
    long width  = cfg->roi.width;
    long height = cfg->roi.height;
    tao_time unknown_time = TAO_UNKNOWN_TIME;
    for (int i = 0; i < nbufs; ++i) {
        cam->bufs[i].data         = NULL;
        cam->bufs[i].size         = buf_dst_xlength*buf_dst_ylength;
        cam->bufs[i].offset       = offset;
        cam->bufs[i].width        = width;
        cam->bufs[i].height       = height;
        cam->bufs[i].stride       = stride;
        cam->bufs[i].encoding     = bufferencoding;
        cam->bufs[i].serial       = 0;
        cam->bufs[i].frame_start  = unknown_time;
        cam->bufs[i].frame_end    = unknown_time;
        cam->bufs[i].buffer_ready = unknown_time;
    }

    // Reset start and end times of next frame to detect overruns.
    dev->frame_start = unknown_time;
    dev->frame_end   = unknown_time;

    // Configure frame grabber for continuous acquisition and enable interrupts
    // for expected events.
    etParamValue events = (PHX_INTRPT_FRAME_START   |
                           PHX_INTRPT_FRAME_END     |
                           PHX_INTRPT_BUFFER_READY  |
                           PHX_INTRPT_FIFO_OVERFLOW |
                           PHX_INTRPT_FRAME_LOST    |
                           PHX_INTRPT_SYNC_LOST);
    if (phx_set(dev, PHX_INTRPT_CLR,                ~(etParamValue)0) != TAO_OK ||
        phx_set(dev, PHX_INTRPT_SET, PHX_INTRPT_GLOBAL_ENABLE|events) != TAO_OK ||
        phx_set(dev, PHX_ACQ_BLOCKING,                    PHX_ENABLE) != TAO_OK ||
        phx_set(dev, PHX_ACQ_CONTINUOUS,                  PHX_ENABLE) != TAO_OK ||
        phx_set(dev, PHX_COUNT_BUFFER_READY,                       1) != TAO_OK) {
        return TAO_ERROR;
    }

    // Setup callback context.
    if (phx_set_parameter(dev, PHX_EVENT_CONTEXT, (void*)cam) != TAO_OK) {
        return TAO_ERROR;
    }

    // Start acquisition.
    if (phx_read_stream(dev, PHX_START, acquisition_callback) != TAO_OK) {
        return TAO_ERROR;
    }

    // Execute specific start command. (FIXME: always unlock all buffers
    // before.)
    if (dev->ops->start != NULL && dev->ops->start(dev) != TAO_OK) {
        (void)PHX_StreamRead(dev->handle, PHX_ABORT, NULL);
        (void)PHX_StreamRead(dev->handle, PHX_UNLOCK, NULL);
        return TAO_ERROR;
    }

    // Report success.
    return TAO_OK;
}

// Virtual method to stop acquisition immediately, that is without waiting for
// the current frame.  This method is only called when the camera is acquiring.
static tao_status on_stop(
    tao_camera* cam)
{
    // Check argument and that run-level is 2 ("acquiring") then change to
    // run-level 1 ("waiting").
    assert(cam->ops == &ops);
    assert(cam->runlevel == 2);
    return stop_acquisition((phnx_device*)cam, true);
}

//-----------------------------------------------------------------------------
// GET/RELEASE ACQUISITION BUFFER
//
// Something not specified in the doc. of the ActiveSilicon Phoenix library is
// that, when continuous acquisition and blocking mode are both enabled, all
// calls to `PHX_BUFFER_GET` yield the same image buffer until
// `PHX_BUFFER_RELEASE` is called.  It seems that there is no needs to have a
// `PHX_BUFFER_GET` matches a `PHX_BUFFER_RELEASE` and that every
// `PHX_BUFFER_RELEASE` moves to the next buffer.  However, acquisition buffers
// are used in their given order so it is not too difficult to figure out where
// we are if we count the number of frames.
//
// See doc. for `tao_camera_ops` and `tao_camera` for explanations about:
//
//    cam->pending = number of pending acquisition buffers waiting for being
//                   processed and released;
//    cam->nbufs   = length of cyclic list of acquistion buffers;
//    cam->last    = index of last pending acquisition buffer in cyclic list;
//
// When acquisition start:
//
//    pending = 0
//    last = nbufs (or -1)
//
// When `cam->pending > 0`, the index of the first (oldest) pending acquisition
// buffer in the cyclic list is given by:
//
//    next = (cam->last + 1)%cam->nbufs
//         = TAO_NEXT_PENDING_ACQUISITION_BUFFER(cam)
//
// When `cam->pending > 0`, the index of the next (last + 1) acquisition buffer
// in the cyclic list is given by:
//
//    first = (cam->nbufs + cam->last + 1 - cam->pending)%cam->nbufs
//          = TAO_LAST_PENDING_ACQUISITION_BUFFER(cam)
//
// A successful call to `PHX_StreamRead(PHX_BUFFER_GET)` yields the first
// (oldest) pending buffer, at index given by
// `TAO_FIRST_PENDING_ACQUISITION_BUFFER`.
//
// A successful call to `PHX_StreamRead(PHX_BUFFER_RELEASE)` drops one pending
// buffer and unveil the new first pending buffer:
//
//    --cam->pending
//
// which is done in phnx_release_buffers().
//
// Note that `cam->last` and `cam->pending` are incremented by the acquisition
// callback, not directly by `on_wait_buffer`, on reception of a "buffer ready"
// event:
//
//    ++cam->pending
//    cam->last = TAO_NEXT_PENDING_ACQUISITION_BUFFER(cam)
//

// Update address of first pending buffer.
static inline void update_first_pending_buffer_address(
    tao_camera* cam)
{
    // Retrieve address of first pending buffer.
    void* data = phnx_get_buffer((phnx_device*)cam);
    assert((cam->pending > 0) == (data != NULL));
    int index = TAO_FIRST_PENDING_ACQUISITION_BUFFER(cam);
    cam->bufs[index].data = data;
}

// Virtual method to wait for the next frame.  This method is only called when
// the camera is acquiring.  This method assumes that the camera has been
// locked by the caller.  On success, the newly acquirred buffer is at updated
// index `cam->last`.
static tao_status on_wait_buffer(
    tao_camera* cam,
    double secs)
{
    // Must be an acquiring Phoenix camera.
    assert(cam->ops == &ops);
    assert(cam->runlevel == 2);
    assert(cam->pending >= 0);
    assert(cam->pending < cam->nbufs);

    // Compute absolute time-out time.
    tao_time abstime;
    bool forever;
    switch (tao_get_absolute_timeout(&abstime, secs)) {

    case TAO_TIMEOUT_PAST:
        ++cam->info.timeouts;
        return TAO_TIMEOUT;

    case TAO_TIMEOUT_NOW:
    case TAO_TIMEOUT_FUTURE:
        forever = false;
        break;

    case TAO_TIMEOUT_NEVER:
        forever = true;
        break;

    default:
        return TAO_ERROR;
    }

    // While there are no pending buffers, wait for events to be signaled or an
    // error to occur.  This is done in a `while` loop to cope with spurious
    // signaled conditions.  Index `cam->last`, number `cam->pending`, and
    // other members are updated by the acquisition callback.
    while (cam->pending < 1) {
        if (forever) {
            if (tao_condition_wait(&cam->cond, &cam->mutex) != TAO_OK) {
                return TAO_ERROR;
            }
        } else {
            tao_status status = tao_condition_abstimed_wait(
                &cam->cond, &cam->mutex, &abstime);
            if (status != TAO_OK) {
                if (status == TAO_TIMEOUT) {
                    ++cam->info.timeouts;
                    return TAO_TIMEOUT;
                } else {
                    return TAO_ERROR;
                }
            }
        }
    }

    // Get address of first pending acquisition buffer.
    update_first_pending_buffer_address(cam);
    return TAO_OK;
}

// Virtual method to release acquisition buffer.  This method is only called
// when the camera is acquiring and when there are at least one pending
// acquisition buffer.
static tao_status on_release_buffer(
    tao_camera* cam)
{
    // This method is only called for an acquiring Phoenix camera and if there
    // is at least one pending acquisition buffer.
    assert(cam->ops == &ops);
    assert(cam->runlevel == 2);
    assert(cam->pending > 0);
    phnx_device* dev = (phnx_device*)cam;

    // cam->bufs[index].data != NULL and `index` is that of the first
    // pending acquisition buffer.
#if NULLIFY_RELEASED_BUFFERS
    int index = TAO_FIRST_PENDING_ACQUISITION_BUFFER(cam);
    cam->bufs[index].data = NULL; // this buffer can no longer be used
#endif // NULLIFY_RELEASED_BUFFERS
    if (phnx_release_buffers(dev, cam->pending - 1) != TAO_OK) {
        return TAO_ERROR;
    }
    if (cam->pending > 0) {
        // If there is at least one more pending buffer, releasing the first
        // (and oldest) one has unveiled the address of the next first (and
        // oldest) one.
        update_first_pending_buffer_address(cam);
    }
    return TAO_OK;
}

//-----------------------------------------------------------------------------
// ACQUISITION CALLBACK
//
// We assume continuous acquisition.  We may want to try to process all frames
// or to always process the last one and discard the frames that we had no time
// to deal with.  So we accept to loose frames but we do not want to loose the
// newest frames nor to have the frame currently processed overwritten by the
// frame-grabber.
//
// The total number of acquisition buffers is then given by:
//
//     nbufs = p ("pending" buffers) + r ("recyclable" buffers)
//
// There is only one processing thread so the oldest pending buffer is the
// one being processed.
//
// There is no re-ordering of the buffers.  They are always used in the same
// (cyclic) order.
//
// The `PHX_INTRPT_BUFFER_READY` event is only generated when a new buffer is
// ready for processing which implies that this buffer was available at the
// time a new frame was sent by the camera.
//
// When buffers are not released quickly enough, there is no special event to
// signal that acquisition is blocked except that `PHX_INTRPT_FRAME_START`
// events arrive with no `PHX_INTRPT_BUFFER_READY` interleaving events.
//
// According to these rules:
//
// - When a buffer ready event arrives, the number of pending buffers has to be
//   incremented.  In blocking acquisition (`PHX_ACQ_BLOCKING` set to
//   `PHX_ENABLE`), it is guaranteed that the number of pending buffers is
//   smaller or equal `nbufs`, the number of acquisition buffers.
//
// - A buffer can only be released if there is at least one pending buffer
//   because releasing a buffer reduces the number of pending buffers by one.
//
// On buffer ready event:
//        pending: p += 1 (0 ≤ p ≤ nbufs)
//     recyclable: r -= 1
//
// When a buffer is released then:
//    if pending == 0
//        no buffer to release
//    else if pending > 0
//         execute `PHX_BUFFER_RELEASE` command
//         pending: p -= 1
//        recycled: r += 1
//
// The safer is to make sure that the frame-grabber can always write to a
// recyclable buffer.  So when a buffer ready event arrives, there should be at
// least 2 recyclable buffers.  If there is one buffer being processed, there
// must be at least 3 acquisition buffers in total.

static void acquisition_callback(
    tHandle handle,
    uint32_t events,
    void* data)
{
    // Get camera and perform minimal sanity check (no needs to lock, abort on
    // error, etc.).
    tao_camera* cam = (tao_camera*)data;
    assert(cam->ops == &ops);
    phnx_device* dev = (phnx_device*)data;

    // Get timestamp immediately (before locking because this may block for
    // some time) aborting on error.
    tao_time now;
    if (tao_get_monotonic_time(&now) != TAO_OK) {
        tao_panic();
    }

    // Lock the camera, handle events and unlock the camera.
    tao_camera_lock(cam);
    {
        if ((PHX_INTRPT_FRAME_START & events) != 0) {
            // Register the time at the start of the frame using the number of
            // seconds to detect overruns.
            if (dev->frame_start.sec > 0) {
                ++cam->info.overruns;
                ++cam->info.frames;
                cam->events |= TAO_EVENT_ERROR;
            }
            dev->frame_start = now;
        }
        if ((PHX_INTRPT_FRAME_END & events) != 0) {
            // Register the time at the end of the frame using the number of
            // seconds to detect overruns.
            if (dev->frame_end.sec > 0) {
                ++cam->info.overruns;
                ++cam->info.frames;
                cam->events |= TAO_EVENT_ERROR;
            }
            dev->frame_end = now;
        }
        if ((PHX_INTRPT_BUFFER_READY & events) != 0) {
            // A new frame is available.
            ++cam->info.frames;
            if (cam->pending < cam->nbufs) {
                // Get index of next informational buffer.  We update all
                // remaining information that is available at that time (not
                // the buffer address though).
                int next = TAO_NEXT_ACQUISTION_BUFFER(cam);
                tao_acquisition_buffer* buf = &cam->bufs[next];
                buf->frame_start  = dev->frame_start;
                buf->frame_end    = dev->frame_end;
                buf->buffer_ready = now;
                buf->serial       = cam->info.frames; // FIXME:
                cam->last = next;
                cam->events |= TAO_EVENT_FRAME;
                ++cam->pending;
            } else {
                // This is not supposed to happen in continuous and blocking
                // acquisition mode.  Nevertheless count this as an overrun.
                ++cam->info.overruns;
                cam->events |= TAO_EVENT_ERROR;
            }
            // Reset start and end times of next frame to detect overruns.
            dev->frame_start = TAO_UNKNOWN_TIME;
            dev->frame_end   = TAO_UNKNOWN_TIME;
        }
        if ((PHX_INTRPT_FIFO_OVERFLOW & events) != 0) {
            // Fifo overflow.
            ++cam->info.overflows;
            cam->events |= TAO_EVENT_ERROR;
        }
        if ((PHX_INTRPT_SYNC_LOST & events) != 0) {
            // Synchronization lost.
            ++cam->info.lostsyncs;
            cam->events |= TAO_EVENT_ERROR;
        }
        if ((PHX_INTRPT_FRAME_LOST & events) != 0) {
            // Frame lost.
            ++cam->info.lostframes;
            cam->events |= TAO_EVENT_ERROR;
        }
        if (cam->events != 0) {
            // Signal condition for waiting thread.
            tao_camera_broadcast(cam);
        }
    }
    tao_camera_unlock(cam);
}

// FIXME: Make this static when old server code is no longer used.
tao_status phnx_release_buffers(
    phnx_device* dev,
    int limit)
{
    if (limit < 0) {
        tao_store_error(__func__, TAO_BAD_ARGUMENT);
        return TAO_ERROR;
    }
    while (dev->base.pending > limit) {
        etStat code = PHX_StreamRead(dev->handle, PHX_BUFFER_RELEASE, NULL);
        if (code != PHX_OK) {
            // FIXME: If an error occurs here, it is probably better to abort
            // acquisition because it means that we have lost the count of the
            // buffers... This could be done by stopping acquisition and
            // switching to runlevel 3.
            phx_push_error("PHX_StreamRead(PHX_BUFFER_RELEASE)", code);
            return TAO_ERROR;
        }
        --dev->base.pending;
    }
    return TAO_OK;
}

// FIXME: Make this static when old server code is no longer used.
void* phnx_get_buffer(
    phnx_device* dev)
{
    stImageBuff vbuf;
    etStat code = PHX_StreamRead(dev->handle, PHX_BUFFER_GET, &vbuf);
    if (code != PHX_OK) {
        phx_push_error("PHX_StreamRead(PHX_BUFFER_GET)", code);
        return NULL;
    }
    return vbuf.pvAddress;
}

//-----------------------------------------------------------------------------
// STOP ACQUISITION, CONNECT/DISCONNECT CAMERA.

// Stop acquisition immediately.
static tao_status stop_acquisition(
    phnx_device* dev,
    bool throwerrors)
{
    // Since we use acquisition buffers allocated by the frame-grabber, we
    // cannot be sure that they will be available after acquisition has been
    // stopped.  We therefore pretend that there are no more pending buffers.
    dev->base.pending = 0;

    // Abort acquisition, unlock acquisition buffers and call "stop" specific
    // method.  In case of errors, the members of the structure are updated
    // *before* doing anything.  The error `PHX_ERROR_NOT_IMPLEMENTED` can be
    // ignored for the `PHX_UNLOCK` command.
    tao_status status = TAO_OK;
    etStat code = PHX_StreamRead(dev->handle, PHX_ABORT, NULL);
    if (code != PHX_OK && throwerrors) {
        phx_push_error("PHX_StreamRead(PHX_ABORT)", code);
        status = TAO_ERROR;
    }
    code = PHX_StreamRead(dev->handle, PHX_UNLOCK, NULL);
    if (code != PHX_OK && code != PHX_ERROR_NOT_IMPLEMENTED && throwerrors) {
        phx_push_error("PHX_StreamRead(PHX_UNLOCK)", code);
        status = TAO_ERROR;
    }
    if (dev->ops->stop != NULL && dev->ops->stop(dev) != TAO_OK && throwerrors) {
        status = TAO_ERROR;
    }
    return status;
}

// Connect or re-connect the camera.
static tao_status connect_camera(
    phnx_device* dev)
{
    assert(sizeof(dev->handle) >= sizeof(tHandle));
    tHandle handle = dev->handle;

    // Open the Phoenix board using the configured handle.  If this is the very
    // first time the camera is open, identify the model of the connected
    // camera.
    etStat code = PHX_Open(dev->handle);
    if (code != PHX_OK) {
        phx_push_error("PHX_Open", code);
        return TAO_ERROR;
    }

    // If this is the very first time the camera is open, check whether we have
    // a CoaXPress camera and identify the model of the connected camera.
    if (dev->ops == NULL) {
        if (phx_detect_coaxpress(dev) != TAO_OK) {
            goto error;
        }
        for (int k = 0; known_cameras[k] != NULL; ++k) {
            if (known_cameras[k]->identify(dev)) {
                dev->ops = known_cameras[k];
                break;
            }
        }
        if (dev->ops == NULL) {
            tao_store_error(__func__, TAO_NOT_FOUND);
            goto error;
        }
    }

    // Initialize specific camera model.
    if (dev->ops->initialize != NULL && dev->ops->initialize(dev) != TAO_OK) {
        goto error;
    }

    // Execute specific "stop" command.  FIXME: Is it a good idea?
    if (dev->ops->stop != NULL && dev->ops->stop(dev) != TAO_OK) {
        goto error;
    }
    return TAO_OK;

    // Close the handle in case of errors.
 error:
    (void)PHX_Close(&handle);
    dev->handle = handle;
    return TAO_ERROR;
}

// Disconnect the camera, that is close the Phoenix board.
static tao_status disconnect_camera(
    phnx_device* dev,
    bool throwerrors)
{
    assert(sizeof(dev->handle) >= sizeof(tHandle));
    tHandle handle = dev->handle;
    etStat code = PHX_Close(&handle);
    dev->handle = handle;
    if (code != PHX_OK && throwerrors) {
        phx_push_error("PHX_Close", code);
        return TAO_ERROR;
    }
    return TAO_OK;
}

//-----------------------------------------------------------------------------
// OTHER PRIVATE ROUTINES

// FIXME: this function is used elsewhere
static void invalid_runlevel(
    tao_camera* cam,
    const char* func)
{
    int code;
    switch (cam->runlevel) {
    case 0:
        code = TAO_NOT_READY;
        break;
    case 1:
        code = TAO_NOT_ACQUIRING;
        break;
    case 2:
        code = TAO_ACQUISITION_RUNNING;
        break;
    case 3:
        code = TAO_MUST_RESET;
        break;
    case 4:
        code = TAO_UNRECOVERABLE;
        break;
    default:
        code = TAO_CORRUPTED;
    }
    tao_store_error(func, code);
}

// Check images and device regions of interest (ROI).  First check that the
// configured ROI and the hardware ROI are correct, then check whether they are
// compatible.
static tao_status check_regions_of_interest(
    const tao_camera_roi* img,
    const tao_camera_roi* dev,
    long sensorwidth,
    long sensorheight)
{
    if (tao_camera_roi_check(img, sensorwidth, sensorheight) != TAO_OK ||
        tao_camera_roi_check(dev, sensorwidth, sensorheight) != TAO_OK ||
        img->xbin != dev->xbin ||
        img->xoff < dev->xoff ||
        img->xoff + img->xbin*img->width > dev->xoff + dev->xbin*dev->width ||
        img->yoff < dev->yoff ||
        img->ybin != dev->ybin ||
        img->yoff + img->ybin*img->height > dev->yoff + dev->ybin*dev->height) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

//-----------------------------------------------------------------------------
// SPECIFIC CONFIGURATION

// Attempt to load a new configuration.  The pixel encoding for the acquisition
// buffers is reset.  (FIXME: Not needed?)
tao_status phnx_load_configuration(
    tao_camera* cam,
    int id)
{
    // Camera must be a "Phoenix" camera.
    phnx_device* dev = phnx_get_device(cam);
    if (dev == NULL) {
        return TAO_ERROR;
    }

    // Camera must not be acquiring.
    if (cam->runlevel != 1) {
        invalid_runlevel(cam, __func__);
        return TAO_ERROR;
    }

    // Call virtual method.
    if (dev->ops->load_config == NULL) {
        tao_store_error(__func__, TAO_UNSUPPORTED);
        return TAO_ERROR;
    }
    if (dev->ops->load_config(dev, id) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

tao_status phnx_save_configuration(
    tao_camera* cam,
    int id)
{
    // Camera must be a "Phoenix" camera.
    phnx_device* dev = phnx_get_device(cam);
    if (dev == NULL) {
        return TAO_ERROR;
    }
    // Camera must not be acquiring.
    if (cam->runlevel != 1) {
        invalid_runlevel(cam, __func__);
        return TAO_ERROR;
    }
    // Call virtual method.
    if (dev->ops->save_config == NULL) {
        tao_store_error(__func__, TAO_UNSUPPORTED);
        return TAO_ERROR;
    }
    if (dev->ops->save_config(dev, id) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

// Retrieve the current camera configuration.  The camera should be locked to
// ensure that configuration does not change but it does not harm if it is not
// locked.  There are no constraints on the camera state.
void phnx_get_configuration(
    tao_camera* cam,
    phnx_config* cfg)
{
    // If camera is a "Phoenix" camera, copy all the settings, otherwise only
    // copy the common ones and set the "extra" settings to zero.
    if (cam->ops == &ops) {
        phnx_device* dev = (phnx_device*)cam;
        cfg->extra = dev->extra;
    } else {
        memset(&cfg->extra, 0, sizeof(phnx_extra_config));
    }
    cfg->base = cam->info.config;
}

tao_status phnx_set_configuration(
    tao_camera* cam,
    const phnx_config* cfg)
{
    // Camera must be a "Phoenix" camera.
    phnx_device* dev = phnx_get_device(cam);
    if (dev == NULL) {
        return TAO_ERROR;
    }
    if (dev->ops->set_config == NULL) {
        tao_store_error(__func__, TAO_UNSUPPORTED);
        return TAO_ERROR;
    }
    if (dev->ops->set_config(dev, cfg) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

//-----------------------------------------------------------------------------
// UTILITIES

phnx_device* phnx_get_device(
    tao_camera* cam)
{
    if (cam == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return NULL;
    }
    if (cam->ops != &ops) {
        tao_store_error(__func__, TAO_BAD_DEVICE);
        return NULL;
    }
    return (phnx_device*)cam;
}

tao_encoding phnx_best_buffer_encoding(
    tao_encoding enc)
{
    tao_encoding col = TAO_ENCODING_COLORANT(enc);
    tao_encoding pxl = TAO_ENCODING_BITS_PER_PIXEL(enc);
    tao_encoding flg = TAO_ENCODING_FLAGS(enc);
    tao_encoding pad = flg & (TAO_ENCODING_FLAGS_MSB_PAD|
                              TAO_ENCODING_FLAGS_LSB_PAD);
    switch (col) {
    case TAO_COLORANT_MONO:
    case TAO_COLORANT_BAYER_RGGB:
    case TAO_COLORANT_BAYER_GRBG:
    case TAO_COLORANT_BAYER_GBRG:
    case TAO_COLORANT_BAYER_BGGR:
        // Each pixel is gray or has one color.
        if (flg == pad && (pxl & 0x7) == 0) {
            // Pixel size is a multiple of 8 bits and only padding flags.
            return TAO_ENCODING_2_(col, pxl);
        }
        if (flg == pad && pad == TAO_ENCODING_FLAGS_MSB_PAD) {
            // Pixel size is not a multiple of 8 bits but zero upper padded.
            // We just round the pixel size to the next multiple of 8 bits.
            return TAO_ENCODING_2_(col, TAO_ROUND_UP(pxl, 8));
        }
        break;
    case TAO_COLORANT_RGB:
    case TAO_COLORANT_BGR:
    case TAO_COLORANT_ARGB:
    case TAO_COLORANT_RGBA:
    case TAO_COLORANT_ABGR:
    case TAO_COLORANT_BGRA:
        // Each pixel has 3 or 4 components.
        if (flg == pad && (pxl & 0x7) == 0) {
            // Pixel size is a multiple of 8 bits and only padding flags.
            return TAO_ENCODING_2_(col, pxl);
        }
        break;
    case TAO_COLORANT_YUV444:
        if (enc == TAO_ENCODING_YUV444) {
            return enc;
        }
        break;
    case TAO_COLORANT_YUV422:
        if (enc == TAO_ENCODING_YUV422) {
            return enc;
        }
        break;
    case TAO_COLORANT_YUV411:
        if (enc == TAO_ENCODING_YUV411) {
            return enc;
        }
        break;
    case TAO_COLORANT_YUV420P:
        if (enc == TAO_ENCODING_YUV420P) {
            return enc;
        }
        break;
    case TAO_COLORANT_YUV420SP:
        if (enc == TAO_ENCODING_YUV420SP) {
            return enc;
        }
        break;
    }
    return TAO_ENCODING_UNKNOWN;
}

char* phnx_format_connection_settings(
    char* str,
    const phnx_connection* con)
{
    sprintf(str, "%d*%d", TAO_MAX(0, con->channels), TAO_MAX(0, con->speed));
    return str;
}

tao_status phnx_parse_connection_settings(
    phnx_connection* con,
    const char* str)
{
    long star, val1, val2;
    char* ptr;

    // No spaces are allowed (because of the splitting of command line
    // arguments).
    star = -1;
    for (long i = 0; str[i] != '\0'; ++i) {
        if (isspace(str[i])) {
            goto error;
        }
        if (str[i] == '*') {
            star = i;
        }
    }
    if (star < 0) {
        goto error;
    }
    val1 = strtol(str, &ptr, 10);
    if (ptr[0] != '*' || val1 < 1 || val1 > UINT32_MAX) {
        goto error;
    }
    val2 = strtol(str + star + 1, &ptr, 10);
    if (ptr[0] != '\0' || val2 < 1 || val2 > UINT32_MAX) {
        goto error;
    }
    if (con != NULL) {
        con->channels = val1;
        con->speed = val2;
    }
    return TAO_OK;

 error:
    errno = EINVAL;
    return TAO_ERROR;
}

static tao_status print_board_info(
    phnx_device* dev,
    const char* pfx, FILE* stream);

tao_status phnx_print_camera_info(
    tao_camera* cam,
    FILE* stream)
{
    // Minimal checks and set defaults.
    phnx_device* dev = phnx_get_device(cam);
    if (dev == NULL) {
        return TAO_ERROR;
    }
    if (cam->runlevel < 1 || cam->runlevel > 2) {
        invalid_runlevel(cam, __func__);
        return TAO_ERROR;
    }
    if (stream == NULL) {
        stream = stdout;
    }

    // Print information and settings.
    tao_status status = TAO_OK;
    fprintf(stream, "Camera vendor: %s\n",
            (dev->vendor[0] != '\0' ? dev->vendor : "Unknown"));
    fprintf(stream, "Camera model:  %s\n",
            (dev->model[0] != '\0' ? dev->model : "Unknown"));
    fprintf(stream, "CoaXPress camera: %s\n",
            (dev->coaxpress ? "yes" : "no"));
    fprintf(stream, "Board information:\n");
    if (print_board_info(dev, "    ", stream) != TAO_OK) {
        status = TAO_ERROR;
    }
    char buffer[PHNX_CONNECTION_STRING_SIZE];
    fprintf(stream, "Connection: %s Mbps\n",
            phnx_format_connection_settings(buffer, &dev->extra.connection));
    fprintf(stream, "Bits per pixel: %d\n",
            (int)TAO_ENCODING_BITS_PER_PIXEL(cam->info.config.sensorencoding));
    fprintf(stream, "Sensor size: %ld × %ld pixels\n",
            cam->info.sensorwidth,  cam->info.sensorheight);
    fprintf(stream, "Region of interest: %ld × %ld at (%ld,%ld)\n",
            cam->info.config.roi.width, cam->info.config.roi.height,
            cam->info.config.roi.xoff, cam->info.config.roi.yoff);
    fprintf(stream, "Active region:      %ld × %ld at (%ld,%ld)\n",
            dev->roi.width, dev->roi.height,
            dev->roi.xoff, dev->roi.yoff);
    fprintf(stream, "Detector bias: %5.1f\n", dev->extra.bias);
    fprintf(stream, "Detector gain: %5.1f\n", dev->extra.gain);
    fprintf(stream, "Exposure time: %g s\n", cam->info.config.exposuretime);
    fprintf(stream, "Frame framerate: %.1f Hz\n", cam->info.config.framerate);
    if (! isnan(cam->info.temperature)) {
        fprintf(stream, "Detector temperature: %.1f °C\n",
                cam->info.temperature);
    }
    return status;
}

static tao_status print_board_info(
    phnx_device* dev,
    const char* pfx, FILE* stream)
{
    // Retrieve information.
    etParamValue bits;
     if (phx_get(dev, PHX_BOARD_INFO, &bits) != TAO_OK) {
        return TAO_ERROR;
    }
#define PRT(msk, txt)                                   \
    do {                                                \
        if ((bits & (msk)) == (msk)) {                  \
            fprintf(stream, "%s%s\n", pfx, txt);        \
        }                                               \
    } while (false)
#define CASE(cst, txt) case cst: fprintf(stream, "%s%s\n", pfx, txt); break
#define CASE1(cst) CASE(cst, #cst)

    PRT(PHX_BOARD_INFO_LVDS, "Board has LVDS camera interface");
    PRT(PHX_BOARD_INFO_CL, "Board has Camera Link interface");
    PRT(PHX_BOARD_INFO_CL_BASE,
        "Board is using Camera Link Base interface only");
    PRT(PHX_BOARD_INFO_CL_MEDIUM,
        "Board is using Camera Link Medium interface");
    PRT(PHX_BOARD_INFO_CL_FULL, "Board is using Camera Link Full interface");
    PRT(PHX_BOARD_INFO_PCI_EXPRESS, "Board has PCI Express interface");
    PRT(PHX_BOARD_INFO_PCI_3V, "3V PCI Interface");
    PRT(PHX_BOARD_INFO_PCI_5V, "5V PCI Interface");
    PRT(PHX_BOARD_INFO_PCI_33M, "33MHz PCI Interface");
    PRT(PHX_BOARD_INFO_PCI_66M, "66MHz PCI Interface");
    PRT(PHX_BOARD_INFO_PCI_32B, "32bit PCI Interface");
    PRT(PHX_BOARD_INFO_PCI_64B, "64bit PCI Interface");
    PRT(PHX_BOARD_INFO_BOARD_3V, "Board is 3V compatible");
    PRT(PHX_BOARD_INFO_BOARD_5V, "Board is 5V compatible");
    PRT(PHX_BOARD_INFO_BOARD_33M, "Board is 33MHz compatible");
    PRT(PHX_BOARD_INFO_BOARD_66M, "Board is 66MHz compatible");
    PRT(PHX_BOARD_INFO_BOARD_32B, "Board is 32bit compatible");
    PRT(PHX_BOARD_INFO_BOARD_64B, "Board is 64bit compatible");
    PRT(PHX_BOARD_INFO_CHAIN_MASTER, "Board has chaining jumper set to Master");
    PRT(PHX_BOARD_INFO_CHAIN_SLAVE, "Board has chaining jumper set to Slave");

    if ((bits & PHX_BOARD_INFO_PCI_EXPRESS) != PHX_BOARD_INFO_PCI_EXPRESS) {
        etParamValue val;
        if (phx_get(dev, PHX_PCIE_INFO, &val) != TAO_OK) {
            return TAO_ERROR;
        }
        switch ((int)(val & PHX_EMASK_PCIE_INFO_LINK_GEN)) {
            CASE(PHX_PCIE_INFO_LINK_GEN1,
                 "PCI Express link operating at Gen 1 PCI Express speed");
            CASE(PHX_PCIE_INFO_LINK_GEN2,
                 "PCI Express link operating at Gen 2 PCI Express speed");
            CASE(PHX_PCIE_INFO_LINK_GEN3,
                 "PCI Express link operating at Gen 3 PCI Express speed");
        default:
            fprintf(stream, "%sUnknown PCI Express link generation\n", pfx);
        }
        switch ((int)(val & PHX_EMASK_PCIE_INFO_LINK_X)) {
            CASE(PHX_PCIE_INFO_LINK_X1,
                 "PCI Express link operating at x1 PCI Express width");
            CASE(PHX_PCIE_INFO_LINK_X2,
                 "PCI Express link operating at x2 PCI Express width");
            CASE(PHX_PCIE_INFO_LINK_X4,
                 "PCI Express link operating at x4 PCI Express width");
            CASE(PHX_PCIE_INFO_LINK_X8,
                 "PCI Express link operating at x8 PCI Express width");
            CASE(PHX_PCIE_INFO_LINK_X12,
                 "PCI Express link operating at x12 PCI Express width");
            CASE(PHX_PCIE_INFO_LINK_X16,
                 "PCI Express link operating at x16 PCI Express width");
            CASE(PHX_PCIE_INFO_LINK_X32,
                 "PCI Express link operating at x32 PCI Express width");
        default:
            fprintf(stream, "%sUnknown PCI Express link width\n", pfx);
        }
        switch ((int)(val & PHX_EMASK_PCIE_INFO_FG_GEN)) {
            CASE(PHX_PCIE_INFO_FG_GEN1,
                 "Frame grabber only supports Gen 1 PCI Express");
            CASE(PHX_PCIE_INFO_FG_GEN2,
                 "Frame grabber supports Gen 2 PCI Express");
            CASE(PHX_PCIE_INFO_FG_GEN3,
                 "Frame grabber supports Gen 3 PCI Express");
        }
        switch ((int)(val & PHX_EMASK_PCIE_INFO_FG_X)) {
            CASE(PHX_PCIE_INFO_FG_X1,  "Frame grabber x1");
            CASE(PHX_PCIE_INFO_FG_X2,  "Frame grabber x2");
            CASE(PHX_PCIE_INFO_FG_X4,  "Frame grabber x4");
            CASE(PHX_PCIE_INFO_FG_X8,  "Frame grabber x8");
            CASE(PHX_PCIE_INFO_FG_X12, "Frame grabber x12");
            CASE(PHX_PCIE_INFO_FG_X16, "Frame grabber x16");
            CASE(PHX_PCIE_INFO_FG_X32, "Frame grabber x32");
        }
        switch ((int)(val & PHX_EMASK_PCIE_INFO_SLOT_GEN)) {
            CASE(PHX_PCIE_INFO_SLOT_GEN1, "Slot Gen1");
            CASE(PHX_PCIE_INFO_SLOT_GEN2, "Slot Gen2");
            CASE(PHX_PCIE_INFO_SLOT_GEN3, "Slot Gen3");
        }
        switch ((int)(val & PHX_EMASK_PCIE_INFO_SLOT_X)) {
            CASE(PHX_PCIE_INFO_SLOT_X1,  "Slot x1");
            CASE(PHX_PCIE_INFO_SLOT_X2,  "Slot x2");
            CASE(PHX_PCIE_INFO_SLOT_X4,  "Slot x4");
            CASE(PHX_PCIE_INFO_SLOT_X8,  "Slot x8");
            CASE(PHX_PCIE_INFO_SLOT_X12, "Slot x12");
            CASE(PHX_PCIE_INFO_SLOT_X16, "Slot x16");
            CASE(PHX_PCIE_INFO_SLOT_X32, "Slot x32");
        }
    }

    // CoaXPress information.
    if (phx_get(dev, PHX_CXP_INFO, &bits) != TAO_OK) {
        return TAO_ERROR;
    }
    PRT(PHX_CXP_CAMERA_DISCOVERED,
        "The CoaXPress camera has completed discovery");
    PRT(PHX_CXP_CAMERA_IS_POCXP,
        "The CoaXPress camera is powered via PoCXP from the frame grabber");
    PRT(PHX_CXP_POCXP_UNAVAILABLE,
        "There is no power to the frame grabber to provide PoCXP to a camera");
    PRT(PHX_CXP_POCXP_TRIPPED,
        "The PoCXP supply to the camera has been shutdown because high "
        "current was detected");
    int links[4], nlinks = 0;
    if ((bits & PHX_CXP_LINK1_USED) != 0) {
        links[nlinks++] = 1;
    }
    if ((bits & PHX_CXP_LINK2_USED) != 0) {
        links[nlinks++] = 2;
    }
    if ((bits & PHX_CXP_LINK3_USED) != 0) {
        links[nlinks++] = 3;
    }
    if ((bits & PHX_CXP_LINK4_USED) != 0) {
        links[nlinks++] = 4;
    }
    if (nlinks == 0) {
        fprintf(stream, "%sNo CoaXPress links in use\n", pfx);
    } else if (nlinks == 1) {
        fprintf(stream, "%sCoaXPress link %d in use\n", pfx, links[0]);
    } else {
        fprintf(stream, "%sCoaXPress links ", pfx);
        for (int i = 0; i < nlinks; ++i) {
            fprintf(stream, "%d%s", links[i],
                    (i < nlinks - 2 ? ", " :
                     (i == nlinks - 2 ? " and " : " are in use\n")));
        }
    }
    PRT(PHX_CXP_LINK1_MASTER, "CoaXPress link 1 is the master link");
    PRT(PHX_CXP_LINK2_MASTER, "CoaXPress link 2 is the master link");
    PRT(PHX_CXP_LINK3_MASTER, "CoaXPress link 3 is the master link");
    PRT(PHX_CXP_LINK4_MASTER, "CoaXPress link 4 is the master link");

    if (bits != 0) {
        etParamValue val;
        if (phx_get(dev, PHX_CXP_BITRATE, &val) != TAO_OK) {
            return TAO_ERROR;
        }
        switch ((int)val) {
            CASE(PHX_CXP_BITRATE_UNKNOWN, "No CoaXPress camera is connected, "
                 "or a camera has not completed discovery");
            CASE(PHX_CXP_BITRATE_CXP1, "Bitrate is 1250 Mbps");
            CASE(PHX_CXP_BITRATE_CXP2, "Bitrate is 2500 Mbps");
            CASE(PHX_CXP_BITRATE_CXP3, "Bitrate is 3125 Mbps");
            CASE(PHX_CXP_BITRATE_CXP5, "Bitrate is 5000 Mbps");
            CASE(PHX_CXP_BITRATE_CXP6, "Bitrate is 6250 Mbps");
        }
        if (phx_get(dev, PHX_CXP_DISCOVERY, &val) != TAO_OK) {
            return TAO_ERROR;
        }
        switch ((int)val) {
            CASE(PHX_CXP_DISCOVERY_UNKNOWN, "No CoaXPress camera is connected, "
                 "or a camera has not completed discovery");
            CASE(PHX_CXP_DISCOVERY_1X,
                 "The camera is using a single CoaXPress link");
            CASE(PHX_CXP_DISCOVERY_2X,
                 "The camera is using two CoaXPress links");
            CASE(PHX_CXP_DISCOVERY_4X,
                 "The camera is using four CoaXPress links");
        }
        if (phx_get(dev, PHX_CXP_BITRATE_MODE, &val) != TAO_OK) {
            return TAO_ERROR;
        }
        switch ((int)val) {
            CASE1(PHX_CXP_BITRATE_MODE_AUTO);
            CASE1(PHX_CXP_BITRATE_MODE_CXP1);
            CASE1(PHX_CXP_BITRATE_MODE_CXP2);
            CASE1(PHX_CXP_BITRATE_MODE_CXP3);
            CASE1(PHX_CXP_BITRATE_MODE_CXP5);
            CASE1(PHX_CXP_BITRATE_MODE_CXP6);
        }
        if (phx_get(dev, PHX_CXP_DISCOVERY_MODE, &val) != TAO_OK) {
            return TAO_ERROR;
        }
        switch ((int)val) {
            CASE1(PHX_CXP_DISCOVERY_MODE_AUTO);
            CASE1(PHX_CXP_DISCOVERY_MODE_1X);
            CASE1(PHX_CXP_DISCOVERY_MODE_2X);
            CASE1(PHX_CXP_DISCOVERY_MODE_4X);
        }
        if (phx_get(dev, PHX_CXP_POCXP_MODE, &val) != TAO_OK) {
            return TAO_ERROR;
        }
        switch ((int)val) {
            CASE1(PHX_CXP_POCXP_MODE_AUTO);
            CASE1(PHX_CXP_POCXP_MODE_OFF);
            CASE1(PHX_CXP_POCXP_MODE_TRIP_RESET);
            CASE1(PHX_CXP_POCXP_MODE_FORCEON);
        }
    }
#undef CASE
#undef CASE1
#undef PRT
    return TAO_OK;
}
