// startup.c -
//
// Helpers to build a program that opens and configures a camera connected to a
// Phoenix frame grabber.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2017-2022, Éric Thiébaut.
// Copyright (C) 2016, Éric Thiébaut & Jonathan Léger.

#include "tao-errors.h"
#include "tao-phoenix.h"

#include <stdarg.h>
#include <stdio.h>
#include <string.h>

void phnx_show_config_id_option(
    FILE* file,
    const tao_option* opt)
{
    if (*(int*)opt->ptr >= 0) {
        fprintf(file, "%d", *(int*)opt->ptr);
    } else {
        fputs("none", file);
    }
}

bool phnx_parse_bias_option(
    const tao_option* opt,
    char* args[])
{
    return tao_parse_double_option(opt, args) && *(double*)opt->ptr >= 0;
}

bool phnx_parse_gain_option(
    const tao_option* opt,
    char* args[])
{
    return tao_parse_double_option(opt, args) && *(double*)opt->ptr >= 1;
}

void phnx_show_frequency_option(
    FILE* file,
    const tao_option* opt)
{
    tao_show_double_option(file, opt);
    fputs(" Hz", file);
}

bool phnx_parse_framerate_option(
    const tao_option* opt,
    char* args[])
{
    return tao_parse_double_option(opt, args) && *(double*)opt->ptr > 0 &&
        *(double*)opt->ptr <= INT32_MAX;
}

void phnx_show_seconds_option(
    FILE* file,
    const tao_option* opt)
{
    tao_show_double_option(file, opt);
    fputs(" s", file);
}

bool phnx_parse_exposuretime_option(
    const tao_option* opt,
    char* args[])
{
    return tao_parse_double_option(opt, args) && *(double*)opt->ptr >= 0 &&
        *(double*)opt->ptr <= INT32_MAX;
}

void phnx_show_encoding_option(
    FILE* file,
    const tao_option* opt)
{
    char buf[TAO_ENCODING_STRING_SIZE], *str;

    if (*(tao_encoding*)opt->ptr == TAO_ENCODING_UNKNOWN) {
        str = "auto";
    } else if (tao_format_encoding(buf, *(tao_encoding*)opt->ptr) == TAO_OK) {
        str = buf;
    } else {
        str = "InvalidEncoding";
    }
    fputs(str, file);
}

bool phnx_parse_encoding_option(
    const tao_option* opt,
    char* args[])
{
    *(tao_encoding*)opt->ptr = tao_parse_encoding(args[0]);
    return *(tao_encoding*)opt->ptr != TAO_ENCODING_UNKNOWN;
}

void phnx_show_connection_option(
    FILE* file,
    const tao_option* opt)
{
    char buf[PHNX_CONNECTION_STRING_SIZE];
    phnx_connection* connection = (phnx_connection*)opt->ptr;
    if (connection->speed > 0 && connection->channels > 0) {
        phnx_format_connection_settings(buf, connection);
        fputs(buf, file);
        fputs(" Mbps", file);
    } else {
        fputs("auto", file);
    }
}

bool phnx_parse_connection_option(
    const tao_option* opt,
    char* args[])
{
    phnx_connection* connection = (phnx_connection*)opt->ptr;
    return phnx_parse_connection_settings(connection, args[0]) == TAO_OK;
}

bool phnx_parse_nbufs_option(
    const tao_option* opt,
    char* args[])
{
    return tao_parse_int_option(opt, args) && *(int*)opt->ptr >= 2;
}

static tao_help_info help_info;
static int load_id = -1;
static int save_id = -1;

int phnx_start_program(
    tao_camera** cam,
    phnx_config* cfg,
    int argc,
    char* argv[],
    tao_option opts[],
    const char* descr,
    const char* args)
{
    // Initialize for help, save and load.
    help_info.program = tao_basename(argv[0]);
    help_info.args = args;
    help_info.purpose = descr;
    help_info.output = stderr;
    help_info.options = opts;
    for (int k = 0; opts[k].name != NULL; ++k) {
        if (opts[k].pass != 1 && opts[k].pass != 2) {
            fprintf(stderr,
                    "%s: malformed option table (pass must be 1 or 2)\n",
                    help_info.program);
            exit(EXIT_FAILURE);
        }
        if (opts[k].ptr == NULL) {
            if (strcmp(opts[k].name, "help") == 0) {
                opts[k].ptr = (void*)&help_info;
            } else if (strcmp(opts[k].name, "save") == 0) {
                opts[k].ptr = &save_id;
            } else if (strcmp(opts[k].name, "load") == 0) {
                opts[k].ptr = &load_id;
            }
        }
    }

    // Parse the command line options and configure the camera.  Two passes are
    // needed: first pass to get the configuration to load, second pass (after
    // opening the camera) to set the options and, possibly, to save the
    // configuration.

    // First pass to options.
    argc = tao_parse_options(NULL, argc, argv, 1, opts);
    if (argc < 0) {
        exit(EXIT_FAILURE);
    }

    // Open the camera and retrieve its configuration.
    *cam = phnx_create_camera(NULL, NULL, NULL);
    if (*cam == NULL) {
        fprintf(stderr, "Failed to create the camera.\n");
        tao_report_error();
        return EXIT_FAILURE;
    }
    if (load_id >= 0 && phnx_load_configuration(*cam, load_id) != TAO_OK) {
        fprintf(stderr, "Failed to load preset configuration.\n");
        return EXIT_FAILURE;
    }
    phnx_get_configuration(*cam, cfg);

    // Second pass to options.
    argc = tao_parse_options(NULL, argc, argv, 2, opts);
    if (argc < 0) {
        exit(EXIT_FAILURE);
    }

    // Configure the camera according to options.
    if (phnx_set_configuration(*cam, cfg) != TAO_OK) {
        fprintf(stderr, "Failed to configure the camera.\n");
        tao_report_error();
        return EXIT_FAILURE;
    }
    if (save_id >= 0 && phnx_save_configuration(*cam, save_id) != TAO_OK) {
        fprintf(stderr, "Failed to save preset configuration.\n");
        tao_report_error();
        return EXIT_FAILURE;
    }

    return argc;
}
