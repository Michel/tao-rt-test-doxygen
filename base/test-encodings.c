// test-encoding.c --
//
// Test pixel encodings in TAO library.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2021, Éric Thiébaut.

#include <tao-encodings.h>

#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define IS_SIGNED(T) ((T)(-1) < (T)(0))

#define test(expr)                                              \
    do {                                                        \
        if (! (expr)) {                                         \
            fprintf(stderr, "assertion failed: %s\n", #expr);   \
            ++nerrors;                                          \
        }                                                       \
        ++ntests;                                               \
    } while (false)

static bool convert(
    tao_encoding enc)
{
    char name[TAO_ENCODING_STRING_SIZE];
    return (tao_format_encoding(name, enc) == TAO_OK &&
            tao_parse_encoding(name) == enc);

}

static void print2(
    FILE* output,
    const char* s1,
    long pad,
    const char* s2,
    int c)
{
    long len = (s1 != NULL ? strlen(s1) : 0);
    fprintf(output, "  %s", (len > 0 ? s1 : ""));
    if (c == ':') {
        fputc(c, output);
        while (++len <= pad) {
            fputc(' ', output);
        }
    } else if (c == '>') {
        fputc(' ', output);
        while (++len <= pad) {
            fputc('-', output);
        }
        fputc(c, output);
        fputc(' ', output);
    } else if (c == '.') {
        fputc(' ', output);
        while (++len <= pad) {
            fputc('.', output);
        }
        fputc(' ', output);
    } else {
        while (++len <= pad) {
            fputc(' ', output);
        }
        fputc(' ', output);
    }
    fprintf(output, "%s\n", s2);
}

#define numberof(arr) sizeof(arr)/sizeof(arr[0])

int main(
    int argc,
    char* argv[])
{
    long nerrors = 0, ntests = 0;
    char name[TAO_ENCODING_STRING_SIZE];
    char macro[200];
    const char* pfxs[] = {"Raw","Mono"}; // must mach encodings below
    tao_encoding cols[] = {TAO_COLORANT_RAW,TAO_COLORANT_MONO};
    const int ncols = numberof(cols);
    int pxls[] = {8,10,12,16,32};
    const int npxls = numberof(pxls);
    int pkts[] = {8,16,24,32,48};
    const int npkts = numberof(pkts);
    unsigned int flgs[] = {0,1,2,4};
    const int nflgs = numberof(flgs);

    // Basic tests.
    test(tao_size_of_eltype(TAO_INT8)   == 1);
    test(tao_size_of_eltype(TAO_UINT8)  == 1);
    test(tao_size_of_eltype(TAO_INT16)  == 2);
    test(tao_size_of_eltype(TAO_UINT16) == 2);
    test(tao_size_of_eltype(TAO_INT32)  == 4);
    test(tao_size_of_eltype(TAO_UINT32) == 4);
    test(tao_size_of_eltype(TAO_INT64)  == 8);
    test(tao_size_of_eltype(TAO_UINT64) == 8);
    test(tao_size_of_eltype(TAO_FLOAT)  == sizeof(float));
    test(tao_size_of_eltype(TAO_DOUBLE) == sizeof(double));

    test(strcmp(tao_name_of_eltype(TAO_INT8),   "int8_t"  ) == 0);
    test(strcmp(tao_name_of_eltype(TAO_UINT8),  "uint8_t" ) == 0);
    test(strcmp(tao_name_of_eltype(TAO_INT16),  "int16_t" ) == 0);
    test(strcmp(tao_name_of_eltype(TAO_UINT16), "uint16_t") == 0);
    test(strcmp(tao_name_of_eltype(TAO_INT32),  "int32_t" ) == 0);
    test(strcmp(tao_name_of_eltype(TAO_UINT32), "uint32_t") == 0);
    test(strcmp(tao_name_of_eltype(TAO_INT64),  "int64_t" ) == 0);
    test(strcmp(tao_name_of_eltype(TAO_UINT64), "uint64_t") == 0);
    test(strcmp(tao_name_of_eltype(TAO_FLOAT),  "float"   ) == 0);
    test(strcmp(tao_name_of_eltype(TAO_DOUBLE), "double"  ) == 0);

    // Show some encoding names.
#define show(enc)                                               \
    do {                                                        \
        print2(stdout, #enc, 40,                                \
               (tao_format_encoding(name, enc) == TAO_OK ?      \
                name : "error"), '>');                          \
    } while (false)
    show(TAO_ENCODING_MONO(8));
    show(TAO_ENCODING_MONO(10));
    show(TAO_ENCODING_RAW_PKT(11,16));
    show(TAO_ENCODING_YUV444);
    show(TAO_ENCODING_YUV422);
    show(TAO_ENCODING_YUV411);
    show(TAO_ENCODING_YUV420P);
    show(TAO_ENCODING_YUV420SP);
    show(TAO_ENCODING_ANDOR_MONO8);
    show(TAO_ENCODING_ANDOR_MONO12);
    show(TAO_ENCODING_ANDOR_MONO12PACKED);
    show(TAO_ENCODING_ANDOR_MONO12CODED);
    show(TAO_ENCODING_ANDOR_MONO12CODEDPACKED);
    show(TAO_ENCODING_ANDOR_MONO16);
    show(TAO_ENCODING_ANDOR_MONO32);
    show(TAO_ENCODING_ANDOR_RGB8PACKED);
    show(TAO_ENCODING_ANDOR_MONO22PARALLEL);
    show(TAO_ENCODING_ANDOR_MONO22PACKEDPARALLEL);
    show(TAO_ENCODING_BAYER_RGGB(8));
    show(TAO_ENCODING_BAYER_GRBG(8));
    show(TAO_ENCODING_FLOAT(32));
#undef show
    fputs("\n", stdout);
    fflush(stdout);

    // Parsing tests.
    test(tao_parse_encoding("YUV444") == TAO_ENCODING_YUV444);
    test(tao_parse_encoding("yuv444") == TAO_ENCODING_YUV444);
    test(tao_parse_encoding("yuv422") == TAO_ENCODING_YUV422);
    test(tao_parse_encoding("yuv411") == TAO_ENCODING_YUV411);
    test(tao_parse_encoding("YUV420p") == TAO_ENCODING_YUV420P);
    test(tao_parse_encoding("YUV420sp") == TAO_ENCODING_YUV420SP);
    test(tao_parse_encoding("yuv420sp") == TAO_ENCODING_YUV420SP);
    test(tao_parse_encoding("YUV420SP") == TAO_ENCODING_YUV420SP);

    test(tao_parse_encoding("Mono(8)") == TAO_ENCODING_MONO(8));
    test(tao_parse_encoding("Mono(12,12)") == TAO_ENCODING_MONO(12));
    test(tao_parse_encoding("Mono(10,10,0)") == TAO_ENCODING_MONO(10));
    test(tao_parse_encoding("Mono(10,10,0x0)") == TAO_ENCODING_MONO(10));
    test(tao_parse_encoding("Mono(10,16)") == TAO_ENCODING_MONO_PKT(10,16));
    test(tao_parse_encoding("Mono(10,16,0)") == TAO_ENCODING_MONO_PKT(10,16));
    test(tao_parse_encoding("Mono(10,16,0x0)") == TAO_ENCODING_MONO_PKT(10,16));

    test(tao_parse_encoding("Raw(8)") == TAO_ENCODING_RAW(8));
    test(tao_parse_encoding("Raw(12,12)") == TAO_ENCODING_RAW(12));
    test(tao_parse_encoding("Raw(10,10,0)") == TAO_ENCODING_RAW(10));
    test(tao_parse_encoding("Raw(10,10,0x0)") == TAO_ENCODING_RAW(10));
    test(tao_parse_encoding("Raw(10,16)") == TAO_ENCODING_RAW_PKT(10,16));
    test(tao_parse_encoding("Raw(10,16,0)") == TAO_ENCODING_RAW_PKT(10,16));
    test(tao_parse_encoding("Raw(10,16,0x0)") == TAO_ENCODING_RAW_PKT(10,16));
    test(tao_parse_encoding("Raw(10,16,0x1)") ==
         TAO_ENCODING_4_(TAO_COLORANT_RAW, 10,16, 0x1));

    for (int i = 0; i < ncols; ++i) {
        const char* pfx = pfxs[i];
        tao_encoding col = cols[i];
        for (int j = 0; j < npxls; ++j) {
            int pxl = pxls[i];
            for (int k = 0; k < npkts; ++k) {
                int pkt = pkts[k];
                if (pkt < pxl) {
                    continue;
                }
                for (int l = 0; l < nflgs; ++l) {
                    tao_encoding enc;
                    unsigned int flg = flgs[l];
                    int n = (flg != 0 ? 1 : (pkt != pxl ? 2 : 3));
                    for (int pass = 1; pass <= n; ++pass) {
                        if (pass == 1) {
                            sprintf(name, "%s(%d,%d,0x%x)", pfx, pxl, pkt, flg);
                            enc = TAO_ENCODING_4_(col, pxl, pkt, flg);
                            sprintf(macro, "TAO_ENCODING_4_(%d,%d,%d,0x%x)",
                                    col, pxl, pkt, flg);
                        } else if (pass == 2) {
                            sprintf(name, "%s(%d,%d)", pfx, pxl, pkt);
                            enc = TAO_ENCODING_3_(col, pxl, pkt);
                            sprintf(macro, "TAO_ENCODING_3_(%d,%d,%d)",
                                    col, pxl, pkt);
                        } else {
                            sprintf(name, "%s(%d)", pfx, pxl);
                            enc = TAO_ENCODING_2_(col, pxl);
                            sprintf(macro, "TAO_ENCODING_2_(%d,%d)",
                                    col, pxl);
                        }
                        ++ntests;
                        if (tao_parse_encoding(name) != enc) {
                            fprintf(stderr, "assertion failed: "
                                    "tao_parse_encoding(%s) == %s\n",
                                    name, macro);
                            ++nerrors;
                        }
                        ++ntests;
                        if (TAO_ENCODING_COLORANT(enc) != col) {
                            fprintf(stderr, "assertion failed: "
                                    "TAO_ENCODING_COLORANT(%s) == %d\n",
                                    macro, (int)col);
                            ++nerrors;
                        }
                        ++ntests;
                        if (TAO_ENCODING_BITS_PER_PIXEL(enc) != pxl) {
                            fprintf(stderr, "assertion failed: "
                                    "TAO_ENCODING_BITS_PER_PIXEL(%s) == %d\n",
                                    macro, pxl);
                            ++nerrors;
                        }
                        ++ntests;
                        if (TAO_ENCODING_BITS_PER_PACKET(enc) != pkt) {
                            fprintf(stderr, "assertion failed: "
                                    "TAO_ENCODING_BITS_PER_PACKET(%s) == %d\n",
                                    macro, pkt);
                            ++nerrors;
                        }
                        ++ntests;
                        if (TAO_ENCODING_FLAGS(enc) != flg) {
                            fprintf(stderr, "assertion failed: "
                                    "TAO_ENCODING_FLAGS(%s) == 0x%x\n",
                                    macro, flg);
                            ++nerrors;
                        }
                    }
                }
            }
        }
    }

    // Conversions.
    test(convert(TAO_ENCODING_YUV444));
    test(convert(TAO_ENCODING_YUV422));
    test(convert(TAO_ENCODING_YUV411));
    test(convert(TAO_ENCODING_YUV420P));
    test(convert(TAO_ENCODING_YUV420SP));
    test(convert(TAO_ENCODING_ANDOR_MONO8));
    test(convert(TAO_ENCODING_ANDOR_MONO12));
    test(convert(TAO_ENCODING_ANDOR_MONO12PACKED));
    test(convert(TAO_ENCODING_ANDOR_MONO12CODED));
    test(convert(TAO_ENCODING_ANDOR_MONO12CODEDPACKED));
    test(convert(TAO_ENCODING_ANDOR_MONO16));
    test(convert(TAO_ENCODING_ANDOR_MONO32));
    test(convert(TAO_ENCODING_ANDOR_RGB8PACKED));
    test(convert(TAO_ENCODING_ANDOR_MONO22PARALLEL));
    test(convert(TAO_ENCODING_ANDOR_MONO22PACKEDPARALLEL));

    // Summary.
    fprintf(stdout, "%ld test(s) passed\n", ntests - nerrors);
    fprintf(stdout, "%ld test(s) failed\n", nerrors);

    return (nerrors == 0 ? EXIT_SUCCESS : EXIT_FAILURE);
}
