// servers.c -
//
// Implement a server "class" in TAO library as an abstraction encapsulating an
// XPA server.  Apart from linking with XPA library, all XPA stuff is hidden by
// this implementation.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2018-2022, Éric Thiébaut.

#include "tao-servers-private.h"
#include "tao-errors.h"
#include "tao-macros.h"
#include "tao-locks.h"

#include "config.h"

#include <errno.h>
#include <math.h>
#include <stdarg.h>
#include <string.h>
#include <xpa.h>

static tao_mutex mutex = TAO_MUTEX_INITIALIZER;
static bool initialized = false;

void tao_clear_server_buffer(
    tao_server* srv)
{
    tao_buffer_clear(&srv->databuf);
}

tao_status tao_put_string_to_server_buffer(
    tao_server* srv,
    const char* str,
    long len)
{
    return tao_buffer_append_string(&srv->databuf, str, len);
}

tao_status tao_print_to_server_buffer(
    tao_server* srv,
    const char* format,
    ...)
{
    tao_status status;
    va_list args;

    va_start(args, format);
    status = tao_buffer_vprintf(&srv->databuf, format, args);
    va_end(args);
    return status;
}

tao_status tao_append_to_server_buffer(
    tao_server* srv,
    const void* ptr,
    long siz)
{
    return tao_buffer_append_bytes(&srv->databuf, ptr, siz);
}

void tao_set_static_reply_message(
    tao_server* srv,
    const char* msg)
{
    srv->message = msg;
}

tao_status tao_set_reply_message(
    tao_server* srv,
    const char* msg,
    long len)
{
    tao_buffer_clear(&srv->mesgbuf);
    if (tao_buffer_append_string(&srv->mesgbuf, msg, len) != TAO_OK) {
        srv->message = NULL;
        return TAO_ERROR;
    }
    srv->message = tao_buffer_get_contents(&srv->mesgbuf, NULL);
    return TAO_OK;
}

tao_status tao_format_reply_message(
    tao_server* srv,
    const char* format,
    ...)
{
    tao_status status;
    va_list args;
    tao_buffer_clear(&srv->mesgbuf);
    va_start(args, format);
    status = tao_buffer_vprintf(&srv->mesgbuf, format, args);
    va_end(args);
    if (status != TAO_OK) {
        srv->message = NULL;
        return TAO_ERROR;
    }
    srv->message = tao_buffer_get_contents(&srv->mesgbuf, NULL);
    return TAO_OK;
}

// This function converts the last error that may have occurred for the calling
// thread into messages written in the server dynamical buffer and then set XPA
// error message with the contents of this buffer.
static const char* get_error_message(
    tao_server* srv)
{
    const char* message;

    if (srv->message != NULL) {
        // Use any message left if any.
        message = srv->message;
        srv->message = NULL;
    } else {
        // Write error messages in the embedded dynamic buffer.
        tao_error* err = tao_get_last_error();
        tao_buffer_clear(&srv->mesgbuf);
        if (err->code != TAO_SUCCESS) {
            // Print error message to the i/o buffer.  A fatal error
            // results in case of failure here.
            if (tao_report_error_to_buffer(
                    &srv->mesgbuf, err, NULL, NULL) != TAO_OK) {
                tao_panic();
            }
            message = tao_buffer_get_contents(&srv->mesgbuf, NULL);
        } else {
            // There were no errors!
            message = "Failure reported but no error information!";
        }
    }
    return message;
}

#define SUCCESS   (0)
#define FAILURE  (-1)

static int callback_done(
    tao_server* srv,
    const char** argv,
    int status)
{
    tao_error* err = tao_get_last_error();
    tao_clear_error(err);
    if (status == SUCCESS) {
        // Normal result, a message is optional.
        if (srv->message != NULL) {
            XPAMessage((XPARec*)srv->handle, (char*)srv->message);
            srv->message = NULL;
        }
    } else {
        // Some errors occurred.
        XPAError((XPARec*)srv->handle, (char*)get_error_message(srv));
        status = FAILURE;
    }
    if (argv != NULL) {
        free(argv);
    }
    if (status == TAO_OK && err->code != TAO_SUCCESS) {
        status = TAO_ERROR;
    }
    return status;
}

// Callback to answer an XPAGet request.
static int send_callback(
    void* send_data,
    void* handle,
    char* command,
    char** bufptr,
    size_t* lenptr)
{
    tao_server* srv = (tao_server*)send_data;
    const char** argv = NULL;
    int argc, status = SUCCESS;

    // Initialization and sanity checks.
    srv->message = NULL;
    if (tao_get_last_error()->code != TAO_SUCCESS) {
        tao_report_error();
    }
    tao_buffer_clear(&srv->databuf);
    tao_buffer_clear(&srv->mesgbuf);
    if (srv->debuglevel > 1) {
        fprintf(stderr, "send: \"%s\"\n", command);
    }
    if (srv->handle != handle) {
        tao_set_static_reply_message(srv, "Server addresses do not match");
        status = FAILURE;
        goto done;
    }
    if (srv->send == NULL) {
        // This server takes no "get" commands.
        tao_set_static_reply_message(srv, "Unknown xpaget command");
        status = FAILURE;
        goto done;
    }

    // Split command in a list of arguments.
    argc = tao_split_command(&argv, command, -1);
    if (argc < 0) {
        status = FAILURE;
        goto done;
    }
    if (argc == 0) {
        // No commands, nothing to do.
        goto done;
    }
    if (srv->debuglevel > 2) {
        for (int i = 0; i < argc; ++i) {
            fprintf(stderr, "send: [%d] >>%s<<\n", i, argv[i]);
        }
    }

    // Execute the command.
    if (srv->send(srv, srv->send_ctx, argc, argv,
                  (void**)bufptr, lenptr) != TAO_OK) {
        status = FAILURE;
    }

 done:
    // Free resources, send message and return status.
    return callback_done(srv, argv, status);
}

// Callback to answer an XPASet request.
static int recv_callback(
    void* recv_data,
    void* handle,
    char* command,
    char* buf,
    size_t siz)
{
    tao_server* srv = (tao_server*)recv_data;
    const char** argv = NULL;
    int argc, status = SUCCESS;

    // Initialization and sanity checks.
    srv->message = NULL;
    if (tao_get_last_error()->code != TAO_SUCCESS) {
        tao_report_error();
    }
    tao_buffer_clear(&srv->databuf);
    tao_buffer_clear(&srv->mesgbuf);
    if (srv->debuglevel > 1) {
        fprintf(stderr, "recv: \"%s\" [%ld byte(s)]\n", command, (long)siz);
    }
    if (srv->handle != handle) {
        tao_set_static_reply_message(srv, "Server addresses do not match");
        status = FAILURE;
        goto done;
    }
    if (srv->recv == NULL) {
        // This server takes no "set" commands.
        tao_set_static_reply_message(srv, "Unknown xpaset command");
        status = FAILURE;
        goto done;
    }

    // Split command in a list of arguments.
    argc = tao_split_command(&argv, command, -1);
    if (argc < 0) {
        status = FAILURE;
        goto done;
    }
    if (argc == 0) {
        // No commands, nothing to do.
        goto done;
    }
    if (srv->debuglevel > 2) {
        for (int i = 0; i < argc; ++i) {
            fprintf(stderr, "recv: [%d] >>>%s<<<\n", i, argv[i]);
        }
    }

    // Execute the command.
    if (srv->recv(srv, srv->recv_ctx, argc, argv, buf, siz) != TAO_OK) {
        status = FAILURE;
    }

 done:
    // Free resources, send message and return status.
    return callback_done(srv, argv, status);
}

tao_status tao_set_reply_data_from_buffer(
    tao_server* srv,
    void** bufptr,
    size_t* lenptr,
    bool newline)
{
    size_t len;
    char* buf = tao_buffer_get_contents(&srv->databuf, &len);
    if (newline && len > 0 && buf[len-1] != '\n') {
        // Append a newline for more readable output when xapset/xpaget are
        // used from the command line.
        if (tao_buffer_append_char(&srv->databuf, '\n') != TAO_OK) {
            return TAO_ERROR;
        }
        buf = tao_buffer_get_contents(&srv->databuf, &len);
    }
    *bufptr = buf;
    *lenptr = len;
    return TAO_OK;
}

tao_server* tao_create_server(
    const char* address,
    const char* help,
    tao_init_callback* init,
    void* init_ctx,
    tao_send_callback* send,
    void* send_ctx,
    tao_recv_callback* recv,
    void* recv_ctx)
{
    tao_server* srv;
    char* separator;
    char* buffer;
    long offset, addrlen, helplen;
    tao_status status = TAO_OK;

    // Manage to call XPACleanup at exit
    if (tao_mutex_lock(&mutex) != TAO_OK) {
        return NULL;
    }
    if (! initialized) {
        if (atexit(XPACleanup) != 0) {
            tao_store_error("atexit(XPACleanup)", TAO_SYSTEM_ERROR);
            status = TAO_ERROR;
        } else {
            initialized = true;
        }
    }
    if (tao_mutex_unlock(&mutex) != TAO_OK) {
        status = TAO_ERROR;
    }
    if (status != TAO_OK) {
        return NULL;
    }

    // Allocate server structure.
    srv = (tao_server*)tao_calloc(1, sizeof(tao_server));
    if (srv == NULL) {
        return NULL;
    }
    tao_buffer_initialize(&srv->databuf);
    tao_buffer_initialize(&srv->mesgbuf);
    srv->running = false;
    srv->debuglevel = 0;
    srv->send = send;
    srv->send_ctx = send_ctx;
    srv->recv = recv;
    srv->recv_ctx = recv_ctx;

    // Check the address (it must be in the form "class:name"), allocate a
    // buffer large enough to store 2 copies of the address (the first copy is
    // for the full address, the second copy is for the class and the name
    // separately) plus the help string.  This copy is also used to hide to the
    // caller the fact that `XPANew` takes `char*` arguments instead of `const
    // char*`.
    addrlen = tao_strlen(address);
    helplen = tao_strlen(help);
    if (addrlen < 3 || (separator = strchr(address, ':')) == NULL ||
        (offset = (separator - address)) < 1 || offset >= addrlen - 2) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        goto error;
    }
    buffer = tao_malloc(2*addrlen + helplen + 3);
    if (buffer == NULL) {
        goto error;
    }
    memcpy(buffer, address, addrlen + 1);
    memcpy(buffer + addrlen + 1, address, addrlen + 1);
    buffer[addrlen + offset + 1] = '\0'; // erase separator
    if (helplen > 0) {
        memcpy(buffer + (addrlen + 1)*2, help, helplen + 1);
    } else {
        buffer[(addrlen + 1)*2] = '\0';
    }
    srv->address = buffer;
    srv->class_  = buffer + (addrlen + 1);
    srv->name    = buffer + (addrlen + offset + 2);
    srv->help    = buffer + (addrlen + 1)*2;

    // Create the XPA server. FIXME: check that "freebuf=false" implies that
    // XPA makes a copy of the buffer provided by the recv callback.
    errno = 0;
    srv->handle = XPANew((char*)srv->class_,
                         (char*)srv->name,
                         (char*)srv->help,
                         send_callback, srv, "acl=true,freebuf=false",
                         recv_callback, srv, "");
    if (srv->handle == NULL) {
        int code = errno;
        tao_store_error("XPANew", (code == 0 ? TAO_SYSTEM_ERROR : code));
        goto error;
    }
    XPAAtExit();
    if (init != NULL && init(srv, init_ctx) != TAO_OK) {
        goto error;
    }
    return srv;

 error:
    tao_destroy_server(srv);
    return NULL;
}

void tao_destroy_server(
    tao_server* srv)
{
    if (srv != NULL) {
        if (srv->handle != NULL) {
            (void)XPAFree((XPARec*)srv->handle);
        }
        if (srv->address != NULL) {
            free((void*)srv->address);
        }
        tao_buffer_destroy(&srv->databuf);
        tao_buffer_destroy(&srv->mesgbuf);
        free((void*)srv);
    }
}

static int seconds_to_milliseconds(
    double secs)
{
    if (isnan(secs)) {
        return INT_MIN;
    } else {
        double msecs = round(secs*1E3);
        if (msecs >= INT_MAX) {
            return INT_MAX;
        } else if (msecs <= INT_MIN) {
            return INT_MIN;
        } else {
            return (int)msecs;
        }
    }
}

int tao_poll_requests(
    double secs,
    int maxreq)
{
    int msec = seconds_to_milliseconds(secs);
    return XPAPoll(TAO_MAX(msec, 0), maxreq);
}

tao_status tao_run_server(
    tao_server* srv,
    double secs)
{
    if (srv == NULL) {
        return TAO_ERROR;
    }
    int msec = seconds_to_milliseconds(secs);
    msec = TAO_MAX(msec, 0);
    srv->running = true;
    while (srv->running) {
        XPAPoll(msec, 1);
    }
    return TAO_OK;
}

tao_status tao_create_and_run_server(
    const char* address,
    const char* help,
    double secs,
    tao_init_callback* init,
    void* init_ctx,
    tao_send_callback* send,
    void* send_ctx,
    tao_recv_callback* recv,
    void* recv_ctx)
{
    tao_error* err = tao_get_last_error();
    tao_clear_error(err);
    tao_server* srv = tao_create_server(address, help,
                                        init, init_ctx,
                                        send, send_ctx,
                                        recv, recv_ctx);
    if (srv == NULL) {
        return TAO_ERROR;
    }
    tao_status status = tao_run_server(srv, secs);
    tao_destroy_server(srv);
    if (status == TAO_OK && err->code != TAO_SUCCESS) {
        status = TAO_ERROR;
    }
    return status;
}

tao_buffer* tao_get_server_buffer(
    tao_server* srv)
{
    return &srv->databuf;
}

int tao_get_server_debuglevel(
    tao_server* srv)
{
    return srv->debuglevel;
}

void tao_set_server_debuglevel(
    tao_server* srv, int val)
{
    srv->debuglevel = (val > 0 ? val : 0);
}

bool tao_is_server_running(
    tao_server* srv)
{
    return (srv != NULL && srv->running);
}

void tao_abort_server(
    tao_server* srv)
{
    srv->running = false;
}

const char* tao_get_server_address(
    tao_server* srv)
{
    return srv->address;
}

const char* tao_get_server_class(
    tao_server* srv)
{
    return srv->class_;
}

const char* tao_get_server_name(
    tao_server* srv)
{
    return srv->name;
}

const char* tao_get_server_help(
    tao_server* srv)
{
    return srv->help;
}

const char* tao_get_server_accesspoint(
    tao_server* srv)
{
    return ((XPA)srv->handle)->method;
}

// Search an entry with a matching name in the list and returns its address if
// found or NULL ortherwise.
//
// The implemented simple linear search is fast enough (about 24ns per search
// with 30 entries in the list).
const tao_server_command* tao_search_server_command(
    const tao_server_command* list, const char* name)
{
    char c;
    if (name != NULL && (c = name[0]) != '\0') {
        const char* str;
        ++name;
        for (int i = 0; (str = list[i].name) != NULL; ++i) {
            if (str[0] == c && strcmp(str + 1, name) == 0) {
                return &list[i];
            }
        }
    }
    return NULL;
}

static void print_command(
    FILE* output,
    const char* name,
    int argc,
    const char* argv[])
{
    int c;
    fprintf(output, "%s command:", name);
    for (int i = 0; i < argc; ++i) {
        const char* arg = argv[i];
        bool escape = false;
        for (int j = 0; (c = arg[j]) != '\0'; ++j) {
            if (c == ' ' || c == '\t' || c == '\n' || c == '\r') {
                escape = true;
                break;
            }
        }
        if (escape) {
            fprintf(output, " '%s'", arg);
        } else {
            fprintf(output, " %s", arg);
        }
    }
    fputs("\n", output);
    fflush(output);
}

// Execute an "xpaget" request.
tao_status tao_serve_send_command(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void** bufptr,
    size_t* sizptr)
{
    if (srv->debuglevel > 0) {
        print_command(stderr, "xpaget", argc, argv);
    }
    if (argc == 0 || (argc == 1 && argv[0][0] == '\0')) {
        return TAO_OK; // No command, nothing to do.
    }
    const tao_server_command* entry = tao_search_server_command(ctx, argv[0]);
    if (entry == NULL || entry->send == NULL) {
        tao_set_static_reply_message(srv, "Unknown xpaget command");
        return TAO_ERROR;
    }
    if (entry->send(srv, entry->send_data,
                    argc, argv, bufptr, sizptr) != TAO_OK) {
        return TAO_ERROR;
    }
#if 0 // FIXME: do we automatically recover data from the server buffer?
    *bufptr = tao_buffer_get_contents(tao_get_server_buffer(srv), sizptr);
#endif
    return TAO_OK;
}

// Execute an "xpaset" request.
tao_status tao_serve_recv_command(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void* buf,
    size_t siz)
{
    if (srv->debuglevel > 0) {
        print_command(stderr, "xpaset", argc, argv);
    }
    if (argc == 0 || (argc == 1 && argv[0][0] == '\0')) {
        return TAO_OK; // No command, nothing to do.
    }
    const tao_server_command* entry =
        tao_search_server_command(ctx, argv[0]);
    if (entry == NULL || entry->recv == NULL) {
        tao_set_static_reply_message(srv, "Unknown xpaset command");
        return TAO_ERROR;
    }
    if (entry->recv(srv, entry->recv_data, argc, argv, buf, siz) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}
