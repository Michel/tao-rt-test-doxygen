// tao-remote-mirrors-private.h -
//
// Private definitions for remote mirrors in TAO library.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2022, Éric Thiébaut.

#ifndef TAO_REMOTE_MIRRORS_PRIVATE_H_
#define TAO_REMOTE_MIRRORS_PRIVATE_H_ 1

#include <tao-remote-objects-private.h>
#include <tao-remote-mirrors.h>

TAO_BEGIN_DECLS

struct tao_remote_mirror_ {
    tao_remote_object base;///< Common part for all shared objects.
    const long       nacts;///< Number of actuators.
    const long     dims[2];///< Dimensions of actuator grid.
    const long refs_offset;///< Offset to actuator reference (in bytes).
    tao_serial        mark;///< Serial number of last data-frame.
};

TAO_END_DECLS

#endif // TAO_REMOTE_MIRRORS_PRIVATE__H_
