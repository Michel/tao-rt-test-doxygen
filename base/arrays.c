// arrays.c -
//
// Multi-dimensional arrays in TAO library (TAO is a library for Adaptive
// Optics software).
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2018-2022, Éric Thiébaut.

#include "tao-arrays-private.h"
#include "tao-utils.h"
#include "tao-errors.h"
#include "tao-macros.h"

#include <string.h>

long tao_count_elements(
    int ndims,
    const long dims[])
{
    if (ndims < 0 || ndims > TAO_MAX_NDIMS) {
        tao_store_error(__func__, TAO_BAD_RANK);
        return 0;
    }
    if (ndims > 0 && dims == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return 0;
    }
    long nelem = 1;
    for (int d = 0; d < ndims; ++d) {
        long dim = dims[d];
        if (dim <= 0) {
            tao_store_error(__func__, TAO_BAD_SIZE);
            return 0;
        }
        if (dim > 1) {
            // Count number of elements and check for overflows.
            if (nelem > LONG_MAX/dim) {
                tao_store_error(__func__, TAO_BAD_SIZE);
                return 0;
            }
            nelem *= dim;
        }
    }
    return nelem;
}

tao_array* tao_create_array(
    tao_eltype eltype,
    int ndims,
    const long dims[])
{
    long nelem = tao_count_elements(ndims, dims);
    if (nelem < 1) {
        return NULL;
    }
    size_t elsize = tao_size_of_eltype(eltype);
    if (elsize < 1) {
        tao_store_error(__func__, TAO_BAD_TYPE);
        return NULL;
    }
    size_t header = sizeof(tao_array);
    size_t size = header + TAO_ALIGNMENT - 1 + elsize*nelem;
    tao_array* arr = (tao_array*)tao_malloc(size);
    if (arr == NULL) {
        return NULL;
    }
    memset(arr, 0, sizeof(tao_array));
    arr->nrefs = 1;
    arr->eltype = eltype;
    arr->nelem = nelem;
    arr->ndims = ndims;
    for (int i = 0; i < ndims; ++i) {
        arr->dims[i] = dims[i];
    }
    for (int d = ndims; d < TAO_MAX_NDIMS; ++d) {
        arr->dims[d] = 1;
    }
    size_t address = (char*)arr - (char*)0;
    arr->data = (void*)TAO_ROUND_UP(address + header, TAO_ALIGNMENT);
    return arr;
}

tao_array* tao_wrap_array(
    tao_eltype eltype,
    int ndims,
    const long dims[],
    void* data,
    void (*free)(void*),
    void* ctx)
{
    long nelem = tao_count_elements(ndims, dims);
    if (nelem < 1) {
        return NULL;
    }
    size_t elsize = tao_size_of_eltype(eltype);
    if (elsize < 1) {
        tao_store_error(__func__, TAO_BAD_TYPE);
        return NULL;
    }
    tao_array* arr = (tao_array*)tao_malloc(sizeof(tao_array));
    if (arr == NULL) {
        return NULL;
    }
    memset(arr, 0, sizeof(tao_array));
    arr->nrefs = 1;
    arr->eltype = eltype;
    arr->nelem = nelem;
    arr->ndims = ndims;
    for (int i = 0; i < ndims; ++i) {
        arr->dims[i] = dims[i];
    }
    for (int d = ndims; d < TAO_MAX_NDIMS; ++d) {
        arr->dims[d] = 1;
    }
    arr->data = data;
    arr->free = free;
    arr->ctx = ctx;
    return arr;
}

tao_array* tao_create_1d_array(
    tao_eltype eltype,
    long dim1)
{
    long dims[1];
    dims[0] = dim1;
    return tao_create_array(eltype, 1, dims);
}

tao_array* tao_create_2d_array(
    tao_eltype eltype,
                    long dim1, long dim2)
{
    long dims[2];
    dims[0] = dim1;
    dims[1] = dim2;
    return tao_create_array(eltype, 2, dims);
}

tao_array* tao_create_3d_array(
    tao_eltype eltype,
    long dim1,
    long dim2,
    long dim3)
{
    long dims[3];
    dims[0] = dim1;
    dims[1] = dim2;
    dims[2] = dim3;
    return tao_create_array(eltype, 3, dims);
}

tao_array* tao_wrap_1d_array(
    tao_eltype eltype,
    long dim1,
    void* data,
    void (*free)(void*),
    void* ctx)
{
    long dims[1];
    dims[0] = dim1;
    return tao_wrap_array(eltype, 1, dims, data, free, ctx);
}

tao_array* tao_wrap_2d_array(
    tao_eltype eltype,
    long dim1,
    long dim2,
    void* data,
    void (*free)(void*), void* ctx)
{
    long dims[2];
    dims[0] = dim1;
    dims[1] = dim2;
    return tao_wrap_array(eltype, 2, dims, data, free, ctx);
}

tao_array* tao_wrap_3d_array(
    tao_eltype eltype,
    long dim1,
    long dim2,
    long dim3,
    void* data,
    void (*free)(void*),
    void* ctx)
{
    long dims[3];
    dims[0] = dim1;
    dims[1] = dim2;
    dims[2] = dim3;
    return tao_wrap_array(eltype, 3, dims, data, free, ctx);
}

tao_array* tao_reference_array(
    tao_array* arr)
{
    ++arr->nrefs;
    return arr;
}

static void destroy_array(
    tao_array* arr)
{
    if (arr->free != NULL) {
        arr->free(arr->ctx);
    }
    free(arr);
}

void tao_unreference_array(
    tao_array* arr)
{
    if (--arr->nrefs == 0) {
        destroy_array(arr);
    }
}

tao_eltype tao_get_array_eltype(
    const tao_array* arr)
{
    return (TAO_LIKELY(arr != NULL) ? arr->eltype : -1);
}

long tao_get_array_length(
    const tao_array* arr)
{
    return (TAO_LIKELY(arr != NULL) ? arr->nelem : 0);
}

int tao_get_array_ndims(
    const tao_array* arr)
{
    return (TAO_LIKELY(arr != NULL) ? arr->ndims : 0);
}

long tao_get_array_dim(
    const tao_array* arr,
    int d)
{
    return (TAO_LIKELY(arr != NULL) ?
            (TAO_UNLIKELY(d < 1) ? 0 :
             (TAO_UNLIKELY(d > TAO_MAX_NDIMS) ? 1 :
              arr->dims[d-1])) : 0);
}

void* tao_get_array_data(
    const tao_array* arr)
{
    return (TAO_LIKELY(arr != NULL) ? arr->data : NULL);
}
