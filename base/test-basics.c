// test-basics.c -
//
// Test basic macros and functions in TAO library.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2022, Éric Thiébaut.

#include <tao-basics.h>
#include <tao-macros.h>
#include <tao-utils.h>
#include <tao-locks.h>
#include <tao-shared-objects-private.h>
#include <tao-remote-objects-private.h>
#include <tao-rwlocked-objects-private.h>

#include <stdbool.h>
#include <string.h>
#include <math.h>

#define IS_SIGNED(T) ((T)(-1) < (T)(0))

#define test(expr)                                              \
    do {                                                        \
        if (! (expr)) {                                         \
            fprintf(stderr, "assertion failed: %s\n", #expr);   \
            ++nerrors;                                          \
        }                                                       \
        ++ntests;                                               \
    } while (false)

int main(
    int argc,
    char* argv[])
{
    long nerrors = 0, ntests = 0;
    const char* str_null = NULL;
    const char* str_empty = "";
    const char* str_hello = "hello";

    // Print size of basic objects.
    printf("sizeof(tao_mutex) = %ld bytes\n",
           (long)sizeof(tao_mutex));
    printf("sizeof(tao_cond) = %ld bytes\n",
           (long)sizeof(tao_cond));
    printf("sizeof(tao_shared_object) = %ld bytes\n",
           (long)sizeof(tao_shared_object));
    printf("sizeof(tao_remote_object) = %ld bytes\n",
           (long)sizeof(tao_remote_object));
    printf("sizeof(tao_rwlocked_object) = %ld bytes\n",
           (long)sizeof(tao_rwlocked_object));

    // Check signedness of time_t.
    test(IS_SIGNED(time_t));

    // Check integer min/max macros.
#ifdef UCHAR_MIN
    test(UCHAR_MIN == TAO_MIN_UNSIGNED_INT(unsigned char));
#endif
    test(UCHAR_MAX == TAO_MAX_UNSIGNED_INT(unsigned char));
    test(SCHAR_MIN == TAO_MIN_SIGNED_INT(signed char));
    test(SCHAR_MAX == TAO_MAX_SIGNED_INT(signed char));

#ifdef USHRT_MIN
    test(USHRT_MIN == TAO_MIN_UNSIGNED_INT(unsigned short));
#endif
    test(USHRT_MAX == TAO_MAX_UNSIGNED_INT(unsigned short));
    test(SHRT_MIN == TAO_MIN_SIGNED_INT(short));
    test(SHRT_MAX == TAO_MAX_SIGNED_INT(short));

#ifdef UINT_MIN
    test(UINT_MIN == TAO_MIN_UNSIGNED_INT(unsigned int));
#endif
    test(UINT_MAX == TAO_MAX_UNSIGNED_INT(unsigned int));
    test(INT_MIN == TAO_MIN_SIGNED_INT(int));
    test(INT_MAX == TAO_MAX_SIGNED_INT(int));

#ifdef ULONG_MIN
    test(ULONG_MIN == TAO_MIN_UNSIGNED_INT(unsigned long));
#endif
    test(ULONG_MAX == TAO_MAX_UNSIGNED_INT(unsigned long));
    test(LONG_MIN == TAO_MIN_SIGNED_INT(long));
    test(LONG_MAX == TAO_MAX_SIGNED_INT(long));

#ifdef ULLONG_MIN
    test(ULLONG_MIN == TAO_MIN_UNSIGNED_INT(unsigned long long));
#endif
    test(ULLONG_MAX == TAO_MAX_UNSIGNED_INT(unsigned long long));
    test(LLONG_MIN == TAO_MIN_SIGNED_INT(long long));
    test(LLONG_MAX == TAO_MAX_SIGNED_INT(long long));

    test(TAO_UINT8_MAX == TAO_MAX_UNSIGNED_INT(uint8_t));
    test(TAO_INT8_MAX == TAO_MAX_SIGNED_INT(int8_t));
    test(TAO_INT8_MIN == TAO_MIN_SIGNED_INT(int8_t));
    test(TAO_INT8_MIN == (-TAO_INT8_MAX - 1));

    test(TAO_UINT16_MAX == TAO_MAX_UNSIGNED_INT(uint16_t));
    test(TAO_INT16_MAX == TAO_MAX_SIGNED_INT(int16_t));
    test(TAO_INT16_MIN == TAO_MIN_SIGNED_INT(int16_t));
    test(TAO_INT16_MIN == (-TAO_INT16_MAX - 1));

    test(TAO_UINT32_MAX == TAO_MAX_UNSIGNED_INT(uint32_t));
    test(TAO_INT32_MAX == TAO_MAX_SIGNED_INT(int32_t));
    test(TAO_INT32_MIN == TAO_MIN_SIGNED_INT(int32_t));
    test(TAO_INT32_MIN == (-TAO_INT32_MAX - 1));

    test(TAO_UINT64_MAX == TAO_MAX_UNSIGNED_INT(uint64_t));
    test(TAO_INT64_MAX == TAO_MAX_SIGNED_INT(int64_t));
    test(TAO_INT64_MIN == TAO_MIN_SIGNED_INT(int64_t));
    test(TAO_INT64_MIN == (-TAO_INT64_MAX - 1));

    test(TAO_CHAR_BITS == 8*sizeof(char));
    test(TAO_SHORT_BITS == 8*sizeof(short));
    test(TAO_INT_BITS == 8*sizeof(int));
    test(TAO_LONG_BITS == 8*sizeof(long));
    test(TAO_LLONG_BITS == 8*sizeof(long long));

    // Check formatting of integers.
    {
        char buffer[256];
        int16_t i16 = 1616;
        sprintf(buffer, TAO_INT16_FORMAT( ,d), i16);
        test(strcmp(buffer, "1616") == 0);
        sprintf(buffer, TAO_INT16_FORMAT(8,d), i16);
        test(strcmp(buffer, "    1616") == 0);
        int32_t i32 = 32;
        sprintf(buffer, TAO_INT32_FORMAT( ,d), i32);
        test(strcmp(buffer, "32") == 0);
        sprintf(buffer, TAO_INT32_FORMAT(-8,d), i32);
        test(strcmp(buffer, "32      ") == 0);
        int64_t i64 = -32915;
        int64_t u64 =  32789;
        sprintf(buffer, TAO_INT64_FORMAT( ,d), i64);
        test(strcmp(buffer, "-32915") == 0);
        sprintf(buffer, TAO_INT64_FORMAT(8,d), i64);
        test(strcmp(buffer, "  -32915") == 0);
        sprintf(buffer, TAO_INT64_FORMAT( ,u), u64);
        test(strcmp(buffer, "32789") == 0);
        sprintf(buffer, TAO_INT64_FORMAT(-8,u), u64);
        test(strcmp(buffer, "32789   ") == 0);
    }

    // Check TAO_STRLEN.
    test(TAO_STRLEN(str_null) == 0);
    test(TAO_STRLEN(str_empty) == 0);
    test(TAO_STRLEN(str_hello) == 5);
    test(TAO_STRLEN("") == 0);
    test(TAO_STRLEN("hello") == 5);

    // Test time functions/macros.
    test(TAO_MILLISECONDS_PER_SECOND == 1000);
    test(TAO_MICROSECONDS_PER_SECOND == 1000*TAO_MILLISECONDS_PER_SECOND);
    test(TAO_NANOSECONDS_PER_SECOND  == 1000*TAO_MICROSECONDS_PER_SECOND);
    test(fabs(TAO_MILLISECOND*TAO_MILLISECONDS_PER_SECOND - 1) < 1e-16);
    test(fabs(TAO_MICROSECOND*TAO_MICROSECONDS_PER_SECOND - 1) < 1e-16);
    test(fabs(TAO_NANOSECOND*TAO_NANOSECONDS_PER_SECOND - 1) < 1e-16);
    test(fabs(TAO_MINUTE/(60*TAO_SECOND) - 1) < 1e-16);
    test(fabs(TAO_HOUR/(60*TAO_MINUTE) - 1) < 1e-16);
    test(fabs(TAO_DAY/(24*TAO_HOUR) - 1) < 1e-16);
    test(fabs(TAO_YEAR/(365.25*TAO_DAY) - 1) < 1e-16);

    // Summary.
    fprintf(stdout, "%ld test(s) passed\n", ntests - nerrors);
    fprintf(stdout, "%ld test(s) failed\n", nerrors);

    return (nerrors == 0 ? EXIT_SUCCESS : EXIT_FAILURE);
}
