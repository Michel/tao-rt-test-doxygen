// remote-cameras.c -
//
// Management of remote cameras.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2022, Éric Thiébaut.

#include "tao-remote-cameras-private.h"
#include "tao-config.h"
#include "tao-errors.h"
#include "tao-generic.h"

#include <assert.h>
#include <string.h>
#include <stdlib.h>

#define BUFFERS_OFFSET TAO_ROUND_UP(sizeof(tao_remote_camera), sizeof(tao_shmid))

static inline tao_shmid* get_bufs(
    tao_remote_camera* cam)
{
    return (tao_shmid*)TAO_COMPUTED_ADDRESS(cam, BUFFERS_OFFSET);
}

tao_remote_camera* tao_remote_camera_create(
    const char* owner,
    long        nbufs,
    unsigned    flags)
{
    // Check arguments.
    if (nbufs < 2) {
        tao_store_error(__func__, TAO_BAD_BUFFERS);
        return NULL;
    }

    // Compute stride, offest, and size to store `nbufs` shared memory
    // identifiers.
    size_t stride = sizeof(tao_shmid);
    size_t offset = TAO_ROUND_UP(sizeof(tao_remote_camera), stride);
    size_t size = offset + nbufs*stride;
    if (offset != BUFFERS_OFFSET) {
        tao_store_error(__func__, TAO_ASSERTION_FAILED);
        return NULL;
    }

    // Allocate remote shared object.
    tao_remote_camera* cam = (tao_remote_camera*)tao_remote_object_create(
        owner, TAO_REMOTE_CAMERA, nbufs, offset, stride, size, flags);
    if (cam == NULL) {
        return NULL;
    }

    // Instanciate object.
    tao_camera_info_initialize(&cam->info);
    tao_shmid* bufs = get_bufs(cam);
    for (long i = 0; i < nbufs; ++i) {
        bufs[i] = TAO_BAD_SHMID;
    }
    for (long i = 0; i < 4; ++i) {
        cam->preproc[i] = TAO_BAD_SHMID;
    }
    return cam;
}

tao_shmid tao_remote_camera_get_image_shmid(
    tao_remote_camera* cam,
    tao_serial serial)
{
    // Check arguments.
    if (cam == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_BAD_SHMID;
    }
    if (serial < 1) {
        tao_store_error(__func__, TAO_BAD_SERIAL);
        return TAO_BAD_SHMID;
    }
    const tao_shmid* bufs = get_bufs(cam);
    tao_shmid shmid = bufs[(serial - 1)%cam->base.nbufs];
    if (shmid == TAO_BAD_SHMID) {
        tao_store_error(__func__, TAO_NO_DATA);
    }
    return shmid;
}

tao_serial tao_remote_camera_wait_image(
    tao_remote_camera* cam,
    tao_serial         serial,
    double             secs)
{
    return tao_remote_object_wait_serial(
        tao_remote_object_cast(cam), serial, secs);
}

tao_status tao_remote_camera_start(
    tao_remote_camera* cam,
    const tao_camera_config* cfg,
    double secs)
{
    // Check arguments.  Manage to use the current configuration if caller does
    // not provide a configuration.
    if (cam == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    if (cfg == NULL) {
        cfg = &cam->info.config;
    }

    // Attempt to lock the remote camera and wait for it becomes ready for a
    // new command.
    tao_status status = tao_remote_object_lock_for_command(
        tao_remote_object_cast(cam), TAO_COMMAND_START, secs);
    if (status == TAO_OK) {
        // Server is ready to accept a new command.  Copy configuration to
        // start with as the next configuration and unlock the ressources.
        memcpy(&cam->nxtcfg, cfg, sizeof(*cfg));
        if (tao_remote_camera_unlock(cam) != TAO_OK) {
            status = TAO_ERROR;
        }
    }
    return status;
}

tao_status tao_remote_camera_stop(
    tao_remote_camera* cam,
    double secs)
{
    return tao_remote_object_send_simple_command(
        tao_remote_object_cast(cam), TAO_COMMAND_STOP, secs);
}

tao_status tao_remote_camera_abort(
    tao_remote_camera* cam,
    double secs)
{
    return tao_remote_object_send_simple_command(
        tao_remote_object_cast(cam), TAO_COMMAND_ABORT, secs);
}

tao_status tao_remote_camera_kill(
    tao_remote_camera* cam,
    double secs)
{
    return tao_remote_object_send_simple_command(
        tao_remote_object_cast(cam), TAO_COMMAND_KILL, secs);
}

#define GETTER(type, path, member, def)                         \
    type tao_remote_camera_get_##member(                        \
        const tao_remote_camera* cam)                           \
    {                                                           \
        return (cam != NULL ? cam->path.member : (def));        \
    }
GETTER(long,         info,            sensorwidth,    0)
GETTER(long,         info,            sensorheight,   0)
GETTER(long,         info.config.roi, xbin,           0)
GETTER(long,         info.config.roi, ybin,           0)
GETTER(long,         info.config.roi, xoff,           0)
GETTER(long,         info.config.roi, yoff,           0)
GETTER(long,         info.config.roi, width,          0)
GETTER(long,         info.config.roi, height,         0)
GETTER(double,       info.config,     framerate,      0.0)
GETTER(double,       info.config,     exposuretime,   0.0)
GETTER(tao_eltype,   info.config,     pixeltype,      TAO_UINT8)
GETTER(tao_encoding, info.config,     sensorencoding, TAO_ENCODING_UNKNOWN)
GETTER(tao_encoding, info.config,     bufferencoding, TAO_ENCODING_UNKNOWN)
//FIXME: GETTER(tao_state,    info,            state,          TAO_STATE_KILLED)
#undef GETTER

// Generic shared object methods.
#define TYPE  remote_camera
#define MAGIC TAO_REMOTE_CAMERA
#define IS_REMOTE_OBJECT 1
#include "./shared-methods.c"
