// encodings.c -
//
// Utilities for pixel encodings in TAO (TAO is a library for Adaptive Optics
// software).
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2018-2022, Éric Thiébaut.

#include "tao-encodings.h"

#include <ctype.h>
#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define if_likely(expr)   if TAO_LIKELY(expr)
#define if_unlikely(expr) if TAO_UNLIKELY(expr)

#define JOIN0(a,b) a##b
#define JOIN(a,b)  JOIN0(a,b)

#define ELTYPE_int8_t    TAO_INT8
#define ELTYPE_uint8_t   TAO_UINT8
#define ELTYPE_int16_t   TAO_INT16
#define ELTYPE_uint16_t  TAO_UINT16
#define ELTYPE_int32_t   TAO_INT32
#define ELTYPE_uint32_t  TAO_UINT32
#define ELTYPE_int64_t   TAO_INT64
#define ELTYPE_uint64_t  TAO_UINT64
#define ELTYPE_float     TAO_FLOAT
#define ELTYPE_double    TAO_DOUBLE

static struct {
    const char*  name;
    const char*  descr;
    size_t       size;
    tao_eltype   type;
    tao_encoding enc;
} types[] = {
    // The following table must be arranged in the same orderas the numbering
    // of `tao_eltype`.
#define DEF(T, descr, enc) \
    { #T, descr, sizeof(T), JOIN(ELTYPE_,T), enc }
    DEF(int8_t, "signed 8-bit integer",
        TAO_ENCODING_SIGNED(8)),
    DEF(uint8_t, "unsigned 8-bit integer",
        TAO_ENCODING_UNSIGNED(8)),
    DEF(int16_t, "signed 16-bit integer",
        TAO_ENCODING_SIGNED(16)),
    DEF(uint16_t, "unsigned 16-bit integer",
        TAO_ENCODING_UNSIGNED(16)),
    DEF(int32_t, "signed 32-bit integer",
        TAO_ENCODING_SIGNED(32)),
    DEF(uint32_t, "unsigned 32-bit integer",
        TAO_ENCODING_UNSIGNED(32)),
    DEF(int64_t, "signed 64-bit integer",
        TAO_ENCODING_SIGNED(64)),
    DEF(uint64_t, "unsigned 64-bit integer",
        TAO_ENCODING_UNSIGNED(64)),
    DEF(float, "single precision floating-point",
        TAO_ENCODING_FLOAT(CHAR_BIT*sizeof(float))),
    DEF(double, "double precision floating-point",
        TAO_ENCODING_FLOAT(CHAR_BIT*sizeof(double))),
#undef DEF
    { NULL, NULL, 0, TAO_ENCODING_UNKNOWN },
};

#define CHECK_ELTYPE(eltype) (TAO_INT8 <= (eltype) && (eltype) <= TAO_DOUBLE)

size_t tao_size_of_eltype(
    tao_eltype eltype)
{
    if_likely(CHECK_ELTYPE(eltype)) {
        return types[eltype - TAO_INT8].size;
    }
    return 0;
}

const char* tao_name_of_eltype(
    tao_eltype eltype)
{
    if_likely(CHECK_ELTYPE(eltype)) {
        return types[eltype - TAO_INT8].name;
    }
    return "unknown_type";
}

const char* tao_description_of_eltype(
    tao_eltype eltype)
{
    if_likely(CHECK_ELTYPE(eltype)) {
        return types[eltype - TAO_INT8].descr;
    }
    return "unknown type";
}

tao_encoding tao_encoding_of_eltype(
    tao_eltype eltype)
{
    if_likely(CHECK_ELTYPE(eltype)) {
        return types[eltype - TAO_INT8].enc;
    }
    return TAO_ENCODING_UNKNOWN;
}

tao_eltype tao_eltype_of_encoding(
    tao_encoding enc)
{
    for (int i = 0; types[i].descr != NULL; ++i) {
        if (types[i].enc == enc) {
            return types[i].type;
        }
    }
    return 0;
}

tao_status tao_format_encoding(
    char* str,
    tao_encoding enc)
{
    const char* col;
    int pxl, pkt, flg;

    if (str == NULL) {
        errno = EFAULT;
        return TAO_ERROR;
    }

    switch (TAO_ENCODING_COLORANT(enc)) {
    case TAO_COLORANT_RAW:
        col = "Raw";
        break;
    case TAO_COLORANT_MONO:
        col = "Mono";
        break;
    case TAO_COLORANT_RGB:
        col = "RGB";
        break;
    case TAO_COLORANT_BGR:
        col = "BGR";
        break;
    case TAO_COLORANT_ARGB:
        col = "ARGB";
        break;
    case TAO_COLORANT_RGBA:
        col = "RGBA";
        break;
    case TAO_COLORANT_ABGR:
        col = "ABGR";
        break;
    case TAO_COLORANT_BGRA:
        col = "BGRA";
        break;
    case TAO_COLORANT_BAYER_RGGB:
        col = "BayerRGGB";
        break;
    case TAO_COLORANT_BAYER_GRBG:
        col = "BayerGRBG";
        break;
    case TAO_COLORANT_BAYER_GBRG:
        col = "BayerGBRG";
        break;
    case TAO_COLORANT_BAYER_BGGR:
        col = "BayerBGGR";
        break;
    case TAO_COLORANT_YUV444:
        if (enc == TAO_ENCODING_YUV444) {
            col = "YUV444";
            goto singleton;
        } else {
            goto badvalue;
        }
        break;
    case TAO_COLORANT_YUV422:
        if (enc == TAO_ENCODING_YUV422) {
            col = "YUV422";
            goto singleton;
        } else {
            goto badvalue;
        }
        break;
    case TAO_COLORANT_YUV411:
        if (enc == TAO_ENCODING_YUV411) {
            col = "YUV411";
            goto singleton;
        } else {
            goto badvalue;
        }
        break;
    case TAO_COLORANT_YUV420P:
        if (enc == TAO_ENCODING_YUV420P) {
            col = "YUV420p";
            goto singleton;
        } else {
            goto badvalue;
        }
        break;
    case TAO_COLORANT_YUV420SP:
        if (enc == TAO_ENCODING_YUV420SP) {
            col = "YUV420sp";
            goto singleton;
        } else {
            goto badvalue;
        }
        break;
    case TAO_COLORANT_SIGNED:
        col = "Signed";
        break;
    case TAO_COLORANT_FLOAT:
        col = "Float";
        break;
    default:
        goto badvalue;
    }

    pxl = TAO_ENCODING_BITS_PER_PIXEL(enc);
    pkt = TAO_ENCODING_BITS_PER_PACKET(enc);
    flg = TAO_ENCODING_FLAGS(enc);
    if (flg != 0) {
        sprintf(str, "%s(%d,%d,0x%02x)", col, pxl, pkt, flg);
    } else if (pkt != pxl) {
        sprintf(str, "%s(%d,%d)", col, pxl, pkt);
    } else {
        sprintf(str, "%s(%d)", col, pxl);
    }
    return TAO_OK;

 singleton:
    strcpy(str, col);
    return TAO_OK;

 badvalue:
    errno = EINVAL;
    return TAO_ERROR;
}

static struct {
    const char*  pfx;
    tao_encoding col;
    bool         arg;
} encodings[] = {
    {"Mono",       TAO_COLORANT_MONO,       true},
    {"Monochrome", TAO_COLORANT_MONO,       true},
    {"Float",      TAO_COLORANT_FLOAT,      true},
    {"Signed",     TAO_COLORANT_SIGNED,     true},
    {"Unsigned",   TAO_COLORANT_UNSIGNED,   true},
    {"Raw",        TAO_COLORANT_RAW,        true},
    {"ARGB",       TAO_COLORANT_ARGB,       true},
    {"ABGR",       TAO_COLORANT_ABGR,       true},
    {"BGR",        TAO_COLORANT_BGR,        true},
    {"BGRA",       TAO_COLORANT_BGRA,       true},
    {"BayerRGGB",  TAO_COLORANT_BAYER_RGGB, true},
    {"BayerGRBG",  TAO_COLORANT_BAYER_GRBG, true},
    {"BayerGBRG",  TAO_COLORANT_BAYER_GBRG, true},
    {"BayerBGGR",  TAO_COLORANT_BAYER_BGGR, true},
    {"BAYER_RGGB", TAO_COLORANT_BAYER_RGGB, true},
    {"BAYER_GRBG", TAO_COLORANT_BAYER_GRBG, true},
    {"BAYER_GBRG", TAO_COLORANT_BAYER_GBRG, true},
    {"BAYER_BGGR", TAO_COLORANT_BAYER_BGGR, true},
    {"RGB",        TAO_COLORANT_RGB,        true},
    {"RGBA",       TAO_COLORANT_RGBA,       true},
    {"YUV444",     TAO_ENCODING_YUV444,     false},
    {"YUV422",     TAO_ENCODING_YUV422,     false},
    {"YUV411",     TAO_ENCODING_YUV411,     false},
    {"YUV420P",    TAO_ENCODING_YUV420P,    false},
    {"YUV420SP",   TAO_ENCODING_YUV420SP,   false},
    {NULL,         TAO_ENCODING_UNKNOWN,    false},
};

tao_encoding tao_parse_encoding(
    const char* str)
{
    tao_encoding col, pxl, pkt, flg;
    char* ptr;
    int nargs;
    long len, val, paren;

    // Check input string.
    if (str == NULL) {
        errno = EFAULT;
        return TAO_ENCODING_UNKNOWN;
    }
    len = strlen(str);
    if (len < 1) {
    error:
        errno = EINVAL;
        return TAO_ENCODING_UNKNOWN;
    }

    // No spaces are allowed (because of the splitting of command line
    // arguments) and there may be at most one parenthesis.
    paren = -1;
    for (long i = 0; i < len; ++i) {
        if (isspace(str[i])) {
            goto error;
        }
        if (str[i] == '(') {
            if (paren >= 0) {
                goto error;
            }
            paren = i;
        }
    }

    // Deal with literal constants (with no additional values).
    if (paren < 0) {
        for (int i = 0; encodings[i].pfx != NULL; ++i) {
            if (encodings[i].arg == false &&
                strcasecmp(str, encodings[i].pfx) == 0) {
                return encodings[i].col;
            }
        }
        goto error;
    }

    // Adjust the length to be that of the prefix.
    len = paren;

    // Parse values.
    nargs = 0;
    pxl = 0;
    pkt = 0;
    flg = 0;
    while (true) { // <--- dummy loop just to break
        // Parse `pxl` value (in decimal form).
        val = strtol(&str[paren + 1], &ptr, 10);
        if (val < 1 || val > 255) {
            goto error;
        }
        pxl = val;
        if (ptr[0] == ')' && ptr[1] == '\0') {
            nargs = 1;
            break;
        } else if (ptr[0] != ',') {
            goto error;
        }

        // Parse `pkt` value (in decimal form).
        val = strtol(ptr + 1, &ptr, 10);
        if (val < 1 || val > 255) {
            goto error;
        }
        pkt = val;
        if (ptr[0] == ')' && ptr[1] == '\0') {
            nargs = 2;
            break;
        } else if (ptr[0] != ',') {
            goto error;
        }

        // Parse `flg` value (in any form).
        val = strtol(ptr + 1, &ptr, 0);
        if (val < 0 || val > 255) {
            goto error;
        }
        flg = val;
        if (ptr[0] == ')' && ptr[1] == '\0') {
            nargs = 3;
            break;
        } else {
            goto error;
        }
    }

    // Search for a matching prefix.
    for (int i = 0; encodings[i].pfx != NULL; ++i) {
        if (encodings[i].arg == true &&
            strncasecmp(str, encodings[i].pfx, len) == 0 &&
            encodings[i].pfx[len] == '\0') {
            col = encodings[i].col;
            if (nargs == 1) {
                return TAO_ENCODING_2_(col, pxl);
            } else if (nargs == 2) {
                return TAO_ENCODING_3_(col, pxl, pkt);
            } else {
                return TAO_ENCODING_4_(col, pxl, pkt, flg);
            }
        }
    }
    goto error;
}
