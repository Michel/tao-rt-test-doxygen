// test-shared-locks.c -
//
// Test process shared mutexes and condition variables.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2022, Éric Thiébaut.

#include <tao-shared-objects-private.h>
#include <tao-locks.h>
#include <tao-errors.h>

#include <stdlib.h>
#include <string.h>
#include <unistd.h>

// To test process shared mutexes and condition variables, we launch two
// processes, the parent and a forked child, which communicate via a single
// "notify" condition variable protected by a mutex.  All shared data is stored
// in shared memory via a custom TAO shared object.  The time taken by this
// kind of communication is measured.

typedef struct shared_data shared_data;

struct shared_data {
    tao_shared_object base; // Base of any TAO shared object
    tao_mutex        mutex;
    tao_cond        notify;
    tao_time          time;
    enum {
        EXPECTING_DATA, // Consumer is expecting some data
        DATA_AVAILABLE, // Producer has provided some data
        QUIT,           // Consumer and producer must quit
    } status;
    bool         broadcast;
};

// Type code of the custom TAO shared object.
#define SHARED_DATA (TAO_SHARED_OBJECT | 5)

static inline void lock_mutex(
    shared_data* data)
{
    if (tao_mutex_lock(&data->mutex) != TAO_OK) {
        tao_panic();
    }
}

static inline void unlock_mutex(
    shared_data* data)
{
    if (tao_mutex_unlock(&data->mutex) != TAO_OK) {
        tao_panic();
    }
}

static inline void wait_condition(
    shared_data* data)
{
    if (tao_condition_wait(&data->notify, &data->mutex) != TAO_OK) {
        tao_panic();
    }
}

static inline void notify_condition(
    shared_data* data)
{
    tao_status status = (data->broadcast ?
                         tao_condition_broadcast(&data->notify) :
                         tao_condition_signal(&data->notify));
    if (status != TAO_OK) {
        tao_panic();
    }
}

static inline void get_monotonic_time(
    tao_time* t)
{
    if (tao_get_monotonic_time(t) != TAO_OK) {
        tao_panic();
    }
}

static inline double elapsed(
    const tao_time* t0)
{
    tao_time t1;
    get_monotonic_time(&t1);
    return ((double)(t1.sec - t0->sec) + 1E-9*(double)(t1.nsec - t0->nsec));
}

static void run_producer(
    tao_shmid shmid)
{
    // Attach to shared data.
    shared_data* data = (shared_data*)tao_shared_object_attach(shmid);
    if (data == NULL) {
        tao_panic();
    }
    if (((tao_shared_object*)data)->type != SHARED_DATA) {
        fprintf(stderr, "Invalid shared object type\n");
        exit(EXIT_FAILURE);
    }

    // Wait for the consumer to be ready.  A first loop is to check for exit
    // condition, a second is to wait for consumer expecting data.  Spurious
    // notifies are correctly handled.
    lock_mutex(data);
    while (data->status != QUIT) {
        if (data->status == EXPECTING_DATA) {
            data->status = DATA_AVAILABLE;
            get_monotonic_time(&data->time);
            notify_condition(data);
        }
        while (data->status == DATA_AVAILABLE) {
            wait_condition(data);
        }
    }
    unlock_mutex(data);
}

static void run_consumer(
    shared_data* data,
    long tests,
    long skips)
{
    // Collect data.
    tao_time_stat_data tsd;
    tao_initialize_time_statistics(&tsd);
    long count = 0;
    lock_mutex(data);
    while (data->status != QUIT) {
        if (data->status == DATA_AVAILABLE) {
            double t = elapsed(&data->time);
            if (++count > skips) {
                tao_update_time_statistics(&tsd, t);
                data->status = (tsd.numb < tests ? EXPECTING_DATA : QUIT);
            }
            notify_condition(data);
        }
        while (data->status == EXPECTING_DATA) {
            wait_condition(data);
        }
    }
    unlock_mutex(data);

    // Print time statistics.
    tao_time_stat ts;
    tao_compute_time_statistics(&ts, &tsd);
    fprintf(stdout, "  notify method -------> %s condition\n",
            (data->broadcast ? "broadcast" : "signal"));
    fprintf(stdout, "  number of skips -----> %ld\n", skips);
    fprintf(stdout, "  number of tests -----> %ld\n", ts.numb);
    fprintf(stdout, "  min. latency time ---> %8.3f µs\n", 1E6*ts.min);
    fprintf(stdout, "  max. latency time ---> %8.3f µs\n", 1E6*ts.max);
    if (ts.numb >= 2) {
	fprintf(stdout, "  avg. latency time ---> %8.3f +/- %.3f µs\n",
		1E6*ts.avg, 1E6*ts.std);
    }
}

int main(
    int argc,
    char* argv[])
{
    long skips = 100;
    long tests = 100000;
    bool broadcast = false;
    const char* progname = tao_basename(argv[0]);

    // Parse arguments.
    int i = 0;
    while (++i < argc) {
        if (argv[i][0] != '-') {
            break;
        } else if (strcmp(argv[i], "-h") == 0 ||
                   strcmp(argv[i], "--help") == 0) {
            printf("Usage: %s [-h|--help] [--frames NUMBER] "
                   "[--buffers NUMBER] [--readers NUMBER] "
                   "[--sleep SECONDS] [--]\n\n", progname);
            printf("Test r/w shared locks.  Options are:\n");
            printf("    --tests NUMBER   "
                   "Number of tests to run [%ld].\n", tests);
            printf("    --skips NUMBER   "
                   "Number of initial tests to skip [%ld].\n", skips);
            printf("    --broadcast      "
                   "Broadcast condition [%s].\n",
                   (broadcast ? "broadcast" : "signal"));
            printf("    --signal         "
                   "Signal condition [%s].\n",
                   (broadcast ? "broadcast" : "signal"));
            printf("    -h, --help        "
                   "Print this help.\n");
            exit(0);
        } else if (strcmp(argv[i], "--tests") == 0) {
            if (i + 1 >= argc) {
            missing_option_value:
                fprintf(stderr, "missing argument for option %s\n", argv[i]);
                return EXIT_FAILURE;
            }
            if (tao_parse_long(argv[i+1], &tests, 0) != TAO_OK || tests < 1) {
            invalid_option_value:
                fprintf(stderr, "invalid value for option %s\n", argv[i]);
                return EXIT_FAILURE;
            }
            ++i;
        } else if (strcmp(argv[i], "--skips") == 0) {
            if (i + 1 >= argc) {
                goto missing_option_value;
            }
            if (tao_parse_long(argv[i+1], &skips, 0) != TAO_OK || skips < 0) {
                goto invalid_option_value;
            }
            ++i;
        } else if (strcmp(argv[i], "--broadcast") == 0) {
            broadcast = true;
        } else if (strcmp(argv[i], "--signal") == 0) {
            broadcast = false;
        } else if (strcmp(argv[i], "--") == 0) {
            ++i;
            break;
        }
    }
    if (i < argc) {
        fprintf(stderr, "%s: too many arguments\n", progname);
        return EXIT_FAILURE;
    }

    // Create shared resources.
    shared_data* data = (shared_data*)tao_shared_object_create(
        SHARED_DATA, sizeof(shared_data), 0600);
    if (data == NULL ||
        tao_mutex_initialize(&data->mutex, true) != TAO_OK ||
        tao_condition_initialize(&data->notify, true) != TAO_OK) {
        tao_panic();
    }
    data->broadcast = broadcast;
    data->status = EXPECTING_DATA;
    tao_shmid shmid =
        tao_shared_object_get_shmid((tao_shared_object*)data);

    // Fork to have two processes.
    if (fork()) {
        // The parent is the "consumer".
        run_consumer(data, tests, skips);
    } else {
        // The child is the "producer".
        run_producer(shmid);
    }
    return EXIT_SUCCESS;
}
