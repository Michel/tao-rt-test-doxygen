// tao-servers-private.h --
//
// Private definitions for servers in TAO.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2018-2021, Éric Thiébaut.

#ifndef TAO_SERVERS_PRIVATE_H_
#define TAO_SERVERS_PRIVATE_H_ 1

#include <tao-servers.h>

TAO_BEGIN_DECLS

// Private structure representing a TAO server.
struct tao_server_ {
    const char*     address; /**< Address of server. */
    const char*      class_; /**< Class part of address. */
    const char*        name; /**< Name part of address. */
    const char*        help; /**< Help string. */
    void*            handle; /**< Handle to XPA server. */
    tao_send_callback* send; /**< Callback to serve "get" requests. */
    void*          send_ctx; /**< Associated contextual data. */
    tao_recv_callback* recv; /**< Callback to serve "set" requests. */
    void*          recv_ctx; /**< Associated contextual data. */
    tao_buffer      databuf; /**< Dynamic buffer for sending back data. */
    tao_buffer      mesgbuf; /**< Dynamic buffer for messages. */
    int          debuglevel; /**< Debug level: the higher, the more verbose. */
    bool            running; /**< Server is handling requests? */
    const char*     message; /**< Message left by the last send/recv callback. */
};

TAO_END_DECLS

#endif // TAO_SERVERS_PRIVATE_H_
