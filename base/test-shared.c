// test-shared.c -
//
// Test shared objects in TAO library.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2022, Éric Thiébaut.

#include <tao-shared-objects-private.h>

#define IS_SIGNED(T) ((T)(-1) < (T)(0))

#define test(expr)                                              \
    do {                                                        \
        if (! (expr)) {                                         \
            fprintf(stderr, "assertion failed: %s\n", #expr);   \
            ++nerrors;                                          \
        }                                                       \
        ++ntests;                                               \
    } while (false)

int main(
    int argc,
    char* argv[])
{
    long nerrors = 0, ntests = 0;

    fprintf(stdout, "sizeof(tao_shared_object): %ld bytes\n",
            sizeof(tao_shared_object));

    // Summary.
    fprintf(stdout, "%ld test(s) passed\n", ntests - nerrors);
    fprintf(stdout, "%ld test(s) failed\n", nerrors);

    return (nerrors == 0 ? EXIT_SUCCESS : EXIT_FAILURE);

}
