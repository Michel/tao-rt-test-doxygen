// rwlocked-objects.c -
//
// Management of basic shared objects with read/write restricted access.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2018-2022, Éric Thiébaut.

#include "tao-basics.h"
#include "tao-errors.h"
#include "tao-macros.h"
#include "tao-generic.h"
#include "tao-shared-objects.h"
#include "tao-rwlocked-objects-private.h"

tao_rwlocked_object* tao_rwlocked_object_create(
    uint32_t    type,
    size_t      size,
    unsigned    flags)
{
    if ((type & TAO_SHARED_SUPERTYPE_MASK) != TAO_RWLOCKED_OBJECT) {
        tao_store_error(__func__, TAO_BAD_TYPE);
        return NULL;
    }
    if (size < sizeof(tao_rwlocked_object)) {
        tao_store_error(__func__, TAO_BAD_SIZE);
        return NULL;
    }
    tao_rwlocked_object* obj = (tao_rwlocked_object*)tao_shared_object_create(
        type, size, flags);
    if (obj == NULL) {
        return NULL;
    }
    return obj;
}

tao_rwlocked_object* tao_rwlocked_object_attach(
    tao_shmid shmid)
{
    tao_shared_object* obj = tao_shared_object_attach(shmid);
    if (obj != NULL &&
        (obj->type & TAO_SHARED_SUPERTYPE_MASK) == TAO_RWLOCKED_OBJECT) {
        return (tao_rwlocked_object*)obj;
    }
    tao_shared_object_detach(obj);
    tao_store_error(__func__, TAO_BAD_TYPE);
    return NULL;
}

#define TYPE rwlocked_object
#define IS_RWLOCKED_OBJECT 1
#include "./shared-methods.c"
