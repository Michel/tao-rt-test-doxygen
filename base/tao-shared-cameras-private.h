// tao-shared-cameras-private.h -
//
// Private definitions for shared cameras and virtual frame-grabbers.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2018-2022, Éric Thiébaut.

#ifndef TAO_SHARED_CAMERAS_PRIVATE_H_
#define TAO_SHARED_CAMERAS_PRIVATE_H_ 1

#include <tao-shared-cameras.h>
#include <tao-rwlocked-objects-private.h>

TAO_BEGIN_DECLS

/**
 * @brief Shared camera information.
 *
 * @ingroup RemoteCameras
 *
 * This structure describes the shared data storing the resources of a remote
 * camera.  After querying the shared memory identifier to the server (the
 * frame grabber), clients can attach this shared data part with
 * tao_shared_camera_attach().  When a client no longer needs this shared data,
 * it shall call tao_shared_camera_detach().
 *
 * This structure **must** be considered as read-only by the clients and
 * information provided by this structure is only valid as long as the client
 * locks this shared structure by calling tao_shared_camera_rdlock() and until
 * the client unlock the structure by calling tao_shared_camera_unlock().
 * Beware to not call tao_shared_camera_detach() while the shared data is
 * locked by the caller.
 *
 * If @n serial > 0, the index in the list of shared arrays memorized by the
 * virtual frame-grabber owning the shared camera is given by:
 *
 * ~~~~~{.c}
 * index = (serial - 1) % nbufs
 * ~~~~~
 *
 */
struct tao_shared_camera_ {
    tao_rwlocked_object base;/**< Shared object backing the storage of the
                                  structure. */
    const char owner[TAO_OWNER_SIZE];
    tao_camera_info     info;/**< Camera information. */
    const tao_serial   nbufs;/**< Lenght of the list of shared arrays memorized
                                  by the virtual frame-grabber owning the
                                  shared camera. */
    volatile
    tao_serial        serial;/**< Number of images posted by virtual
                                  frame-grabber since its creation.  It is a
                                  unique, monotically increasing number,
                                  starting at 1 (0 means no images have been
                                  provided yet).  This value can be used to
                                  determine the index of the corresponding
                                  shared image in the cyclic list of
                                  frame-grabber shared arrays. */
    volatile
    tao_shmid      lastframe;/**< Identifier of the shared array backing the
                                  storage of the last acquired image, -1 means
                                  unused or invalid. */
    volatile
    tao_shmid      nextframe;/**< Identifier of the shared array backing the
                                  storage of the next acquired image, -1 means
                                  unused or invalid. */
};

/**
 * @brief Frame-grabber structure for a server.
 *
 * @ingroup FrameGrabbers
 *
 * The server stores camera information that is shared with its clients (as an
 * instance of `tao_shared_camera`) and a list of shared arrays for
 * providing processed images to the clients.  The length of this list is
 * given by member @a nbufs and its contents is recycled if possible.
 *
 * To allow for clients to maintain a mirror of this list, the length of the
 * list is kept constant after tao_framegrabber_create() has been called and
 * the index in the list of the shared array corresponding to a posted image is
 * given by:
 *
 * ~~~~~{.c}
 * index = (serial - 1) % nbufs
 * ~~~~~
 *
 * where @b serial is the number of *posted* images, that is the number of
 * shared images provided by the frame-grabber to others.  If @b serial is
 * zero, there are no shared images to fetch.  The serial number is initially
 * set to zero by tao_framegrabber_create() and incremented by one by each
 * successful call to tao_framegrabber_post_buffer().
 */
struct tao_framegrabber_ {
    tao_shared_camera*  camera;///< Attached shared camera information.
    const tao_serial     nbufs;///< Lenght of list of shared arrays.
    volatile tao_serial serial;///< Number of published images.
    tao_shared_array**    list;///< List of shared arrays.
    tao_shared_array*   locked;///< If non-NULL, shared array of the list that
                               ///  is currently locked for writing by the
                               ///  frame-grabber.
    unsigned             flags;///< Access permissions for the shared data.
};

TAO_END_DECLS

#endif // TAO_SHARED_CAMERAS_PRIVATE_H_
