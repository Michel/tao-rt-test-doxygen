// test-errors.c -
//
// Test error management in TAO library.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2022, Éric Thiébaut.

#include <tao-errors.h>

#include <errno.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

#define IS_SIGNED(T) ((T)(-1) < (T)(0))

#define test(expr)                                              \
    do {                                                        \
        if (! (expr)) {                                         \
            fprintf(stderr, "assertion failed: %s\n", #expr);   \
            ++nerrors;                                          \
        }                                                       \
        ++ntests;                                               \
    } while (false)

static struct timespec t0;

static void tic(
    void)
{
    clock_gettime(CLOCK_MONOTONIC, &t0);
}

static double toc(
    void)
{
    struct timespec t1;
    clock_gettime(CLOCK_MONOTONIC, &t1);
    return (t1.tv_sec - t0.tv_sec) + (t1.tv_nsec - t0.tv_nsec)*1E-9;
}

int main(
    int argc,
    char* argv[])
{
    long nerrors = 0, ntests = 0;
    tao_error* err = tao_get_last_error();
    test(err != NULL);

    const tao_error err1 = { .func = "foo", .code = TAO_BAD_ARGUMENT };
    test(err1.proc == NULL);
    tao_store_error(err1.func, err1.code);
    test(err == tao_get_last_error());
    test(err->func == err1.func);
    test(err->code == err1.code);

    const tao_error err2 = { .func = "bar", .code = TAO_UNWRITABLE };
    test(err2.proc == NULL);
    tao_store_error(err2.func, err2.code);
    test(err == tao_get_last_error());
    test(err->func == err2.func);
    test(err->code == err2.code);

    const tao_error err3 = { .func = "malloc", .code = ENOMEM };
    test(err2.proc == NULL);
    errno = err3.code;
    tao_store_system_error(err3.func);
    test(err == tao_get_last_error());
    test(err->func == err3.func);
    test(err->code == err3.code);

    const tao_error err4 = { .func = "free", .code = EFAULT };
    test(err2.proc == NULL);
    errno = err4.code;
    tao_store_system_error(err4.func);
    test(err == tao_get_last_error());
    test(err->func == err4.func);
    test(err->code == err4.code);

    // Evaluate tao_get_last_error().
    long nfails = 0;
    long ncalls = 10000000;
    double t, tsum, tmin, tmax, tavg;
    tsum = tmin = tmax = 0;
    for (long k = 0; k < ncalls; ++k) {
        tic();
        if (err != tao_get_last_error()) {
            ++nfails;
        }
        t = toc();
        if (k == 0) {
            tmin = tmax = t;
        } else {
            if (t < tmin) tmin = t;
            if (t > tmax) tmax = t;
        }
        tsum += t;
    }
    tavg = tsum/ncalls;
    fprintf(stdout, "\nResults for `tao_get_last_error()`:\n");
    fprintf(stdout, " ├─ %ld errors / %ld calls\n", nfails, ncalls);
    fprintf(stdout, " ├─ average time spent: %.3f ns\n", tavg*1e9);
    fprintf(stdout, " ├─ minimum time spent: %.3f ns\n", tmin*1e9);
    fprintf(stdout, " └─ maximum time spent: %.3f ns\n", tmax*1e9);
    test(nfails == 0);

    // Summary.
    fprintf(stdout, "\nSummary of tests::\n");
    fprintf(stdout, " ├─ %ld test(s) passed\n", ntests - nerrors);
    fprintf(stdout, " └─ %ld test(s) failed\n", nerrors);
    return (nerrors == 0 ? EXIT_SUCCESS : EXIT_FAILURE);
}
