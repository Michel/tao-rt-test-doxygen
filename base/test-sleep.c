// test-sleep.c --
//
// Test sleeping for a while.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2021, Éric Thiébaut.

#include <tao-utils.h>

#include <stdbool.h>
#include <string.h>
#include <unistd.h>

static inline void get_monotonic_time(
    struct timespec* ts)
{
    clock_gettime(CLOCK_MONOTONIC, ts);
    //if (tao_get_monotonic_time(ts) != TAO_OK) {
    //    tao_panic();
    //}
}

static inline double elapsed(
    const struct timespec* t0)
{
    struct timespec t1;
    get_monotonic_time(&t1);
    return ((double)(t1.tv_sec - t0->tv_sec) +
            1E-9*(double)(t1.tv_nsec - t0->tv_nsec));
}

int main(
    int argc,
    char* argv[])
{
    double amount = 0.001;
    long number = 100000;
    bool use_usleep = false;
    const char* progname = tao_basename(argv[0]);

    // Parse arguments.
    int i = 0;
    while (++i < argc) {
        if (argv[i][0] != '-') {
            break;
        } else if (strcmp(argv[i], "-h") == 0 ||
                   strcmp(argv[i], "--help") == 0) {
            printf("Usage: %s [OPTIONS...] [--] "
                   "[NUMBER [AMOUNT [UNITS]]]\n\n", progname);
            printf("Test sleeping.  Options are:\n");
            printf("    -h, --help    "
                   "Print this help.\n");
            printf("    --usleep      "
                   "Use usleep() function.\n");
            printf("\nArguments are:\n");
            printf("    NUMBER        "
                   "Number of tests to run [%ld].\n", number);
            printf("    AMOUNT        "
                   "Amount of time to sleep [%g].\n", amount);
            printf("    UNITS         "
                   "Time units, one of 's', 'ms', 'µs' or 'ns' [s].\n");
            exit(0);
        } else if (strcmp(argv[i], "--usleep") == 0) {
            use_usleep = true;
        } else if (strcmp(argv[i], "--") == 0) {
            ++i;
            break;
        }
    }
    if (argc - i > 3) {
        fprintf(stderr, "%s: too many arguments\n", progname);
        return EXIT_FAILURE;
    }
    if (i < argc) {
        if (tao_parse_long(argv[i], &number, 0) != TAO_OK || number < 1) {
            fprintf(stderr, "%s: invalid number of tests %s\n",
                    progname, argv[i]);
            return EXIT_FAILURE;
        }
        ++i;
    }
    if (i < argc) {
        if (tao_parse_double(argv[i], &amount) != TAO_OK || amount < 0) {
            fprintf(stderr, "%s: invalid amount of time %s\n",
                    progname, argv[i]);
            return EXIT_FAILURE;
        }
        ++i;
    }
    if (i < argc) {
        if (strcmp(argv[i], "s") == 0) {
            amount *= 1.0;
        } else if (strcmp(argv[i], "ms") == 0) {
            amount *= 1e-3;
        } else if (strcmp(argv[i], "us") == 0 || strcmp(argv[i], "µs") == 0) {
            amount *= 1e-6;
        } else if (strcmp(argv[i], "ns") == 0) {
            amount *= 1e-9;
        } else {
            fprintf(stderr, "%s: invalid time units %s\n",
                    progname, argv[i]);
            return EXIT_FAILURE;
        }
    }

    // Run tests.
    tao_time_stat_data tsd;
    struct timespec t0;
    tao_initialize_time_statistics(&tsd);
    if (use_usleep) {
        useconds_t usec = amount*1e6 + 0.5;
        amount = usec*1e-6;
        while (tsd.numb < number) {
            get_monotonic_time(&t0);
            usleep(usec);
            double t = elapsed(&t0);
            tao_update_time_statistics(&tsd, t);
        }
    } else {
        while (tsd.numb < number) {
            get_monotonic_time(&t0);
            tao_sleep(amount);
            double t = elapsed(&t0);
            tao_update_time_statistics(&tsd, t);
        }
    }

    // Print time statistics.
    tao_time_stat ts;
    tao_compute_time_statistics(&ts, &tsd);
    fprintf(stdout, "  number of tests ---> %ld\n", ts.numb);
    fprintf(stdout, "  min. sleep time ---> %8.3f µs\n", 1E6*ts.min);
    fprintf(stdout, "  max. sleep time ---> %8.3f µs\n", 1E6*ts.max);
    if (ts.numb >= 2) {
	fprintf(stdout, "  avg. sleep time ---> %8.3f +/- %.3f µs\n",
		1E6*ts.avg, 1E6*ts.std);
    }
    fprintf(stdout, "  requested time ---> %8.3f µs\n", 1E6*amount);
    return EXIT_SUCCESS;
}
