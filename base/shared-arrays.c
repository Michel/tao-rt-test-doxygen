// shared-arrays.c -
//
// Implementation of multi-dimensional arrays whose contents can be shared
// between processes.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2018-2022, Éric Thiébaut.

#include "tao-shared-arrays-private.h"
#include "tao-generic.h"
#include "tao-errors.h"

tao_eltype tao_shared_array_get_eltype(
    const tao_shared_array* arr)
{
    return (arr != NULL ? arr->eltype : -1);
}

long tao_shared_array_get_length(
    const tao_shared_array* arr)
{
    return (arr != NULL ? arr->nelem : 0);
}

int tao_shared_array_get_ndims(
    const tao_shared_array* arr)
{
    return (arr != NULL ? arr->ndims : 0);
}

long tao_shared_array_get_dim(
    const tao_shared_array* arr,
    int d)
{
    if (arr == NULL || d < 1) {
        return 0;
    } else if (d > TAO_MAX_NDIMS) {
        return 1;
    } else {
        return arr->dims[d-1];
    }
}

void* tao_shared_array_get_data(
    const tao_shared_array* arr)
{
    return (arr != NULL) ? TAO_SHARED_ARRAY_DATA(arr) : (void*)0;
}

tao_serial tao_shared_array_get_serial(
    const tao_shared_array* arr)
{
    return (arr != NULL) ? arr->serial : 0;
}

void tao_shared_array_set_serial(
    tao_shared_array* arr,
    tao_serial serial)
{
    if (arr != NULL) {
        arr->serial = serial;
    }
}

void tao_shared_array_get_timestamp(
    const tao_shared_array* restrict arr,
    int                              idx,
    tao_time*               restrict ts)
{
    if (ts != NULL) {
        if (arr != NULL && 0 <= idx && idx < TAO_SHARED_ARRAY_TIMESTAMPS) {
            *ts = arr->ts[idx];
        } else {
            *ts = TAO_UNKNOWN_TIME;
        }
    }
}

void tao_shared_array_set_timestamp(
    tao_shared_array* restrict arr,
    int                          idx,
    const tao_time*   restrict ts)
{
    if (arr != NULL && ts != NULL && 0 <= idx
        && idx < TAO_SHARED_ARRAY_TIMESTAMPS) {
        arr->ts[idx] = *ts;
    }
}

tao_shared_array* tao_shared_array_create_1d(
    tao_eltype eltype,
    long dim1,
    unsigned flags)
{
    long dims[1];
    dims[0] = dim1;
    return tao_shared_array_create(eltype, 1, dims, flags);
}

tao_shared_array* tao_shared_array_create_2d(
    tao_eltype eltype,
    long dim1,
    long dim2,
    unsigned flags)
{
    long dims[2];
    dims[0] = dim1;
    dims[1] = dim2;
    return tao_shared_array_create(eltype, 2, dims, flags);
}

tao_shared_array* tao_shared_array_create_3d(
    tao_eltype eltype,
    long dim1,
    long dim2,
    long dim3,
    unsigned flags)
{
    long dims[3];
    dims[0] = dim1;
    dims[1] = dim2;
    dims[2] = dim3;
    return tao_shared_array_create(eltype, 3, dims, flags);
}

static inline void forced_store_eltype(
    const tao_eltype* ptr,
    tao_eltype val)
{
    *(tao_eltype*)ptr = val;
}

tao_shared_array* tao_shared_array_create(
    tao_eltype eltype,
    int ndims,
    const long dims[],
    unsigned flags)
{
    size_t elsize = tao_size_of_eltype(eltype);
    if (elsize < 1) {
        tao_store_error(__func__, TAO_BAD_TYPE);
        return NULL;
    }
    long nelem = tao_count_elements(ndims, dims);
    if (nelem < 1) {
        return NULL;
    }
    size_t offset = TAO_SHARED_ARRAY_DATA_OFFSET;
    size_t nbytes = offset + nelem*elsize;
    tao_shared_array* obj =
        (tao_shared_array*)tao_rwlocked_object_create(
            TAO_SHARED_ARRAY, nbytes, flags);
    if (obj == NULL) {
        return NULL;
    }
    tao_forced_store(&obj->nelem, nelem);
    tao_forced_store(&obj->ndims, ndims);
    for (int d = 0; d < ndims; ++d) {
        tao_forced_store(&obj->dims[d], dims[d]);
    }
    for (int d = ndims; d < TAO_MAX_NDIMS; ++d) {
        tao_forced_store(&obj->dims[d], 1);
    }
    forced_store_eltype(&obj->eltype, eltype);
    obj->serial = 0;
    for (int i = 0; i < TAO_SHARED_ARRAY_TIMESTAMPS; ++i) {
        obj->ts[i] = TAO_UNKNOWN_TIME;
    }
    return obj;
}

tao_shared_array* tao_shared_array_fill(
    tao_shared_array* arr,
    double val)
{
    long n = tao_shared_array_get_length(arr);
    void* ptr = tao_shared_array_get_data(arr);
    if (n > 0 && ptr != NULL) {
#define FILL(T)                                 \
        do {                                    \
            T x = (T)val;                       \
            T* a = (T*)ptr;                     \
            for (long i = 0; i < n; ++i) {      \
                a[i] = x;                       \
            }                                   \
        } while (false)
        switch (arr->eltype) {
        case TAO_INT8  : FILL(int8_t  ); break;
        case TAO_UINT8 : FILL(uint8_t ); break;
        case TAO_INT16 : FILL(int16_t ); break;
        case TAO_UINT16: FILL(uint16_t); break;
        case TAO_INT32 : FILL(int32_t ); break;
        case TAO_UINT32: FILL(uint32_t); break;
        case TAO_INT64 : FILL(int64_t ); break;
        case TAO_UINT64: FILL(uint64_t); break;
        case TAO_FLOAT : FILL(float   ); break;
        case TAO_DOUBLE: FILL(double  ); break;
        }
#undef FILL
    }
    return arr;
}

// Generic shared object methods.
#define TYPE shared_array
#define MAGIC TAO_SHARED_ARRAY
#define IS_RWLOCKED_OBJECT 1
#include "./shared-methods.c"
