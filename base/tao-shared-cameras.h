// tao-shared-cameras.h -
//
// Definitions for shared cameras and virtual frame-grabbers.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2022, Éric Thiébaut.

#ifndef TAO_SHARED_CAMERAS_H_
#define TAO_SHARED_CAMERAS_H_ 1

#include <tao-basics.h>
#include <tao-cameras.h>
#include <tao-shared-memory.h>
#include <tao-shared-arrays.h>
#include <tao-shared-objects.h>
#include <tao-remote-objects.h>
#include <tao-utils.h>

TAO_BEGIN_DECLS

//-----------------------------------------------------------------------------
// SHARED CAMERAS

/**
 * @defgroup SharedCameras Shared cameras
 *
 * @ingroup Cameras
 *
 * @brief Shared objects used to communicate with camera servers.
 *
 * A shared camera instance is a structure stored in shared memory which is
 * used to communicate with a camera server.
 *
 * @{
 */

/**
 * @brief Opaque structure to a shared camera.
 *
 * Shared cameras are read/write locked objects (see @ref tao_rwlocked_object)
 * used to communicate with camera servers in TAO.
 *
 * @see tao_shared_camera_.
 */
typedef struct tao_shared_camera_ tao_shared_camera;

/**
 * Attach an existing shared camera to the address space of the caller.
 *
 * This function attaches an existing shared camera to the address space of the
 * caller.  As a result, the number of attachments of the shared camera is
 * incremented by one.  When the camera is no longer used by the caller, the
 * caller must call tao_shared_camera_detach() to detach the camera from its
 * address space, decrement its number of attachments by one and eventually
 * free the shared memory associated with the camera.
 *
 * @warning The same process must not attach a shared camera more than once.
 *
 * @param shmid  Unique identifier of the shared object.
 *
 * @return The address of the shared camera in the address space of the caller;
 * `NULL` on failure.
 *
 * @see tao_shared_camera_detach, tao_shared_camera_get_shmid.
 */
extern tao_shared_camera* tao_shared_camera_attach(
    tao_shmid shmid);

/**
 * Detach a shared camera.
 *
 * Detach a shared camera from the address space of the caller and decrement
 * the number of attachments of the shared camera.
 *
 * @warning Detaching a shared camera does not detach shared arrays backing the
 * storage of the images acquired by this camera.  They have to be explicitly
 * detached.
 *
 * @param cam    Pointer to a shared camera attached to the address space of
 *               the caller.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR on failure.
 *
 * @see tao_shared_camera_attach.
 */
extern tao_status tao_shared_camera_detach(
    tao_shared_camera* cam);

/**
 * Get the identifier of shared camera data.
 *
 * @param cam    Pointer to a shared camera attached to the address space of
 *               the caller and locked by the caller.
 *
 * @return The identifier of the shared camera data.  This value can be used
 *         by another process to attach to its address space the shared camera.
 *         `TAO_BAD_SHMID` is returned if @a cam is `NULL`.
 *
 * @see tao_shared_camera_attach.
 */
extern tao_shmid tao_shared_camera_get_shmid(
    const tao_shared_camera* cam);

/**
 * Get the name of the owner/creator of a shared camera.
 *
 * @param cam    Pointer to a shared camera attached to the address space of
 *               the caller.
 *
 * @return The name of the shared camera owner.  An empty string if `cam` is
 *         `NULL`.  Whatever the result, the last caller's error is left
 *         unchanged.
 */
extern const char* tao_shared_camera_get_owner(
    const tao_shared_camera* cam);

/**
 * Lock a shared camera for reading or writing.
 *
 * Lock a shared camera for read-only or read-write access.  The caller is
 * responsible for eventually releasing the lock with
 * tao_shared_camera_unlock().
 *
 * @param cam    Pointer to a shared camera attached to the address space of
 *               the caller.
 * @param access The requested access mode: `TAO_READ_ACCESS` or
 *               `TAO_WRITE_ACCESS`.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR on failure.
 */
extern tao_status tao_shared_camera_rdlock(
    tao_shared_camera* cam);

/**
 * Attempt to lock a shared camera for reading or writing.
 *
 * Try to lock a shared camera for read-only or read-write access without
 * blocking.  The caller is responsible for eventually releasing the lock with
 * tao_shared_camera_unlock().
 *
 * @param cam    Pointer to a shared camera attached to the address space of
 *               the caller.
 *
 * @param access The requested access mode: `TAO_READ_ACCESS` or
 *               `TAO_WRITE_ACCESS`.
 *
 * @return @ref TAO_OK on success, @ref TAO_TIMEOUT if the lock cannot be
 *         immediately acquired or @ref TAO_ERROR on failure.
 */
extern tao_status tao_shared_camera_try_rdlock(
    tao_shared_camera* cam);

/**
 * Lock a shared camera for reading or writing with a timeout.
 *
 * Try to lock a shared camera for read-only or read-write access without
 * blocking more than a given duration.  The caller is responsible for
 * eventually releasing the lock with tao_shared_camera_unlock().
 *
 * @param cam    Pointer to a shared camera attached to the address space of
 *               the caller.
 *
 * @param access The requested access mode: `TAO_READ_ACCESS` or
 *               `TAO_WRITE_ACCESS`.
 *
 * @param secs   Maximum time to wait (in seconds).  If this amount of time is
 *               very large, e.g. more than @ref TAO_MAX_TIME_SECONDS, the
 *               effect is the same as calling tao_shared_camera_rdlock().  If
 *               this amount of time is very short, the effect is the same as
 *               calling tao_shared_camera_try_rdlock().
 *
 * @return @ref TAO_OK if the lock has been locked by the caller before the
 *         specified time limit, @ref TAO_TIMEOUT if timeout occurred before or
 *         @ref TAO_ERROR in case of error.
 */
extern tao_status tao_shared_camera_abstimed_rdlock(
    tao_shared_camera* cam,
    const tao_time* abstime);

extern tao_status tao_shared_camera_timed_rdlock(
    tao_shared_camera* cam,
    double secs);

extern tao_status tao_shared_camera_wrlock(
    tao_shared_camera* cam);

extern tao_status tao_shared_camera_try_wrlock(
    tao_shared_camera* cam);

extern tao_status tao_shared_camera_abstimed_wrlock(
    tao_shared_camera* cam,
    const tao_time* abstime);

extern tao_status tao_shared_camera_timed_wrlock(
    tao_shared_camera* cam,
    double secs);

/**
 * Unlock a shared camera.
 *
 * @param cam    Pointer to a shared camera attached to the address space of
 *               the caller.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR on failure.
 *
 * @see tao_shared_object_unlock.
 */
extern tao_status tao_shared_camera_unlock(
    tao_shared_camera* cam);

/**
 * Get the current state of the camera.
 *
 * @param cam   Address of shared camera in address space of caller.
 *
 * @return `-1` if @a cam is `NULL`; otherwise, `0` if device not yet open, `1`
 *         if device open but no acquisition is running, `2` if acquisition is
 *         running.
 */
extern tao_state tao_shared_camera_get_state(
    const tao_shared_camera* cam);

/**
 * Get the pixel type for the captured images after pre-processing.
 *
 * @param cam   Address of shared camera in address space of caller.
 *
 * @return The pixel type for the captured images after pre-processing, `-1`
 *         if @a cam is `NULL`.
 */
extern tao_eltype tao_shared_camera_get_pixeltype(
    const tao_shared_camera* cam);

/**
 * Get the encoding of pixels in images sent by the camera.
 *
 * @param cam   Address of shared camera in address space of caller.
 *
 * @return The encoding of pixels in the raw captured images,
 *         `TAO_ENCODING_UNKNOWN` if @a cam is `NULL`.
 */
extern tao_encoding tao_shared_camera_get_sensorencoding(
    const tao_shared_camera* cam);

/**
 * Get the encoding of pixels in acquisition buffers.
 *
 * @param cam   Address of shared camera in address space of caller.
 *
 * @return The encoding of pixels in acquisition buffers,
 *         `TAO_ENCODING_UNKNOWN` if @a cam is `NULL`.
 */
extern tao_encoding tao_shared_camera_get_bufferencoding(
    const tao_shared_camera* cam);

/**
 * Get the width of the detector.
 *
 * This function yields the number of pixels per line of the detector which is
 * the maximum width for captured iamges.
 *
 * @param cam   Address of shared camera in address space of caller.
 *
 * @return The number of pixels per line of the detector, `0` if @a cam is
 *         `NULL`.
 */
extern long tao_shared_camera_get_sensorwidth(
    const tao_shared_camera* cam);

/**
 * Get the height of the detector.
 *
 * This function yields the number of lines of pixels of the detector which is
 * the maximum height for captured iamges.
 *
 * @param cam   Address of shared camera in address space of caller.
 *
 * @return The number of lines of pixels of the detector, `0` if @a cam is
 *         `NULL`.
 */
extern long tao_shared_camera_get_sensorheight(
    const tao_shared_camera* cam);

/**
 * Get the horizontal binning factor.
 *
 * @param cam   Address of shared camera in address space of caller.
 *
 * @return The horizontal binning factor in physical pixels, `0` if
 *         @a cam is `NULL`.
 */
extern long tao_shared_camera_get_xbin(
    const tao_shared_camera* cam);

/**
 * Get the vertical binning factor.
 *
 * @param cam   Address of shared camera in address space of caller.
 *
 * @return The vertical binning factor in physical pixels, `0` if
 *         @a cam is `NULL`.
 */
extern long tao_shared_camera_get_ybin(
    const tao_shared_camera* cam);

/**
 * Get the horizontal offset of captured images.
 *
 * @param cam   Address of shared camera in address space of caller.
 *
 * @return The horizontal offset in physical pixels of the region of interest
 *         set for the captured images, `0` if @a cam is `NULL`.
 */
extern long tao_shared_camera_get_xoff(
    const tao_shared_camera* cam);

/**
 * Get the vertical offset of captured images.
 *
 * @param cam   Address of shared camera in address space of caller.
 *
 * @return The vertical offset in physical pixels of the region of interest
 *         set for the captured images, `0` if @a cam is `NULL`.
 */
extern long tao_shared_camera_get_yoff(
    const tao_shared_camera* cam);

/**
 * Get the width of the captured images.
 *
 * This function yields the number of macro-pixels per line of the captured
 * images.  If no sub-sampling nor re-binning of physical pixels is used a
 * macro-pixel corresponds to a physical pixel.
 *
 * @param cam   Address of shared camera in address space of caller.
 *
 * @return The number of macro-pixels per line of the captured images, `0` if
 *         @a cam is `NULL`.
 */
extern long tao_shared_camera_get_width(
    const tao_shared_camera* cam);

/**
 * Get the height of the captured images.
 *
 * This function yields the number of lines of macro-pixels in the captured
 * images.  If no sub-sampling nor re-binning of physical pixels is used a
 * macro-pixel corresponds to a physical pixel.
 *
 * @param cam   Address of shared camera in address space of caller.
 *
 * @return The number of lines of macro-pixels in the captured images, `0` if
 *         @a cam is `NULL`.
 */
extern long tao_shared_camera_get_height(
    const tao_shared_camera* cam);

/**
 * Get the frame rate.
 *
 * @param cam   Address of shared camera in address space of caller.
 *
 * @return The number of frame per seconds, `0` if @a cam is `NULL`.
 */
extern double tao_shared_camera_get_framerate(
    const tao_shared_camera* cam);

/**
 * Get the duration of the exposure.
 *
 * @param cam   Address of shared camera in address space of caller.
 *
 * @return The exposure time in seconds for the captured images, `0` if @a cam
 *         is `NULL`.
 */
extern double tao_shared_camera_get_exposuretime(
    const tao_shared_camera* cam);

/**
 * Get the length of the list of shared arrays memorized by the owner of
 * a shared camera.
 *
 * @param cam    Pointer to a shared camera attached to the address space of
 *               the caller and locked by the caller.
 *
 * @note The length of the list is immutable, the caller does not have to lock
 *       the shared camera to insure consistency of the result.
 *
 * @return The length of the list of shared arrays memorized by the owner of
 *         the shared camera, `0` if @a cam is `NULL`.  Whatever the result,
 *         the last caller's error is left unchanged.
 *
 * @see tao_shared_camera_rdlock.
 */
extern long tao_shared_camera_get_nbufs(
    const tao_shared_camera* cam);

/**
 * Get the serial number of the last available image.
 *
 * This function yields the serial number of the last image available from a
 * shared camera.  This is also the number of images posted by the server
 * owning the shared camera so far.
 *
 * @param cam    Pointer to a shared camera attached to the address space of
 *               the caller and locked by the caller.
 *
 * @note The serial number of last image may change (i.e., when acquisition is
 *       running), but the caller may not have locked the shared camera because
 *       the serial number is an atomic variable.  For efficiency reasons, this
 *       function does not perform error checking.
 *
 * @return A nonnegative integer.  A strictly positive value which is the
 *         serial number of the last available image if any, 0 if image
 *         acquisition has not yet started of if `cam` is `NULL`.  Whatever the
 *         result, the last caller's error is left unchanged.
 *
 * @see tao_shared_camera_rdlock.
 */
extern tao_serial tao_shared_camera_get_serial(
    const tao_shared_camera* cam);

/**
 * Get the identifier of the last acquired image.
 *
 * This function yields the shared memory identifier of the last image acquired
 * by a shared camera.  Since the last image may change (because acquisition is
 * running), the caller is assumed to have locked the shared camera, at least
 * for read-only access.
 *
 * @param cam    Pointer to a shared camera attached to the address space of
 *               the caller and locked by the caller.
 *
 * @return The identifier of the last acquired image or @ref TAO_BAD_SHMID if
 *         none or if @a cam is `NULL`.
 *
 * @see tao_shared_camera_rdlock, tao_attach_last_image.
 */
extern tao_shmid tao_get_last_image_shmid(
    const tao_shared_camera* cam);

/**
 * Get the identifier of the next acquired image.
 *
 * @param cam    Pointer to a shared camera attached to the address space of
 *               the caller and locked by the caller.
 *
 * @warning Since the next image may change (because acquisition is running),
 *          the caller is assumed to have locked the shared camera, at least
 *          for read-only access.
 *
 * @return The identifier of the last acquired image or @ref TAO_BAD_SHMID if
 *         none or if @a cam is `NULL`.
 *
 * @see tao_shared_camera_rdlock.
 */
extern tao_shmid tao_get_next_image_shmid(
    const tao_shared_camera* cam);

/**
 * Attach the last acquired image to the address space of the caller.
 *
 * This function attaches the last image acquired by a frame grabber to the
 * address space of the caller.  The caller is responsible of calling
 * tao_shared_array_detach() to detach the image from its address space.
 *
 * Since the last image may change (because acquisition is running), the caller
 * is assumed to have locked the shared camera, at least for read-only access.
 * It is the caller responsability to lock the returned image to make sure it
 * is not overwritten while reading its contents.
 *
 * @param cam    Address of a shared camera attached to the address space of
 *               the caller.
 *
 * The following example shows how to retrieve the last image data provided
 * the image counter has increased since previous image:
 *
 * ~~~~~{.c}
 * tao_shared_camera* cam = ...;
 * uint64_t previous_counter = ...;
 * tao_shared_array* arr = NULL;
 * tao_shared_camera_rdlock(NULL, cam);
 * {
 *     tao_serial last_counter = tao_shared_camera_get_serial(cam);
 *     if (last_counter > previous_counter) {
 *         arr = tao_attach_last_image(NULL, cam);
 *         previous_counter = last_counter;
 *     }
 * }
 * tao_shared_camera_unlock(NULL, cam);
 * ~~~~~
 *
 * Note the use of braces to emphasizes the block of statements protected by
 * the lock.  Also note that `NULL` is passed as the address of the variable to
 * track errors so any error will be considered as fatal in this example.
 *
 * The same example without tao_attach_last_image():
 *
 * ~~~~~{.c}
 * tao_shared_camera* cam = ...;
 * uint64_t previous_counter = ...;
 * tao_shared_array* arr = NULL;
 * tao_shared_camera_rdlock(NULL, cam);
 * {
 *     tao_serial last_counter = tao_shared_camera_get_serial(cam);
 *     if (last_counter > previous_counter) {
 *         tao_shmid shmid = tao_get_last_image_shmid(cam);
 *         if (shmid != TAO_BAD_SHMID) {
 *             arr = tao_shared_array_attach(NULL, shmid);
 *             previous_counter = last_counter;
 *         }
 *     }
 * }
 * tao_shared_camera_unlock(NULL, cam);
 * ~~~~~
 *
 * @return The address of the shared camera in the address space of the caller;
 *         `NULL` on failure or if there is no valid last image.
 *
 * @see tao_get_last_image_shmid, tao_shared_camera_get_serial,
 *      tao_attach_next_image.
 */
extern tao_shared_array* tao_attach_last_image(
    tao_shared_camera* cam);

/**
 * Attach the next acquired image to the address space of the caller.
 *
 * This function attaches the next image that will be acquired by a frame
 * grabber to the address space of the caller.  The caller is responsible of
 * calling tao_shared_array_detach() to detach the image from its address
 * space.
 *
 * Since the next image may change (because acquisition is running), the caller
 * is assumed to have locked the shared camera, at least for read-only access.
 *
 * The next image is locked for writing by the camera server until the next
 * image is acquired and pre-processed.  In order to wait for the acquisition
 * to complete, the caller shall attempt to lock the returned image for reading
 * (possibly with a timeout) this also ensure it is not overwritten while
 * reading its contents.  Note that the frame counter of the shared image is
 * updated by the camera server just before unlocking the image.
 *
 * @param cam    Address of a shared camera attached to the address space of
 *               the caller.
 *
 * The following example shows how to retrieve the next images data while
 * acquisition is running and making sure the image counter does increase
 * between successive images:
 *
 * ~~~~~{.c}
 * tao_shared_camera* cam = ...;
 * uint64_t previous_counter = 0;
 * tao_shared_array* arr = NULL;
 * bool acquiring = true;
 * while (acquiring) {
 *     tao_shared_camera_rdlock(NULL, cam);
 *     {
 *         arr = tao_attach_next_image(NULL, cam);
 *     }
 *     tao_shared_camera_unlock(NULL, cam);
 *     tao_shared_array_rdlock(NULL, arr); // block until acquisition done
 *     {
 *         uint64_t counter = tao_shared_array_get_counter(arr);
 *         if (counter > previous_counter) {
 *             ...; // do something with the image data
 *             previous_counter = counter;
 *         } else if (counter == 0) {
 *             // acquisition has been stopped
 *             acquiring = false;
 *         }
 *     }
 *     tao_shared_array_unlock(NULL, arr);
 *     tao_shared_array_detach(NULL, arr);
 * }
 * ~~~~~
 *
 * Note the use of braces to emphasizes the block of statements protected by
 * the lock.  Also note that `NULL` is passed as the address of the variable to
 * track errors so any error will be considered as fatal in this example.
 *
 * @return The address of the shared camera in the address space of the caller;
 *         `NULL` on failure or if there is no valid last image.
 *
 * @see tao_get_next_image_shmid, tao_attach_last_image.
 */
extern tao_shared_array* tao_attach_next_image(
    tao_shared_camera* cam);

/**
 * @}
 */

//-----------------------------------------------------------------------------
// VIRTUAL FRAME-GRABBERS

/**
 * @defgroup FrameGrabbers  Frame-grabbers
 *
 * @ingroup Cameras
 *
 * @brief Virtual frame-grabbers implemented as cyclic list of shared arrays.
 *
 * A TAO virtual frame-grabber is intended to implement image servers.  It
 * consists in a shared camera and a circular list of shared arrays.  The
 * shared camera is used to provide, as quickly as possible, information to the
 * clients (which are supposed to only access this structure for reading) and
 * the shared arrays are used to store the (pre-processed) images.
 *
 * A camera server connects to a camera device (via an instance of
 * `tao_camera`) and creates a virtual frame grabber (an instance of
 * `tao_framegrabber`).  The virtual frame grabber has an embeded shared
 * camera (an instance of `tao_shared_camera`) to communicate with the
 * clients and a cyclic buffer of shared arrays to store acquired images.
 *
 * Assuming `vfg` is the virtual frame-grabber (an address to an instance of
 * `tao_framegrabber`) and `dev` is the camera device (an address to an
 * instance of `tao_camera`), a typical acquisition loop is:
 *
 * ~~~~~{.c}
 * // Get shared camera associated with the frame-grabber:
 * tao_shared_camera* cam = tao_framegrabber_get_shared_camera(vfg);
 *
 * // Start acquisition.
 * tao_shared_camera_wrlock(cam);
 * tao_camera_start_acquisition(dev);
 * tao_framegrabber_start_acquisition(vfg);
 * tao_shared_camera_unlock(cam);
 * while (running) {
 *     // Wait for the next acquired image by the camera device.
 *     tao_camera_wait(dev);
 *
 *     // Get next acquisition frame buffer (locked for read-write access).
 *     tao_shared_array* arr = tao_framegrabber_get_buffer(vfg);
 *
 *     // Convert image data and update timestamp.
 *     ...;
 *
 *     // Post acquisition frame buffer.
 *     tao_shared_camera_wrlock(cam);
 *     tao_framegrabber_post_buffer(vfg);
 *     tao_shared_camera_unlock(cam);
 * }
 * tao_shared_camera_wrlock(cam);
 * tao_framegrabber_stop_acquisition(vfg);
 * tao_shared_camera_unlock(cam);
 * ~~~~~
 *
 * @{
 */

/**
 * Opaque frame-grabber structure for a server.
 */
typedef struct tao_framegrabber_ tao_framegrabber;

/**
 * Create a frame-grabber structure.
 *
 * The caller is responsible for eventually calling
 * tao_framegrabber_destroy().
 *
 * @param owner    Short string identifying the creator of the shared
 *                 resources. Can be `NULL`, otherwise, maximal size including
 *                 the final null character is `TAO_OWNER_SIZE`.
 * @param nbufs    Number of shared arrays to memorize (at least 2).
 * @param flags    Options and client access permissions for the shared data.
 *
 * @return The address of a frame-grabber structure; `NULL` in case of errors.
 *
 * @see tao_framegrabber_destroy().
 */
extern tao_framegrabber* tao_framegrabber_create(
    const char* owner,
    int length,
    unsigned perms);

/**
 * Finalize a frame-grabber structure.
 *
 * @param fg       Address of the frame-grabber structure (can be `NULL`).
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR on failure.
 *
 * @see tao_framegrabber_create().
 */
extern tao_status tao_framegrabber_destroy(
    tao_framegrabber* fg);

/**
 * Get shared camera associated with a frame-grabber.
 *
 * @param fg    Address of the frame-grabber structure.
 *
 * @return The address of the shared camera information, `NULL` on error.
 *
 * @see tao_shared_camera_attach.
 */
extern tao_shared_camera* tao_framegrabber_get_shared_camera(
    tao_framegrabber* fg);

/**
 * Get current frame-grabber acquisition buffer.
 *
 * The current frame-grabber acquisition buffer is a shared array which is
 * locked for read-write access by the frame-grabber.

 * @param fg    Address of the frame-grabber structure.
 *
 * @return The address of the shared array corresponding to the current
 *         acquisition buuffer, `NULL` if none.
 *
 * @see tao_shared_camera_attach.
 */
extern tao_shared_array* tao_framegrabber_get_buffer(
    tao_framegrabber* fg);

/**
 * Post frame-grabber acquisition buffer.
 *
 * This function is called by the owner of the frame-grabber once a new
 * acquired image has been received, pre-processed and the result stored into
 * the acquisition buffer to made available for the clients.
 *
 * The frame-grabber acquisition buffer is a shared array that is locked for
 * read-write access by the frame-grabber and that can be retrieved by calling
 * tao_framegrabber_get_buffer().
 *
 * Provided the frame-grabber currently owns an acquisition buffer for
 * read-write access, posting the acquisition buffer is done as follows.  The
 * counter of the acquisition buffer is incremented and, if its time-stamp has
 * not been updated, it is set according to the current monotonic time.  The
 * acquisition buffer is then unlocked so that pending readers are immediately
 * unblocked.
 *
 * After posting the acquisition buffer, the next acquisition buffer in the
 * frame-grabber is chosen and locked for read-write access.  If the next
 * acquisition buffer in the frame-grabber list of buffers is suitable (has
 * correct element type and size) and can immediately be locked, it is
 * recycled; otherwise a new acquisition buffer is allocated to replace the
 * next acquisition buffer in the list, detaching the former acquisition
 * buffer.  This strategy is to spare the time needed for creating acquisition
 * buffers (and for clients the time to attach and detach these buffers) while
 * keeping the frame-graber responsive in case clients do not unlock
 * acquisition buffers fast enough.
 *
 * This function must be called while the caller has locked the shared camera
 * (see tao_framegrabber_get_shared_camera()) associated with the frame-grabber
 * for read-write access.  A typical usage is (ignoring errors):
 *
 * ~~~~~{.c}
 * tao_shared_camera* cam = tao_framegrabber_get_shared_camera(fg);
 * tao_shared_camera_wrlock(cam);
 * tao_framegrabber_post_buffer(fg);
 * tao_shared_camera_unlock(cam);
 * ~~~~~
 *
 * @param fg     Address of the frame-grabber structure.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR on failure.
 */
extern tao_status tao_framegrabber_post_buffer(
    tao_framegrabber* fg);

/**
 * Initialize frame-grabber acquisition.
 *
 * This function is called by the owner of the frame-grabber before entering
 * the image acquisition loop to prepare the first acquisition buffer.
 *
 * The frame-grabber acquisition buffer is a shared array that is locked for
 * read-write access by the frame-grabber and that can be retrieved by calling
 * tao_framegrabber_get_buffer().
 *
 * This function must be called while the caller has locked the shared camera
 * (see tao_framegrabber_get_shared_camera()) associated with the frame-grabber
 * for read-write access.  A typical usage is:
 *
 * ~~~~~{.c}
 * tao_shared_camera* cam = tao_framegrabber_get_shared_camera(fg);
 * tao_shared_camera_wrlock(NULL, cam);
 * tao_framegrabber_start_acquisition(NULL, fg);
 * tao_shared_camera_unlock(NULL, cam);
 * ~~~~~
 *
 * @param fg     Address of the frame-grabber structure.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR on failure.
 */
extern tao_status tao_framegrabber_start_acquisition(
    tao_framegrabber* fg);

/**
 * Finalize frame-grabber acquisition.
 *
 * This function is called by the owner of the frame-grabber after exiting
 * the image acquisition loop to drop the last (unused) acquisition buffer.
 *
 * The frame-grabber acquisition buffer is a shared array that is locked for
 * read-write access by the frame-grabber and that can be retrieved by calling
 * tao_framegrabber_get_buffer().
 *
 * This function must be called while the caller has locked the shared camera
 * (see tao_framegrabber_get_shared_camera()) associated with the frame-grabber
 * for read-write access.  A typical usage is:
 *
 * ~~~~~{.c}
 * tao_shared_camera* cam = tao_framegrabber_get_shared_camera(fg);
 * tao_shared_camera_wrlock(NULL, cam);
 * tao_framegrabber_stop_acquisition(NULL, fg);
 * tao_shared_camera_unlock(NULL, cam);
 * ~~~~~
 *
 * @param fg     Address of the frame-grabber structure.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR on failure.
 */
extern tao_status tao_framegrabber_stop_acquisition(
    tao_framegrabber* fg);

/**
 * @}
 */

TAO_END_DECLS

#endif // TAO_SHARED_CAMERAS_H_
