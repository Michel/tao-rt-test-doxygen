// buffers.c -
//
// Dynamic i/o buffers in TAO library.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2018-2022, Éric Thiébaut.

#include "tao-buffers.h"
#include "tao-macros.h"
#include "tao-errors.h"
#include "tao-utils.h"

#include <errno.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define if_likely(expr)   if TAO_LIKELY(expr)
#define if_unlikely(expr) if TAO_UNLIKELY(expr)

// These bits are non-zero so that a static buffer whose structure is initially
// zero-filled is correctly interpreted.
#define VOLATILE_STRUCT  (1 << 0)
#define VOLATILE_DATA    (1 << 1)

#define GRAIN   64
#define MINSIZE 256

static inline tao_status check_buffer_struct(
    const tao_buffer* buf)
{
    if_unlikely(buf == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    if_unlikely((buf->data == NULL && buf->size != 0) ||
                buf->offset > buf->size ||
                buf->pending > buf->size ||
                buf->offset + buf->pending > buf->size) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
        return TAO_OK;
}

// Yields next size of buffer when growing.  If size is less or equal MINSIZE,
// return MINSIZE; otherwise, return 1.5 times the size rounded up to GRAIN
// bytes.  This is to avoid reallocating too many times.
static size_t next_buffer_size(
    size_t size)
{
    if (size <= MINSIZE) {
        return MINSIZE;
    } else {
        return TAO_ROUND_UP(size + (size >> 1), GRAIN);
    }
}

// Having a zero-filled structure is correctly interpreted as an initialized
// empty static buffer.  This has the same effects as initailizing the buffer
// contents with the macro TAO_BUFFER_INITIALIZER.
void tao_buffer_initialize(
    tao_buffer* buf)
{
    buf->data = NULL;
    buf->size = 0;
    buf->offset = 0;
    buf->pending = 0;
    buf->flags = 0;
}

tao_buffer* tao_buffer_create(
    size_t size)
{
    tao_buffer* buf = (tao_buffer*)tao_malloc(sizeof(tao_buffer));
    if (buf != NULL) {
        buf->data = NULL;
        buf->size = 0;
        buf->offset = 0;
        buf->pending = 0;
        buf->flags = VOLATILE_STRUCT;
        if (size > 0) {
            if (size < MINSIZE) {
                size = MINSIZE;
            } else {
                size = TAO_ROUND_UP(size, GRAIN);
            }
            buf->data = tao_malloc(size);
            if (buf->data == NULL) {
                free((void*)buf);
                return NULL;
            }
            buf->size = size;
            buf->flags |= VOLATILE_DATA;
        }
    }
    return buf;
}

void tao_buffer_destroy(
    tao_buffer* buf)
{
    if (buf != NULL) {
        void* data = buf->data;
        unsigned int flags = buf->flags;
        if ((flags & VOLATILE_STRUCT) != 0) {
            // Free the container.
            free((void*)buf);
        } else {
            // Reset the container.
            tao_buffer_initialize(buf);
        }
        if (data != NULL && (flags & VOLATILE_DATA) != 0) {
            // Free the contents.
            free(data);
        }
    }
}

tao_status tao_buffer_resize(
    tao_buffer* buf,
    size_t cnt)
{
    // Check arguments.
    if (check_buffer_struct(buf) != TAO_OK) {
        return TAO_ERROR;
    }

    // If all contents has been consumed, reset the offset to avoid
    // unnecessarily storing bytes in the middle of the buffer.
    if (buf->pending < 1) {
        buf->offset = 0;
    }

    // If the number of available bytes in the buffer is insufficient, either
    // move the buffer contents or resize the buffer.
    size_t avail = buf->size - (buf->offset + buf->pending);
    if (cnt > avail) {
        if (buf->pending + cnt <= buf->size) {
            // Buffer size is sufficient, just move the contents.
            tao_buffer_flush(buf);
        } else {
            // Reallocate the buffer.
            size_t minsize = buf->pending + cnt;
            size_t newsize = next_buffer_size(minsize);
            char* olddata = buf->data;
            char* newdata = (char*)tao_malloc(newsize);
            if (newdata == NULL) {
                return TAO_ERROR;
            }
            if (buf->pending > 0) {
                // Copy the contents of the old buffer at the beginning of the
                // new buffer.
                memcpy(newdata, olddata + buf->offset, buf->pending);
            }
            buf->offset = 0;
            buf->data = newdata;
            buf->size = newsize;
            if ((buf->flags & VOLATILE_DATA) == 0) {
                // Mark the internal data as being volatile.
                buf->flags |= VOLATILE_DATA;
            } else if (olddata != NULL) {
                // Free the old volatile data.
                free((void*)olddata);
            }
        }

        // Update number of available bytes.
        avail = buf->size - (buf->offset + buf->pending);
        if (avail < cnt) {
            tao_store_error(__func__, TAO_ASSERTION_FAILED);
            return TAO_ERROR;
        }
    }
    return TAO_OK;
}

void tao_buffer_flush(
    tao_buffer* buf)
{
    if (buf->offset > 0 && buf->pending > 0) {
        // Move the contents to the beginning of the internal buffer.
        memmove(buf->data, buf->data + buf->offset, buf->pending);
        buf->offset = 0;
    }
}

void tao_buffer_clear(
    tao_buffer* buf)
{
    buf->pending = 0;
    buf->offset = 0;
}

size_t tao_buffer_get_contents_size(
    tao_buffer* buf)
{
    return buf->pending;
}

void* tao_buffer_get_contents(
    const tao_buffer* buf,
    size_t* sizptr)
{
    if (sizptr != NULL) {
        *sizptr = buf->pending;
    }
    return buf->data + buf->offset;
}

size_t tao_buffer_get_unused_size(
    const tao_buffer* buf)
{
    return buf->size - (buf->offset + buf->pending);
}

size_t tao_buffer_get_total_unused_size(
    const tao_buffer* buf)
{
    return buf->size - buf->pending;
}

size_t tao_buffer_get_unused_part(
    const tao_buffer* buf,
    void** data)
{
    // Offset of first unused byte.
    size_t offset = buf->offset + buf->pending;
    *data = buf->data + offset;
    return buf->size - offset;
}

tao_status tao_buffer_adjust_contents_size(
    tao_buffer* buf,
    ssize_t adj)
{
    // Check arguments and make the necessary adjustment.
    if (check_buffer_struct(buf) != TAO_OK) {
        return TAO_ERROR;
    }
    if (adj < 0) {
        // Consume some pending bytes.
        size_t cnt = -adj;
        if (cnt < buf->pending) {
            // Some contents has been consumed.
            buf->offset += cnt;
        } else if (cnt == buf->pending) {
            // All contents has been consumed.
            buf->offset = 0;
            buf->pending = 0;
        } else {
            // Refuse to consume more data than available.
            tao_store_error(__func__, TAO_OUT_OF_RANGE);
            return TAO_ERROR;
        }
    } else if (adj > 0) {
        // Some data have been appended to the buffer.
        size_t cnt = +adj;
        size_t maxcnt = buf->size - (buf->offset + buf->pending);
        if (cnt <= maxcnt) {
            // Grow the contents size.
            buf->pending += cnt;
        } else {
            // Refuse to add more data than remaining unused bytes after the
            // contents.
            tao_store_error(__func__, TAO_OUT_OF_RANGE);
            return TAO_ERROR;
        }
    }
    return TAO_OK;
}

ssize_t tao_buffer_read_from_fd(
    tao_buffer* buf,
    int fd,
    size_t cnt)
{
    // Quick return?
    if (cnt < 1) {
        return 0;
    }

    // If all contents has been consumed, reset the offset to avoid
    // unnecessarily storing bytes in the middle of the buffer.
    if (buf->pending < 1) {
        buf->offset = 0;
    }

    // If the number of available bytes in the buffer is insufficient, either
    // move the buffer contents or resize the buffer.
    size_t avail = buf->size - (buf->offset + buf->pending);
    if (cnt > avail) {
        if (buf->pending + cnt <= buf->size) {
            // Buffer size is sufficient, just move the contents.
            tao_buffer_flush(buf);
        } else {
            // Reallocate the buffer.
            if (tao_buffer_resize(buf, cnt) == TAO_ERROR) {
                return -1;
            }
        }

        // Update number of available bytes.
        avail = buf->size - (buf->offset + buf->pending);
        if (avail < cnt) { // FIXME: not necessary
            tao_store_error(__func__, TAO_ASSERTION_FAILED);
            return -1;
        }
    }

    // Attempt to read as many bytes as possible (that is, not just `cnt`
    // bytes).  FIXME: bad idea.
    ssize_t nr = read(fd, buf->data + buf->offset, avail);
    if (nr == -1) {
        // Some error occurred.
        tao_store_system_error("read");
        return -1;
    }
    buf->pending += nr;
    return nr;
}

ssize_t tao_buffer_write_to_fd(
    tao_buffer* buf,
    int fd)
{
    // Check arguments.
    if (check_buffer_struct(buf) != TAO_OK) {
        return TAO_ERROR;
    }

    // If there are no bytes to write.  Reset the offset (flush the contents)
    // and return.
    if (buf->pending < 1) {
        buf->offset = 0;
        return 0;
    }

    // Attempt to write as many bytes as possible.
    ssize_t nw = write(fd, buf->data + buf->offset, buf->pending);
    if (nw > 0) {
        if (nw == buf->pending) {
            // All bytes have been written.  Quickly flush the buffer.
            buf->pending = 0;
            buf->offset  = 0;
        } else {
            // Some bytes have been written.
            buf->pending -= nw;
            buf->offset  += nw;
        }
        return nw;
    }
    if (nw == -1) {
        int code = errno;
        if (code == EAGAIN || code == EWOULDBLOCK) {
            // The operation would block.
            return 0;
        } else {
            // Some error occurred.
            tao_store_system_error("write");
            return -1;
        }
    }
    // No bytes have been written.
    return 0;
}

tao_status tao_buffer_printf(
    tao_buffer* buf,
    const char* format,
    ...)
{
    tao_status status;
    va_list args;

    va_start(args, format);
    status = tao_buffer_vprintf(buf, format, args);
    va_end(args);
    return status;
}

tao_status tao_buffer_vprintf(
    tao_buffer* buf,
    const char* format,
    va_list args)
{
    if (check_buffer_struct(buf) != TAO_OK) {
        return TAO_ERROR;
    }
    long end = buf->offset + buf->pending;
    while (true) {
        // Append formated message to the i/o buffer, resizing and adjusting
        // the size of the buffer as needed.
        va_list temp;
        long len, siz = buf->size - end;
        va_copy(temp, args);
        len = vsnprintf(buf->data + end, siz, format, temp);
        va_end(temp);
        if (len < siz) {
            if (len < 0) {
                // Some error occurred.
                tao_store_system_error("vsnprintf");
                return TAO_ERROR;
            } else {
                // Unused part was large enough.  Adjust buffer size and
                // return.  The final null byte is not considered as part of
                // the contents.
                buf->pending += len;
                return TAO_OK;
            }
        }
        // Unused part was too small.  Enlarge it before retrying.
        if (tao_buffer_resize(buf, len + 1) == TAO_ERROR) {
            return TAO_ERROR;
        }
    }
}

tao_status tao_buffer_append_bytes(
    tao_buffer* buf,
    const void* ptr,
    long siz)
{
    if (siz > 0) {
        if (tao_buffer_resize(buf, siz) == TAO_ERROR) {
            return TAO_ERROR;
        }
        memcpy(buf->data + buf->offset + buf->pending, ptr, siz);
        buf->pending += siz;
    }
    return TAO_OK;
}

tao_status tao_buffer_append_string(
    tao_buffer* buf,
    const char* str,
    long len)
{
    if (len < 0) {
        len = TAO_STRLEN(str);
    }
    if (tao_buffer_resize(buf, len + 1) == TAO_ERROR) {
        return TAO_ERROR;
    }
    long end = buf->offset + buf->pending;
    if (len > 0) {
        memcpy(buf->data + end, str, len);
        buf->pending += len; // not the terminating null
    }
    buf->data[end+len] = '\0';
    return TAO_OK;
}

tao_status tao_buffer_append_char(
    tao_buffer* buf,
    int c)
{
    if (tao_buffer_resize(buf, 2) == TAO_ERROR) {
        return TAO_ERROR;
    }
    long end = buf->offset + buf->pending;
    buf->data[end] = c;
    buf->data[end+1] = '\0';
    buf->pending += 1; // not the terminating null
    return TAO_OK;
}
