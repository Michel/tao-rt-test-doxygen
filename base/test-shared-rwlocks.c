// test-shared-rwlocks.c -
//
// Test process shared read/write locks.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2022, Éric Thiébaut.

#include <tao-rwlocked-objects-private.h>
#include <tao-errors.h>
#include <tao-locks.h>

#include <stdlib.h>
#include <string.h>
#include <unistd.h>

// To test process shared read/write locks, we launch several processes, the
// parent (the writer) and several forked children (the readers).  The locking
// of frame buffers is driven by read/write locks.
//
// All shared data is stored in shared memory via a custom TAO shared
// object.  The time taken by this kind of communication is measured.
//
// If field `sleep` is true, the readers sleep some small amount of time if the
// writer is not ready; otherwise, the readers wait for the writer to be ready
// on a condition variable.
//
// The latency and the losses strongly depend on the frame rate: the higher the
// frame rate, the lower the latency and the higher the number of lost frames.

#define FPS       1000 // frames per seconds
#define FRAMES    1000
#define READERS      3
#define BUFFERS      5
#define MAX_BUFFERS 30 // must be <= the number of bits in an int

typedef struct shared_frame shared_frame;
typedef struct shared_data shared_data;

struct shared_frame {
    tao_rwlock   lock; // Read/write lock
    tao_time     time; // Time stamp
    tao_serial number; // Frame number (at 1kHz, overflow occurs after
		       // more than 292 Myr)
};

struct shared_data {
    tao_rwlocked_object        base; // Base of any TAO shared object
    double                 pollsecs; // Seconds to wait between polls
    double                      fps; // Frames per second
    shared_frame frame[MAX_BUFFERS];
    int                     buffers; // Actual number of frame buffers.
    tao_serial          last_number; // Last frame number
    int                  last_index; // Last frame index (-1 means not started)
    tao_serial          next_number; // Next frame number
    int                  next_index; // Next frame index (-1 means not started)
    bool                   quitting; // All processes supposed to quit
};

static inline void lock_resources_for_reading(
    shared_data* data)
{
    if (tao_rwlocked_object_rdlock(&(data)->base) != TAO_OK) {
        tao_panic();
    }
}

static inline void lock_resources_for_writing(
    shared_data* data)
{
    if (tao_rwlocked_object_wrlock(&(data)->base) != TAO_OK) {
        tao_panic();
    }
}

static inline void unlock_resources(
    shared_data* data)
{
    if (tao_rwlocked_object_unlock(&(data)->base) != TAO_OK) {
        tao_panic();
    }
}

static inline void get_monotonic_time(
    tao_time* t)
{
    if (tao_get_monotonic_time(t) != TAO_OK) {
        tao_panic();
    }
}

static inline void initialize_rwlock(
    tao_rwlock* rwlock,
    bool shared)
{
    if (tao_rwlock_initialize(rwlock, shared) != TAO_OK) {
        tao_panic();
    }
}

static inline void lock_rwlock_for_reading(
    tao_rwlock* rwlock)
{
    if (tao_rwlock_rdlock(rwlock) != TAO_OK) {
        tao_panic();
    }
}

#if 0
static inline void lock_rwlock_for_writing(
    tao_rwlock* rwlock)
{
    if (tao_rwlock_wrlock(rwlock) != TAO_OK) {
        tao_panic();
    }
}
#endif

static inline void unlock_rwlock(
    tao_rwlock* rwlock)
{
    if (tao_rwlock_unlock(rwlock) != TAO_OK) {
        tao_panic();
    }
}

// Type code of the custom TAO shared object.
#define SHARED_DATA (TAO_RWLOCKED_OBJECT | 5)

static double elapsed(
    const tao_time* t1,
    const tao_time* t0)
{
    return ((double)(t1->sec - t0->sec) + 1E-9*(double)(t1->nsec - t0->nsec));
}

// Readers must not attempt to acquire a read lock on a frame buffer while
// having locked the global resources. The writer can do it.

static void run_writer(
    shared_data* data,
    long frames)
{
    double secs;
    double sleeping = 0;
    long overflows = 0;
    long skips = 0;
    long index = -1; // index of frame locked for writing, -1 if none.
    long number = 0; // number of frame locked for writing, 0 if none

    printf("Writer is starting...\n");

    // Initialize inter-frame time and initial time.
    tao_time start, now, next, dt;
    tao_seconds_to_time(&dt, 1.0/data->fps);
    get_monotonic_time(&start);
    next = start;

    // Get index and number of first frame which has already been locked for
    // writing by the main process, that is the writer.
    lock_resources_for_reading(data);
    index  = data->next_index;
    number = data->next_number;
    unlock_resources(data);

    // Compute offset between frame index and frame number.
    long offset = number - index;

    // Loop over frames.
    for (long count = 1; count <= frames; ++count) {
	// Determine arrival time of frame and sleep a bit to simulate
	// writing/arrival of frame data.
	tao_add_times(&next, &next, &dt);
	get_monotonic_time(&now);
	secs = elapsed(&next, &now);
	if (secs > TAO_NANOSECOND) {
            tao_sleep(secs);
            sleeping += secs;
        }

        if (index >= 0) {
            // Register timestamp and frame number, then unlock frame.
            get_monotonic_time(&data->frame[index].time);
            data->frame[index].number = number;
            unlock_rwlock(&data->frame[index].lock);
            index = -1;
        }

        // Update last frame information.
        lock_resources_for_writing(data);
        if (data->next_index >= 0) {
            data->last_index  = data->next_index;
            data->last_number = data->next_number;
        }
        unlock_resources(data);

	// Lock next unlocked frame for writing.
        number += 1; // next frame number
        long i0 = number - offset;
        for (long k = 0; k < data->buffers; ++k) {
            long i = (i0 + k)%(long)data->buffers;
            tao_status status = tao_rwlock_try_wrlock(&data->frame[i].lock);
            if (status == TAO_OK) {
                // Lock for writing was successful.
                index = i;
                break;
            }
            ++skips;
        }
        if (index < 0) {
            ++overflows;
        }

        // Update next frame information.
        lock_resources_for_writing(data);
        data->next_index  = index;
        data->next_number = number;
        unlock_resources(data);
    }

    // Total number of seconds since start.
    get_monotonic_time(&now);
    secs = elapsed(&now, &start);

    // Notify that all readers must quit.
    lock_resources_for_writing(data);
    printf("\nWriter is quitting:\n");
    printf("  overflows ---> %ld\n", overflows);
    printf("  skips -------> %ld\n", skips);
    printf("  sleeping ----> %.1f%%\n", 1e2*sleeping/secs);
    data->quitting = true;
    data->next_index = -1;
    data->next_number = number;
    if (index >= 0) {
        data->frame[index].number = 0;
        unlock_rwlock(&data->frame[index].lock);
        index = -1;
    }
    unlock_resources(data);
}

static void run_reader(
    int shmid)
{
    tao_time now;
    tao_serial previous = 0; // Previously processed frame number
    long spurious = 0; // Number of spurious wake-up
    long waits = 0;
    long losts = 0;
    tao_time_stat_data tsd;

    tao_initialize_time_statistics(&tsd);
    printf("Reader is starting...\n");

    // Attach to shared data.
    shared_data* data = (shared_data*)tao_shared_object_attach(shmid);
    if (data == NULL) {
        tao_panic();
    }
    if (((tao_shared_object*)data)->type != SHARED_DATA) {
        fprintf(stderr, "Invalid shared object type\n");
        exit(EXIT_FAILURE);
    }

    while (true) {
        // Get index of next frame buffer.
        lock_resources_for_reading(data);
        while (! data->quitting && data->next_number <= previous) {
            // Wait for changes.
            double secs = data->pollsecs;
            unlock_resources(data);
            tao_sleep(secs);
            lock_resources_for_reading(data);
            ++waits;
        }
        bool quitting = data->quitting;
        int index = data->next_index;
        unlock_resources(data);

        // Stop if quitting.
        if (quitting) {
            break;
        }

        // Process next frame (if any expected).
        if (index >= 0) {
            // Lock frame for reading, blocking until the frame is unlocked by
	    // the writer.
            lock_rwlock_for_reading(&data->frame[index].lock);

	    // Get current frame number and number N of frames since previously
	    // processed one.
	    tao_serial current = data->frame[index].number;
            if (current > previous) {
                get_monotonic_time(&now);
                double t = elapsed(&now, &data->frame[index].time);
                tao_update_time_statistics(&tsd, t);
                if (current > previous + 1) {
                    // Frame number increment by more than one.
                    losts += current - (previous + 1);
                }
                previous = current;
            } else {
                ++spurious;
            }

            // Unlock frame.
	    unlock_rwlock(&data->frame[index].lock);
	}
    }

    // Get time statistics
    tao_time_stat ts;
    tao_compute_time_statistics(&ts, &tsd);

    // Lock for writing to avoid messy output.
    lock_resources_for_writing(data);
    fprintf(stdout, "\nReader is quitting:\n");
    fprintf(stdout, "  spurious wake-ups ---> %ld\n", spurious);
    fprintf(stdout, "  number of losts -----> %ld\n", losts);
    fprintf(stdout, "  number of waits -----> %ld\n", waits);
    fprintf(stdout, "  number of frames ----> %ld\n", ts.numb);
    fprintf(stdout, "  min. latency time ---> %8.3f µs\n", 1E6*ts.min);
    fprintf(stdout, "  max. latency time ---> %8.3f µs\n", 1E6*ts.max);
    if (ts.numb >= 2) {
	fprintf(stdout, "  avg. latency time ---> %8.3f +/- %.3f µs\n",
		1E6*ts.avg, 1E6*ts.std);
    }
    unlock_resources(data);
}

int main(
    int argc,
    char* argv[])
{
    long  frames = FRAMES;
    long readers = READERS;
    long buffers = BUFFERS;
    double   fps = FPS;
    double  secs = 1e-6;
    tao_status status;

    // Parse arguments.
    int i = 0;
    while (++i < argc) {
        if (argv[i][0] != '-') {
            break;
        } else if (strcmp(argv[i], "-h") == 0 ||
                   strcmp(argv[i], "--help") == 0) {
            printf("Usage: %s [-h|--help] [--frames NUMBER] "
                   "[--buffers NUMBER] [--readers NUMBER] "
                   "[--sleep SECONDS] [--]\n\n", argv[0]);
            printf("Test r/w shared locks.  Options are:\n");
            printf("    --frames NUMBER   "
                   "Number of frames to simulate [%ld].\n", frames);
            printf("    --buffers NUMBER  "
                   "Number of cyclic buffers [%ld].\n", buffers);
            printf("    --readers NUMBER  "
                   "Number of reader processes [%ld].\n", readers);
            printf("    --fps NUMBER      "
                   "Number of frames per second [%.1f].\n", fps);
            printf("    --sleep SECONDS   "
                   "Seconds to sleep between polls [%gs].\n", secs);
            printf("    -h, --help        "
                   "Print this help.\n");
            exit(0);
        } else if (strcmp(argv[i], "--frames") == 0) {
            if (i + 1 >= argc) {
            missing_option_value:
                fprintf(stderr, "missing argument for option %s\n", argv[i]);
                return EXIT_FAILURE;
            }
            if (tao_parse_long(argv[i+1], &frames, 0) != TAO_OK ||
                frames < 10) {
            invalid_option_value:
                fprintf(stderr, "invalid value for option %s\n", argv[i]);
                return EXIT_FAILURE;
            }
            ++i;
        } else if (strcmp(argv[i], "--buffers") == 0) {
            if (i + 1 >= argc) {
                goto missing_option_value;
            }
            if (tao_parse_long(argv[i+1], &buffers, 0) != TAO_OK ||
                buffers < 2 ||
                buffers > MAX_BUFFERS) {
                goto invalid_option_value;
            }
            ++i;
        } else if (strcmp(argv[i], "--readers") == 0) {
            if (i + 1 >= argc) {
                goto missing_option_value;
            }
            if (tao_parse_long(argv[i+1], &readers, 0) != TAO_OK ||
                readers < 0) {
                goto invalid_option_value;
            }
            ++i;
        } else if (strcmp(argv[i], "--fps") == 0) {
            if (i + 1 >= argc) {
                goto missing_option_value;
            }
            if (tao_parse_double(argv[i+1], &fps) != TAO_OK || fps <= 0) {
                goto invalid_option_value;
            }
            ++i;
        } else if (strcmp(argv[i], "--sleep") == 0) {
            if (i + 1 >= argc) {
                goto missing_option_value;
            }
            if (tao_parse_double(argv[i+1], &secs) != TAO_OK || secs < 0) {
                goto invalid_option_value;
            }
            ++i;
        } else if (strcmp(argv[i], "--") == 0) {
            ++i;
            break;
        }
    }
    if (i < argc) {
        fprintf(stderr, "too many arguments\n");
        return EXIT_FAILURE;
    }

    // Create shared resources.
    shared_data* data = (shared_data*)tao_rwlocked_object_create(
        SHARED_DATA, sizeof(shared_data), 0600);
    if (data == NULL) {
        tao_panic();
    }
    for (long index = 0; index < buffers; ++index) {
        initialize_rwlock(&data->frame[index].lock, true);
        data->frame[index].number = 0;
    }
    data->pollsecs = secs;
    data->fps = fps;
    data->buffers = buffers;
    data->quitting = false;
    tao_shmid shmid = tao_rwlocked_object_get_shmid(
        (tao_rwlocked_object*)data);

    // Exercise all r/w locks.
    for (long index = 0; index < buffers; ++index) {
        status = tao_rwlock_try_rdlock(&data->frame[index].lock);
        if (status != TAO_OK) {
            fprintf(stderr, "Unable to lock frame %ld for reading\n", index);
        } else {
            unlock_rwlock(&data->frame[index].lock);
        }
        status = tao_rwlock_try_wrlock(&data->frame[index].lock);
        if (status != TAO_OK) {
            fprintf(stderr, "Unable to lock frame %ld for writing\n", index);
        } else {
            unlock_rwlock(&data->frame[index].lock);
        }
    }

    // Pre-lock the first frame.  So that readers automatically wait for the
    // writer to be ready.
    status = tao_rwlock_try_wrlock(&data->frame[0].lock);
    if (status != TAO_OK) {
        fprintf(stderr, "Unable to lock frame %ld for writing\n", (long)0);
        return EXIT_FAILURE;
    }
    data->next_index  =  0;
    data->next_number =  1;
    data->last_index  = -1;
    data->last_number =  0;

    // Fork to have readers.
    fprintf(stdout,
            "Testing read/write locks for %ld readers, %ld buffers, "
            "%ld frames at %.1f Hz and sleeping for %.0f µs between polls.\n",
            readers, buffers, frames, fps, 1e6*secs);
    if (readers > 0) {
        while (true) {
            if (fork() == 0) {
                // Children are "readers".
                run_reader(shmid);
                break;
            } else {
                // The parent is the "writer".  Start with a small delay (1ms)
                // after having forked the last children to make sure all
                // readers started.
                if (--readers == 0) {
                    tao_sleep(1e-3);
                    run_writer(data, frames);
                    break;
                }
            }
        }
    } else {
        run_writer(data, frames);
    }
    return EXIT_SUCCESS;
}
