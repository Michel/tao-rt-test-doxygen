// conjgrad.c --
//
// Linear conjugate-gradient method in TAO library.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2020-2021, Éric Thiébaut.

#ifndef TAO_CONJGRAD_C_
#define TAO_CONJGRAD_C_ 1

#include <stdbool.h>
#include <string.h>
#include <math.h>

#define MAX(x,y)    ((x) >= (y) ? (x) : (y))

#define BAD_ARGUMENT          -1
#define TOO_MANY_ITERATIONS   -2
#define NOT_POSITIVE_DEFINITE -3
#define XTEST_SATISFIED        1
#define FTEST_SATISFIED        2
#define GTEST_SATISFIED        3

extern void tao_vcopy_dbl(
    double* dst,
    const double* src,
    long n);

extern double tao_vnorm2_dbl(
    const double* x,
    long n);

extern double tao_vsqnorm2_dbl(
    const double* x,
    long n);

extern double tao_vdot_dbl(
    const double* x,
    const double* y,
    long n);

extern void tao_vscale_dbl(
    double alpha,
    double* x,
    long n);

extern void tao_vupdate_dbl(
    double* x,
    double alpha,
    const double* s,
    long n);

extern void tao_vcombine_dbl(
    double* dst,
    double alpha,
    const double* x,
    double beta,
    const double* y,
    long n);

extern int tao_conjgrad_dbl(
    double* x,
    void (*A)(
        void* ctx,
        double* dst,
        const double* src,
        long n),
    void* ctx,
    const double* b,
    double* p,
    double* q,
    double* r,
    long n,
    double ftol,
    double gatol,
    double grtol,
    double xtol,
    long maxiter,
    long restart);

extern void tao_vcopy_flt(
    float* dst,
    const float* src,
    long n);

extern float tao_vnorm2_flt(
    const float* x,
    long n);

extern float tao_vsqnorm2_flt(
    const float* x,
    long n);

extern float tao_vdot_flt(
    const float* x,
    const float* y,
    long n);

extern void tao_vscale_flt(
    float alpha,
    float* x,
    long n);

extern void tao_vupdate_flt(
    float* x, float alpha,
    const float* s,
    long n);

extern void tao_vcombine_flt(
    float* dst,
    float alpha,
    const float* x,
    float beta,
    const float* y,
    long n);

extern int tao_conjgrad_flt(
    float* x,
    void (*A)(void* ctx,
              float* dst,
              const float* src,
              long n),
    void* ctx,
    const float* b,
    float* p,
    float* q,
    float* r,
    long n,
    float ftol,
    float gatol,
    float grtol,
    float xtol,
    long maxiter,
    long restart);

#define T float
#define SQRT       sqrtf
#define VCOPY      tao_vcopy_flt
#define VNORM2     tao_vnorm2_flt
#define VSQNORM2   tao_vsqnorm2_flt
#define VDOT       tao_vdot_flt
#define VSCALE     tao_vscale_flt
#define VUPDATE    tao_vupdate_flt
#define VCOMBINE   tao_vcombine_flt
#define CONJGRAD   tao_conjgrad_flt
#include __FILE__

#define T double
#define SQRT       sqrt
#define VCOPY      tao_vcopy_dbl
#define VNORM2     tao_vnorm2_dbl
#define VSQNORM2   tao_vsqnorm2_dbl
#define VDOT       tao_vdot_dbl
#define VSCALE     tao_vscale_dbl
#define VUPDATE    tao_vupdate_dbl
#define VCOMBINE   tao_vcombine_dbl
#define CONJGRAD   tao_conjgrad_dbl
#include __FILE__

#else // TAO_CONJGRAD_C_ defined

#ifdef VCOPY
void VCOPY(T* dst,
           const T* src,
           long n)
{
    if (n > 0) {
        memcpy(dst, src, n*sizeof(T));
    }
}
#endif // VCOPY

#ifdef VSQNORM2
T VSQNORM2(const T* x,
           long n)
{
    T s = 0;
    for (long i = 0; i < n; ++i) {
        T val = x[i];
        s += val*val;
    }
    return s;
}
#endif // VSQNORM2

#ifdef VNORM2
T VNORM2(const T* x,
         long n)
{
#ifdef VSQNORM2
    T s = VSQNORM2(x, n);
#else
    T s = 0;
    for (long i = 0; i < n; ++i) {
        T val = x[i];
        s += val*val;
    }
#endif
    return SQRT(s);
}
#endif // VNORM2

#ifdef VDOT
T VDOT(const T* x,
       const T* y,
       long n)
{
    T s = 0;
    for (long i = 0; i < n; ++i) {
        s += x[i]*y[i];
    }
    return s;
}
#endif // VDOT

#ifdef VSCALE
void VSCALE(T alpha,
            T* x,
            long n)
{
    if (alpha == 0) {
        if (n > 0) {
            memset(x, 0, n*sizeof(T));
        }
    } else if (alpha == -1) {
        for (long i = 0; i < n; ++i) {
            x[i] = -x[i];
        }
    } else if (alpha != 1) {
        for (long i = 0; i < n; ++i) {
            x[i] *= alpha;
        }
    }
}
#endif // VSCALE

#ifdef VUPDATE
void VUPDATE(
    T* x,
    T alpha,
    const T* s,
    long n)
{
    if (alpha == 1) {
        for (long i = 0; i < n; ++i) {
            x[i] += s[i];
        }
    } else if (alpha == -1) {
        for (long i = 0; i < n; ++i) {
            x[i] -= s[i];
        }
    } else if (alpha != 0) {
        for (long i = 0; i < n; ++i) {
            x[i] += alpha*s[i];
        }
    }
}
#endif // VUPDATE

#ifdef VCOMBINE
void VCOMBINE(
    T* dst,
    T alpha,
    const T* x,
    T beta,
    const T* y,
    long n)
{
    if (alpha == 1) {
        if (beta == 0) {
            if (n > 0) {
                memcpy(dst, x, n*sizeof(T));
            }
        } else if (beta == 1) {
            for (long i = 0; i < n; ++i) {
                dst[i] = x[i] + y[i];
            }
        } else if (beta == -1) {
            for (long i = 0; i < n; ++i) {
                dst[i] = x[i] - y[i];
            }
        } else {
            for (long i = 0; i < n; ++i) {
                dst[i] = beta*y[i] + x[i];
            }
        }
    } else if (alpha == -1) {
        if (beta == 0) {
            for (long i = 0; i < n; ++i) {
                dst[i] = -x[i];
            }
        } else if (beta == 1) {
            for (long i = 0; i < n; ++i) {
                dst[i] = y[i] - x[i];
            }
        } else if (beta == -1) {
            for (long i = 0; i < n; ++i) {
                dst[i] = -(x[i] + y[i]);
            }
        } else {
            for (long i = 0; i < n; ++i) {
                dst[i] = beta*y[i] - x[i];
            }
        }
    } else if (alpha == 0) {
        if (beta == 0) {
            if (n > 0) {
                memset(dst, 0, n*sizeof(T));
            }
        } else if (beta == 1) {
            if (n > 0) {
                memcpy(dst, y, n*sizeof(T));
            }
        } else if (beta == -1) {
            for (long i = 0; i < n; ++i) {
                dst[i] = -y[i];
            }
        } else {
            for (long i = 0; i < n; ++i) {
                dst[i] = beta*y[i];
            }
        }
    } else {
        if (beta == 0) {
            for (long i = 0; i < n; ++i) {
                dst[i] = alpha*x[i];
            }
        } else if (beta == 1) {
            for (long i = 0; i < n; ++i) {
                dst[i] = alpha*x[i] + y[i];
            }
        } else if (beta == -1) {
            for (long i = 0; i < n; ++i) {
                dst[i] = alpha*x[i] - y[i];
            }
        } else {
            for (long i = 0; i < n; ++i) {
                dst[i] = alpha*x[i] + beta*y[i];
            }
        }
    }
}
#endif // VCOMBINE

#ifdef CONJGRAD
int CONJGRAD(
    T* x,
    void (*A)(void* ctx, T* dst, const T* src, long n),
    void* ctx,
    const T* b,
    T* p,
    T* q,
    T* r,
    long n,
    T ftol,
    T gatol,
    T grtol,
    T xtol,
    long maxiter,
    long restart)
{
    // Local variables.
    T alpha, gamma, rho, oldrho, ftest, gtest, xtest, psi, psimax, zero;
    long k;

    // Initialization.
    if (ftol < 0 || ftol >= 1 || gatol < 0 || grtol < 0 || grtol >= 1 ||
        xtol < 0 || xtol >= 1 || restart < 1) {
        return -1; // FIXME: bad argument
    }
    if (maxiter < 1) {
        return TOO_MANY_ITERATIONS;
    }
    if (VSQNORM2(x, n) > 0) { // Cheap trick to check whether x is non-zero.
        // Compute r = b - A⋅x.
        A(ctx, r, x, n);
        VCOMBINE(r, 1, b, -1, r, n);
    } else {
        // Save applying A since x = 0.
        VCOPY(r, b, n);
    }
    zero = (T)0;
    rho = VSQNORM2(r, n);
    ftest = ftol;
    xtest = xtol;
    gtest = (grtol > zero ? grtol*SQRT(rho) : zero);
    gtest = MAX(gtest, gatol);
    psi = zero;
    psimax = zero;
    oldrho = zero;

    // Conjugate gradient iterations.
    k = 0;
    while (true) {
        k += 1;
        if (SQRT(rho) <= gtest) {
            // Normal convergence (gtest statisfied).
            return GTEST_SATISFIED;
        } else if (k > maxiter) {
            // Too many iteration(s).
            return TOO_MANY_ITERATIONS;
        }
        if ((k%restart) == 1) { // FIXME:
            // Restart or first iteration.
            if (k > 1) {
                // Restart.
                A(ctx, r, x, n);
                VCOMBINE(r, 1, b, -1, r, n);
            }
            VCOPY(p, r, n);
        } else {
            T beta = rho/oldrho;
            VCOMBINE(p, beta, p, +1, r, n);
        }

        // Compute optimal step size.
        A(ctx, q, p, n);
        gamma = VDOT(p, q, n);
        if (gamma <= 0) {
            return NOT_POSITIVE_DEFINITE;
        }
        alpha = rho/gamma;

        // Update variables and check for convergence.
        VUPDATE(x, +alpha, p, n);
        psi = alpha*rho/2;      // psi = f(x_{k}) - f(x_{k+1})
        psimax = MAX(psi, psimax);
        if (psi <= ftest*psimax) {
            // Normal convergence (ftest statisfied).
            return FTEST_SATISFIED;
        }
        if (xtest > 0 && alpha*VNORM2(p, n) <= xtest*VNORM2(x, n)) {
            // Normal convergence (xtest statisfied).
            return XTEST_SATISFIED;
        }

        // Update residuals and related quantities.
        VUPDATE(r, -alpha, q, n);
        oldrho = rho;
        rho = VSQNORM2(r, n);
    }
}


#endif // CONJGRAD defined

#undef T
#undef SQRT
#undef VCOPY
#undef VSCALE
#undef VNORM2
#undef VSQNORM2
#undef VDOT
#undef VUPDATE
#undef VCOMBINE
#undef CONJGRAD

#endif // TAO_CONJGRAD_C_ not defined
