// utils.c --
//
// Utility functions.  Mostly provided to parse values in strings and to deal
// with time.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2018-2021, Éric Thiébaut.

#include "tao-utils.h"
#include "tao-errors.h"
#include "tao-macros.h"

#include <float.h>
#include <math.h>
#include <errno.h>
#include <stdbool.h>
#include <string.h>

//-----------------------------------------------------------------------------
// DYNAMIC MEMORY

void* tao_malloc(
    size_t size)
{
    void* ptr = malloc(size);
    if (ptr == NULL) {
        tao_store_system_error("malloc");
    }
    return ptr;
}

void* tao_calloc(
    size_t nelem,
    size_t elsize)
{
    void* ptr = calloc(nelem, elsize);
    if (ptr == NULL) {
        tao_store_system_error("calloc");
    }
    return ptr;
}

void* tao_realloc(
    void* ptr,
    size_t size)
{
    void* newptr = realloc(ptr, size);
    if (newptr == NULL) {
        tao_store_system_error("realloc");
    }
    return newptr;
}

void tao_free(
    void* ptr)
{
    if (ptr != NULL) {
        free(ptr);
    }
}

//-----------------------------------------------------------------------------
// STRINGS

size_t tao_strlen(
    const char* str)
{
    return TAO_STRLEN(str);
}

const char* tao_basename(
    const char* path)
{
    if (path != NULL) {
        const char* ptr = strrchr(path, '/');
        if (ptr != NULL) {
            return ptr + 1;
        }
    }
    return path;
}

tao_status tao_parse_int(
    const char* str,
    int* ptr,
    int base)
{
    long val;
    if (ptr == NULL) {
        errno = EFAULT;
        return TAO_ERROR;
    }
    if (tao_parse_long(str, &val, base) != TAO_OK) {
        return TAO_ERROR;
    }
    if (val < INT_MIN || val > INT_MAX) {
        errno = ERANGE;
        return TAO_ERROR;
    }
    *ptr = (int)val;
    return TAO_OK;
}

tao_status tao_parse_long(
    const char* str,
    long* ptr,
    int base)
{
    char* end;
    long val;
    if (str == NULL || ptr == NULL) {
        errno = EFAULT;
        return TAO_ERROR;
    }
    errno = 0; // to detect errors, e.g. overflows
    val = strtol(str, &end, base);
    if (errno != 0) {
        return TAO_ERROR;
    }
    if (end == str || *end != '\0') {
        errno = EINVAL;
        return TAO_ERROR;
    }
    *ptr = val; // only change value in case of success
    return TAO_OK;
}

tao_status tao_parse_double(
    const char* str,
    double* ptr)
{
    char* end;
    double val;
    if (str == NULL || ptr == NULL) {
        errno = EFAULT;
        return TAO_ERROR;
    }
    errno = 0; // to detect errors
    val = strtod(str, &end);
    if (errno != 0) {
        return TAO_ERROR;
    }
    if (end == str || *end != '\0') {
        errno = EINVAL;
        return TAO_ERROR;
    }
    *ptr = val; // only change value in case of success
    return TAO_OK;
}

//-----------------------------------------------------------------------------
// TIME

#if defined(CLOCK_REALTIME) && defined(CLOCK_MONOTONIC)

static inline tao_status get_time(
    int id,
    tao_time* t)
{
    struct timespec ts;
    if (clock_gettime(id, &ts) != 0) {
        *t = TAO_UNKNOWN_TIME;
        tao_store_system_error("clock_gettime");
        return TAO_ERROR;
    }
    t->sec  = ts.tv_sec;
    t->nsec = ts.tv_nsec;
    return TAO_OK;
}

#elif !defined(CLOCK_REALTIME) && !defined(CLOCK_MONOTONIC)

// Use gettimeofday as an ersatz for clock_gettime.
#  define CLOCK_REALTIME  0
#  define CLOCK_MONOTONIC 1
static inline tao_status get_time(
    int id,
    tao_time* t)
{
    const char* func = "gettimeofday";
    if (t == NULL) {
        errno = EFAULT;
        tao_store_system_error(func);
        return TAO_ERROR;
    }
    tao_status status = TAO_OK;
    if (id != CLOCK_REALTIME && id != CLOCK_MONOTONIC) {
        errno = EINVAL;
        *t = TAO_UNKNOWN_TIME;
        tao_store_system_error(func);
        return TAO_ERROR;
    }
    struct timeval tv;
    if (gettimeofday(&tv, NULL) != 0) {
        *t = TAO_UNKNOWN_TIME;
        tao_store_system_error(func);
        return TAO_ERROR;
    }
    t->sec  = tv.tv_sec;
    t->nsec = tv.tv_usec*1000;
    return TAO_OK;
}

#else
# error CLOCK_REALTIME and CLOCK_MONOTONIC not both defined nor both undefined
#endif

tao_status tao_get_monotonic_time(
    tao_time* dest)
{
    return get_time(CLOCK_MONOTONIC, dest);
}

tao_status tao_get_current_time(
    tao_time* dest)
{
    return get_time(CLOCK_REALTIME, dest);
}

tao_time* tao_normalize_time(tao_time* t)
{
    tao_time_member s  = t->sec;
    tao_time_member ns = t->nsec;
    TAO_NORMALIZE_TIME(tao_time_member, s, ns);
    t->sec  = s;
    t->nsec = ns;
    return t;
}

tao_time* tao_copy_time(
    tao_time* dst,
    const tao_time* src)
{
    *dst = *src;
    return dst;
}

tao_time* tao_add_times(
    tao_time* dest,
    const tao_time* a,
    const tao_time* b)
{
    tao_time_member s  = a->sec  + b->sec;
    tao_time_member ns = a->nsec + b->nsec;
    TAO_NORMALIZE_TIME(tao_time_member, s, ns);
    dest->sec = s;
    dest->nsec = ns;
    return dest;
}

tao_time* tao_subtract_times(
    tao_time* dest,
    const tao_time* a,
    const tao_time* b)
{
    tao_time_member s  = a->sec  - b->sec;
    tao_time_member ns = a->nsec - b->nsec;
    TAO_NORMALIZE_TIME(tao_time_member, s, ns);
    dest->sec = s;
    dest->nsec = ns;
    return dest;
}

static inline double to_seconds(double s, double ns)
{
    return s + 1E-9*ns;
}

static inline double to_milliseconds(double s, double ns)
{
    return 1E3*s + 1E-6*ns;
}

static inline double to_microseconds(double s, double ns)
{
    return 1E6*s + 1E-3*ns;
}

static inline double to_nanoseconds(double s, double ns)
{
    return 1E9*s + ns;
}

double tao_time_to_seconds(
    const tao_time* t)
{
    return to_seconds(t->sec, t->nsec);
}

double tao_time_to_milliseconds(
    const tao_time* t)
{
    return to_milliseconds(t->sec, t->nsec);
}

double tao_time_to_microseconds(
    const tao_time* t)
{
    return to_microseconds(t->sec, t->nsec);
}

double tao_time_to_nanoseconds(
    const tao_time* t)
{
    return to_nanoseconds(t->sec, t->nsec);
}

double tao_elapsed_seconds(
    const tao_time* t,
    const tao_time* t0)
{
    return to_seconds(t->sec - t0->sec, t->nsec - t0->nsec);
}

double tao_elapsed_milliseconds(
    const tao_time* t,
    const tao_time* t0)
{
    return to_milliseconds(t->sec - t0->sec, t->nsec - t0->nsec);
}

double tao_elapsed_microseconds(
    const tao_time* t,
    const tao_time* t0)
{
    return to_microseconds(t->sec - t0->sec, t->nsec - t0->nsec);
}

double tao_elapsed_nanoseconds(
    const tao_time* t,
    const tao_time* t0)
{
    return to_nanoseconds(t->sec - t0->sec, t->nsec - t0->nsec);
}

tao_time* tao_seconds_to_time(
    tao_time* dest,
    double secs)
{
    // Take care of overflows (even though such overflows probably indicate an
    // invalid usage of the time).
    if (isnan(secs)) {
        dest->sec = 0;
        dest->nsec = -1;
    } else {
        // Compute the number of seconds (as a floating-point) and the number
        // of nanoseconds (as an integer).  The code below also accounts for
        // +/-Inf.  The formula guarantees that the number of nanoseconds is
        // nonnegative but it may be greater or equal one billion so fix it.
        double s = floor(secs);
        if (s >= (double)TAO_MAX_TIME_SECONDS) {
            dest->sec = TAO_MAX_TIME_SECONDS;
            dest->nsec = 0;
        } else if (s <= (double)TAO_MIN_TIME_SECONDS) {
            dest->sec = TAO_MIN_TIME_SECONDS;
            dest->nsec = 0;
        } else {
            tao_time_member ns = lround((secs - s)*1E9); // always >= 0
            if (ns >= TAO_NANOSECONDS_PER_SECOND) {
                ns -= TAO_NANOSECONDS_PER_SECOND;
                s += 1;
            }
            dest->sec = (tao_time_member)s;
            dest->nsec = ns;
        }
    }
    return dest;
}

tao_time* tao_timespec_to_time(
    tao_time* dst,
    const struct timespec* src)
{
    tao_time_member sec = src->tv_sec;
    tao_time_member nsec = src->tv_nsec;
    TAO_NORMALIZE_TIME(tao_time_member, sec, nsec);
    dst->sec = sec;
    dst->nsec = nsec;
    return dst;
}

tao_time* tao_timeval_to_time(
    tao_time* dst,
    const struct timeval* src)
{
    tao_time_member sec = src->tv_sec;
    tao_time_member nsec = src->tv_usec*1000;
    TAO_NORMALIZE_TIME(tao_time_member, sec, nsec);
    dst->sec = sec;
    dst->nsec = nsec;
    return dst;
}

// FIXME: See library by Daniel Collins about dealing with timespec
// (https://github.com/solemnwarning/timespec).
char* tao_sprintf_time(
    char* str,
    const tao_time* t)
{
    int64_t sec  = t->sec;
    int64_t nsec = t->nsec;

    // First make the absolute value of the number of nanoseconds between 0 and
    // 999,999,999 inclusive.
    if (nsec > TAO_NANOSECONDS_PER_SECOND) {
        int64_t n = nsec/TAO_NANOSECONDS_PER_SECOND;
        sec  += n;
        nsec -= n*TAO_NANOSECONDS_PER_SECOND;
    } else if (nsec < -TAO_NANOSECONDS_PER_SECOND) {
        int64_t n = (-nsec)/TAO_NANOSECONDS_PER_SECOND;
        sec  -= n;
        nsec += n*TAO_NANOSECONDS_PER_SECOND;
    }

    // Make the number of seconds and of nanoseconds of the same sign.
    if (sec > 0) {
        if (nsec < 0) {
            sec  -= 1;
            nsec += TAO_NANOSECONDS_PER_SECOND;
        }
    } else if (sec < 0) {
        if (nsec > 0) {
            sec  += 1;
            nsec -= TAO_NANOSECONDS_PER_SECOND;
        }
    }

    // Format the time.
    bool negate = (sec < 0 || (sec == 0 && nsec < 0));
    if (negate) {
        sec = -sec;
        nsec = -nsec;
    }
    sprintf(str, "%s" TAO_INT64_FORMAT(,d) "." TAO_INT64_FORMAT(09,d),
            (negate ? "-" : ""), sec, nsec);
    return str;
}

size_t tao_snprintf_time(
    char* str,
    size_t size,
    const tao_time* t)
{
    char buf[32];
    tao_sprintf_time(buf, t);
    size_t len = strlen(buf);
    if (str != NULL && size > 0) {
        if (len < size) {
            strcpy(str, buf);
        } else if (size > 1) {
            strncpy(str, buf, size - 1);
            str[size - 1] = '\0';
        } else {
            str[0] = '\0';
        }
    }
    return len;
}

void tao_fprintf_time(
    FILE *stream,
    const tao_time* t)
{
    char buf[32];
    tao_sprintf_time(buf, t);
    fputs(buf, stream);
}

tao_timeout tao_get_absolute_timeout(
    tao_time* abstime,
    double secs)
{
    // Get current time as soon as possible, so that the time consumed by the
    // following computations is automatically taken into account.  The
    // absolute time assumed by POSIX threads is given by CLOCK_REALTIME.
    if (get_time(CLOCK_REALTIME, abstime) != TAO_OK) {
        return TAO_TIMEOUT_ERROR;
    }
    if (isnan(secs)) {
        tao_store_error(__func__, TAO_BAD_ARGUMENT);
        return TAO_TIMEOUT_ERROR;
    }
    if (fabs(secs) < TAO_TIMEOUT_MIN) {
        // The time adjustment, rounded to the assumed clock resolution, is
        // less than the assumed clock resolution.
        return TAO_TIMEOUT_NOW;
    }

    // Add the relative timeout to the actual time.  First just add the number
    // of nanoseconds and normalize the result, then check for
    // `tao_time_member` overflow.  We are assuming that current time since
    // the Epoch is not near the limit +/-TAO_MAX_TIME_SECONDS.
    double int_secs = floor(secs); // 0 ≤ secs - int_secs < 1s
    tao_time_member s  = 0;
    tao_time_member ns = abstime->nsec + lround((secs - int_secs)*1e9);
    TAO_NORMALIZE_TIME(tao_time_member, s, ns);
    int_secs = (int_secs + (double)s) + (double)abstime->sec;
    if (int_secs > (double)TAO_MAX_TIME_SECONDS) {
        abstime->sec  = TAO_MAX_TIME_SECONDS;
        abstime->nsec = TAO_NANOSECONDS_PER_SECOND - 1;
        return TAO_TIMEOUT_NEVER;
    } else if (int_secs < (double)TAO_MIN_TIME_SECONDS) {
        abstime->sec  = TAO_MIN_TIME_SECONDS;
        abstime->nsec = 0;
        return TAO_TIMEOUT_PAST;
    } else {
        abstime->sec  = (tao_time_member)int_secs;
        abstime->nsec = ns;
        if (secs >= 0) {
            return TAO_TIMEOUT_FUTURE;
        } else {
            return TAO_TIMEOUT_PAST;
        }
    }
}

double tao_get_maximum_absolute_time(
    void)
{
    return (double)TAO_MAX_TIME_SECONDS;
}

tao_status tao_sleep(
    double secs)
{
    if (isnan(secs) || secs < 0 || secs > (double)TAO_MAX_TIME_SECONDS) {
        tao_store_error(__func__, TAO_BAD_ARGUMENT);
        return TAO_ERROR;
    }
    if (secs > 0) {
        tao_time_member s = (tao_time_member)secs;
        tao_time_member ns = (tao_time_member)((secs - (double)s)*1E9 + 0.5);
        TAO_NORMALIZE_TIME(tao_time_member, s, ns);
        struct timespec ts = { .tv_sec = s, .tv_nsec = ns, };
        if (nanosleep(&ts, NULL) != 0) {
            tao_store_system_error("nanosleep");
            return TAO_ERROR;
        }
    }
    return TAO_OK;
}

void tao_initialize_time_statistics(
    tao_time_stat_data* tsd)
{
    tsd->min = DBL_MAX;
    tsd->max = 0;
    tsd->sum1 = 0;
    tsd->sum2 = 0;
    tsd->numb = 0;
}

void tao_update_time_statistics(
    tao_time_stat_data* tsd,
    double t)
{
    if (tsd->numb < 1) {
        tsd->min = t;
        tsd->max = t;
        tsd->sum1 = t;
        tsd->sum2 = t*t;
        tsd->numb = 1;
    } else {
        tsd->min = (t < tsd->min ? t : tsd->min);
        tsd->max = (t > tsd->max ? t : tsd->max);
        tsd->sum1 += t;
        tsd->sum2 += t*t;
        tsd->numb += 1;
    }
}

tao_time_stat* tao_compute_time_statistics(
    tao_time_stat* ts,
    tao_time_stat_data const* tsd)
{
    ts->min = tsd->min;
    ts->max = tsd->max;
    if (tsd->numb >= 1) {
        ts->avg = tsd->sum1/tsd->numb;
        if (tsd->numb >= 2) {
            ts->std = sqrt((tsd->sum2 - ts->avg*tsd->sum1)/(tsd->numb - 1));
        } else {
            ts->std = 0;
        }
    } else {
        ts->avg = 0;
        ts->std = 0;
    }
    ts->numb = tsd->numb;
    return ts;
}

void tao_print_time_statistics(
    FILE* out,
    char const* pfx,
    tao_time_stat const* ts)
{
    if (out == NULL) {
        out = stdout;
    }
    double const scl = 1E6; // print timings in microseconds
    fprintf(out, "%snumber of evaluations: %ld\n", pfx, ts->numb);
    fprintf(out, "%smin. time: %7.3f µs\n", pfx, ts->min*scl);
    fprintf(out, "%smax. time: %7.3f µs\n", pfx, ts->max*scl);
    fprintf(out, "%savg. time: %7.3f ± %.3f µs\n", pfx,
            ts->avg*scl, ts->std*scl);
}
