// semaphores.c -
//
// Management of semaphores camera data.  These functions are mainly wrappers
// over POSIX semaphores augmented by TAO-style error management.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2022, Éric Thiébaut.

#include "tao-locks.h"
#include "tao-errors.h"

#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>

tao_status tao_semaphore_initialize(
    sem_t* sem,
    bool shared,
    unsigned int value)
{
    if (sem_init(sem, shared, value) != 0) {
        tao_store_system_error("sem_init");
        return TAO_ERROR;
    }
    return TAO_OK;
}

tao_status tao_semaphore_destroy(
    sem_t* sem)
{
    if (sem_destroy(sem) != 0) {
        tao_store_system_error("sem_destroy");
        return TAO_ERROR;
    }
    return TAO_OK;
}

tao_status tao_semaphore_get_value(
    sem_t* sem,
    int* val)
{
    if (sem_getvalue(sem, val) != 0) {
        tao_store_system_error("sem_getvalue");
        return TAO_ERROR;
    }
    return TAO_OK;
}

tao_status tao_semaphore_post(
    sem_t* sem)
{
    if (sem_post(sem) != 0) {
        tao_store_system_error("sem_post");
        return TAO_ERROR;
    }
    return TAO_OK;
}

tao_status tao_semaphore_wait(
    sem_t* sem)
{
    if (sem_wait(sem) != 0) {
        tao_store_system_error("sem_wait");
        return TAO_ERROR;
    }
    return TAO_OK;
}

tao_status tao_semaphore_try_wait(
    sem_t* sem)
{
    if (sem_trywait(sem) != 0) {
        int code = errno;
        if (code == EAGAIN) {
            return TAO_TIMEOUT;
        } else {
            tao_store_error("sem_trywait", code);
            return TAO_ERROR;
        }
    }
    return TAO_OK;
}

tao_status tao_semaphore_abstimed_wait(
    sem_t* sem,
    const tao_time* abstime)
{
    struct timespec ts = {
        .tv_sec = abstime->sec,
        .tv_nsec = abstime->nsec,
    };
    if (sem_timedwait(sem, &ts) != 0) {
        int code = errno;
        if (code == ETIMEDOUT) {
            return TAO_TIMEOUT;
        } else {
            tao_store_error("sem_timedwait", code);
            return TAO_ERROR;
        }
    }
    return TAO_OK;
}

tao_status tao_semaphore_timed_wait(
    sem_t* sem,
    double secs)
{
    tao_time abstime;
    switch(tao_get_absolute_timeout(&abstime, secs)) {
    case TAO_TIMEOUT_PAST:
        return TAO_TIMEOUT;

    case TAO_TIMEOUT_NOW:
        return tao_semaphore_try_wait(sem);

    case TAO_TIMEOUT_FUTURE:
        return tao_semaphore_abstimed_wait(sem, &abstime);

    case TAO_TIMEOUT_NEVER:
        return tao_semaphore_wait(sem);

    default:
        return TAO_ERROR;
    }
}

sem_t* tao_semaphore_create(
    const char* name,
    int perms,
    unsigned int value)
{
    int flags = O_CREAT | O_EXCL;
    mode_t mode = perms & (S_IRWXU|S_IRWXG|S_IRWXO);
    sem_t* sem = sem_open(name, flags, mode, value);
    if (sem == NULL) {
        tao_store_system_error("sem_open");
    }
    return sem;
}

sem_t* tao_semaphore_open(
    const char* name)
{
    sem_t* sem = sem_open(name, 0);
    if (sem == NULL) {
        tao_store_system_error("sem_open");
    }
    return sem;
}

tao_status tao_semaphore_close(
    sem_t* sem)
{
    if (sem_close(sem) != 0) {
        tao_store_system_error("sem_close");
        return TAO_ERROR;
    }
    return TAO_OK;
}

tao_status tao_semaphore_unlink(
    const char* name,
    bool force)
{
    if (sem_unlink(name) != 0) {
        int code = errno;
        if (force && code == ENOENT) {
            // Ignore non-existing semaphore.
            return TAO_OK;
        }
        tao_store_error("sem_unlink", code);
        return TAO_ERROR;
    }
    return TAO_OK;
}
