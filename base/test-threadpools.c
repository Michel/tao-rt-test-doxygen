// test-threadpools.c -
//
// Test thread-pools in TAO real-time library.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2020-2022, Éric Thiébaut.

#include <tao-threadpools.h>
#include <tao-errors.h>
#include <tao-utils.h>

#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

#define MAXJOBS    50 // Maximum number of pending jobs at a given time.
#define WORKERS     4
#define WIDTH     400
#define HEIGHT    410
#define PIXELS  (WIDTH*HEIGHT)
#define CHUNK    5000
#define TESTS   40000

#define NJOBS (((PIXELS) + (CHUNK) - 1)/(CHUNK))

#define T float

#define MALLOC(N,T)  ((T*)malloc((N)*sizeof(T)))

typedef struct {
    const unsigned char* raw;
    const T* a;
    const T* b;
    const T* q;
    const T* r;
    T* wgt;
    T* dat;
    long number;
} workspaces;

typedef struct {
    long offset, number;
    const workspaces* wrk;
} job_data;

static job_data all_job_data[MAXJOBS];

static double elapsed(
    const tao_time* t1,
    const tao_time* t0)
{
    return ((double)(t1->sec - t0->sec) + 1E-9*(double)(t1->nsec - t0->nsec));
}

static void apply_processing_1(
    T* restrict wgt,
    T* restrict dat,
    const unsigned char* restrict raw,
    const T* restrict a,
    const T* restrict b,
    const T* restrict q,
    const T* restrict r,
    long number)
{
    T zero = (T)0;
    for (long i = 0; i < number; ++i) {
        T data = (raw[i] - b[i])*a[i];
        dat[i] = data;
        wgt[i] = q[i]/((data >= zero ? data : zero) + r[i]);
    }
}

static void apply_processing_2(
    T* restrict wgt,
    T* restrict dat,
    const unsigned char* restrict raw,
    const T* restrict a,
    const T* restrict b,
    const T* restrict q,
    const T* restrict r,
    long number)
{
    T zero = (T)0;
    for (long i = 0; i < number; ++i) {
        dat[i] = (raw[i] - b[i])*a[i];
    }
    for (long i = 0; i < number; ++i) {
        wgt[i] = q[i]/((dat[i] >= zero ? dat[i] : zero) + r[i]);
    }
}

static void apply_processing_3(
    T* restrict wgt,
    T* restrict dat,
    const unsigned char* restrict raw,
    const T* restrict a,
    const T* restrict b,
    const T* restrict q,
    const T* restrict r,
    long number)
{
    T zero = (T)0;
    for (long i = 0; i < number; ++i) {
        dat[i] = raw[i];
    }
    for (long i = 0; i < number; ++i) {
        dat[i] = (dat[i] - b[i])*a[i];
    }
    for (long i = 0; i < number; ++i) {
        wgt[i] = q[i]/((dat[i] >= zero ? dat[i] : zero) + r[i]);
    }
}

static void run_job_1(
    void* arg)
{
    job_data* jd = (job_data*)arg;
    long offset = jd->offset;
    apply_processing_1(jd->wrk->wgt + offset,
                       jd->wrk->dat + offset,
                       jd->wrk->raw + offset,
                       jd->wrk->a + offset,
                       jd->wrk->b + offset,
                       jd->wrk->q + offset,
                       jd->wrk->r + offset, jd->number);
}

static void run_job_2(
    void* arg)
{
    job_data* jd = (job_data*)arg;
    long offset = jd->offset;
    apply_processing_2(jd->wrk->wgt + offset,
                       jd->wrk->dat + offset,
                       jd->wrk->raw + offset,
                       jd->wrk->a + offset,
                       jd->wrk->b + offset,
                       jd->wrk->q + offset,
                       jd->wrk->r + offset, jd->number);
}

static void run_job_3(
    void* arg)
{
    job_data* jd = (job_data*)arg;
    long offset = jd->offset;
    apply_processing_3(jd->wrk->wgt + offset,
                       jd->wrk->dat + offset,
                       jd->wrk->raw + offset,
                       jd->wrk->a + offset,
                       jd->wrk->b + offset,
                       jd->wrk->q + offset,
                       jd->wrk->r + offset, jd->number);
}

static double apply_processing(
    tao_threadpool* pool,
    const workspaces* wrk,
    long chunk,
    void (*run_job)(
        void* arg))
{
    long number;
    tao_time t0, t1;
    if (tao_get_monotonic_time(&t0) != TAO_OK) {
        tao_panic();
    }
    number = wrk->number;
    if (chunk >= number) {
        all_job_data[0].offset = 0;
        all_job_data[0].number = number;
        all_job_data[0].wrk = wrk;
        run_job(all_job_data);
    } else {
        tao_status status;
        if (chunk*MAXJOBS < number) {
            chunk = (number + MAXJOBS - 1)/MAXJOBS;
        }
        for (long i = 0, offset = 0; offset < number; offset += chunk, ++i) {
            all_job_data[i].offset = offset;
            all_job_data[i].number = (offset + chunk <= number ? chunk :
                                  number - offset);
            all_job_data[i].wrk = wrk;
            status = tao_threadpool_push_job(pool, run_job, &all_job_data[i]);
            if (status != TAO_OK) {
                fprintf(stderr, "failed to push job (%s)\n", strerror(errno));
                exit(1);
            }
        }
        status = tao_threadpool_wait(pool);
        if (status != TAO_OK) {
            fprintf(stderr, "failed to wait jobs (%s)\n", strerror(errno));
            exit(1);
        }
    }
    if (tao_get_monotonic_time(&t1) != TAO_OK) {
        tao_panic();
    }
    return elapsed(&t1, &t0);
}

static void test_processing(
    tao_threadpool* pool,
    const workspaces* wrk,
    long chunk, long ntests,
    int method)
{
    void (*run_job)(void*);
    double tsum = 0, tsum2 = 0, tmin = 0, tmax = 0;
    long count;

    if (method == 1) {
        run_job = run_job_1;
    } else if (method == 2) {
        run_job = run_job_2;
    } else if (method == 3) {
        run_job = run_job_3;
    } else {
        fprintf(stderr, "invalid method %d\n", method);
        exit(1);
    }

    // Perform the number of requested tests.  Start counting at -1 to have at
    // least one ignored test for initializing, warming up.
    count = -1;
    while (count < ntests) {
        double t = apply_processing(pool, wrk, chunk, run_job);
        ++count;
        if (count == 1) {
            tmin = t;
            tmax = t;
            tsum = t;
            tsum2 = t*t;
        } else {
            if (t < tmin) tmin = t;
            if (t > tmax) tmax = t;
            tsum += t;
            tsum2 += t*t;
        }
    }

    fprintf(stdout, "\n");
    fprintf(stdout, "Pre-processing of %ld pixel images by %ld chunk(s) "
            "of %ld pixels with method %d:\n",
            wrk->number, (wrk->number + chunk - 1)/chunk, chunk, method);
    fprintf(stdout, "  number of tests: %ld\n", count);
    fprintf(stdout, "  min. execution time: %g µs\n", 1E6*tmin);
    fprintf(stdout, "  max. execution time: %g µs\n", 1E6*tmax);
    if (count >= 2) {
	fprintf(stdout, "  avg. execution time: %g +/- %.3g µs\n",
		1E6*tsum/count,
		1E6*sqrt((tsum2 - tsum*tsum/count)/(count - 1)));
    }
}

int main(
    int argc,
    char* argv[])
{
    tao_threadpool* pool;
    workspaces wrk;
    long chunk = CHUNK;
    long tests = TESTS;
    long workers = WORKERS;
    long pixels = PIXELS;
    unsigned char* raw;
    T* a;
    T* b;
    T* q;
    T* r;
    T* dat;
    T* wgt;
    T* datref;
    T* wgtref;
    int i;

    // Parse options.
    i = 0;
    while (++i < argc) {
        if (argv[i][0] != '-') {
            break;
        } else if (strcmp(argv[i], "-h") == 0 ||
                   strcmp(argv[i], "--help") == 0) {
            fprintf(stdout, "Usage: %s [-h|--help] [--tests NUMBER] "
                    "[--chunk SIZE] [--workers NUMBER] [--pixels NUMBER] "
                    "[--]\n", argv[0]);
            exit(0);
        } else if (strcmp(argv[i], "--tests") == 0) {
            if (i + 1 >= argc) {
            missing_option_value:
                fprintf(stderr, "missing argument for option %s\n", argv[i]);
                return 1;
            }
            if (tao_parse_long(argv[i+1], &tests, 0) != TAO_OK || tests < 2) {
            invalid_option_value:
                fprintf(stderr, "invalid value for option %s\n", argv[i]);
                return 1;
            }
            ++i;
        } else if (strcmp(argv[i], "--chunk") == 0) {
            if (i + 1 >= argc) {
                goto missing_option_value;
            }
            if (tao_parse_long(argv[i+1], &chunk, 0) != TAO_OK || chunk < 1) {
                goto invalid_option_value;
            }
            ++i;
        } else if (strcmp(argv[i], "--workers") == 0) {
            if (i + 1 >= argc) {
                goto missing_option_value;
            }
            if (tao_parse_long(argv[i+1], &workers, 0) != TAO_OK || workers < 1) {
                goto invalid_option_value;
            }
            ++i;
        } else if (strcmp(argv[i], "--pixels") == 0) {
            if (i + 1 >= argc) {
                goto missing_option_value;
            }
            if (tao_parse_long(argv[i+1], &pixels, 0) != TAO_OK || pixels < 1) {
                goto invalid_option_value;
            }
            ++i;
        } else if (strcmp(argv[i], "--") == 0) {
            ++i;
            break;
        }
    }
    if (i < argc) {
        fprintf(stderr, "too many arguments\n");
        return 1;
    }

    raw = MALLOC(pixels, unsigned char);
    a = MALLOC(pixels, T);
    b = MALLOC(pixels, T);
    q = MALLOC(pixels, T);
    r = MALLOC(pixels, T);
    dat = MALLOC(pixels, T);
    wgt = MALLOC(pixels, T);
    datref = MALLOC(pixels, T);
    wgtref = MALLOC(pixels, T);

    if (raw == NULL || a == NULL || b == NULL || q == NULL || r == NULL ||
        dat == NULL || wgt == NULL || datref == NULL || wgtref == NULL) {
        fprintf(stderr, "failed to allocate arrays (%s)\n", strerror(errno));
        return 1;
    }

    for (long i = 0; i < pixels; ++i) {
        raw[i] = (i%256);
    }
    for (long i = 0; i < pixels; ++i) {
        a[i] = 1;
    }
    for (long i = 0; i < pixels; ++i) {
        b[i] = 0;
    }
    for (long i = 0; i < pixels; ++i) {
        q[i] = 1;
    }
    for (long i = 0; i < pixels; ++i) {
        r[i] = 1;
    }

    pool = tao_threadpool_create(workers);
    if (pool == NULL) {
        fprintf(stderr, "failed to create a thread-pool of %ld threads (%s)\n",
                workers, strerror(errno));
        return 1;
    }
    printf("all %ld workers started\n", workers);

    wrk.raw = raw;
    wrk.a = a;
    wrk.b = b;
    wrk.q = q;
    wrk.r = r;
    wrk.wgt = wgt;
    wrk.dat = dat;
    wrk.number = pixels;

    apply_processing_1(wgtref, datref, raw, a, b, q, r, pixels);

    for (int method = 1; method <= 3; ++method) {
        test_processing(pool, &wrk, wrk.number, tests, method);
    }

    for (int method = 1; method <= 3; ++method) {
        test_processing(pool, &wrk, chunk, tests, method);
    }

    tao_threadpool_destroy(pool);
    free(raw);
    free(a);
    free(b);
    free(q);
    free(r);
    free(dat);
    free(wgt);

    return 0;
}
