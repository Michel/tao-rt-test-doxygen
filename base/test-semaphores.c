// test-semaphores.c -
//
// Test process shared semaphores.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2022, Éric Thiébaut.

#include <tao-rwlocked-objects-private.h>
#include <tao-errors.h>
#include <tao-locks.h>

#include <math.h>
#include <stdlib.h>
#include <unistd.h>

// To test process shared read/write locks, we launch several processes,
// the parent (the writer) and several forked children (the readers).  The
// locking of frame buffers is driven by read/write locks.
//
// All shared data is stored in shared memory via a custom TAO shared
// object.  The time taken by this kind of communication is measured.
//
// The latency and the losses strongly depend on the frame rate: the higher
// the frame rate, the lower the latency and the higher the number of lost
// frames.

#ifndef NBUFS
#    define NBUFS 3
#endif

#if NBUFS < 2
#    error too few frame buffers
#endif

// Maximum number of readers.
#ifndef NREADERS
#    define NREADERS 3
#endif

typedef struct shared_frame shared_frame;
typedef struct shared_data  shared_data;

struct shared_frame {
    tao_mutex   mutex; // Mutex to control access to this structure */
    long        locks; // -1 if locked by a writer, 0 if not owned, n>0
                       // if owned by n readers */
    tao_time     time; // Time stamp */
    tao_serial number; // Frame number (at 1kHz, overflow occurs after
                       // more than 292 Myr) */
};

struct shared_data {
    tao_rwlocked_object  base; // Base of any TAO shared object
    sem_t       sem[NREADERS]; // Semaphores used to signal
                               // readers that a new image have
                               // just been acquired.
    double                fps; // Frames per second
    shared_frame frame[NBUFS];
    tao_serial    last_number; // Last frame number
    int            last_index; // Last frame index (-1 means not started)
    bool             quitting; // All processes supposed to quit
};

#define lock_resources_for_reading(data) \
    tao_rwlocked_object_rdlock(&(data)->base)

#define lock_resources_for_writing(data) \
    tao_rwlocked_object_wrlock(&(data)->base)

#define unlock_resources(data) \
    tao_rwlocked_object_unlock(&(data)->base)


// Type code of the custom TAO shared object.
#define SHARED_DATA (TAO_RWLOCKED_OBJECT | 5)

static double elapsed(const tao_time* t1, const tao_time* t0)
{
    return ((double)(t1->sec - t0->sec) +
            1E-9*(double)(t1->nsec - t0->nsec));
}

#define ASSERT(expr)                                            \
    do {                                                        \
        if (!(expr)) {                                          \
            tao_store_error(__func__, TAO_ASSERTION_FAILED);    \
        }                                                       \
    } while (false)

// Notify all readers that a new frame is available or that quitting is
// requested.
static void notify_readers(shared_data* data)
{
    for (int i = 0; i < NREADERS; ++i) {
        int val;
        tao_semaphore_get_value(&data->sem[i], &val);
        if (val == 0) {
            tao_semaphore_post(&data->sem[i]);
        }
    }
}

static void run_writer(shared_data* data, long nframes)
{
    double secs;
    tao_time t0, t1, dt;
    int index;
    bool locked;

    // Initialize inter-frame time and initial time.
    tao_seconds_to_time(&dt, 1.0/data->fps);
    tao_get_monotonic_time(&t1);

    // Loop over frames.
    index = -1;
    for (long number = 1; number <= nframes; ++number) {
        // Determine arrival time of next frame.
        tao_add_times(&t1, &t1, &dt);

        // Lock next frame for writing.
        index = (index + 1)%NBUFS;
        while (true) {
            tao_mutex_lock(&data->frame[index].mutex);
            locked = (data->frame[index].locks == 0);
            if (locked) {
                data->frame[index].locks = -1;
            }
            tao_mutex_unlock(&data->frame[index].mutex);
            if (locked) {
                break;
            }
            tao_sleep(0.1/data->fps);
        }

        // Sleep a bit to simulate writing/arrival of frame data.
        tao_get_monotonic_time(&t0);
        secs = elapsed(&t1, &t0);
        if (secs > TAO_NANOSECOND) {
            tao_sleep(secs);
        }

        // Register timestamp and frame number.
        tao_get_monotonic_time(&data->frame[index].time);
        data->frame[index].number = number;

        // Unlock frame.
        tao_mutex_lock(&data->frame[index].mutex);
        ASSERT(data->frame[index].locks == -1);
        data->frame[index].locks = 0;
        tao_mutex_unlock(&data->frame[index].mutex);

        // Update global information and signal that a new image is available.
        lock_resources_for_writing(data);
        data->last_number = number;
        data->last_index = index;
        notify_readers(data);
        unlock_resources(data);
    }

    // Ask readers to quit.
    lock_resources_for_writing(data);
    data->quitting = true;
    data->last_index = -1;
    notify_readers(data);
    unlock_resources(data);
}

static void run_reader(int shmid, int semidx)
{
    tao_time time;
    tao_serial previous = -1; // Previously processed frame number
    long spurious = 0; // Number of spurious wake-up
    long overflows = 0;
    double t, tmin = 0.0, tmax = 0.0, tsum = 0.0, tsum2 = 0.0;
    long count = 0; // Count of processed frames
    bool locked;
    int index;

    // Attach to shared data.
    shared_data* data = (shared_data*)tao_shared_object_attach(shmid);
    if (data == NULL) {
        tao_panic();
    }
    if (((tao_shared_object*)data)->type != SHARED_DATA) {
        fprintf(stderr, "Invalid shared object type\n");
        exit(EXIT_FAILURE);
    }

    // Run reader loop.
    while (true) {
        // Check for quitting condition and get index of next frame buffer.
        lock_resources_for_reading(data);
        if (data->quitting) {
            goto quit;
        }
        unlock_resources(data);

        // Wait for a new frame and get its index.
        tao_semaphore_wait(&data->sem[semidx]);
        lock_resources_for_reading(data);
        if (data->quitting) {
            goto quit;
        }
        index = data->last_index;
        unlock_resources(data);

        // Lock frame for reading.
        tao_mutex_lock(&data->frame[index].mutex);
        locked = (data->frame[index].locks >= 0);
        if (locked) {
            data->frame[index].locks += 1;
        }
        tao_mutex_unlock(&data->frame[index].mutex);
        if (locked) {
            // Get current frame number and number N of frames since previously
            // processed one.
            tao_serial current = data->frame[index].number;
            tao_serial n = (previous == -1 ? 1 : current - previous);
            if (n > 0) {
                // A new frame is available.
                if (n > 1) {
                    // Some frames have been lost.
                    overflows += n - 1;
                }
                tao_get_monotonic_time(&time);
                t = elapsed(&time, &data->frame[index].time);
                ++count;
                if (count == 1) {
                    tmin = t;
                    tmax = t;
                    tsum = t;
                    tsum2 = t*t;
                } else {
                    if (t < tmin) tmin = t;
                    if (t > tmax) tmax = t;
                    tsum += t;
                    tsum2 += t*t;
                }
            } else if (n == 0) {
                // This is the same frame as before.
                ++spurious;
            }
            previous = current;

            // Release read lock on frame.
            tao_mutex_lock(&data->frame[index].mutex);
            ASSERT(data->frame[index].locks > 0);
            data->frame[index].locks -= 1;
            tao_mutex_unlock(&data->frame[index].mutex);
        }
    }

 quit:

    // Print before unlocking to avoid messy output.
    fprintf(stdout, "\n");
    fprintf(stdout, "lost frames: %ld\n", overflows);
    fprintf(stdout, "spurious notifications: %ld\n", spurious);
    fprintf(stdout, "number of frames: %ld\n", count);
    fprintf(stdout, "min. latency time: %g µs\n", 1E6*tmin);
    fprintf(stdout, "max. latency time: %g µs\n", 1E6*tmax);
    if (count >= 2) {
        fprintf(stdout, "avg. latency time: %g +/- %.3g µs\n",
                1E6*tsum/count,
                1E6*sqrt((tsum2 - tsum*tsum/count)/(count - 1)));
    }

    // Unlock global resources.
    unlock_resources(data);

}

int main(int argc, char* argv[])
{
    long nframes = 3000;
    long nreaders = NREADERS;
    double fps = 1000;
    int shmid, semidx;

    // Parse arguments.
    if (argc == 2) {
        if (tao_parse_long(argv[1], &nframes, 0) != TAO_OK || nframes < 1) {
            fprintf(stderr, "%s: invalid nframes of operations (%s)\n",
                    argv[0], argv[1]);
            return EXIT_FAILURE;
        }
    } else if (argc != 1) {
        fprintf(stderr, "usage: %s [NFRAMES=%ld]\n", argv[0], nframes);
        return EXIT_FAILURE;
    }

    // Create shared resources.
    shared_data* data = (shared_data*)tao_rwlocked_object_create(
        SHARED_DATA, sizeof(shared_data), 0600);
    for (int index = 0; index < NBUFS; ++index) {
        tao_mutex_initialize(&data->frame[index].mutex, true);
        data->frame[index].number = 0;
    }
    for (int i = 0; i < NREADERS; ++i) {
        tao_semaphore_initialize(&data->sem[i], true, 0);
    }
    data->fps = fps;
    data->last_number = 0;
    data->last_index = -1;
    data->quitting = false;
    shmid = tao_rwlocked_object_get_shmid((tao_rwlocked_object*)data);

    // Fork to have readers.
    fprintf(stdout,
            "Testing semaphores at %.3g frames/second,\n"
            "with %ld readers, %d buffers and %ld frames:\n",
            fps, nreaders, NBUFS, nframes);
    semidx = nreaders;
    while (true) {
        --semidx;
        if (fork() == 0) {
            // Children are "readers".
            run_reader(shmid, semidx);
            break;
        } else if (semidx == 0) {
            // The parent is the "writer".
            run_writer(data, nframes);
            break;
        }
    }
    return EXIT_SUCCESS;
}
