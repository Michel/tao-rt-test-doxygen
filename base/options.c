// options.c -
//
// Parsing of command line options.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2022, Éric Thiébaut.

#include "config.h"

#include <math.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#include "tao-basics.h"
#include "tao-options.h"
#include "tao-errors.h"
#include "tao-cameras.h"

static void error(
    tao_buffer* buf,
    const char* format,
    ...) TAO_FORMAT_PRINTF(2,3);

int tao_parse_options(
    tao_buffer* msg,
    int argc,
    char* argv[],
    int pass,
    const tao_option* options)
{
    bool optional = true;
    const char* option;
    int i, j, k, n;

    if (argc < 1) {
        return 0;
    }
    for (i = j = 1; i < argc; ++i) {
        option = NULL;
        if (optional) {
            if (argv[i][0] != '-') {
                optional = false;
            } else if (argv[i][1] == '-') {
                if (argv[i][2] == '\0') {
                    optional = false;
                } else {
                    option = argv[i] + 2;
                }
            } else {
                option = argv[i] + 1;
            }
        }
        if (option != NULL) {
            int nargs;
            bool found = false;
            for (k = 0; options[k].name != NULL; ++k) {
                if (strcmp(option, options[k].name) == 0) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                error(msg,
                      "unknown option `%s`, try `-help` for a short help",
                      argv[i]);
                goto failure;
            }
            nargs = options[k].nargs;
            if (i + nargs >= argc) {
                error(msg, "expecting %d argument%s after option `%s`",
                      nargs, argv[i], nargs > 1 ? "s" : "");
                goto failure;
            }
            if (pass == options[k].pass) {
                // Process the option.
                if (!options[k].parse(&options[k], &argv[i + 1])) {
                    error(msg, "invalid argument%s for option `%s`",
                          nargs > 1 ? "s" : "", argv[i]);
                    goto failure;
                }
                i += nargs;
            } else {
                // Keep option, and its argument(s), in argument list.
                argv[j++] = argv[i];
                while (nargs-- > 0) {
                    argv[j++] = argv[++i];
                }
            }
        } else {
            // Keep argument in argument list.
            argv[j++] = argv[i];
        }
    }

    // Set exceeding arguments to NULL and update new count of arguments.
    n = j;
    while (j < argc) {
        argv[j++] = NULL;
    }
    argc = n;

    // If this is the last pass, strip an optional first "--" argument.
    if (argc <= 1 || strcmp(argv[1], "--") != 0) {
        return argc;
    }
    for (k = 0; options[k].name != NULL; ++k) {
        if (options[k].pass > pass) {
            // This is not the last pass.
            return argc;
        }
    }
    for (j = 2; j < argc; ++j) {
        argv[j - 1] = argv[j];
    }
    argv[argc - 2] = NULL;
    return argc - 1;

    // Copy the remaining argument and report error.
 failure:
    while (i < argc) {
        argv[j++] = argv[i++];
    }
    while (j < argc) {
        argv[j++] = NULL;
    }
    return -1;
}

static void error(
    tao_buffer* buf,
    const char* format,
    ...)
{
    va_list args;

    va_start(args, format);
    if (buf != NULL) {
        if (tao_buffer_vprintf(buf, format, args) != TAO_OK) {
            tao_report_error();
        }
    } else {
        vfprintf(stderr, format, args);
        fputs("\n", stderr);
    }
    va_end(args);
}

bool tao_print_help(
    const tao_option* opt,
    char* args[])
{
    tao_help_info* info = (tao_help_info*)opt->ptr;
    const tao_option* options = info->options;
    FILE* output = (info->output != NULL ? info->output : stderr);

    // Print "usage" line.
    fprintf(output, "Usage: %s [OPTIONS] [--]",
            info->program != NULL ? info->program : "PROGRAM");
    if (info->args != NULL && info->args[0] != '\0') {
        fputc(' ', output);
        fputs(info->args, output);
    }
    fputc('\n', output);

    // Print description.
    if (info->purpose != NULL && info->purpose[0] != '\0') {
        long len = strlen(info->purpose);
        fputc('\n', output);
        if (len >= 1) {
            fputs(info->purpose, output);
            if (info->purpose[len - 1] != '\n') {
                if (info->purpose[len - 1] != '.') {
                    fputs(".\n", output);
                } else {
                    fputc('\n', output);
                }
            }
        }
    }

    // Print options.
    fputs("\nOptions:\n", output);
    for (int k = 0; options[k].name != NULL; ++k) {
        bool show_args  = (options[k].args != NULL &&
                           options[k].args[0] != '\0');
        bool show_descr = (options[k].descr != NULL &&
                           options[k].descr[0] != '\0');
        bool show_value = (options[k].show != NULL);
        long len = strlen(options[k].name);
        fputs("  -", output);
        fputs(options[k].name, output);
        if (show_args) {
            fputs(" ", output);
            fputs(options[k].args, output);
            len += strlen(options[k].args) + 1;
        }
        if (show_descr || show_value) {
            while (len < 30) {
                fputc(' ', output);
                ++len;
            }
        }
        if (show_descr) {
            fputs(options[k].descr, output);
        }
        if (show_value) {
            if (show_descr) {
                fputc(' ', output);
            }
            fputc('[', output);
            options[k].show(output, &options[k]);
            fputc(']', output);
        }
        if (show_descr || show_value) {
            fputc('.', output);
        }
        fputc('\n', output);
        fflush(output);
    }
    return true;
}

bool tao_print_help_and_exit0(
    const tao_option* opt,
    char* args[])
{
    tao_print_help(opt, args);
    exit(EXIT_SUCCESS);
}

bool tao_print_help_and_exit1(
    const tao_option* opt,
    char* args[])
{
    tao_print_help(opt, args);
    exit(EXIT_FAILURE);
}

void tao_show_string_option(
    FILE* file,
    const tao_option* opt)
{
    fputs(*(char**)opt->ptr, file);
}

bool tao_parse_string_option(
    const tao_option* opt,
    char* args[])
{
    *(char**)opt->ptr = args[0];
    return true;
}

void tao_show_switch_option(
    FILE* file,
    const tao_option* opt)
{
    fputs(*(bool*)opt->ptr ? "true" : "false", file);
}

bool tao_parse_switch_option(
    const tao_option* opt,
    char* args[])
{
    *(bool*)opt->ptr = true;
    return true;
}

void tao_show_toggle_option(
    FILE* file,
    const tao_option* opt)
{
    fputs(*(bool*)opt->ptr ? "true" : "false", file);
}

bool tao_parse_toggle_option(
    const tao_option* opt,
    char* args[])
{
    *(bool*)opt->ptr = !(*(bool*)opt->ptr);
    return true;
}

void tao_show_yesno_option(
    FILE* file,
    const tao_option* opt)
{
    fputs(*(bool*)opt->ptr ? "yes" : "no", file);
}

bool tao_parse_yesno_option(
    const tao_option* opt,
    char* args[])
{
    if (strcmp(args[0], "yes") == 0) {
      *(bool*)opt->ptr = true;
      return true;
    } else if (strcmp(args[0], "no") == 0) {
      *(bool*)opt->ptr = false;
      return true;
    } else {
        return false;
    }
}

void tao_show_int_option(
    FILE* file,
    const tao_option* opt)
{
    fprintf(file, "%d", *(int*)opt->ptr);
}

bool tao_parse_int_option(
    const tao_option* opt,
    char* args[])
{
    char c;
    return sscanf(args[0], "%d %c", (int*)opt->ptr, &c) == 1;
}

bool tao_parse_nonnegative_int_option(
    const tao_option* opt,
    char* args[])
{
    return tao_parse_int_option(opt, args) && *(int*)opt->ptr >= 0;
}

bool tao_parse_positive_int_option(
    const tao_option* opt,
    char* args[])
{
    return tao_parse_int_option(opt, args) && *(int*)opt->ptr > 0;
}

void tao_show_long_option(
    FILE* file,
    const tao_option* opt)
{
    fprintf(file, "%ld", *(long*)opt->ptr);
}

bool tao_parse_long_option(
    const tao_option* opt,
    char* args[])
{
    char c;
    return sscanf(args[0], "%ld %c", (long*)opt->ptr, &c) == 1;
}

bool tao_parse_nonnegative_long_option(
    const tao_option* opt,
    char* args[])
{
    return tao_parse_long_option(opt, args) && *(long*)opt->ptr >= 0;
}

bool tao_parse_positive_long_option(
    const tao_option* opt,
    char* args[])
{
    return tao_parse_long_option(opt, args) && *(long*)opt->ptr > 0;
}

void tao_show_double_option(
    FILE* file,
    const tao_option* opt)
{
    fprintf(file, "%g", *(double*)opt->ptr);
}

bool tao_parse_double_option(
    const tao_option* opt,
    char* args[])
{
    char c;
    return sscanf(args[0], " %lf %c", (double*)opt->ptr, &c) == 1 &&
        ! isnan(*(double*)opt->ptr);
}

bool tao_parse_nonnegative_double_option(
    const tao_option* opt,
    char* args[])
{
    return tao_parse_double_option(opt, args) && *(double*)opt->ptr >= 0;
}

bool tao_parse_positive_double_option(
    const tao_option* opt,
    char* args[])
{
    return tao_parse_double_option(opt, args) && *(double*)opt->ptr > 0;
}

void tao_show_roi_option(
    FILE* file,
    const tao_option* opt)
{
    tao_image_roi* roi = (tao_image_roi*)opt->ptr;
    fprintf(file, "%ld,%ld,%ld,%ld",
            roi->xoff, roi->yoff, roi->width, roi->height);
}

bool tao_parse_roi_option(
    const tao_option* opt,
    char* args[])
{
    char c;
    tao_image_roi* roi = (tao_image_roi*)opt->ptr;
    return sscanf(args[0], " %ld , %ld , %ld , %ld %c",
                  &roi->xoff, &roi->yoff, &roi->width, &roi->height,
                  &c) == 4;
}
