// remote-objects.c -
//
// Management of basic shared objects used to communicate with remote server.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2018-2022, Éric Thiébaut.

#include <string.h>

#include "tao-basics.h"
#include "tao-errors.h"
#include "tao-macros.h"
#include "tao-generic.h"
#include "tao-shared-objects.h"
#include "tao-remote-objects-private.h"

const char* tao_state_get_name(
    tao_state state)
{
    switch (state) {
    case TAO_STATE_INITIALIZING:
        return "initializing";
    case TAO_STATE_WAITING:
        return "waiting";
    case TAO_STATE_STARTING:
        return "starting";
    case TAO_STATE_WORKING:
        return "working";
    case TAO_STATE_STOPPING:
        return "stopping";
    case TAO_STATE_ABORTING:
        return "aborting";
    case TAO_STATE_KILLED:
        return "killed";
    default:
        return "unknown";
    }
}

tao_remote_object* tao_remote_object_create(
    const char* owner,
    tao_object_type type,
    long nbufs,
    long offset,
    long stride,
    size_t size,
    unsigned flags)
{
    long len = TAO_STRLEN(owner);
    if (len < 1 || len >= TAO_OWNER_SIZE) {
        tao_store_error(__func__, TAO_BAD_NAME);
        return NULL;
    }
    if ((type & TAO_SHARED_SUPERTYPE_MASK) != TAO_REMOTE_OBJECT) {
        tao_store_error(__func__, TAO_BAD_TYPE);
        return NULL;
    }
    if (nbufs < 2) {
        tao_store_error(__func__, TAO_BAD_BUFFERS);
        return NULL;
    }
    if (offset < sizeof(tao_remote_object) || stride < 0 ||
        size < offset + nbufs*stride) {
        tao_store_error(__func__, TAO_BAD_SIZE);
        return NULL;
    }
    tao_remote_object* obj = (tao_remote_object*)tao_shared_object_create(
        type, size, flags);
    if (obj == NULL) {
        return NULL;
    }
    for (long i = 0; i < len; ++i) {
        ((char*)obj->owner)[i] = owner[i];
    }
    ((char*)obj->owner)[len] = '\0';
    tao_forced_store(&obj->nbufs,  nbufs);
    tao_forced_store(&obj->offset, offset);
    tao_forced_store(&obj->stride, stride);
    obj->serial = 0;
    obj->state = TAO_STATE_INITIALIZING;
    obj->command = TAO_COMMAND_NONE;
    return obj;
}

tao_remote_object* tao_remote_object_attach(
    tao_shmid shmid)
{
    tao_shared_object* obj = tao_shared_object_attach(shmid);
    if (obj != NULL &&
        (obj->type & TAO_SHARED_SUPERTYPE_MASK) == TAO_REMOTE_OBJECT) {
        return (tao_remote_object*)obj;
    }
    tao_shared_object_detach(obj);
    tao_store_error(__func__, TAO_BAD_TYPE);
    return NULL;
}

// Basic methods.
#define TYPE remote_object
#define IS_REMOTE_OBJECT 1
#include "./shared-methods.c"

// Check whether the server owning the remote object is alive.
//
// NOTE: Since server state is an *atomic* variable, the caller may not have
// locked the object.
static inline bool is_alive(
    const tao_remote_object* obj)
{
    return (obj->state != TAO_STATE_KILLED);
}

// Inline function to check whether a client should wait for the remote server
// to be ready to accept commands.
//
// NOTE: Since communication is asynchronous, we can assume that a server may
//       receive at most one command while initializing.
static inline bool wait_for_command(
    tao_status status,
    const tao_remote_object* obj)
{
    return (status == TAO_OK && is_alive(obj)
            && obj->command != TAO_COMMAND_NONE);
}

// Inline function to check whether a client should wait for the requested
// frame to be available.
static inline bool wait_for_serial(
    tao_status status,
    const tao_remote_object* obj,
    tao_serial serial)
{
    return (status == TAO_OK && is_alive(obj)
            && obj->serial < serial);
}

tao_serial tao_remote_object_wait_serial(
    tao_remote_object* obj,
    tao_serial         serial,
    double             secs)
{
    // Check argument(s) and figure out which buffer to wait for.
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        goto error;
    }
    tao_serial last = obj->serial; // NOTE: ok because atomic
    if (last < 0) {
        tao_store_error(__func__, TAO_CORRUPTED);
        goto error;
    }
    if (serial <= 0) {
        // Manage to wait for next buffer.
        serial = last + 1;
    }
    tao_status status = TAO_OK;
    if (serial > last) {
        // Convert seconds to absolute time, then wait for next buffers until a
        // sufficient serial number is reached or the time limit is exhausted.
        tao_time abstime;
        bool locked = false;
        switch (tao_get_absolute_timeout(&abstime, secs)) {
        case TAO_TIMEOUT_NEVER:
            status = tao_remote_object_lock(obj);
            locked = (status == TAO_OK);
            while (wait_for_serial(status, obj, serial)) {
                status = tao_remote_object_wait_condition(obj);
            }
            break;
        case TAO_TIMEOUT_FUTURE:
            status = tao_remote_object_abstimed_lock(obj, &abstime);
            locked = (status == TAO_OK);
            while (wait_for_serial(status, obj, serial)) {
                status = tao_remote_object_abstimed_wait_condition(
                    obj, &abstime);
            }
            break;
        case TAO_TIMEOUT_NOW:
            status = tao_remote_object_try_lock(obj);
            locked = (status == TAO_OK);
            if (wait_for_serial(status, obj, serial)) {
                status = TAO_TIMEOUT;
            }
            break;
        case TAO_TIMEOUT_PAST:
            // Return a value indicating a timeout.
            status = TAO_TIMEOUT;
            break;
        default:
            // The only remaining possibility is that
            // `tao_get_absolute_timeout` returned an error
            // (TAO_TIMEOUT_ERROR).
            status = TAO_ERROR;
            break;
        }
        // Unlock ressources.
        if (locked && tao_remote_object_unlock(obj) != TAO_OK) {
            status = TAO_ERROR;
        }
    }

    // If no problems occurred, the result is the serial number of the
    // requested buffer; otherwise, it is a nonpositive number indicating the
    // problem.  The serial number of the last output buffer is loaded again to
    // maximize the odds of success of the operation.
    if (status != TAO_ERROR) {
        last = obj->serial; // NOTE: ok because atomic
        if (last < 0) {
            tao_store_error(__func__, TAO_CORRUPTED);
            goto error;
        }
        if (serial <= last) {
            if (serial > last - obj->nbufs) {
                // The requested buffer is available in the cyclic list of
                // buffers.
                return serial;
            } else {
                // The requested buffer is too old.
                return -1;
            }
        } else if (is_alive(obj)) {
            // A timeout occurred.
            return 0;
        } else {
            // The requested buffer will never be available because the server
            // is no longer alive.
            return -2;
        }
    }

    // An error has occurred.
error:
    return -3;
}

tao_status tao_remote_object_lock_for_command(
    tao_remote_object* obj,
    tao_command command,
    double secs)
{
    // Minimal check.
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }

    // Convert seconds to absolute time, then lock resources and wait until the
    // server is ready for a new command, killed, or some error occurs.
    tao_status status;
    tao_time abstime;
    bool locked;
    switch (tao_get_absolute_timeout(&abstime, secs)) {
    case TAO_TIMEOUT_NEVER:
        status = tao_remote_object_lock(obj);
        locked = (status == TAO_OK);
        while (wait_for_command(status, obj)) {
            status = tao_remote_object_wait_condition(obj);
        }
        break;
    case TAO_TIMEOUT_FUTURE:
        status = tao_remote_object_abstimed_lock(obj, &abstime);
        locked = (status == TAO_OK);
        while (wait_for_command(status, obj)) {
            status = tao_remote_object_abstimed_wait_condition(obj, &abstime);
        }
        break;
    case TAO_TIMEOUT_NOW:
        status = tao_remote_object_try_lock(obj);
        locked = (status == TAO_OK);
        if (wait_for_command(status, obj)) {
            status = TAO_TIMEOUT;
        }
        break;
    case TAO_TIMEOUT_PAST:
        locked = false;
        status = TAO_TIMEOUT;
        break;
    default: // TAO_TIMEOUT_ERROR
        locked = false;
        status = TAO_ERROR;
        break;
    }
    if (status == TAO_OK) {
        // Server is either dead or idle.
        if (obj->state == TAO_STATE_KILLED) {
            // Server has been killed.
            if (command != TAO_COMMAND_KILL) {
#if TAO_ASSUME_TIMOUT_IF_SERVER_KILLED
                status = TAO_TIMEOUT;
#else
                tao_store_error(func, TAO_NOT_RUNNING);
                status = TAO_ERROR;
#endif
            }
        } else if (obj->command == TAO_COMMAND_NONE) {
            // Server is ready to accept a new command.
            if (command != TAO_COMMAND_NONE) {
                obj->command = command;
                if (tao_remote_object_broadcast_condition(obj) != TAO_OK) {
                    obj->command = TAO_COMMAND_NONE;
                    status = TAO_ERROR;
                }
            }
        } else  {
            // Unexpected result.
            tao_store_error(__func__, TAO_ASSERTION_FAILED);
            status = TAO_ERROR;
        }
    }

    // In case of error, if object has been locked, unlock it.
    if (status != TAO_OK && locked && tao_remote_object_unlock(obj) != TAO_OK) {
        status = TAO_ERROR;
    }

    return status;
}

tao_status tao_remote_object_send_simple_command(
    tao_remote_object* obj,
    tao_command command,
    double secs)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    if (command == TAO_COMMAND_KILL) {
        if (! is_alive(obj)) {
            // Assume success for a "kill" command when the server is no longer
            // alive.
            return TAO_OK;
        }
    }
    tao_status status = tao_remote_object_lock_for_command(obj, command, secs);
    if (status == TAO_OK) {
        // Command is about to be sent, nothing else to do than unlock.
        if (tao_remote_object_unlock(obj) != TAO_OK) {
            status = TAO_ERROR;
        }
    }
    if (command == TAO_COMMAND_KILL) {
        // Assume success upon timout if the server is no longer alive.
        if (status == TAO_TIMEOUT && ! is_alive(obj)) {
            status = TAO_OK;
        }
    }
    return status;
}

tao_status tao_remote_object_kill(
    tao_remote_object* obj,
    double secs)
{
    // NOTE: Checking of arguments is done by called functions.
    return tao_remote_object_send_simple_command(obj, TAO_COMMAND_KILL, secs);
}
