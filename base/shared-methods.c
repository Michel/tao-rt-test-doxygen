// shared-methods.c --
//
// Generic code for basic methods of TAO shared objects.  The generated code
// depends on the following macros:
//
// - TYPE must be defined with the C type name (without the tao_ prefix).
//
// - MAGIC can be defined with the magic type identifier to automatically
//   generate code to attach objects of this type (not for basic shared,
//   remote, and rwlocked object types).
//
// - IS_REMOTE_OBJECT should be defined to non-zero for types derived from
//   tao_remote_object and left undefined or defined to zero for other types.
//
// - IS_RWLOCKED_OBJECT should be defined to non-zero for types derived from
//   tao_rwlocked_object and left undefined or defined to zero for other types.
//
// - IS_SHARED_OBJECT should be defined to non-zero for types derived from
//   tao_shared_object and left undefined or defined to zero for other types.
//   If IS_REMOTE_OBJECT or IS_RWLOCKED_OBJECT is defined and non-zero,
//   IS_SHARED_OBJECT is automatically defined to non-zero.
//
// These macros are undefined at the end of this file so taht mutiple include's
// are allowed.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2018-2021, Éric Thiébaut.

#ifndef TYPE
# error Macro TYPE must be defined
#endif

#ifndef IS_RWLOCKED_OBJECT
# define IS_RWLOCKED_OBJECT 0
#endif

#ifndef IS_REMOTE_OBJECT
# define IS_REMOTE_OBJECT 0
#endif

#ifndef IS_SHARED_OBJECT
# define IS_SHARED_OBJECT (IS_RWLOCKED_OBJECT || IS_REMOTE_OBJECT)
#endif

#if IS_RWLOCKED_OBJECT && IS_REMOTE_OBJECT
# error Macros IS_RWLOCKED_OBJECT and IS_REMOTE_OBJECT are exclusive
#endif

#if (IS_RWLOCKED_OBJECT || IS_REMOTE_OBJECT) && ! IS_SHARED_OBJECT
# error Macro IS_SHARED_OBJECT must be true if IS_RWLOCKED_OBJECT or IS_REMOTE_OBJECT is true
#endif

#ifdef MAGIC
TAO_XJOIN2(tao_,TYPE)* TAO_XJOIN3(tao_,TYPE,_attach)(
    tao_shmid shmid)
{
    tao_shared_object* obj = tao_shared_object_attach(shmid);
    if (obj != NULL && obj->type == MAGIC) {
        return (TAO_XJOIN2(tao_,TYPE)*)obj;
    }
    tao_shared_object_detach(obj);
    tao_store_error(__func__, TAO_BAD_TYPE);
    return NULL;
}
#endif /* MAGIC */

#if IS_RWLOCKED_OBJECT || IS_REMOTE_OBJECT
tao_status TAO_XJOIN3(tao_,TYPE,_detach)(
    TAO_XJOIN2(tao_,TYPE)* obj)
{
    return tao_shared_object_detach(tao_shared_object_cast(obj));
}
#endif /* IS_RWLOCKED_OBJECT || IS_REMOTE_OBJECT */

#if IS_SHARED_OBJECT

// Constant, no needs to lock object.
size_t TAO_XJOIN3(tao_,TYPE,_get_size)(
    const TAO_XJOIN2(tao_,TYPE)* obj)
{
    return (obj == NULL) ? 0 : tao_shared_object_cast(obj)->size;
}

// Constant, no needs to lock object.
uint32_t TAO_XJOIN3(tao_,TYPE,_get_type)(
    const TAO_XJOIN2(tao_,TYPE)* obj)
{
    return (obj == NULL) ? 0 : tao_shared_object_cast(obj)->type;
}

// Constant, no needs to lock object.
tao_shmid TAO_XJOIN3(tao_,TYPE,_get_shmid)(
    const TAO_XJOIN2(tao_,TYPE)* obj)
{
    return (obj == NULL) ? TAO_BAD_SHMID : tao_shared_object_cast(obj)->shmid;
}

#endif /* IS_SHARED_OBJECT */

#if IS_SHARED_OBJECT && !IS_RWLOCKED_OBJECT

// Unlock a shared object.
tao_status TAO_XJOIN3(tao_,TYPE,_unlock)(
    TAO_XJOIN2(tao_,TYPE)* obj)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return tao_mutex_unlock(
        &tao_shared_object_cast(obj)->mutex);
}

// Lock a shared object.
tao_status TAO_XJOIN3(tao_,TYPE,_lock)(
    TAO_XJOIN2(tao_,TYPE)* obj)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return tao_mutex_lock(
        &tao_shared_object_cast(obj)->mutex);
}

// Attempt to lock a shared object without blocking.
tao_status TAO_XJOIN3(tao_,TYPE,_try_lock)(
    TAO_XJOIN2(tao_,TYPE)* obj)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return tao_mutex_try_lock(
        &tao_shared_object_cast(obj)->mutex);
}

// Attempt to lock a shared object without blocking for longer than a given
// number of seconds.
tao_status TAO_XJOIN3(tao_,TYPE,_timed_lock)(
    TAO_XJOIN2(tao_,TYPE)* obj,
    double secs)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return tao_mutex_timed_lock(
        &tao_shared_object_cast(obj)->mutex,
        secs);
}

// Attempt to lock a shared object without blocking for longer than an absolute
// time limit.
tao_status TAO_XJOIN3(tao_,TYPE,_abstimed_lock)(
    TAO_XJOIN2(tao_,TYPE)* obj,
    const tao_time* abstime)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return tao_mutex_abstimed_lock(
        &tao_shared_object_cast(obj)->mutex,
        abstime);
}

// Signal changes in a shared object to one other thread or process.
tao_status TAO_XJOIN3(tao_,TYPE,_signal_condition)(
    TAO_XJOIN2(tao_,TYPE)* obj)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return tao_condition_signal(
        &tao_shared_object_cast(obj)->cond);
}

// Signal changes in a shared object to all other threads and processes.
tao_status TAO_XJOIN3(tao_,TYPE,_broadcast_condition)(
    TAO_XJOIN2(tao_,TYPE)* obj)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return tao_condition_broadcast(
        &tao_shared_object_cast(obj)->cond);
}

// Wait for signaled changes in a shared object.
tao_status TAO_XJOIN3(tao_,TYPE,_wait_condition)(
    TAO_XJOIN2(tao_,TYPE)* obj)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return tao_condition_wait(
        &tao_shared_object_cast(obj)->cond,
        &tao_shared_object_cast(obj)->mutex);
}

// Wait for signaled changes in a shared object without blocking for longer
// than a given number of seconds.
tao_status TAO_XJOIN3(tao_,TYPE,_timed_wait_condition)(
    TAO_XJOIN2(tao_,TYPE)* obj,
    double secs)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return tao_condition_timed_wait(
        &tao_shared_object_cast(obj)->cond,
        &tao_shared_object_cast(obj)->mutex,
        secs);
}

// Wait for signaled changes in a shared object without blocking for longer
// than an absolute time limit.
tao_status TAO_XJOIN3(tao_,TYPE,_abstimed_wait_condition)(
    TAO_XJOIN2(tao_,TYPE)* obj,
    const tao_time* abstime)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    return tao_condition_abstimed_wait(
        &tao_shared_object_cast(obj)->cond,
        &tao_shared_object_cast(obj)->mutex,
        abstime);
}

#endif /* IS_SHARED_OBJECT && !IS_RWLOCKED_OBJECT */

#if IS_RWLOCKED_OBJECT

#ifndef RWLOCKED_OBJECT_IS_READABLE
#define RWLOCKED_OBJECT_IS_READABLE
static inline bool rwlocked_object_is_readable(
    tao_rwlocked_object* obj)
{
    return obj->users >= 0 && obj->writers == 0;
}
#endif

#ifndef RWLOCKED_OBJECT_IS_WRITABLE
#define RWLOCKED_OBJECT_IS_WRITABLE
static inline bool rwlocked_object_is_writable(
    tao_rwlocked_object* obj)
{
    return obj->users == 0;
}
#endif

tao_status TAO_XJOIN3(tao_,TYPE,_unlock)(
    TAO_XJOIN2(tao_,TYPE)* obj)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    tao_rwlocked_object* root = tao_rwlocked_object_cast(obj);
    tao_status status = tao_mutex_lock(&root->base.mutex);
    if (status == TAO_OK) {
        // Object has been locked.
        bool notify = false;
        int errcode = TAO_SUCCESS;
        if (root->users == -1) {
            // Remove the write lock and notify others.
            root->users = 0;
            notify = true;
        } else if (root->users > 0) {
            // Remove one read lock and notify others if are there no remaining
            // readers.
            root->users -= 1;
            notify = (root->users == 0);
        } else {
            // Something wrong has been done.
            errcode = (root->users == 0 ? TAO_NOT_LOCKED : TAO_CORRUPTED);
        }
        if (notify) {
            status = tao_condition_broadcast(&root->base.cond);
        }
        if (tao_mutex_unlock(&root->base.mutex) != TAO_OK) {
            status = TAO_ERROR;
        }
        if (errcode != TAO_SUCCESS) {
            tao_store_error(__func__, errcode);
            status = TAO_ERROR;
        }
    }
    return status;
}

// Lock r/w object for read-only access.
tao_status TAO_XJOIN3(tao_,TYPE,_rdlock)(
    TAO_XJOIN2(tao_,TYPE)* obj)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    tao_rwlocked_object* root = tao_rwlocked_object_cast(obj);
    tao_status status = tao_mutex_lock(&root->base.mutex);
    if (status == TAO_OK) {
        // Object has been locked.
        while (status == TAO_OK && ! rwlocked_object_is_readable(root)) {
            status = tao_condition_wait(&root->base.cond, &root->base.mutex);
        }
        if (status == TAO_OK) {
            root->users += 1;
        }
        if (tao_mutex_unlock(&root->base.mutex) != TAO_OK) {
            status = TAO_ERROR;
        }
    }
    return status;
}

tao_status TAO_XJOIN3(tao_,TYPE,_try_rdlock)(
    TAO_XJOIN2(tao_,TYPE)* obj)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    tao_rwlocked_object* root = tao_rwlocked_object_cast(obj);
    tao_status status = tao_mutex_try_lock(&root->base.mutex);
    if (status == TAO_OK) {
        // Object has been locked.
        if (rwlocked_object_is_readable(root)) {
            root->users += 1;
        } else {
            status = TAO_TIMEOUT;
        }
        if (tao_mutex_unlock(&root->base.mutex) != TAO_OK) {
            status = TAO_ERROR;
        }
    }
    return status;
}

tao_status TAO_XJOIN3(tao_,TYPE,_abstimed_rdlock)(
    TAO_XJOIN2(tao_,TYPE)* obj,
    const tao_time* abstime)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    tao_rwlocked_object* root = tao_rwlocked_object_cast(obj);
    tao_status status = tao_mutex_abstimed_lock(&root->base.mutex, abstime);
    if (status == TAO_OK) {
        // Object has been locked.
        while (status == TAO_OK && ! rwlocked_object_is_readable(root)) {
            status = tao_condition_abstimed_wait(
                &root->base.cond, &root->base.mutex, abstime);
        }
        if (status == TAO_OK) {
            root->users += 1;
        }
        if (tao_mutex_unlock(&root->base.mutex) != TAO_OK) {
            status = TAO_ERROR;
        }
    }
    return status;
}

tao_status TAO_XJOIN3(tao_,TYPE,_timed_rdlock)(
    TAO_XJOIN2(tao_,TYPE)* obj,
    double secs)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    tao_time abstime;
    switch (tao_get_absolute_timeout(&abstime, secs)) {
    case TAO_TIMEOUT_PAST:
        return TAO_TIMEOUT;
    case TAO_TIMEOUT_NOW:
        return TAO_XJOIN3(tao_,TYPE,_try_rdlock)(obj);
    case TAO_TIMEOUT_FUTURE:
        return TAO_XJOIN3(tao_,TYPE,_abstimed_rdlock)(obj, &abstime);
    case TAO_TIMEOUT_NEVER:
        return TAO_XJOIN3(tao_,TYPE,_rdlock)(obj);
    default:
        return TAO_ERROR;
    }
}

// Lock r/w object for read-write access.
tao_status TAO_XJOIN3(tao_,TYPE,_wrlock)(
    TAO_XJOIN2(tao_,TYPE)* obj)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    tao_rwlocked_object* root = tao_rwlocked_object_cast(obj);
    tao_status status = tao_mutex_lock(&root->base.mutex);
    if (status == TAO_OK) {
        // Object has been locked.
        root->writers += 1; // one more pending writer
        while (status == TAO_OK && ! rwlocked_object_is_writable(root)) {
            status = tao_condition_wait(&root->base.cond, &root->base.mutex);
        }
        root->writers -= 1; // one less pending writer
        if (status == TAO_OK) {
            root->users = -1;
        }
        if (tao_mutex_unlock(&root->base.mutex) != TAO_OK) {
            status = TAO_ERROR;
        }
    }
    return status;
}

tao_status TAO_XJOIN3(tao_,TYPE,_try_wrlock)(
    TAO_XJOIN2(tao_,TYPE)* obj)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    tao_rwlocked_object* root = tao_rwlocked_object_cast(obj);
    tao_status status = tao_mutex_try_lock(&root->base.mutex);
    if (status == TAO_OK) {
        // Object has been locked.
        if (rwlocked_object_is_writable(root)) {
            root->users = -1;
        } else {
            status = TAO_TIMEOUT;
        }
        if (tao_mutex_unlock(&root->base.mutex) != TAO_OK) {
            status = TAO_ERROR;
        }
    }
    return status;
}

tao_status TAO_XJOIN3(tao_,TYPE,_abstimed_wrlock)(
    TAO_XJOIN2(tao_,TYPE)* obj,
    const tao_time* abstime)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    tao_rwlocked_object* root = tao_rwlocked_object_cast(obj);
    tao_status status = tao_mutex_abstimed_lock(&root->base.mutex, abstime);
    if (status == TAO_OK) {
        // Object has been locked.
        root->writers += 1; // one more pending writer
        while (status == TAO_OK && ! rwlocked_object_is_writable(root)) {
            status = tao_condition_abstimed_wait(
                &root->base.cond, &root->base.mutex, abstime);
        }
        root->writers -= 1; // one less pending writer
        if (status == TAO_OK) {
            root->users = -1;
        }
        if (tao_mutex_unlock(&root->base.mutex) != TAO_OK) {
            status = TAO_ERROR;
        }
    }
    return status;
}

tao_status TAO_XJOIN3(tao_,TYPE,_timed_wrlock)(
    TAO_XJOIN2(tao_,TYPE)* obj,
    double secs)
{
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    tao_time abstime;
    switch (tao_get_absolute_timeout(&abstime, secs)) {
    case TAO_TIMEOUT_PAST:
        return TAO_TIMEOUT;
    case TAO_TIMEOUT_NOW:
        return TAO_XJOIN3(tao_,TYPE,_try_wrlock)(obj);
    case TAO_TIMEOUT_FUTURE:
        return TAO_XJOIN3(tao_,TYPE,_abstimed_wrlock)(obj, &abstime);
    case TAO_TIMEOUT_NEVER:
        return TAO_XJOIN3(tao_,TYPE,_wrlock)(obj);
    default:
        return TAO_ERROR;
    }
}

#endif /* IS_RWLOCKED_OBJECT */


#if IS_REMOTE_OBJECT

// Constant, no needs to lock object.
const char* TAO_XJOIN3(tao_,TYPE,_get_owner)(
    const TAO_XJOIN2(tao_,TYPE)* obj)
{
    return (obj == NULL) ? "" : tao_remote_object_cast(obj)->owner;
}

// Constant, no needs to lock object.
long TAO_XJOIN3(tao_,TYPE,_get_nbufs)(
    const TAO_XJOIN2(tao_,TYPE)* obj)
{
    return (obj == NULL) ? 0 : tao_remote_object_cast(obj)->nbufs;
}

// Variable but atomic, object may not be locked.
tao_serial TAO_XJOIN3(tao_,TYPE,_get_serial)(
    const TAO_XJOIN2(tao_,TYPE)* obj)
{
    return (obj == NULL) ? 0 : tao_remote_object_cast(obj)->serial;
}

// Variable but atomic, object may not be locked.
tao_state TAO_XJOIN3(tao_,TYPE,_get_state)(
    const TAO_XJOIN2(tao_,TYPE)* obj)
{
    return (obj == NULL) ? TAO_STATE_KILLED : tao_remote_object_cast(obj)->state;
}

// Variable but atomic, object may not be locked.
int TAO_XJOIN3(tao_,TYPE,_is_alive)(
    const TAO_XJOIN2(tao_,TYPE)* obj)
{
    return (obj != NULL && tao_remote_object_cast(obj)->state >= 0);
}

#endif /* IS_REMOTE_OBJECT */

#undef TYPE
#undef MAGIC
#undef IS_RWLOCKED_OBJECT
#undef IS_REMOTE_OBJECT
#undef IS_SHARED_OBJECT
