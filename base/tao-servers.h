// tao-servers.c -
//
// Definitions for servers in TAO library.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2018-2022, Éric Thiébaut.

#ifndef TAO_SERVERS_H_
#define TAO_SERVERS_H_ 1

#include <tao-basics.h>
#include <tao-buffers.h>

TAO_BEGIN_DECLS

/**
 * @addtogroup XPAServers
 *
 * TAO implement a *class* of servers whose behavior are driven by two
 * callbacks: a *send* callback to handle *get* requests and a *receive*
 * callback to handle *set* requests.
 *
 * A server uses [XPA Messaging System](https://github.com/ericmandel/xpa)
 * and is implemented on top of an XPA server.
 *
 * Upon completion of a set/get request, the server sends a reply to the
 * client.  In case of error (the callback returns a non-zero status), the
 * message is assumed to be an error message.
 *
 * Each server has two dynamical buffers (of type `tao_buffer`) one may be
 * used to store reply messages while the other may be used to store the data
 * results of clients requests.
 *
 * Before calling a callback, a server resets the reply message to be `NULL`,
 * and the internal dynamical buffers and clear the last TAO error of the
 * thread.
 *
 * @{
 */

/**
 * Opaque structure for a TAO server.
 */
typedef struct tao_server_ tao_server;

/**
 * Prototype of callback to handle initialization.
 */
typedef tao_status tao_init_callback(
    tao_server* srv,
    void* ctx);

/**
 * Prototype of callback to handle a "get" request.
 */
typedef tao_status tao_send_callback(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void** bufptr,
    size_t* sizptr);

/**
 * Prototype of callback to handle a "set" request.
 */
typedef tao_status tao_recv_callback(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void* buf,
    size_t siz);

/**
 * Create a server with callbacks.
 *
 * The @b init callback is called once just after server creation.
 *
 * @param address  XPA address of server in the form "class:name".
 * @param help     Some help string.
 * @param init     Callback to initialize internals.
 * @param init_ctx Contextual data associated with the @a init callback.
 * @param send     Callback to handle "get" requests.
 * @param send_ctx Contextual data associated with the @a send callback.
 * @param recv     Callback to handle "set" requests.
 * @param recv_ctx Contextual data associated with the @a recv callback.
 *
 * @return The address of a new server, `NULL` in case of errors.
 */
extern tao_server* tao_create_server(
    const char* address,
    const char* help,
    tao_init_callback* init,
    void* init_ctx,
    tao_send_callback* send,
    void* send_ctx,
    tao_recv_callback* recv,
    void* recv_ctx);

/**
 * Destroy a server.
 *
 * @param srv      Address of server (can be `NULL`).
 */
extern void tao_destroy_server(
    tao_server* srv);

/**
 * Clear the internal data buffer of a server.
 *
 * Each server owns two dynamical buffers (of type `tao_buffer`) one for
 * messages, the other for data.  This function resets the internal data buffer
 * of @a srv so that its contents is empty.
 *
 * @param srv      Address of server.
 */
extern void tao_clear_server_buffer(
    tao_server* srv);

/**
 * Append a string to the internal data buffer of a server.
 *
 * Each server owns two dynamical buffers (of type `tao_buffer`) one for
 * messages, the other for data.  This function appends a string to the
 * internal data buffer of @a srv.
 *
 * @param srv      Address of server.
 * @param str      The string to append.
 * @param len      If nonnegative, length of string (not accounting for a
 *                 final null); otherwise, @a str is assumed to be null
 *                 terminated and its length is given by tao_strlen().
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR on error.
 *
 * @see tao_put_string_to_buffer().
 */
extern tao_status tao_put_string_to_server_buffer(
    tao_server* srv,
    const char* str,
    long len);

/**
 * Print a formatted message into the internal data buffer of a server.
 *
 * Each server owns two dynamical buffers (of type `tao_buffer`) one for
 * messages, the other for data.  This function prints a formatted string to
 * the internal data buffer of @a srv.
 *
 * @param srv      Address of server.
 * @param format   Format string.
 * @param ...      Arguments.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR on error.
 *
 * @see tao_print_to_buffer().
 */
extern tao_status tao_print_to_server_buffer(
    tao_server* srv,
    const char* format,
    ...) TAO_FORMAT_PRINTF(2,3);

/**
 * Append some data to the internal data buffer of a server.
 *
 * Each server owns two dynamical buffers (of type `tao_buffer`) one for
 * messages, the other for data.  This function appends some data to the
 * internal dynamical buffer of @a srv.
 *
 * @param srv      Address of server.
 * @param ptr      Address of first data byte to append.
 * @param siz      Number of bytes to append.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR on error.
 *
 * @see tao_append_to_buffer().
 */
extern tao_status tao_append_to_server_buffer(
    tao_server* srv,
    const void* ptr,
    long siz);

/**
 * Set the data sent back by a "get" request to be the contents of the internal
 * data buffer of a server.
 *
 * Each server owns two dynamical buffers (of type `tao_buffer`) one for
 * messages, the other for data.  This function set the values at addresses
 * @b bufptr and @b lenptr to refer to the internal data buffer of @a srv.
 *
 * @param srv      Address of server.
 * @param bufptr   Address of buffer pointer.
 * @param lenptr   Address of buffer size.
 * @param newline  If true, make sure the contents of the data buffer is
 *                 terminated by a newline character.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR on error.
 *
 * @see tao_append_to_buffer().
 */
extern tao_status tao_set_reply_data_from_buffer(
    tao_server* srv,
    void** bufptr,
    size_t* lenptr,
    bool newline);

/**
 * Set a non-volatile reply message.
 *
 * This function set the reply message for the server @a srv to be given by a
 * static (non-volatile and null terminated) string.  Use
 * tao_set_reply_message() to store in the server message buffer a message
 * string which is either volatile or non-null terminated.  Any prior contents
 * of the server internal message buffer is ignored (and replaced by the
 * message).
 *
 * @warning This function should only be used by a server callback to leave an
 *          error message to be transfered to the client.
 *
 * @param srv      Address of server.
 * @param msg      Message string (must be non-volatile and null terminated).
 */
extern void tao_set_static_reply_message(
    tao_server* srv,
    const char* msg);

/**
 * Copy a reply message in a server dynamical buffer.
 *
 * Each server owns two dynamical buffers (of type `tao_buffer`) one for
 * messages, the other for data.  This function stores the given reply message
 * in the internal message buffer of @a srv.  Any prior contents of the server
 * internal message buffer is lost (and replaced by the message).
 *
 * @warning This function should only be used by a server callback to leave an
 *          error message to be transfered to the client.
 *
 * @param srv      Address of server.
 * @param msg      Message string.
 * @param len      If nonnegative, length of message (not accounting for a
 *                 final null); otherwise, @a msg is assumed to be null
 *                 terminated and its length is given by tao_strlen().
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR on error.
 */
extern tao_status tao_set_reply_message(
    tao_server* srv,
    const char* msg,
    long len);

/**
 * Write a formatted reply message.
 *
 * Each server owns two dynamical buffers (of type `tao_buffer`) one for
 * messages, the other for data.  This function writes a formatted message in
 * the server internal message buffer.  This message is intended to be
 * transfered to the client when the callback returns.  Any prior contents of
 * the server internal message buffer is lost (and replaced by the message).
 *
 * @warning This function should only be used by a server callback to leave a
 *          message to be transfered to the client.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR on error.
 */
extern tao_status tao_format_reply_message(
    tao_server* srv,
    const char* format,
    ...) TAO_FORMAT_PRINTF(2,3);

/**
 * Poll for requests to servers.
 */
extern int tao_poll_requests(
    double secs,
    int maxreq);

/**
 * Run an existing server.
 */
extern tao_status tao_run_server(
    tao_server* srv,
    double secs);

/**
 * Create and run a simple server with callbacks.
 *
 * This routine creates a new TAO server and run it.  The server is finalized,
 * when the server exists its event loop.
 */
extern tao_status tao_create_and_run_server(
    const char* address,
    const char* help,
    double secs,
    tao_init_callback* init,
    void* init_ctx,
    tao_send_callback* send,
    void* send_ctx,
    tao_recv_callback* recv,
    void* recv_ctx);

extern tao_buffer* tao_get_server_buffer(
    tao_server* srv);

/**
 * Query whether server is running.
 *
 * @param srv     The address of the server (may be `NULL`).
 *
 * @return Whether the server is answering requests.
 */
extern bool tao_is_server_running(
    tao_server* srv);

/**
 * Make the server quit.
 *
 * This function may be called to request that the server exits its event loop
 * as soon as possible, that is after heving processed the current request or
 * after the polling timeout.  If the server event loop is handled by
 * tao_run_server(), this will also destroy the server.  If the server event
 * loop is directly controlled by other means (e.g., by calling @ref
 * tao_poll_requests()), the server may be restarted.
 *
 * @param srv     The address of the server.
 */
extern void tao_abort_server(
    tao_server* srv);

/**
 * Set server debug level.
 *
 * @param srv     The address of the server.
 * @param lvl     The debug level: the higher, the more verbose.
 */
extern void tao_set_server_debuglevel(
    tao_server* srv,
    int lvl);

/**
 * Get server debug level.
 *
 * @param srv     The address of the server.
 *
 * @return The current debug level of the server.
 */
extern int tao_get_server_debuglevel(
    tao_server* srv);

extern const char* tao_get_server_address(
    tao_server* srv);

extern const char* tao_get_server_class(
    tao_server* srv);

extern const char* tao_get_server_name(
    tao_server* srv);

extern const char* tao_get_server_help(
    tao_server* srv);

extern const char* tao_get_server_accesspoint(
    tao_server* srv);

/**
 * Structure to store a server command.
 */
typedef struct tao_server_command_ {
    const char*        name;
    tao_send_callback* send;
    void*              send_data;
    tao_recv_callback* recv;
    void*              recv_data;
} tao_server_command;

/**
 * Search a matching entry in a list of server commands.
 *
 * Search an entry with a matching name in the list and returns its address if
 * found or `NULL` ortherwise.
 *
 * @param list  List of commands.  Last entry must be have a `NULL` name.
 * @param name  Name to search.
 *
 * @return Address of matching entry if found, `NULL` otherwise.
 */
extern const tao_server_command* tao_search_server_command(
    const tao_server_command* list,
    const char* name);

/**
 * Execute an "xpaget" request.
 *
 * This function implement a simplified "send" callback for a TAO server whose
 * behavior depends on the first command argument (the command name) which is
 * used to call the associated command callback in the command list given by
 * the @b ctx argument.
 *
 * @param srv     The address of the server.
 * @param ctx     Contextual data.  Must be the address of an array of @ref
 *                tao_server_command structures whose last entry has a `NULL`
 *                name.
 * @param argc    The number of arguments in @b argv.
 * @param argv    List of command arguments (first one is the command name).
 * @param bufptr  Address of variable to store returned data if any.
 * @param sizptr  Address of variable to store size of returned data if any.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR on error.
 */
extern tao_status tao_serve_send_command(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void** bufptr,
    size_t* sizptr);

/**
 * Execute an "xpaset" request.
 *
 * This function implement a simplified "receive" callback for a TAO server
 * whose behavior depends on the first command argument (the command name)
 * which is used to call the associated command callback in the command list
 * given by the @b ctx argument.
 *
 * @param srv     The address of the server.
 * @param ctx     Contextual data.  Must be the address of an array of @ref
 *                tao_server_command structures whose last entry has a `NULL`
 *                name.
 * @param argc    The number of arguments in @b argv.
 * @param argv    List of command arguments (first one is the command name).
 * @param buf     Address sent data if any.
 * @param siz     Size of sent data if any.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR on error.
 */
extern tao_status tao_serve_recv_command(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void* buf,
    size_t siz);

/**
 * @}
 */

TAO_END_DECLS

#endif // TAO_SERVERS_H_
