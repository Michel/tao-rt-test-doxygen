// errors.c -
//
// Management of errors in TAO library.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2018-2022, Éric Thiébaut.

#ifndef TAO_ERRORS_C_
#define TAO_ERRORS_C_ 1

#include "tao-errors.h"
#include "tao-locks.h"

#include "config.h"

#include <threads.h>
#include <errno.h>
#include <string.h>

#define USE_STRERROR 0 // FIXME: should be in config.h

#define MALLOC(T) ((T*)malloc(sizeof(T)))
#define NEW(T)    ((T*)calloc(1, sizeof(T)))

static const char* error_format = "%s%s in function `%s` [%s]%s";
static const char* default_prefix = "(TAO-ERROR) ";

static tao_status default_error_handler(const tao_error* err);

// Private structure used to store per-thread data.
static thread_local struct thread_data {
    tao_error             last_error; ///< Last error details.
    tao_error_handler* error_handler; ///< Error handler.
} thread_data = {
    .last_error = (tao_error){
        .func = NULL,
        .proc = NULL,
        .code = TAO_SUCCESS
    },
    .error_handler = default_error_handler
};

static inline tao_error* get_last_error(void)
{
    return &thread_data.last_error;
}

tao_error* tao_get_last_error(
    void)
{
    return get_last_error();
}

// Exclusive lock used to avoid messing messages.
static tao_mutex io_mutex = TAO_MUTEX_INITIALIZER;

// Private structure used to format reported errors.
typedef struct reporter_context_ {
    void*       output; // FILE* or tao_buffer*
    const char* prefix; // start with this string
    const char* suffix; // end with this string
} reporter_context;

static tao_status buffer_reporter(
    void* data,
    const char* reason,
    const char* func,
    const char* info,
    int code)
{
    reporter_context* ctx = data;
    return tao_buffer_printf((tao_buffer*)ctx->output, error_format,
                               ctx->prefix, reason, func, info, ctx->suffix);
}

static tao_status stream_reporter(
    void* data,
    const char* reason,
    const char* func,
    const char* info,
    int code)
{
    reporter_context* ctx = data;
    int res = fprintf((FILE*)ctx->output, error_format,
                      ctx->prefix, reason, func, info, ctx->suffix);
    if (res < 0) {
        tao_store_system_error("fprintf");
        return TAO_ERROR;
    }
    return TAO_OK;
}

void tao_panic(
    void)
{
    tao_report_error_to_stderr(NULL, "(TAO-PANIC) ", NULL);
    exit(1);
}

// Set last error information.
static inline void set_last_error(
    const char*       func,
    int               code,
    tao_error_getter* proc)
{
    tao_error* err = get_last_error();
    err->func = func;
    err->proc = proc;
    err->code = code;
}

void tao_store_other_error(
    const char*       func,
    int               code,
    tao_error_getter* proc)
{
    set_last_error(func, code, proc);
}

void tao_store_error(
    const char* func,
    int         code)
{
    set_last_error(func, code, NULL);
}

void tao_store_system_error(
    const char* func)
{
    int code = errno;
    if (code <= 0) {
        code = TAO_SYSTEM_ERROR;
    }
    set_last_error(func, code, NULL);
}

void tao_clear_error(
    tao_error* err)
{
    if (err == NULL) {
        err = get_last_error();
    }
    err->func = NULL;
    err->proc = NULL;
    err->code = TAO_SUCCESS;
}

int tao_any_errors(
    const tao_error* err)
{
    if (err == NULL) {
        err = get_last_error();
    }
    return (err->code != TAO_SUCCESS);
}

tao_error_handler* tao_error_set_handler(
    tao_error_handler* func)
{
    tao_error_handler** handler = &thread_data.error_handler;
    tao_error_handler* prev = *handler;
    *handler = (func == NULL ? default_error_handler : func);
    return prev;
}

void tao_report_error(
    void)
{
    struct thread_data* data = &thread_data;
    if (data->error_handler == NULL ||
        data->error_handler(&data->last_error) != TAO_OK) {
        tao_panic();
    }
}

static tao_status report_error_to_stream(
    const char*      func,
    FILE*            file,
    const tao_error* err,
    const char*      pfx,
    const char*      sfx)
{
    // Instanciate context with defaults.
    reporter_context ctx = {
        .output = file,
        .prefix = (pfx != NULL ? pfx : default_prefix),
        .suffix = (sfx != NULL ? sfx : "\n")
    };
    // Lock i/o mutex to avoid mixing messages from different threads.
    tao_status status = tao_mutex_lock(&io_mutex);
    if (status == TAO_OK) {
        if (tao_report_error_with_reporter(
                stream_reporter, &ctx, err) != TAO_OK) {
            status = TAO_ERROR;
        }
        if (tao_mutex_unlock(&io_mutex) != TAO_OK) {
            status = TAO_ERROR;
        }
    }
    return status;
}

tao_status tao_report_error_to_stream(
    FILE*            file,
    const tao_error* err,
    const char*      pfx,
    const char*      sfx)
{
    if (file == NULL) {
        file = stderr;
    }
    return report_error_to_stream(__func__, file, err, pfx, sfx);
}

tao_status tao_report_error_to_stderr(
    const tao_error* err,
    const char*      pfx,
    const char*      sfx)
{
    return report_error_to_stream(__func__, stderr, err, pfx, sfx);
}

static tao_status default_error_handler(const tao_error* err)
{
    return tao_report_error_to_stderr(err, NULL, NULL);
}

tao_status tao_report_error_to_buffer(
    tao_buffer*      buf,
    const tao_error* err,
    const char*      pfx,
    const char*      sfx)
{
    // Instanciate context with defaults.
    reporter_context ctx = {
        .output = buf,
        .prefix = (pfx != NULL ? pfx : default_prefix),
        .suffix = (sfx != NULL ? sfx : "")
    };
    return tao_report_error_with_reporter(buffer_reporter, &ctx, err);
}

tao_status tao_report_error_with_reporter(
    tao_error_reporter* reporter,
    void* ctx,
    const tao_error* err)
{
    // Report last error by default.
    if (err == NULL) {
        err = get_last_error();
    }

    // Retrieve error details and call error reporter.
    char buffer[20];
    const char* func = (err->func == NULL ? "unknown_function" : err->func);
    const char* reason;
    const char* info;
    int code = err->code;
    tao_retrieve_error_details(code, &reason, &info, err->proc, buffer);
    return reporter(ctx, reason, func, info, code);
}

void tao_retrieve_error_details(
    int code,
    const char** reason,
    const char** info,
    tao_error_getter* proc,
    char* buffer)
{
    if (proc != NULL) {
        // Use callback to retrieve error details.
        if (reason != NULL || info != NULL) {
            if (reason != NULL) {
                *reason = NULL;
            }
            if (info != NULL) {
                *info = NULL;
            }
            proc(code, reason, info);
        }
    } else {
        // Assume a system error or a TAO error.
        if (reason != NULL) {
            *reason = tao_get_error_reason(code);
        }
        if (info != NULL) {
            *info = tao_get_error_name(code);
            if (*info != NULL) {
                if (code > 0) {
                    if (strcmp(*info, "UNKNOWN_SYSTEM_ERROR") == 0) {
                        *info = NULL;
                    }
                } else if (code < 0) {
                    if (strcmp(*info, "UNKNOWN_ERROR") == 0) {
                        *info = NULL;
                    }
                }
            }
        }
    }
    if (reason != NULL && *reason == NULL) {
        *reason = "Some error occurred";
    }
    if (info != NULL && *info == NULL && buffer != NULL) {
        // Use the numerical value of the error code.
        sprintf(buffer, "%d", code);
        *info = buffer;
    }
}

#define GET_ERR_FUNC 1
#include __FILE__

#define GET_ERR_FUNC 2
#include __FILE__

#else // TAO_ERRORS_C_ defined

#ifdef GET_ERR_FUNC

#   undef FUNC
#   undef CASE
#   if GET_ERR_FUNC == 1
#       define FUNC       tao_get_error_reason
#       define CASE(id, str) case id: return str
#   elif GET_ERR_FUNC == 2
#       define FUNC       tao_get_error_name
#       define CASE(id, str) case id: return #id
#   else
#       error Invalid value for GET_ERR_FUNC
#   endif

const char* FUNC(
    int code)
{
    switch (code) {
        CASE(TAO_UNWRITABLE, "Not writable");
        CASE(TAO_UNSUPPORTED, "Unsupported feature");
        CASE(TAO_UNRECOVERABLE, "Unrecoverable error");
        CASE(TAO_UNREADABLE, "Not readable");
        CASE(TAO_UNCLOSED_STRING, "Unclosed string");
        CASE(TAO_SYSTEM_ERROR, "Unknown system error");
        CASE(TAO_OVERWRITTEN, "Contents has been overwritten");
        CASE(TAO_OUT_OF_RANGE, "Out of range argument");
        CASE(TAO_NO_FITS_SUPPORT, "Compiled with no FITS support");
        CASE(TAO_NO_DATA, "No data available");
        CASE(TAO_NOT_YET_IMPLEMENTED, "Not yet implemented");
        CASE(TAO_NOT_RUNNING, "Server is not running");
        CASE(TAO_NOT_READY, "Device not ready");
        CASE(TAO_NOT_LOCKED, "Resource not locked by caller");
        CASE(TAO_NOT_FOUND, "Item not found");
        CASE(TAO_NOT_ACQUIRING, "Acquisition not started");
        CASE(TAO_MUST_RESET, "Device must be reset");
        CASE(TAO_MISSING_SEPARATOR, "Separator missing");
        CASE(TAO_EXHAUSTED, "Resource exhausted");
        CASE(TAO_DESTROYED, "Ressource has been destroyed");
        CASE(TAO_CORRUPTED, "Corrupted structure");
        CASE(TAO_CANT_TRACK_ERROR, "Insufficient memory to track errors");
        CASE(TAO_BROKEN_CYCLE, "Broken cycle or unordered operations");
        CASE(TAO_BAD_VALUE, "Invalid parameter value");
        CASE(TAO_BAD_TYPE, "Invalid type");
        CASE(TAO_BAD_STAGE, "Invalid or unexpected stage");
        CASE(TAO_BAD_SPEED, "Invalid connection speed");
        CASE(TAO_BAD_SIZE, "Invalid size");
        CASE(TAO_BAD_SERIAL, "Invalid serial number");
        CASE(TAO_BAD_ROI, "Invalid region of interest");
        CASE(TAO_BAD_RANK, "Invalid number of dimensions");
        CASE(TAO_BAD_PREPROCESSING, "Bad pre-processing setting");
        CASE(TAO_BAD_NAME, "Bad parameter name");
        CASE(TAO_BAD_MAGIC, "Invalid magic number");
        CASE(TAO_BAD_GAIN, "Invalid detector gain");
        CASE(TAO_BAD_FRAMERATE, "Invalid acquisition frame rate");
        CASE(TAO_BAD_FILENAME, "Invalid file name");
        CASE(TAO_BAD_EXPOSURETIME, "Invalid exposure time");
        CASE(TAO_BAD_ESCAPE, "Unknown escape sequence");
        CASE(TAO_BAD_ENCODING, "Bad encoding");
        CASE(TAO_BAD_DEVICE, "Invalid device");
        CASE(TAO_BAD_DEPTH, "Invalid bits per pixel");
        CASE(TAO_BAD_COMMAND, "Invalid command");
        CASE(TAO_BAD_CHARACTER, "Illegal character");
        CASE(TAO_BAD_CHANNELS, "Invalid number of channels");
        CASE(TAO_BAD_BUFFERS, "Bad number of buffers");
        CASE(TAO_BAD_BIAS, "Invalid detector bias");
        CASE(TAO_BAD_ATTACHMENTS, "Invalid number of attachments");
        CASE(TAO_BAD_ARGUMENT, "Invalid argument");
        CASE(TAO_BAD_ADDRESS, "Invalid address");
        CASE(TAO_ASSERTION_FAILED, "Assertion failed");
        CASE(TAO_ALREADY_IN_USE, "Resource already in use");
        CASE(TAO_ALREADY_EXIST, "Destination already exists");
        CASE(TAO_ACQUISITION_RUNNING, "Acquisition running");
        CASE(TAO_SUCCESS, "Operation was successful");
#ifdef EPERM
        CASE(EPERM, "Operation not permitted");
#endif
#ifdef ENOENT
        CASE(ENOENT, "No such file or directory");
#endif
#ifdef ESRCH
        CASE(ESRCH, "No such process");
#endif
#ifdef EINTR
        CASE(EINTR, "Interrupted system call");
#endif
#ifdef EIO
        CASE(EIO, "I/O error");
#endif
#ifdef ENXIO
        CASE(ENXIO, "No such device or address");
#endif
#if defined(E2BIG) && (!defined(EOVERFLOW) || (E2BIG != EOVERFLOW))
        CASE(E2BIG, "Argument list too long");
#endif
#ifdef ENOEXEC
        CASE(ENOEXEC, "Exec format error");
#endif
#ifdef EBADF
        CASE(EBADF, "Bad file number");
#endif
#ifdef ECHILD
        CASE(ECHILD, "No child processes");
#endif
#ifdef EAGAIN
        CASE(EAGAIN, "Resource temporarily unavailable");
#endif
#ifdef ENOMEM
        CASE(ENOMEM, "Not enough memory");
#endif
#ifdef EACCES
        CASE(EACCES, "Permission denied");
#endif
#ifdef EFAULT
        CASE(EFAULT, "Bad address");
#endif
#ifdef ENOTBLK
        CASE(ENOTBLK, "Block device required");
#endif
#ifdef EBUSY
        CASE(EBUSY, "Device or resource busy");
#endif
#ifdef EEXIST
        CASE(EEXIST, "File already exists");
#endif
#ifdef EXDEV
        CASE(EXDEV, "Cross-device link");
#endif
#ifdef ENODEV
        CASE(ENODEV, "No such device");
#endif
#ifdef ENOTDIR
        CASE(ENOTDIR, "Not a directory");
#endif
#ifdef EISDIR
        CASE(EISDIR, "Illegal operation on a directory");
#endif
#ifdef EINVAL
        CASE(EINVAL, "Invalid argument");
#endif
#ifdef ENFILE
        CASE(ENFILE, "File table overflow");
#endif
#ifdef EMFILE
        CASE(EMFILE, "Too many open files");
#endif
#ifdef ENOTTY
        CASE(ENOTTY, "Not a typewriter");
#endif
#ifdef ETXTBSY
        CASE(ETXTBSY, "Text file or pseudo-device busy");
#endif
#ifdef EFBIG
        CASE(EFBIG, "File too large");
#endif
#ifdef ENOSPC
        CASE(ENOSPC, "No space left on device");
#endif
#ifdef ESPIPE
        CASE(ESPIPE, "Invalid seek");
#endif
#ifdef EROFS
        CASE(EROFS, "Read-only file system");
#endif
#ifdef EMLINK
        CASE(EMLINK, "Too many links");
#endif
#ifdef EPIPE
        CASE(EPIPE, "Broken pipe");
#endif
#ifdef EDOM
        CASE(EDOM, "Math argument out of function domain");
#endif
#ifdef ERANGE
        CASE(ERANGE, "Math result not representable");
#endif
#ifdef EADDRINUSE
        CASE(EADDRINUSE, "Address already in use");
#endif
#ifdef EADDRNOTAVAIL
        CASE(EADDRNOTAVAIL, "Cannot assign requested address");
#endif
#ifdef EADV
        CASE(EADV, "Advertise error");
#endif
#ifdef EAFNOSUPPORT
        CASE(EAFNOSUPPORT, "Address family not supported by protocol");
#endif
#ifdef EALIGN
        CASE(EALIGN, "EALIGN");
#endif
#if defined(EALREADY) && (!defined(EBUSY) || (EALREADY != EBUSY))
        CASE(EALREADY, "Operation already in progress");
#endif
#ifdef EBADE
        CASE(EBADE, "Bad exchange descriptor");
#endif
#ifdef EBADFD
        CASE(EBADFD, "File descriptor in bad state");
#endif
#ifdef EBADMSG
        CASE(EBADMSG, "Not a data message");
#endif
#ifdef ECANCELED
        CASE(ECANCELED, "Operation canceled");
#endif
#ifdef EBADR
        CASE(EBADR, "Bad request descriptor");
#endif
#ifdef EBADRPC
        CASE(EBADRPC, "RPC structure is bad");
#endif
#ifdef EBADRQC
        CASE(EBADRQC, "Bad request code");
#endif
#ifdef EBADSLT
        CASE(EBADSLT, "Invalid slot");
#endif
#ifdef EBFONT
        CASE(EBFONT, "Bad font file format");
#endif
#ifdef ECHRNG
        CASE(ECHRNG, "Channel number out of range");
#endif
#ifdef ECOMM
        CASE(ECOMM, "Communication error on send");
#endif
#ifdef ECONNABORTED
        CASE(ECONNABORTED, "Software caused connection abort");
#endif
#ifdef ECONNREFUSED
        CASE(ECONNREFUSED, "Connection refused");
#endif
#ifdef ECONNRESET
        CASE(ECONNRESET, "Connection reset by peer");
#endif
#if defined(EDEADLK) && (!defined(EWOULDBLOCK) || (EDEADLK != EWOULDBLOCK))
        CASE(EDEADLK, "Resource deadlock avoided");
#endif
#if defined(EDEADLOCK) && (!defined(EDEADLK) || (EDEADLOCK != EDEADLK))
        CASE(EDEADLOCK, "Resource deadlock avoided");
#endif
#ifdef EDESTADDRREQ
        CASE(EDESTADDRREQ, "Destination address required");
#endif
#ifdef EDIRTY
        CASE(EDIRTY, "Mounting a dirty fs w/o force");
#endif
#ifdef EDOTDOT
        CASE(EDOTDOT, "Cross mount point");
#endif
#ifdef EDQUOT
        CASE(EDQUOT, "Disk quota exceeded");
#endif
#ifdef EDUPPKG
        CASE(EDUPPKG, "Duplicate package name");
#endif
#ifdef EHOSTDOWN
        CASE(EHOSTDOWN, "Host is down");
#endif
#ifdef EHOSTUNREACH
        CASE(EHOSTUNREACH, "Host is unreachable");
#endif
#if defined(EIDRM) && (!defined(EINPROGRESS) || (EIDRM != EINPROGRESS))
        CASE(EIDRM, "Identifier removed");
#endif
#ifdef EINIT
        CASE(EINIT, "Initialization error");
#endif
#ifdef EINPROGRESS
        CASE(EINPROGRESS, "Operation now in progress");
#endif
#ifdef EISCONN
        CASE(EISCONN, "Socket is already connected");
#endif
#ifdef EISNAME
        CASE(EISNAM, "Is a name file");
#endif
#ifdef ELBIN
        CASE(ELBIN, "ELBIN");
#endif
#ifdef EL2HLT
        CASE(EL2HLT, "Level 2 halted");
#endif
#ifdef EL2NSYNC
        CASE(EL2NSYNC, "Level 2 not synchronized");
#endif
#ifdef EL3HLT
        CASE(EL3HLT, "Level 3 halted");
#endif
#ifdef EL3RST
        CASE(EL3RST, "Level 3 reset");
#endif
#ifdef ELIBACC
        CASE(ELIBACC, "Cannot access a needed shared library");
#endif
#ifdef ELIBBAD
        CASE(ELIBBAD, "Accessing a corrupted shared library");
#endif
#ifdef ELIBEXEC
        CASE(ELIBEXEC, "Cannot exec a shared library directly");
#endif
#if defined(ELIBMAX) && (!defined(ECANCELED) || (ELIBMAX != ECANCELED))
        CASE (ELIBMAX,
              "Attempting to link in more shared libraries than system limit");
#endif
#ifdef ELIBSCN
        CASE(ELIBSCN, "Corrupted .lib section in a.out");
#endif
#ifdef ELNRNG
        CASE(ELNRNG, "Link number out of range");
#endif
#if defined(ELOOP) && (!defined(ENOENT) || (ELOOP != ENOENT))
        CASE(ELOOP, "Too many levels of symbolic links");
#endif
#ifdef EMSGSIZE
        CASE(EMSGSIZE, "Message too long");
#endif
#ifdef EMULTIHOP
        CASE(EMULTIHOP, "Multihop attempted");
#endif
#ifdef ENAMETOOLONG
        CASE(ENAMETOOLONG, "File name too long");
#endif
#ifdef ENAVAIL
        CASE(ENAVAIL, "Not available");
#endif
#ifdef ENET
        CASE(ENET, "ENET");
#endif
#ifdef ENETDOWN
        CASE(ENETDOWN, "Network is down");
#endif
#ifdef ENETRESET
        CASE(ENETRESET, "Network dropped connection on reset");
#endif
#ifdef ENETUNREACH
        CASE(ENETUNREACH, "Network is unreachable");
#endif
#ifdef ENOANO
        CASE(ENOANO, "Anode table overflow");
#endif
#if defined(ENOBUFS) && (!defined(ENOSR) || (ENOBUFS != ENOSR))
        CASE(ENOBUFS, "No buffer space available");
#endif
#ifdef ENOCSI
        CASE(ENOCSI, "No CSI structure available");
#endif
#if defined(ENODATA) && (!defined(ECONNREFUSED) || (ENODATA != ECONNREFUSED))
        CASE(ENODATA, "No data available");
#endif
#ifdef ENOLCK
        CASE(ENOLCK, "No locks available");
#endif
#ifdef ENOLINK
        CASE(ENOLINK, "Link has been severed");
#endif
#ifdef ENOMSG
        CASE(ENOMSG, "No message of desired type");
#endif
#ifdef ENONET
        CASE(ENONET, "Machine is not on the network");
#endif
#ifdef ENOPKG
        CASE(ENOPKG, "Package not installed");
#endif
#ifdef ENOPROTOOPT
        CASE(ENOPROTOOPT, "Bad protocol option");
#endif
#if defined(ENOSR) && (!defined(ENAMETOOLONG) || (ENAMETOOLONG != ENOSR))
        CASE(ENOSR, "Out of stream resources");
#endif
#if defined(ENOSTR) && (!defined(ENOTTY) || (ENOTTY != ENOSTR))
        CASE(ENOSTR, "Not a stream device");
#endif
#ifdef ENOSYM
        CASE(ENOSYM, "Unresolved symbol name");
#endif
#ifdef ENOSYS
        CASE(ENOSYS, "Function not implemented");
#endif
#ifdef ENOTCONN
        CASE(ENOTCONN, "Socket is not connected");
#endif
#ifdef ENOTRECOVERABLE
        CASE(ENOTRECOVERABLE, "State not recoverable");
#endif
#if defined(ENOTEMPTY) && (!defined(EEXIST) || (ENOTEMPTY != EEXIST))
        CASE(ENOTEMPTY, "Directory not empty");
#endif
#ifdef ENOTNAM
        CASE(ENOTNAM, "Not a name file");
#endif
#ifdef ENOTSOCK
        CASE(ENOTSOCK, "Socket operation on non-socket");
#endif
#ifdef ENOTSUP
        CASE(ENOTSUP, "Operation not supported");
#endif
#ifdef ENOTUNIQ
        CASE(ENOTUNIQ, "Name not unique on network");
#endif
#if defined(EOPNOTSUPP) &&  (!defined(ENOTSUP) || (ENOTSUP != EOPNOTSUPP))
        CASE(EOPNOTSUPP, "Operation not supported on socket");
#endif
#ifdef EOTHER
        CASE(EOTHER, "Other error");
#endif
#if defined(EOVERFLOW) && (!defined(EFBIG) || (EOVERFLOW != EFBIG)) && (!defined(EINVAL) || (EOVERFLOW != EINVAL))
        CASE(EOVERFLOW, "File too big");
#endif
#ifdef EOWNERDEAD
        CASE(EOWNERDEAD, "Owner died");
#endif
#if defined(EPFNOSUPPORT) && (!defined(ENOLCK) || (ENOLCK != EPFNOSUPPORT))
        CASE(EPFNOSUPPORT, "Protocol family not supported");
#endif
#ifdef EPROCLIM
        CASE(EPROCLIM, "Too many processes");
#endif
#ifdef EPROCUNAVAIL
        CASE(EPROCUNAVAIL, "Bad procedure for program");
#endif
#ifdef EPROGMISMATCH
        CASE(EPROGMISMATCH, "Program version wrong");
#endif
#ifdef EPROGUNAVAIL
        CASE(EPROGUNAVAIL, "RPC program not available");
#endif
#ifdef EPROTO
        CASE(EPROTO, "Protocol error");
#endif
#ifdef EPROTONOSUPPORT
        CASE(EPROTONOSUPPORT, "Protocol not supported");
#endif
#ifdef EPROTOTYPE
        CASE(EPROTOTYPE, "Protocol wrong type for socket");
#endif
#if defined(EREFUSED) && (!defined(ECONNREFUSED) || (EREFUSED != ECONNREFUSED))
        CASE(EREFUSED, "EREFUSED");
#endif
#ifdef EREMCHG
        CASE(EREMCHG, "Remote address changed");
#endif
#ifdef EREMDEV
        CASE(EREMDEV, "Remote device");
#endif
#ifdef EREMOTE
        CASE(EREMOTE, "Pathname hit remote file system");
#endif
#ifdef EREMOTEIO
        CASE(EREMOTEIO, "Remote i/o error");
#endif
#ifdef EREMOTERELEASE
        CASE(EREMOTERELEASE, "EREMOTERELEASE");
#endif
#ifdef ERPCMISMATCH
        CASE(ERPCMISMATCH, "RPC version is wrong");
#endif
#ifdef ERREMOTE
        CASE(ERREMOTE, "Object is remote");
#endif
#ifdef ESHUTDOWN
        CASE(ESHUTDOWN, "Cannot send after socket shutdown");
#endif
#ifdef ESOCKTNOSUPPORT
        CASE(ESOCKTNOSUPPORT, "Socket type not supported");
#endif
#ifdef ESRMNT
        CASE(ESRMNT, "srmount error");
#endif
#ifdef ESTALE
        CASE(ESTALE, "Stale remote file handle");
#endif
#ifdef ESUCCESS
        CASE(ESUCCESS, "Error 0");
#endif
#if defined(ETIME) && (!defined(ELOOP) || (ETIME != ELOOP))
        CASE(ETIME, "Timer expired");
#endif
#if defined(ETIMEDOUT) && (!defined(ENOSTR) || (ETIMEDOUT != ENOSTR))
        CASE(ETIMEDOUT, "Connection timed out");
#endif
#ifdef ETOOMANYREFS
        CASE(ETOOMANYREFS, "Too many references: cannot splice");
#endif
#ifdef EUCLEAN
        CASE(EUCLEAN, "Structure needs cleaning");
#endif
#ifdef EUNATCH
        CASE(EUNATCH, "Protocol driver not attached");
#endif
#ifdef EUSERS
        CASE(EUSERS, "Too many users");
#endif
#ifdef EVERSION
        CASE(EVERSION, "Version mismatch");
#endif
#if defined(EWOULDBLOCK) && (!defined(EAGAIN) || (EWOULDBLOCK != EAGAIN))
        CASE(EWOULDBLOCK, "Operation would block");
#endif
#ifdef EXFULL
        CASE(EXFULL, "Message tables full");
#endif
    }
    if (code > 0) {
        // Unknown system error.
#if GET_ERR_FUNC == 1
#    if defined(USE_STRERROR) && (USE_STRERROR != 0)
        static bool init = false;
        if (! init) {
            (void)setlocale(LC_ALL, "C");
            init = true;
        }
        return strerror(code);
#    else
        return "Unknown system error";
#    endif
#else
        return "UNKNOWN_SYSTEM_ERROR";
#endif
    } else {
        // Unknown error in TAO library.
#if GET_ERR_FUNC == 1
        return "Unknown error";
#else
        return "UNKNOWN_ERROR";
#endif
    }
}

#undef CASE
#undef FUNC
#undef GET_ERR_FUNC

#endif // GET_ERR_FUNC defined

#endif // TAO_ERRORS_C_ not defined
