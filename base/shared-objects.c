// shared-objects.c -
//
// Management of shared memory and implementation of basic objects whose
// contents can be shared between processes.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2018-2022, Éric Thiébaut.

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdatomic.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <time.h>

#include "tao-basics.h"
#include "tao-errors.h"
#include "tao-locks.h"
#include "tao-macros.h"
#include "tao-generic.h"
#include "tao-shared-memory.h"
#include "tao-shared-objects-private.h"
#include "tao-remote-objects-private.h"
#include "tao-rwlocked-objects-private.h"

//-----------------------------------------------------------------------------
// SHARED MEMORY

// The least significant 9 bits of flags specify the permissions granted to the
// owner, group, and others. The following macro is to select allowed
// permissions.
#define PERMS_MASK (S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH)

// Bad address for a shared memory segment (as returned by `shmat`).
#define BAD_SHM_ADDR ((void*)-1)

// Call `shmdt`, update caller's last error in case of failure.
static inline tao_status shared_memory_detach(
    void* addr)
{
    if (shmdt(addr) != 0) {
        tao_store_system_error("shmdt");
        return TAO_ERROR;
    }
    return TAO_OK;
}

// Call `shmctl` with `IPC_RMID`, updating caller's last error in case of
// failure.
static inline tao_status shared_memory_destroy(
    tao_shmid shmid)
{
    if (shmctl(shmid, IPC_RMID, NULL) != 0) {
        tao_store_system_error("shmctl");
        return TAO_ERROR;
    }
    return TAO_OK;
}

void* tao_shared_memory_create(
    tao_shmid* shmid_ptr,
    size_t     size,
    unsigned   perms)
{
    void* addr = BAD_SHM_ADDR;

    // Create a new segment of shared memory and get its identifier.  Note that
    // `shmget` guarantees that the contents of the shared memory segment is
    // zero-filled when `IPC_CREAT` and `IPC_EXCL` are both set.
    int shmid = shmget(
        IPC_PRIVATE, size, (perms & PERMS_MASK) | (IPC_CREAT | IPC_EXCL));
    if (shmid == -1) {
        tao_store_system_error("shmget");
        goto error;
    }

    // Attach the shared memory segment to the address space of the caller.
    addr = shmat(shmid, NULL, 0);
    if (addr == BAD_SHM_ADDR) {
        tao_store_system_error("shmat");
        goto error;
    }

    // Store shared memory identifier and return attachment address.
    if (shmid_ptr != NULL) {
        *shmid_ptr = shmid;
    }
    return addr;

    // In case of error, detach shared memory form caller's address space.
 error:
    if (addr != BAD_SHM_ADDR) {
        shared_memory_detach(addr);
    }
    if (shmid_ptr != NULL) {
        *shmid_ptr = TAO_BAD_SHMID;
    }
    return NULL;
}

tao_status tao_shared_memory_stat(
    tao_shmid shmid,
    size_t*   segsz,
    int64_t*  nattch)
{
    tao_status status = TAO_OK;
    struct shmid_ds ds;
    if (shmctl(shmid, IPC_STAT, &ds) != 0) {
        ds.shm_segsz  = 0;
        ds.shm_nattch = 0;
        status = TAO_ERROR;
    }
    if (segsz != NULL) {
        *segsz = ds.shm_segsz;
    }
    if (nattch != NULL) {
        *nattch = ds.shm_nattch;
    }
    return status;
}

void* tao_shared_memory_attach(
    tao_shmid shmid,
    size_t*   sizeptr)
{
    void* addr = shmat(shmid, NULL, 0);
    if (addr == BAD_SHM_ADDR) {
        tao_store_system_error("shmat");
        return NULL;
    }
    if (sizeptr != NULL) {
        // The caller is interested in getting the size of the shared memory
        // segment.
        struct shmid_ds ds;
        if (shmctl(shmid, IPC_STAT, &ds) != 0) {
            tao_store_system_error("shmctl");
            tao_shared_memory_detach(addr);
            return NULL;
        }
        *sizeptr = ds.shm_segsz;
    }
    return addr;
}

tao_status tao_shared_memory_detach(
    void* addr)
{
    // Just ignore if address is null.
    return (addr == NULL) ? TAO_OK : shared_memory_detach(addr);
}

tao_status tao_shared_memory_destroy(
    tao_shmid shmid)
{
    return shared_memory_destroy(shmid);
}

//-----------------------------------------------------------------------------
// SHARED OBJECTS

tao_shared_object* tao_shared_object_create(
    uint32_t type,
    size_t   size,
    unsigned flags)
{

    // Check arguments.
    if ((type & TAO_SHARED_MASK) != TAO_SHARED_MAGIC) {
        tao_store_error(__func__, TAO_BAD_MAGIC);
        return NULL;
    }
    if (size < sizeof(tao_shared_object)) {
        tao_store_error(__func__, TAO_BAD_SIZE);
        return NULL;
    }

    // Create shared memory with at least read and write access for the caller.
    unsigned perms = (flags & PERMS_MASK) | (S_IRUSR | S_IWUSR);
    tao_shmid shmid;
    tao_shared_object* obj = tao_shared_memory_create(&shmid, size, perms);
    if (obj == NULL) {
        return NULL;
    }

    // Unless the object shall be persistent, manage to destroy shared memory
    // on last detach.  For Mac-OS, this can only be done *after* last detach.
    int initlevel = 0;
#ifndef __APPLE__ //
    if ((flags & TAO_PERSISTENT) == 0) {
        if (shared_memory_destroy(shmid) != TAO_OK) {
            goto error;
        }
    }
#endif

    // Initialize mutex and condition variable so that they can be shared
    // with other processes.
    if (tao_mutex_initialize(&obj->mutex, true) != TAO_OK) {
        goto error;
    }
    ++initlevel;
    if (tao_condition_initialize(&obj->cond, true) != TAO_OK) {
        goto error;
    }
    ++initlevel;

    // Instanciate object members.
    obj->nrefs = 1;
    tao_forced_store(&obj->size,  size);
    tao_forced_store(&obj->shmid, shmid);
    tao_forced_store(&obj->flags, flags);
    tao_forced_store(&obj->type,  type);
    return obj;

 error:
    if (initlevel >= 2) {
        pthread_cond_destroy(&obj->cond);
    }
    if (initlevel >= 1) {
        pthread_mutex_destroy(&obj->mutex);
    }
    tao_shared_memory_detach((void*)obj);
    return NULL;
}

tao_shared_object* tao_shared_object_attach(
    tao_shmid shmid)
{
    // Attach shared memory segment to the address space of the caller and
    // check that the size and the type are consistent.
    size_t size;
    tao_shared_object* obj =
        (tao_shared_object*)tao_shared_memory_attach(shmid, &size);
    if (obj == NULL) {
        return NULL;
    }
    if (size < sizeof(*obj) || size < obj->size) {
        tao_store_error(__func__, TAO_BAD_SIZE);
        goto detach;
    }
    if ((obj->type & TAO_SHARED_MASK) != TAO_SHARED_MAGIC) {
        tao_store_error(__func__, TAO_BAD_MAGIC);
        goto detach;
    }

    // Atomically increment the number of attachments and check whether object
    // is about to be destroyed.  Note that the `flags` member is constant and
    // can be safely read without locking the object.
    int64_t nrefs_before = atomic_fetch_add_explicit(
        &obj->nrefs, 1, memory_order_relaxed);
    bool destroyed = (nrefs_before < 1 && (obj->flags & TAO_PERSISTENT) == 0);
    if (destroyed) {
        tao_store_error(__func__, TAO_DESTROYED);
        goto detach;
    }
    return obj;

detach:
    tao_shared_memory_detach((void*)obj);
    return NULL;
}

tao_status tao_shared_object_detach(
    tao_shared_object* obj)
{
    // Minimal check.
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }

    // Atomically decrement the number of attachments and check whether object
    // must be destroyed.  Note that the `flags` member is constant and can be
    // safely read without locking the object.
    int64_t nrefs_before = atomic_fetch_sub_explicit(
        &obj->nrefs, 1, memory_order_relaxed);
    bool destroy = (nrefs_before == 1 && (obj->flags & TAO_PERSISTENT) == 0);

    // If object is no longer referenced, destroy the mutex and the condition
    // variable of the shared object.  Compared to `tao_mutex_destroy`, there
    // should be no needs to wait for the mutex to be released (because an
    // unreferenced object should not be locked).
    tao_status status = TAO_OK;
    if (destroy) {
        int code = pthread_mutex_destroy(&obj->mutex);
        if (code != 0) {
            tao_store_error("pthread_mutex_destroy", code);
            status = TAO_ERROR;
        }
        code = pthread_cond_destroy(&obj->cond);;
        if (code != 0) {
            tao_store_error("pthread_cond_destroy", code);
            status = TAO_ERROR;
        }
    }

    // Detach shared memory.
#ifdef __APPLE__
    tao_shmid shmid = obj->shmid;
#endif
    if (shared_memory_detach(obj) != TAO_OK) {
        status = TAO_ERROR;
    }
#ifdef __APPLE__
    if (destroy) {
        // Eventually destroy the shared memory segment.  This must be done after
        // last detach on Mac-OS.
        if (shared_memory_destroy(shmid) != TAO_OK) {
            status = TAO_ERROR;
        }
    }
#endif
    return status;
}

#define TYPE shared_object
#define IS_SHARED_OBJECT 1
#include "./shared-methods.c"
