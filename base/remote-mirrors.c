// remote-mirrors.c -
//
// Management of remote deformable mirrors.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2022, Éric Thiébaut.

#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdatomic.h>

#include "tao-basics.h"
#include "tao-config.h"
#include "tao-errors.h"
#include "tao-generic.h"
#include "tao-remote-mirrors-private.h"

// Offset of the layout indices in a remote mirror.
#define LAYOUT_OFFSET_IN_REMOTE_MIRROR \
    TAO_ROUND_UP(sizeof(tao_remote_mirror), sizeof(long))

// Offset of the reference values in a data-frame.
#define DATA_OFFSET_IN_DATAFRAME \
    TAO_ROUND_UP(sizeof(tao_dataframe_header), sizeof(double))

// Yields number of bytes rounded-up to `TAO_ALIGNMENT`.
static inline size_t aligned(size_t size)
{
    return TAO_ROUND_UP(size, TAO_ALIGNMENT);
}

//-----------------------------------------------------------------------------
// REMOTE DEFORMABLE MIRROR

// Serial number must be > 0
static inline void* remote_object_get_buffer(
    const tao_remote_object* obj,
    tao_serial serial)
{
    size_t offset = obj->offset + ((serial - 1)%obj->nbufs)*obj->stride;
    return TAO_COMPUTED_ADDRESS(obj, offset);
}

static inline long* remote_mirror_get_inds(
    const tao_remote_mirror* obj)
{
    return TAO_COMPUTED_ADDRESS(obj, LAYOUT_OFFSET_IN_REMOTE_MIRROR);
}

static inline double* remote_mirror_get_refs(
    const tao_remote_mirror* obj)
{
    return TAO_COMPUTED_ADDRESS(obj, obj->refs_offset);
}

static inline double* remote_mirror_get_req_cmds(
    const tao_remote_mirror* obj)
{
    return TAO_COMPUTED_ADDRESS(
        obj, obj->refs_offset + obj->nacts*sizeof(double));
}

static inline double* remote_mirror_get_act_cmds(
    const tao_remote_mirror* obj)
{
    return TAO_COMPUTED_ADDRESS(
        obj, obj->refs_offset + 2*obj->nacts*sizeof(double));
}

// Check whether the server owning the remote object is alive.
//
// NOTE: Since server state is an *atomic* variable, the caller may not have
// locked the object.
static inline bool is_alive(
    const tao_remote_mirror* obj)
{
    return (obj->base.state != TAO_STATE_KILLED);
}

static inline tao_dataframe_header* fetch_frame(
    const tao_remote_mirror* obj,
    tao_serial               serial,
    double**                 refs,
    double**                 cmds)
{
    tao_dataframe_header* header = remote_object_get_buffer(
        tao_remote_object_cast(obj), serial);
    *refs = TAO_COMPUTED_ADDRESS(header, DATA_OFFSET_IN_DATAFRAME);
    *cmds = (*refs) + obj->nacts;
    return header;
}

tao_remote_mirror* tao_remote_mirror_create(
    const char* owner,
    long        nbufs,
    const long* inds,
    long        dim1,
    long        dim2,
    unsigned    flags)
{
    // Check arguments.
    if (inds == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return NULL;
    }
    if (nbufs < 2) {
        tao_store_error(__func__, TAO_BAD_BUFFERS);
        return NULL;
    }
    if (dim1 < 1 || dim2 < 1) {
        tao_store_error(__func__, TAO_BAD_SIZE);
        return NULL;
    }
    long ninds = dim1*dim2;
    long nacts = 0;
    for (long i = 0; i < ninds; ++i) {
        if (inds[i] >= 0) {
            ++nacts;
        }
    }
    if (nacts < 1) {
        tao_store_error(__func__, TAO_BAD_ARGUMENT);
        return NULL;
    }
    for (long i = 0; i < ninds; ++i) {
        if (inds[i] >= nacts) {
            tao_store_error(__func__, TAO_OUT_OF_RANGE);
            return NULL;
        }
    }

    // Compute sizes and offsets of members.
    size_t inds_size = ninds*sizeof(long);
    size_t cmds_size = nacts*sizeof(double);
    size_t inds_offset = LAYOUT_OFFSET_IN_REMOTE_MIRROR;
    size_t refs_offset = TAO_ROUND_UP(inds_offset + inds_size, sizeof(double));

    // Compute sizes and offsets of buffers.  The stride is the (rounded) size
    // of a data-frame buffer (header + refs + cmds).
    size_t offset = aligned(refs_offset + 3*cmds_size);
    size_t stride = aligned(DATA_OFFSET_IN_DATAFRAME + 2*cmds_size);

    // Total size of instance.
    size_t size = offset + nbufs*stride;

    // Allocate remote shared object.
    tao_remote_mirror* obj = (tao_remote_mirror*)tao_remote_object_create(
        owner, TAO_REMOTE_MIRROR, nbufs, offset, stride, size, flags);
    if (obj == NULL) {
        return NULL;
    }

    // Instanciate object.
    tao_forced_store(&obj->nacts,       nacts);
    tao_forced_store(&obj->dims[0],     dim1);
    tao_forced_store(&obj->dims[1],     dim2);
    tao_forced_store(&obj->refs_offset, refs_offset);
    long* dest_inds = remote_mirror_get_inds(obj);
    for (long i = 0; i < ninds; ++i) {
        if (inds[i] >= 0) {
            dest_inds[i] = inds[i];
        } else {
            dest_inds[i] = -1;
        }
    }
    return obj;
}

#define TYPE remote_mirror
#define MAGIC TAO_REMOTE_MIRROR
#define IS_REMOTE_OBJECT 1
#include "./shared-methods.c"

tao_serial tao_remote_mirror_get_mark(
    const tao_remote_mirror* obj)
{
    return (obj == NULL) ? 0 : obj->mark;
}

// Constant, no needs to lock object.
long tao_remote_mirror_get_nacts(
    const tao_remote_mirror* obj)
{
    return (obj == NULL) ? 0 : obj->nacts;
}

// Constant, no needs to lock object.
const long* tao_remote_mirror_get_dims(
    const tao_remote_mirror* obj)
{
    return (obj == NULL) ? NULL : obj->dims;
}

// Constant, no needs to lock object.
const long* tao_remote_mirror_get_layout(
    const tao_remote_mirror* obj,
    long dims[2])
{
    if (obj == NULL) {
        if (dims != NULL) {
            dims[0] = 0;
            dims[1] = 0;
        }
        return NULL;
    } else {
        if (dims != NULL) {
            dims[0] = obj->dims[0];
            dims[1] = obj->dims[1];
        }
        return remote_mirror_get_inds(obj);
    }
}

double *tao_remote_mirror_get_reference(
    const tao_remote_mirror* obj)
{
    return (obj == NULL) ? NULL : remote_mirror_get_refs(obj);
}

double *tao_remote_mirror_get_requested_commands(
    const tao_remote_mirror* obj)
{
    return (obj == NULL) ? NULL : remote_mirror_get_req_cmds(obj);
}

double *tao_remote_mirror_get_actual_commands(
    const tao_remote_mirror* obj)
{
    return (obj == NULL) ? NULL : remote_mirror_get_act_cmds(obj);
}

tao_status tao_remote_mirror_kill(
    tao_remote_mirror* obj,
    double secs)
{
    return tao_remote_object_send_simple_command(
        tao_remote_object_cast(obj), TAO_COMMAND_KILL, secs);
}

tao_status tao_remote_mirror_reset(
    tao_remote_mirror* obj,
    double secs)
{
    return tao_remote_object_send_simple_command(
        tao_remote_object_cast(obj), TAO_COMMAND_RESET, secs);
}

tao_status tao_remote_mirror_set_reference(
    tao_remote_mirror* obj,
    const double* vals,
    long nvals,
    double secs)
{
    // Check arguments.
    if (obj == NULL || vals == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    if (nvals != obj->nacts) {
        tao_store_error(__func__, TAO_BAD_SIZE);
        return TAO_ERROR;
    }

    // Prepare for i/o.
    double* dest = remote_mirror_get_refs(obj);

    // Wait until server be ready for a new command.
    tao_status status = tao_remote_object_lock_for_command(
        tao_remote_object_cast(obj), TAO_COMMAND_NONE, secs);
    if (status == TAO_OK) {
        // Copy reference commands and unlock ressources.
        memcpy(dest, vals, nvals*sizeof(double));
        if (tao_remote_mirror_unlock(obj) != TAO_OK) {
            status = TAO_ERROR;
        }
    }
    return status;
}

tao_status tao_remote_mirror_send_commands(
    tao_remote_mirror* obj,
    const double* vals,
    long nvals,
    tao_serial mark,
    double secs,
    tao_serial* serial)
{
    // Check arguments.
    if (obj == NULL || vals == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    if (nvals != obj->nacts) {
        tao_store_error(__func__, TAO_BAD_SIZE);
        return TAO_ERROR;
    }

    // Prepare for i/o.
    double* dest = remote_mirror_get_req_cmds(obj);

    // Wait until server ready for a new command.
     tao_status status = tao_remote_object_lock_for_command(
        tao_remote_object_cast(obj), TAO_COMMAND_SEND, secs);
    if (status == TAO_OK) {
        // Server was ready for the new command.  Get next serial number, copy
        // actuators values, and unlock object.
        tao_serial next = obj->base.serial + 1;
        if (next < 1) {
            next = 0;
        }
        if (serial != NULL) {
            *serial = next;
        }
        memcpy(dest, vals, nvals*sizeof(double));
        obj->mark = mark;
        if (tao_remote_mirror_unlock(obj) != TAO_OK) {
            status = TAO_ERROR;
        }
    }
    return status;
}

tao_serial tao_remote_mirror_wait_data(
    tao_remote_mirror* obj,
    tao_serial         serial,
    double             secs)
{
    return tao_remote_object_wait_serial(
        tao_remote_object_cast(obj), serial, secs);
}

// FIXME: check running?
tao_status tao_remote_mirror_fetch_data(
    const tao_remote_mirror* obj,
    tao_serial               serial,
    double*                  refs,
    double*                  cmds,
    long                     nvals,
    tao_dataframe_info*      info)
{
    // Check arguments, then copy and check that data did not get overwritten
    // in the mean time.
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    if (obj->nacts < 1 || obj->base.nbufs < 2) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    if (nvals != ((refs != NULL || cmds != NULL) ? obj->nacts : 0)) {
        tao_store_error(__func__, TAO_BAD_SIZE);
        return TAO_ERROR;
    }
    size_t nbytes = obj->nacts*sizeof(double);
    if (serial < 1) {
        if (serial != 0) {
            tao_store_error(__func__, TAO_BAD_SERIAL);
            return TAO_ERROR;
        }
        if (info != NULL) {
            memset(info, 0, sizeof(*info));
        }
        if (refs != NULL) {
            memset(refs, 0, nbytes);
        }
        if (cmds != NULL) {
            memset(cmds, 0, nbytes);
        }
    } else {
        // FIXME: check whether serial is too high, server is running, etc.
        double* src_refs;
        double* src_cmds;
        tao_dataframe_header* header = fetch_frame(
            obj, serial, &src_refs, &src_cmds);
        if (atomic_load(&header->serial) != serial) {
            if (info != NULL) {
                info->serial = 0;
                info->mark   = 0;
                info->time   = TAO_TIME(0, 0);
            }
            return TAO_TIMEOUT;
        }
        if (info != NULL) {
            info->serial = serial;
            info->mark   = header->mark;
            info->time   = header->time;
        }
        if (refs != NULL) {
            memcpy(refs, src_refs, nbytes);
        }
        if (cmds != NULL) {
            memcpy(cmds, src_cmds, nbytes);
        }
        if (atomic_load(&header->serial) != serial) {
            return TAO_TIMEOUT;
        }
    }
    return TAO_OK;
}

/**
 * Publish deformable mirror data-frame.
 *
 * This function shall be called by a deformable mirror server to store the
 * contents of a deformable mirror data-frame in shared memory.  The serial
 * number of the published data-frame is incremented and other processes get
 * notified that the shared data have changed.
 *
 * The remote deformable mirror must be locked by the caller
 *
 * @param obj      Pointer to remote deformable mirror.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure.
 */
static tao_status publish_dataframe(
    tao_remote_mirror* obj)
{
    // Check arguments.
    if (obj == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    long nacts = obj->nacts;
    if (nacts < 1 || obj->base.nbufs < 2) {
        tao_store_error(__func__, TAO_CORRUPTED);
        return TAO_ERROR;
    }
    if (obj->base.serial < 0) {
        tao_store_error(__func__, TAO_BAD_SERIAL);
        return TAO_ERROR;
    }

    // Increment counter of published frames.
    tao_serial serial = ++obj->base.serial;

    // Obtain publication time.
    tao_time time;
    tao_status status = tao_get_monotonic_time(&time);
    if (status != TAO_OK) {
        return TAO_ERROR;
    }

    // Write data-frame header and data.  First set the data-frame serial
    // number to zero to let others know that data-frame is being overwritten.
    // Finally set the data-frame serial number to its value when all contents
    // has been updated.
    const double* src_refs = remote_mirror_get_refs(obj);
    const double* src_cmds = remote_mirror_get_act_cmds(obj);
    double* dst_refs;
    double* dst_cmds;
    tao_dataframe_header* header = fetch_frame(
        obj, serial, &dst_refs, &dst_cmds);
    size_t nbytes = nacts*sizeof(double);
    atomic_store(&header->serial, 0); // 0 indicates invalid data-frame
    header->mark = obj->mark;
    header->time = time;
    memcpy(dst_refs, src_refs, nbytes);
    memcpy(dst_cmds, src_cmds, nbytes);
    atomic_store(&header->serial, serial); // ≥ 1 indicates valid data-frame

    return status;
}

tao_status tao_remote_mirror_run_loop(
    tao_remote_mirror* obj,
    tao_remote_mirror_operations* ops,
    void* ctx)
{
    // Check arguments.
    if (obj == NULL || ops == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    if (ops->name == NULL) {
        tao_store_error(__func__, TAO_BAD_NAME);
        return TAO_ERROR;
    }
    if (obj->nacts < 1) {
        tao_store_error(__func__, TAO_BAD_SIZE);
        return TAO_ERROR;
    }

    // Lock remote mirror before anyone else has the opportunity to lock it.
    if (tao_remote_mirror_lock(obj) != TAO_OK) {
        if (ops->debug) {
            fprintf(stderr, "%s: failed to lock remote mirror instance\n",
                    ops->name);
        }
        return TAO_ERROR;
    }

    // Set state to indicate that initialization is completed.
    obj->base.state = TAO_STATE_WAITING;

    // Publish the shmid's of the shared resources.
    if (ops->debug) {
        fprintf(stderr, "%s: remote mirror available at shmid=%d\n",
                ops->name, (int)tao_remote_mirror_get_shmid(obj));
    }
    tao_status status = tao_config_write_long(
        ops->name, tao_remote_mirror_get_shmid(obj));

    // Run loop (on entry of the loop we own the lock on the remote mirror
    // instance).
    bool publish = false;
    while (true) {
        // Wait for next command.
        while (status == TAO_OK && is_alive(obj)
               && obj->base.command == TAO_COMMAND_NONE) {
            status = tao_remote_mirror_wait_condition(obj);
        }
        if (status != TAO_OK) {
            break;
        }

        // Execute command.
        if (obj->base.command == TAO_COMMAND_RESET) {
            if (ops->debug) {
                fprintf(stderr, "%s: execute \"reset\" command\n", ops->name);
            }
            status = ops->on_reset(obj, ctx);
            if (status != TAO_OK) {
                if (ops->debug) {
                    fprintf(stderr, "%s: failed to reset deformable mirror\n",
                            ops->name);
                }
                break;
            }
            publish = true;
        } else if (obj->base.command == TAO_COMMAND_SEND) {
            if (ops->debug) {
                fprintf(stderr, "%s: execute \"send\" command\n", ops->name);
            }
            status = ops->on_send(obj, ctx);
            if (status != TAO_OK) {
                if (ops->debug) {
                    fprintf(stderr, "%s: failed to send actuators command\n",
                            ops->name);
                }
                break;
            }
            publish = true;
        } else if (obj->base.command == TAO_COMMAND_KILL) {
            if (ops->debug) {
                fprintf(stderr, "%s: execute \"quit\" command\n", ops->name);
            }
            break;
        } else {
            if (ops->debug) {
                fprintf(stderr, "%s: unknown command received (%d)\n",
                        ops->name, (int)obj->base.command);
            }
        }

        // Publish next telemetry frame if requested.
        if (publish) {
            status = publish_dataframe(obj);
            if (status != TAO_OK) {
                if (ops->debug) {
                    fprintf(stderr, "%s: failed to publish mirror telemetry\n",
                            ops->name);
                }
                break;
            }
            publish = false;
        }

        // Change command to idle and notify others that the server is ready for a
        // new command.
        obj->base.command = TAO_COMMAND_NONE;
        status = tao_remote_mirror_broadcast_condition(obj);
        if (status != TAO_OK) {
            if (ops->debug) {
                fprintf(stderr, "%s: failed to broadcast condition\n",
                        ops->name);
            }
            break;
        }
    }

    // The server is no longer running and is unreachable.
    obj->base.command = TAO_COMMAND_NONE;
    obj->base.state = TAO_STATE_KILLED;
    if (tao_config_write_long(ops->name, TAO_BAD_SHMID) != TAO_OK) {
         status = TAO_ERROR;
    }
    if (tao_remote_mirror_broadcast_condition(obj) != TAO_OK) {
        status = TAO_ERROR;
    }
    if (tao_remote_mirror_unlock(obj) != TAO_OK) {
        status = TAO_ERROR;
    }
    return status;
}

//-----------------------------------------------------------------------------
// UTILITIES

// Macros to access 2-D arrays in column-major order.
#define  MSK(i1,i2)   msk[(i1) + dim1*(i2)]
#define INDS(j1,j2)  inds[(j1) + dim1*(j2)]

long tao_indexed_layout_build(
    long* inds,
    const uint8_t* msk,
    long dim1,
    long dim2,
    unsigned int orient)
{
    long k = 0;
    if (msk == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return -1L;
    }
    if (dim1 < 1 || dim2 < 1) {
        tao_store_error(__func__, TAO_BAD_SIZE);
        return -1L;
    }
    if (inds == NULL) {
        // Just count the number of active sites.
        long ntot = dim1*dim2;
        for (long i = 0; i < ntot; ++i) {
            if (msk[i] != 0) {
                ++k;
            }
        }
    } else {
        // What is the direction of the numbering?
        bool reverse_1 = (orient & 1) != 0;
        bool reverse_2 = (orient & 2) != 0;
        bool swap_axes = (orient & 4) != 0;

        // Fill array of indices according to direction of numbering.
        if (swap_axes) {
            // Numbering is in row-major order.
            for (long i1 = 0; i1 < dim1; ++i1) {
                long j1 = (reverse_1 ? (dim1 - 1) - i1 : i1);
                for (long i2 = 0; i2 < dim2; ++i2) {
                    long j2 = (reverse_2 ? (dim2 - 1) - i2 : i2);
                    if (MSK(i1,i2) != 0) {
                        INDS(j1,j2) = k;
                        ++k;
                    } else {
                        INDS(j1,j2) = -1;
                    }
                }
            }
        } else {
            // Numbering is in column-major order.
            for (long i2 = 0; i2 < dim2; ++i2) {
                long j2 = (reverse_2 ? (dim2 - 1) - i2 : i2);
                for (long i1 = 0; i1 < dim1; ++i1) {
                    long j1 = (reverse_1 ? (dim1 - 1) - i1 : i1);
                    if (MSK(i1,i2) != 0) {
                        INDS(j1,j2) = k;
                        ++k;
                    } else {
                        INDS(j1,j2) = -1;
                    }
                }
            }
        }
    }
    return k;
}

#undef  MSK
#undef INDS

uint8_t* tao_mirror_mask_create(
    long dim1,
    long dim2,
    long nacts)
{
    if (dim1 < 1 || dim2 < 1 || nacts < 1) {
        tao_store_error(__func__, TAO_BAD_SIZE);
        return NULL;
    }
    long len = dim1*dim2;
    uint8_t* mask = tao_malloc(len*sizeof(uint8_t));
    if (mask == NULL) {
        return NULL;
    }
    return tao_mirror_mask_instanciate(mask, dim1, dim2, nacts, NULL);
}

uint8_t* tao_mirror_mask_instanciate(
    uint8_t* mask,
    long     dim1,
    long     dim2,
    long     nacts,
    long*    work)
{
    if (dim1 < 1 || dim2 < 1 || nacts < 1) {
        tao_store_error(__func__, TAO_BAD_SIZE);
        return NULL;
    }
    if (mask == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return NULL;
    }
    long len = dim1*dim2;
    if (nacts > len) {
        for (long i = 0; i < len; ++i) {
            mask[i] = true;
        }
        return mask;
    }
    bool own_work = (work == NULL);
    if (own_work) {
        // Allocate workspace.
        work = tao_malloc(len*sizeof(long));
        if (work == NULL) {
            return NULL;
        }
    }
    // Instanciate workspace with f(i1,i2) and compute its extreme values.
    long q1 = dim1 - 1;
    long q2 = dim2 - 1;
    for (long i2 = 0; i2 < dim2; ++i2) {
        long f2 = (q2 - i2)*i2;
        for (long i1 = 0; i1 < dim1; ++i1) {
            long f1 = (q1 - i1)*i1;
            work[i1 + dim1*i2] = f1 + f2;
        }
    }
    long fmin = work[0], fmax = work[0];
    for (long i = 1; i < len; ++i) {
        long f = work[i];
        fmin = tao_min(fmin, f);
        fmax = tao_max(fmax, f);
    }

    // Build the tightest bracket t1 ≥ t2 such that n1 ≤ nacts ≤ n2, with
    // n1 = count(msk ≥ t1) and similarly for n2 and t2.
    long t1 = fmin, n1 = len;
    long t2 = fmin, n2 = len;
    while (n1 > nacts) {
        t2 = t1;
        n2 = n1;
        t1 = fmax;
        n1 = 0;
        for (long i = 0; i < len; ++i) {
            long f = work[i];
            if ((t2 < f) & (f < t1)) {
                // Strictly between the disks defined by t1 and t2.
                t1 = f;
            }
            n1 += (f >= t1);
        }
    }
    long t = (nacts - n1 < n2 - nacts ? t1 : t2);
    long n = (t == t1 ? n1 : n2);
    if (n != nacts) {
        fprintf(stderr, "WARNING: Only found an approximation with %ld "
                "actuators instead of %ld.\n", n, nacts);
    }
    for (long i = 0; i < len; ++i) {
        mask[i] = (work[i] >= t);
    }
    if (own_work) {
        tao_free(work);
    }
    return mask;
}

uint8_t* tao_mirror_mask_create_from_text(
    char const* shape[],
    long        nrows,
    long        dims[2])
{
    // Check arguments.
    if (shape == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return NULL;
    }
    if (nrows <= 0) {
        // Count number of rows.
        nrows = 0;
        while (shape[nrows] != NULL) {
            ++nrows;
        }
    }

    // Count number of columns.
    long ncols = 0;
    for (long i = 0; i < nrows; ++i) {
        long n = (shape[i] == NULL) ? 0 : strlen(shape[i]);
        if (i == 0) {
            ncols = n;
        } else if (n != ncols) {
            tao_store_error(__func__, TAO_BAD_SIZE);
            return NULL;
        }
    }
    if (ncols < 1 || nrows < 1) {
        tao_store_error(__func__, TAO_BAD_SIZE);
        return NULL;
    }

    // Convert the shape into a mask (the shape is in row-major storage order,
    // the mask is in column-major storage order).
    uint8_t* msk = tao_malloc(ncols*nrows*sizeof(uint8_t));
    if (msk == NULL) {
        return NULL;
    }
    for (long i = 0; i < nrows; ++i) {
        const char* row = shape[i];
        for (long j = 0; j < ncols; ++j) {
            msk[i*ncols + j] = (row[j] == ' ' ? 0 : 1);
        }
    }

    // Return results.
    if (dims != NULL) {
        dims[0] = ncols;
        dims[1] = nrows;
    }
    return msk;
}
