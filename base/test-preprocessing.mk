#
# test-preprocessing.mk -
#
# Makefile for testing performances of image pre-processing.
#
# Usage (for a single test or for all tests):
#    make -f test-preprocessing.mk [run-test] [TARGET=...]
#    make -f test-preprocessing.mk run-tests
#
#---------------------------------------------------------------------------
#
# This file if part of TAO real-time software licensed under the MIT license
# (https://git-cral.univ-lyon1.fr/tao/tao-rt).
#
# Copyright (C) 2020-2021, Éric Thiébaut.
#
srcdir = .

# Replace `tee` by `cat` if `tee` is not available.
TEE = tee

THIS_FILE := $(lastword $(MAKEFILE_LIST))

TARGET = gcc-native
TARGETS = \
    clang-O3-fast-math-native \
    clang-O3-fast-math-sse2 \
    clang-O3-fast-math-sse4 \
    clang-O3-fast-math-avx2 \
    gcc-O3-fast-math-native \
    gcc-O3-fast-math-sse2 \
    gcc-O3-fast-math-sse4 \
    gcc-O3-fast-math-avx2 \
    gcc-O3-fast-math-unroll-native \
    gcc-O3-fast-math-unroll-sse2 \
    gcc-O3-fast-math-unroll-sse4 \
    gcc-O3-fast-math-unroll-avx2

GUESS = $(srcdir)/../scripts/tao-guess
GUESS_CC = $(shell "$(GUESS)" --cc $(TARGET))
GUESS_CFLAGS = $(shell "$(GUESS)" --cflags $(TARGET))
GUESS_CC_VERSION = $(shell "$(GUESS)" --version $(TARGET))
GUESS_CPU_MODEL = $(shell "$(GUESS)" --cpu)

default: run-test

test-preprocessing-$(TARGET): $(srcdir)/test-preprocessing.c $(srcdir)/test-preprocessing.h $(srcdir)/tao-preprocessing.h
	$(GUESS_CC) $(CPPFLAGS) $(GUESS_CFLAGS) \
	    -DCOMPILER_COMMAND="\"$(GUESS_CC) $(GUESS_CFLAGS)\"" \
	    -DCOMPILER_VERSION="\"$(GUESS_CC_VERSION)\"" \
	    "$<" -o "$@" -lm

test-preprocessing-$(TARGET).out: test-preprocessing-$(TARGET)
	./test-preprocessing-$(TARGET) "--cpu=$(GUESS_CPU_MODEL)" \
	    | $(TEE) test-preprocessing-$(TARGET).out

run-test: test-preprocessing-$(TARGET).out

run-tests:
	for target in $(TARGETS); do \
	    make -f $(THIS_FILE) TARGET=$$target run-test; \
	done
