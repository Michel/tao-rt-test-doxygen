// logmsg.c --
//
// Logging messages.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2018-2021, Éric Thiébaut.

#include "tao-utils.h"
#include "tao-locks.h"

#include "config.h"

#include <stdarg.h>

// Mutex to protect message handler settings.
static tao_mutex message_mutex = TAO_MUTEX_INITIALIZER;

// Current level of verbosity (protected by above mutex).
static tao_message_type message_level = TAO_MESG_DEBUG;

// Current message output file (protected by above mutex).
static FILE* message_output = NULL;

static const char* message_prefix(
    tao_message_type type)
{
    switch (type) {
    case TAO_MESG_DEBUG:
        return "(TAO-DEBUG) ";
    case TAO_MESG_INFO:
        return "(TAO-INFO) ";
    case TAO_MESG_WARN:
        return "(TAO-WARN) ";
    case TAO_MESG_ERROR:
        return "(TAO-ERROR) ";
    case TAO_MESG_ASSERT:
        return "(TAO-ASSERT) ";
    default:
        return "(TAO-MESG} ";
    }
}

void tao_set_message_level(
    tao_message_type level)
{
    if (level < TAO_MESG_DEBUG) {
        level = TAO_MESG_DEBUG;
    }
    if (level > TAO_MESG_QUIET) {
        level = TAO_MESG_QUIET;
    }
    if (pthread_mutex_lock(&message_mutex) == 0) {
        message_level = level;
        (void)pthread_mutex_unlock(&message_mutex);
    }
}

tao_message_type tao_get_message_level(
    void)
{
    tao_message_type level = TAO_MESG_DEBUG;
    if (pthread_mutex_lock(&message_mutex) == 0) {
        level = message_level;
        (void)pthread_mutex_unlock(&message_mutex);
    }
    return level;
}

void tao_inform(
    tao_message_type type,
    const char* format,
    ...)
{
    if (pthread_mutex_lock(&message_mutex) == 0) {
        if (type >= message_level) {
            va_list ap;
            FILE* output = (message_output == NULL ? stderr : message_output);
            (void)fputs(message_prefix(type), output);
            va_start(ap, format);
            (void)vfprintf(output, format, ap);
            va_end(ap);
        }
        (void)pthread_mutex_unlock(&message_mutex);
    }
}
