// test-search.c -
//
// Benchmark search methods in a table of names.  This is to demonstrate that a
// hash table is not needed when the number of entries is not huge (a few
// tens).  For instance, with 30 entries, it takes about 24ns on average to
// find an entry with a simple linear search as implemented by
// `search_method_4` (CPU: Intel Core i7-5500U at 2.40GHz).
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2021, Éric Thiébaut.

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef int command_callback(
    void* data,
    char* argv[],
    int argc);

static int ok(
    void* data,
    char* argv[],
    int argc)
{
    fprintf(stderr, "ok\n");
    return 0;
}

static struct command_entry {
    const char*           name;
    command_callback* callback;
    void*                 data;
} commands[] = {
    {"accesspoint", ok, NULL},
    {"bufferencoding", ok, NULL},
    {"buffers", ok, NULL},
    {"debug", ok, NULL},
    {"drop", ok, NULL},
    {"exposuretime", ok, NULL},
    {"framerate", ok, NULL},
    {"frames", ok, NULL},// Number of frames received so far.
    {"height", ok, NULL},
    {"lostframes", ok, NULL}, // Number of lost frames.
    {"lostsyncs", ok, NULL}, // Number of synchronization losts so far.
    {"origin", ok, NULL}, // Origin of time.
    {"overflows", ok, NULL}, // Number of overflows.
    {"overruns", ok, NULL},// Number of frames lost so far because of overruns.
    {"ping", ok, NULL},
    {"pixeltype", ok, NULL},
    {"roi", ok, NULL},
    {"sensorencoding", ok, NULL},
    {"sensorheight", ok, NULL},
    {"sensorwidth", ok, NULL},
    {"shmid", ok, NULL},
    {"state", ok, NULL},
    {"suspended", ok, NULL},
    {"temperature", ok, NULL},
    {"timeouts", ok, NULL}, // Number of timeouts so far.
    {"width", ok, NULL},
    {"xbin", ok, NULL},
    {"xoff", ok, NULL},
    {"ybin", ok, NULL},
    {"yoff", ok, NULL},
    {NULL, NULL, NULL},
};

struct timespec t0;

static void tic(
    void)
{
    clock_gettime(CLOCK_MONOTONIC, &t0);
}

static double toc(
    void)
{
    struct timespec t1;
    clock_gettime(CLOCK_MONOTONIC, &t1);
    return (t1.tv_sec - t0.tv_sec) + (t1.tv_nsec - t0.tv_nsec)*1E-9;
}

// First simplest version: just compare strings.
//
// Benchmark: 77ns on average, 75ns at best.
static int search_method_1(
    const struct command_entry* list,
    const char* name)
{
    if (name != NULL) {
        for (int i = 0; commands[i].name != NULL; ++i) {
            if (strcmp(commands[i].name, name) == 0) {
                return i;
            }
        }
    }
    return -1;
}

// Second simplest version: just compare strings, using a local variable for
// the command name.
//
// Benchmark: 77ns on average, 75ns at best.
static int search_method_2(
    const struct command_entry* list,
    const char* name)
{
    if (name != NULL) {
        const char* str;
        for (int i = 0; (str = commands[i].name) != NULL; ++i) {
            if (strcmp(str, name) == 0) {
                return i;
            }
        }
    }
    return -1;
}

// First improved version: compare first characters and then strings.  This
// version is twice as fast as the previous ones.
//
// Benchmark: 30ns on average, 29ns at best.
static int search_method_3(
    const struct command_entry* list,
    const char* name)
{
    char c;
    if (name != NULL && (c = name[0]) != '\0') {
        const char* str;
        for (int i = 0; (str = commands[i].name) != NULL; ++i) {
            if (str[0] == c && strcmp(str, name) == 0) {
                return i;
            }
        }
    }
    return -1;
}

// Second improved version: compare first characters and then strings after
// first character.  This version is a bit faster than the previous one.
//
// Benchmark: 24ns on average, 23ns at best.
static int search_method_4(
    const struct command_entry* list,
    const char* name)
{
    char c;
    if (name != NULL && (c = name[0]) != '\0') {
        const char* str;
        ++name;
        for (int i = 0; (str = commands[i].name) != NULL; ++i) {
            if (str[0] == c && strcmp(str + 1, name) == 0) {
                return i;
            }
        }
    }
    return -1;
}

static void benchmark(
    int (*search)(
        const struct command_entry*,
        const char*),
    const struct command_entry* command_list,
    int repeat)
{
    // count commands
    int n;
    for (n = 0; command_list[n].name != NULL; ++n) {
        ;
    }

    // evaluate search
    char name[200];
    double t, tsum, tmin, tmax, tavg;
    tsum = tmin = tmax = 0;
    for (int k = 0; k < repeat; ++k) {
        tic();
        for (int j = 0; j < n; ++j) {
            strcpy(name, command_list[j].name); // to force having a different
                                                // address
            if (search(command_list, name) < 0) {
                fprintf(stderr, "bogus search\n");
                exit(1);
            }
        }
        t  = toc()/n; // mean time per entry
        if (k == 0) {
            tmin = tmax = t;
        } else {
            if (t < tmin) tmin = t;
            if (t > tmax) tmax = t;
        }
        tsum += t;
    }
    tavg = tsum/repeat;
    printf("  average time spent: %.3fns\n", tavg*1e9);
    printf("  minimum time spent: %.3fns\n", tmin*1e9);
    printf("  maximum time spent: %.3fns\n", tmax*1e9);
}

int main(
    int argc,
    char* argv[])
{
    const int repeat = 100000;

    printf("\nFirst simple search method:\n");
    benchmark(search_method_1, commands, repeat);
    printf("\nSecond simple search method:\n");
    benchmark(search_method_2, commands, repeat);
    printf("\nFirst improved search method:\n");
    benchmark(search_method_3, commands, repeat);
    printf("\nSecond improved search method:\n");
    benchmark(search_method_4, commands, repeat);
    return 0;
}
