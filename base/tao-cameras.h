// tao-cameras.h -
//
// Definitions and API for cameras in TAO.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2022, Éric Thiébaut.

#ifndef TAO_CAMERAS_H_
#define TAO_CAMERAS_H_ 1

#include <tao-basics.h>
#include <tao-utils.h>
#include <tao-options.h>
#include <tao-encodings.h>
#include <tao-shared-arrays.h>
#include <tao-remote-objects.h>

TAO_BEGIN_DECLS

// Opaque structure for a shared camera.
typedef struct tao_shared_camera_ tao_shared_camera;

//-----------------------------------------------------------------------------
// COMMON TYPES AND METHODS

/**
 * @addtogroup CamerasTools Camera tools
 *
 * @ingroup Cameras
 *
 * @brief Common types and methods for cameras.
 *
 * @{
 */

/**
 * @brief Level of image pre-processing.
 */
typedef enum tao_preprocessing_ {
    TAO_PREPROCESSING_NONE   = 0,///< Just convert pixel values.
    TAO_PREPROCESSING_AFFINE = 1,///< Apply affine correction.
    TAO_PREPROCESSING_FULL   = 2 ///< Apply affine correction and compute
                                 ///  weights.
} tao_preprocessing;

/**
 * Region of interest on a camera.
 */
typedef struct tao_camera_roi_ {
    long   xbin;///< Horizontal binning (in physical pixels).
    long   ybin;///< Vertical binning (in physical pixels).
    long   xoff;///< Horizontal offset of the acquired images with respect to
                ///  the left border of the detector (in physical pixels).
    long   yoff;///< Vertical offset of the acquired images with respect to the
                ///  bottom border of the detector (in physical pixels).
    long  width;///< Number of pixels per line of the acquired images (in
                ///  macro-pixels).
    long height;///< Number of lines of pixels in the acquired images (in
                ///  macro-pixels).
} tao_camera_roi;

/**
 * Copy the settings of a region of interest.
 *
 * This function copies the settings of the source @b src into the destination
 * @b dest and returns @b dest.
 *
 * @warning For efficiency, this function does not check its arguments.
 *
 * @param dest   Address of destination.
 * @param  src   Address of source.
 *
 * @return The destination @b dest.
 *
 * @see tao_camera_roi, tao_camera_roi_check(),
 * tao_camera_roi_define().
 */
extern tao_camera_roi* tao_camera_roi_copy(
    tao_camera_roi* dest,
    const tao_camera_roi* src);

/**
 * Define a region of interest.
 *
 * This function defines the members of @b roi and returns @b roi.
 *
 * @warning For efficiency, this function does not check its arguments.
 *
 * @param   dest   Address of region of interest.
 * @param   xbin   Horizontal binning (in physical pixels).
 * @param   ybin   Vertical binning (in physical pixels).
 * @param   xoff   Horizontal offset of ROI (in physical pixels).
 * @param   yoff   Vertical offset of ROI (in physical pixels).
 * @param  width   Horizontal size of ROI (in macro-pixels).
 * @param height   Vertical size of ROI (in macro-pixels).
 *
 * @return The region of interest @b roi.
 *
 * @see tao_camera_roi, tao_camera_roi_copy(),
 * tao_camera_roi_check().
 */
extern tao_camera_roi* tao_camera_roi_define(
    tao_camera_roi* dest,
    long xbin,
    long ybin,
    long xoff,
    long yoff,
    long width,
    long height);

/**
 * Check a region of interest.
 *
 * This function checks whether the region of interest settings in @b roi are
 * valid and compatible with the sensor dimensions given by @b sensorwidth and
 * @b sensorheight.
 *
 * @param          roi   Address of the region of interest to check.
 * @param  sensorwidth   Horizontal size of detector (in pixels).
 * @param sensorheight   Vertical size of detector (in pixels).
 *
 * @return @ref TAO_OK if the ROI is valid, @ref TAO_ERROR otherwise.
 *
 * @see tao_camera_roi, tao_camera_roi_copy(),
 * tao_camera_roi_define().
 */
extern tao_status tao_camera_roi_check(
    const tao_camera_roi* roi,
    long sensorwidth,
    long sensorheight);

/**
 * Pending events for a camera.
 *
 * Pending events are given as a combination of bits.  The following events
 * are implemented:
 *
 * - @b TAO_EVENT_START: Start acquisition.
 * - @b TAO_EVENT_FRAME: A new frame is available.
 * - @b TAO_EVENT_ERROR: Some (recoverable) error occurred.
 * - @b TAO_EVENT_STOP: Stop acquisition.
 * - @b TAO_EVENT_ABORT: Abort acquisition.
 * - @b TAO_EVENT_QUIT: Acquisition thread must quit.
 */
typedef unsigned int tao_event;

#define TAO_EVENT_COMMAND (((tao_event)1) << 0) // Command sent
#define TAO_EVENT_FRAME   (((tao_event)1) << 1) // New frame available
#define TAO_EVENT_ERROR   (((tao_event)1) << 2) // Some error occurred

/**
 * Common camera configuration.
 *
 * This structure contains the (usually) configurable parameters common to any
 * kind of camera.
 */
typedef struct tao_camera_config_ {
    tao_camera_roi              roi;///< Region of interest on the detector.
    double                framerate;///< Acquisition rate in frames per second.
    double             exposuretime;///< Exposure time in seconds.
    long                      nbufs;///< Number of acquisition buffers.
    tao_eltype            pixeltype;///< Pixel type in pre-processed images.
    tao_encoding     sensorencoding;///< Pixel encoding in images acquired by
                                    ///  the sensor.
    tao_encoding     bufferencoding;///< Pixel encoding for acquisition
                                    ///  buffers.
    tao_preprocessing preprocessing;///< Level of image pre-processing.
} tao_camera_config;

/**
 * Print camera configuration.
 *
 * @param out   The output file stream where to print.  The standard output
 *              stream is assumed if @a out is `NULL`.
 *
 * @param cfg   Address of camera configuration structure.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR in case of failure.
 *
 * @see tao_camera_info_print().
 */
extern tao_status tao_camera_config_print(
    FILE* out,
    const tao_camera_config* cfg);

/**
 * Common camera information.
 *
 * This structure contains the parameters common to any kind of camera.
 */
typedef struct tao_camera_info_ {
    long         sensorwidth;///< Number of physical pixels per row of the
                             ///  detector.
    long        sensorheight;///< Number of physical pixels per column of the
                             ///  detector.
    tao_camera_config config;///< Configurable parameters.
    tao_state          state;///< State of the camera.
    double       temperature;///< Camera temperature (in degrees Celsius).
    tao_time          origin;///< Origin of time.
    tao_serial        frames;///< Number of frames acquired so far.
    tao_serial      overruns;///< Number of frames lost so far because of
                             ///  overruns.
    tao_serial    lostframes;///< Number of lost frames.
    tao_serial     overflows;///< Number of overflows.
    tao_serial     lostsyncs;///< Number of synchronization losts so far.
    tao_serial      timeouts;///< Number of timeouts so far.
} tao_camera_info;

/**
 * Print camera configuration.
 *
 * This function prints the contents of a given camera information and calls
 * tao_camera_config_print() to print the configuration part.
 *
 * @param  out   The output file stream where to print.
 * @param info   Address of camera information structure.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR in case of failure.
 *
 * @see tao_camera_config_print().
 */
extern tao_status tao_camera_info_print(
    FILE* out,
    const tao_camera_info* info);

extern void tao_camera_info_initialize(
    tao_camera_info* info);

/**
 * @}
 */

//-----------------------------------------------------------------------------
// UNIFIED INTERFACE FOR CAMERA DEVICES

/**
 * @addtogroup UnifiedCameras  Unified cameras
 *
 * @ingroup Cameras
 *
 * @brief Unified API for cameras.
 *
 * TAO provides generic structures and functions to operate real cameras in a
 * unified and yet flexible way.
 *
 * The following functions implement the API:
 *
 * - tao_camera_create() creates a new camera instance.
 *
 * - tao_camera_destroy() destroys a camera instance.
 *
 * - tao_camera_lock() and tao_camera_try_lock() locks a camera instance for
 *   exclusive (read-write) access.
 *
 * - tao_camera_unlock() unlocks a camera instance.
 *
 * - tao_camera_signal() and tao_camera_boeadcast() notify other threads that
 *   something may have changed in the camera instance.
 *
 * - tao_camera_wait(), tao_camera_abstimed_wait(), and tao_camera_timed_wait()
 *   wait for some changes(s) to occur in the camera instance.
 *
 * - tao_camera_start_acquisition() starts image acquisition by a camera instance.
 *
 * - tao_camera_stop_acquisition() stops image acquisition by a camera instance.
 *
 * - tao_camera_wait_acquisition_buffer() waits for a new image to be available
 *   from a camera instance.
 *
 * - tao_camera_release_acquisition_buffer() releases an image acquired by a
 *   camera instance.
 *
 * - tao_camera_get_pending_acquisition_buffers() yields the current number of
 *   pending acquired images available from a camera instance.
 *
 * - tao_camera_reset() attempts to reset a camera to an idle state.
 *
 * - tao_camera_check_configuration(), tao_camera_get_configuration(),
 *   tao_camera_set_configuration(), and tao_camera_reflect_configuration()
 *   deal with the configuration of a camera instance.
 *
 * - tao_camera_set_origin_of_time() and tao_camera_get_origin_of_time()
 *   respectively sets and retrieves the origin of time for a camera instance.
 *
 * - tao_camera_get_elapsed_seconds(), tao_camera_get_elapsed_milliseconds(),
 *   tao_camera_get_elapsed_microseconds(), and
 *   tao_camera_get_elapsed_nanoseconds() yields the time elapsed since the
 *   origin of time for a camera instance.
 *
 * @warning A camera can be shared between several threads (of the same
 *          process).  A mutex and a condition variable are embedded into a
 *          camera instance to protect its resources and synchronize its users
 *          and locking/unlocking the camera appropriately is critical.  Most
 *          functions operating on a camera assume that the caller have locked
 *          the camera.  The only exceptions are: tao_camera_destroy() and, of
 *          course, tao_camera_lock() and tao_camera_try_lock().  It is always
 *          the responsibility of the owner of a lock to eventually unlock it.
 *
 * @{
 */

/**
 * @brief Acquisition buffer information.
 *
 * This structure is used to store all needed information about an acquisition
 * buffer returned by tao_camera_wait().  The contents is considered as purely
 * informative by the high-level interface.  For instance the high-level
 * interface does not attempt to alloc or free the acquisition buffer data,
 * these tasks are performed by the low-level interface (e.g. by the "start"
 * and "finalize" virtual methods).
 *
 * A valid buffer counter should be equal to the corresponding frame counter
 * which is greater or equal 1.  When acquisition is started by
 * tao_camera_start_acquisition(), all buffer counters are set to 0.
 */
typedef struct tao_acquisition_buffer_ {
    void*            data;///< Address of buffer data.
    long             size;///< Number of bytes in buffer.
    long           offset;///< Offset (in bytes) of first pixel in ROI.
    long            width;///< Number of pixel per line in ROI.
    long           height;///< Number of lines in ROI.
    long           stride;///< Bytes per line in buffer (including padding).
    tao_encoding encoding;///< Pixel encoding in buffer.
    tao_serial     serial;///< Serial number of frame.
    tao_time  frame_start;///< Start time of frame.
    tao_time    frame_end;///< End time of frame.
    tao_time buffer_ready;///< Buffer ready time.
} tao_acquisition_buffer;

/**
 * @brief Opaque camera structure.
 */
typedef struct tao_camera_ tao_camera;

/**
 * @brief Opaque camera operations structure.
 */
typedef struct tao_camera_ops_ tao_camera_ops;

/**
 * @brief Create a new camera instance.
 *
 * This function create a new camera instance.  This function is typically
 * called by other "camera constructors".  Argument @b ops provides all the
 * methods needed to operate the specific model of camera.  Argument @b ctx is
 * some contextual data (stored as member `ctx` by this function but never
 * directly used otherwise).  Argument @b size specifies the number of bytes to
 * allocate for the camera instance (at least `sizeof(tao_camera)` bytes will
 * be allocated to store the @ref tao_camera structure.  See the code of
 * phnx_create_camera() for an example.
 *
 * @warning In case of failure of tao_camera_create(), the `finalize` method in
 *          @b ops is not called.
 *
 * @param ops   Virtual operations table.
 * @param ctx   Address of contextual data.
 * @param size  Number of bytes to allocate.
 *
 * @return The address of the new (initialized) camera; `NULL` in case of
 *         failure.
 */
extern tao_camera* tao_camera_create(
    const tao_camera_ops* ops,
    void* ctx,
    size_t size);

/**
 * @brief Release the ressources associated to a camera.
 *
 * This function aborts any acquisition with the associated camera and releases
 * all related ressources.  The camera must not be locked when calling this
 * function.
 *
 * @warning The camera must not have been locked by the caller.
 *
 * @param camera      A pointer to the camera instance.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR in case of failure.
 *
 * @see tao_camera_create().
 */
extern tao_status tao_camera_destroy(
    tao_camera* cam);

/**
 * @brief Lock a camera.
 *
 * This function locks the mutex of the camera @b cam.  The caller is
 * responsible of eventually calling tao_camera_unlock().
 *
 * Locking a camera is needed before changing anything in the camera instance
 * because the camera instance may be shared between the thread handling frame
 * grabber events and the other threads.  Make sure to lock and unlock a camera
 * before calling functions that may modify the camera structure.  Make sure
 * that the camera is not locked before calling tao_camera_destroy().
 *
 * @warning The camera must not have been locked by the caller.
 *
 * @param cam    Address of camera instance (must be non-`NULL`).
 */
extern void tao_camera_lock(
    tao_camera* cam);

/**
 * @brief Try to lock a camera.
 *
 * This function attempts to lock the mutex of the camera @b cam without
 * blocking.  If the call is successful, the caller is responsible of
 * eventually calling tao_camera_unlock().
 *
 * @warning The camera must not have been locked by the caller.
 *
 * @param cam    Address of camera instance (must be non-`NULL`).
 *
 * @return @ref TAO_OK if camera has been locked by the caller; @ref TAO_ERROR
 *         otherwise (that is, if the camera is already locked by some other
 *         thread/process).
 */
extern tao_status tao_camera_try_lock(
    tao_camera* cam);

/**
 * @brief Unlock a camera.
 *
 * This function unlocks the mutex of the camera @b cam.  The caller must have
 * locked the camera @b cam with tao_camera_lock() or tao_camera_try_lock().
 *
 * @warning The caller must own the lock on the camera.
 *
 * @param cam    Address of camera instance (must be non-`NULL`).
 */
extern void tao_camera_unlock(
    tao_camera* cam);

/**
 * @brief Signal a change for a camera to one waiting threads.
 *
 * This function restarts one of the threads that are waiting on the condition
 * variable of the camera @b cam.  Nothing happens, if no threads are waiting
 * on the condition variable of @b cam.  The caller is assumed to have locked
 * the camera before calling this function and to unlock the camera soon after
 * calling this function to effectively trigger the notification to others.
 *
 * @warning The caller must own the lock on the camera.
 *
 * @param cam    Address of camera instance (must be non-`NULL`).
 *
 * @see tao_camera_wait(), tao_camera_timed_wait(), tao_camera_abstimed_wait(),
 *      tao_camera_broadcast().
 */
extern void tao_camera_signal(
    tao_camera* cam);

/**
 * @brief Signal a change for a camera to all waiting threads.
 *
 * This function restarts all the threads that are waiting on the condition
 * variable of the camera @b cam.  Nothing happens, if no threads are waiting
 * on the condition variable of @b cam.  The caller is assumed to have locked
 * the camera before calling this function and to unlock the camera soon after
 * calling this function to effectively trigger the notification to others.
 *
 * @warning The caller must own the lock on the camera.
 *
 * @param cam    Address of camera instance (must be non-`NULL`).
 *
 * @see tao_camera_wait(), tao_camera_timed_wait(), tao_camera_abstimed_wait(),
 * tao_camera_signal().
 */
extern void tao_camera_broadcast(
    tao_camera* cam);

/**
 * @brief Wait for a condition to be signaled for a camera.
 *
 * This function atomically unlocks the exclusive lock associated with a
 * camera instance and waits for its associated condition variable to be
 * signaled.  The thread execution is suspended and does not consume any CPU
 * time until the condition variable is signaled.  The mutex of the camera must
 * have been locked (e.g., with tao_camera_lock()) by the calling thread
 * on entrance to this function.  Before returning to the calling thread, this
 * function re-acquires the mutex.
 *
 * @warning The caller must own the lock on the camera.
 *
 * @param cam    Address of camera instance (must be non-`NULL`).
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure.
 */
extern tao_status tao_camera_wait(
    tao_camera* cam);

/**
 * Wait for a condition to be signaled for a camera without blocking
 * longer than an absolute time limit.
 *
 * This function behaves like tao_camera_wait() but blocks no longer than a
 * given duration.
 *
 * @warning The caller must own the lock on the camera.
 *
 * @param cam    Address of camera instance (must be non-`NULL`).
 *
 * @param lim    Absolute time limit with the same conventions as
 *               tao_get_current_time().
 *
 * @return @ref TAO_OK if the lock has been locked by the caller before the
 *         specified time limit, @ref TAO_TIMEOUT if timeout occurred before or
 *         @ref TAO_ERROR in case of error.
 */
extern tao_status tao_camera_abstimed_wait(
    tao_camera* cam,
    const tao_time* abstime);

/**
 * Wait for a condition to be signaled for a camera without blocking
 * longer than a a relative time limit.
 *
 * This function behaves like tao_camera_wait() but blocks no longer than a
 * given duration.
 *
 * @warning The caller must own the lock on the camera.
 *
 * @param cam    Address of camera instance (must be non-`NULL`).
 *
 * @param secs   Maximum amount of time (in seconds).  If this amount of time
 *               is very large, e.g. more than @ref TAO_MAX_TIME_SECONDS, the
 *               effect is the same as calling
 *               tao_camera_wait().
 *
 * @return @ref TAO_OK if the lock has been locked by the caller before the
 *         specified time limit, @ref TAO_TIMEOUT if timeout occurred before or
 *         @ref TAO_ERROR in case of error.
 */
extern tao_status tao_camera_timed_wait(
    tao_camera* cam,
    double secs);

/**
 * Check a camera configuration.
 *
 * This function checks whether a configuration is valid for a given camera.
 *
 * @warning The caller must own the lock on the camera.
 *
 * @param cam    Address of camera instance.
 * @param cfg    Address of configuration to check.
 *
 * @return @ref TAO_OK if the configuration is valid; @ref TAO_ERROR in case of
 *         errors.
 */
extern tao_status tao_camera_check_configuration(
    tao_camera* cam,
    const tao_camera_config* cfg);

/**
 * @brief Update camera configuration from current hardware settings.
 *
 * This function updates the camera internal configuration from the current
 * hardware settings.
 *
 * @warning The caller must own the lock on the camera.
 *
 * @param cam    Address of camera instance.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR in case of errors.
 */
extern tao_status tao_camera_update_configuration(
    tao_camera* cam);

/**
 * @brief Retrieve camera configuration.
 *
 * This function copies the camera internal configuration to the destination
 * structure.   The caller may call tao_camera_update_configuration() before
 * to make sure that the camera configuration reflects the hardware settings.
 *
 * @warning The caller must own the lock on the camera.
 *
 * @param cam   Address of camera instance.
 *
 * @param cfg   Address of destination configuration structure.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR in case of errors.
 */
extern tao_status tao_camera_get_configuration(
    const tao_camera* cam,
    tao_camera_config* cfg);

/**
 * @brief Change camera settings.
 *
 * This function attempts to set the camera settings according to the source
 * configuration.  In case of errors, the caller may call
 * tao_camera_update_configuration() and then tao_camera_get_configuration() to
 * retrieve actual hardware settings.
 *
 * @warning The caller must own the lock on the camera.
 *
 * @param cam   Address of camera instance.
 *
 * @param cfg   Address of soruce configuration structure.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR in case of errors.
 */
extern tao_status tao_camera_set_configuration(
    tao_camera* cam,
    const tao_camera_config* cfg);

extern void tao_camera_roi_option_show(
    FILE* file,
    const tao_option* opt);

extern bool tao_camera_roi_option_parse(
    const tao_option* opt,
    char* args[]);

/**
 * @brief Copy hardware camera settings to shared camera data.
 *
 * This function copies the current hardware settings of the camera to update
 * the shared camera information.
 *
 * @warning It is assumed that the caller has locked the two structures, the
 *          source for reading and the destination for writing.
 *
 * @param dest   Address of the destination camera instance.
 * @param src    Address of the source camera instance.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR in case of failure.
 */
extern tao_status tao_camera_reflect_configuration(
    tao_shared_camera* dest,
    const tao_camera* src);

/**
 * @brief Wait for an acquisition buffer to be available.
 *
 * This function waits for an acquisition buffer to be available from a camera
 * not blocking longer than a given duration.
 *
 * @warning The caller must own the lock on the camera when calling
 *          tao_camera_wait_acquisition_buffer().  Unlocking the camera during
 *          the processing of the acquisition buffer, as in the below example,
 *          is recommended if this operation is time consuming.
 *
 * @note Do not forget to call tao_camera_release_acquisition_buffer() after
 *       the processing otherwise the same acquisition buffer will be
 *       immediately returned by the next call to
 *       tao_camera_wait_acquisition_buffer() if @b drop is less than 1.
 *
 * Complete example of acquisition loop:
 *
 * ~~~~~{.c}
 * long timouts = 0, frames = 0, number = 100;
 * int drop = 0; // keep all images
 * double secs = 30.0; // max. number of seconds to wait for each buffer
 * tao_camera_lock(cam);
 * tao_status status = tao_camera_start_acquisition(cam);
 * while (status == TAO_OK && frames < number) {
 *     const tao_acquisition_buffer* buf;
 *     status = tao_camera_wait_acquisition_buffer(cam, &buf, secs, drop);
 *     if (status == TAO_OK) {
 *         // Process acquisition buffer and release buffer (unlock camera
 *         // during processing).
 *         tao_camera_unlock(cam);
 *         process(buf->data, buf->size, ...); // do the processing
 *         tao_camera_lock(cam);
 *         status = tao_camera_release_acquisition_buffer(cam);
 *         ++frames;
 *     } else if (status == TAO_TIMEOUT) {
 *         // Timeout occurred.
 *         if (++timeouts > 4) {
 *             fprintf(stderr, "Too many timouts, aborting acquisition...\");
 *         } else {
 *             status = TAO_OK;
 *         }
 *     }
 * }
 * tao_camera_stop_acquisition(cam);
 * tao_camera_unlock(cam);
 * if (tao_any_errors()) {
 *     tao_report_errors();
 * }
 * ~~~~~
 *
 * @param cam     Address of camera instance.
 *
 * @param bufptr  Address of structure to retrieve information about acquisition
 *                buffer.  The caller shall consider the contents of @b buf as
 *                read-only.
 *
 * @param secs    Maximum number of seconds to wait.  Must be nonnegative.
 *                Wait forever if @b secs is too large, e.g. more than
 *                @ref TAO_MAX_TIME_SECONDS.
 *
 * @param drop    This parameter specifies how to deal with pending acquisition
 *                buffers.  If there are no pending acquisition buffers, the
 *                function waits for a new buffer to be acquired (but no longer
 *                than the given timeout).  Otherwise, if @b drop is less than
 *                1, the first pending buffer is delivered; if @b drop is
 *                equal to 1, the last pending buffer is delivered and all
 *                other pending buffers are released; if @b drop is greater
 *                than 1, all pending buffers are released and only a freshly
 *                acquired buffer can be returned.
 *
 * @return @ref TAO_OK, if a new acquisition buffer was available; @ref
 *         TAO_TIMEOUT, if no new acquisition buffer were available before
 *         `secs` seconds expired; @ref TAO_ERROR if some errors occurred.
 *
 * @see tao_camera_start_acquisition(), tao_camera_lock(), tao_camera_unlock(),
 * tao_camera_release_acquisition_buffer(), tao_camera_stop_acquisition(),
 * tao_get_absolute_timeout().
 */
extern tao_status tao_camera_wait_acquisition_buffer(
    tao_camera* cam,
    const tao_acquisition_buffer** bufptr,
    double secs,
    int drop);

/**
 * @brief Release usage of acquisition buffer.
 *
 * This function releases the usage of one acquisition buffer of camera @b cam.
 * It may be called to release the acquisition buffer returned by
 * tao_camera_wait_acquisition_buffer() but may also be called to drop unwanted
 * frames.  Call tao_camera_get_pending_acquisition_buffers() to avoid
 * errors if there are no pending acquisition buffers to release.  Calling this
 * function decrements the number of pending buffers.
 *
 * This function can only be called while the camera is acquiring.  That is
 * between successive calls to tao_camera_start_acquisition() and
 * tao_camera_stop_acquisition().
 *
 * @warning The caller must own the lock on the camera.
 *
 * @param cam    Address of camera instance.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR on failure.
 *
 * @see tao_camera_wait_acquisition_buffer(),
 *      tao_camera_get_pending_acquisition_buffers().
 */
extern tao_status tao_camera_release_acquisition_buffer(
    tao_camera* cam);

/**
 * @brief Get number of pending acquisition buffers.
 *
 * This function returns the number of pending acquisition buffers, that is the
 * number of frames acquired by the camera and available for processing.
 *
 * @warning The caller must own the lock on the camera.
 *
 * @param cam    Address of camera instance.
 *
 * @return The number of pending acquisition buffers; `0` if not acquiring.
 *
 * @see tao_camera_wait_acquisition_buffer(),
 *      tao_camera_get_pending_acquisition_buffers().
 */
extern long tao_camera_get_pending_acquisition_buffers(
    const tao_camera* cam);

/**
 * @brief Start image acquisition.
 *
 * This function starts image acquisition by a given camera.  If acquisition is
 * already running, nothing is done.
 *
 * @warning The caller must own the lock on the camera.
 *
 * @param cam    Address of camera instance.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR on failure.
 */
extern tao_status tao_camera_start_acquisition(
    tao_camera* cam);

/**
 * @brief Stop image acquisition.
 *
 * This function stops image acquisition by a given camera.  If acquisition is
 * not running, nothing is done.
 *
 * @warning The caller must own the lock on the camera.
 *
 * @param cam    Address of camera instance.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR on failure.
 */
extern tao_status tao_camera_stop_acquisition(
    tao_camera* cam);

/**
 * @brief Reset camera.
 *
 * This function attempts to reset a camera to idle state after an error.  If
 * acquistion is running, it is stopped.
 *
 * @warning The caller must own the lock on the camera.
 *
 * @param cam    Address of camera instance.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR on failure.
 */
extern tao_status tao_camera_reset(
    tao_camera* cam);

/**
 * @brief Set the origin of time for a camera.
 *
 * This function changes the origin of time of a camera instance.
 *
 * @warning The caller must own the lock on the camera.
 *
 * @param cam    Address of camera instance.
 *
 * @param orig   Address of structure with the origin of time.  If `NULL`,
 *               tao_get_monotonic_time() is called to set the origin of
 *               to the current time (using the monotonic clock if available).
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR in case of errors.
 */
extern tao_status tao_camera_set_origin_of_time(
    tao_camera* cam,
    const tao_time* orig);

/**
 * @brief Get the origin of time of a camera.
 *
 * This function retrieves the origin of time of a camera instance.
 *
 * @warning The caller must own the lock on the camera.
 *
 * @param orig   Address of structure to store the origin of time of the
 *               camera.  Can be `NULL` to not store the origin of time.
 *
 * @param cam    Address of camera instance.
 *
 * @return A pointer to a structure with the origin of time of @b cam.
 */
extern const tao_time* tao_camera_get_origin_of_time(
    tao_time* orig,
    const tao_camera* cam);

/**
 * @brief Get the number of seconds since a camera origin of time.
 *
 * This function yields the elapsed time (in seconds) between the origin of
 * time registered in a camera instance and a given absolute time or the
 * current time.
 *
 * @warning The caller must own the lock on the camera.
 *
 * @param cam    Address of camera instance.
 * @param t      Address of structure with the absolute time.  If `NULL`,
 *               tao_get_monotonic_time() is called to set the origin of
 *               to the current time (using the monotonic clock if available).
 *
 * @return A number of seconds.
 *
 * @see tao_camera_set_origin_of_time(), tao_camera_get_elapsed_milliseconds(),
 * tao_camera_get_elapsed_microseconds(), tao_camera_get_elapsed_nanoseconds(),
 * tao_get_monotonic_time().
 */
extern double tao_camera_get_elapsed_seconds(
    const tao_camera* cam,
    const tao_time* t);

/**
 * @brief Get the number of milliseconds since a camera origin of time.
 *
 * This function yields the elapsed time (in milliseconds) between the origin
 * of time registered in a camera instance and a given absolute time or the
 * current time.
 *
 * @warning The caller must own the lock on the camera.
 *
 * @param cam    Address of camera instance.
 * @param t      Address of structure with the absolute time.  If `NULL`,
 *               tao_get_monotonic_time() is called to set the origin of
 *               to the current time (using the monotonic clock if available).
 *
 * @return A number of milliseconds.
 *
 * @see tao_camera_set_origin_of_time(), tao_camera_get_elapsed_seconds(),
 * tao_camera_get_elapsed_microseconds(), tao_camera_get_elapsed_nanoseconds(),
 * tao_get_monotonic_time().
 */
extern double tao_camera_get_elapsed_milliseconds(
    const tao_camera* cam,
    const tao_time* t);

/**
 * @brief Get the number of microseconds since a camera origin of time.
 *
 * This function yields the elapsed time (in microseconds) between the origin
 * of time registered in a camera instance and a given absolute time or the
 * current time.
 *
 * @warning The caller must own the lock on the camera.
 *
 * @param cam    Address of camera instance.
 * @param t      Address of structure with the absolute time.  If `NULL`,
 *               tao_get_monotonic_time() is called to set the origin of
 *               to the current time (using the monotonic clock if available).
 *
 * @return A number of microseconds.
 *
 * @see tao_camera_set_origin_of_time(), tao_camera_get_elapsed_seconds(),
 * tao_camera_get_elapsed_milliseconds(), tao_camera_get_elapsed_nanoseconds(),
 * tao_get_monotonic_time().
 */
extern double tao_camera_get_elapsed_microseconds(
    const tao_camera* cam,
    const tao_time* t);

/**
 * @brief Get the number of nanoseconds since a camera origin of time.
 *
 * This function yields the elapsed time (in nanoseconds) between the origin of
 * time registered in a camera instance and a given absolute time or the
 * current time.
 *
 * @warning The caller must own the lock on the camera.
 *
 * @param cam    Address of camera instance.
 * @param t      Address of structure with the absolute time.  If `NULL`,
 *               tao_get_monotonic_time() is called to set the origin of
 *               to the current time (using the monotonic clock if available).
 *
 * @return A number of nanoseconds.
 *
 * @see tao_camera_set_origin_of_time(), tao_camera_get_elapsed_seconds(),
 * tao_camera_get_elapsed_milliseconds(),
 * tao_camera_get_elapsed_microseconds(), tao_get_monotonic_time().
 */
extern double tao_camera_get_elapsed_nanoseconds(
    const tao_camera* cam,
    const tao_time* t);

/**
 * @}
 */

TAO_END_DECLS

#endif // TAO_CAMERAS_H_
