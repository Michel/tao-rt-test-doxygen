// cameras.c -
//
// Unified API for camera devices in TAO.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2018-2022, Éric Thiébaut.

#include "tao-cameras-private.h"
#include "tao-shared-cameras-private.h"
#include "tao-shared-arrays-private.h"
#include "tao-errors.h"
#include "tao-generic.h"

#include "config.h"

#include <errno.h>
#include <math.h>
#include <string.h>

static inline void fatal_error(
    const char* func,
    int code)
{
    tao_store_error(func, code);
    tao_panic();
}

//-----------------------------------------------------------------------------
// REGIONS OF INTEREST

tao_camera_roi* tao_camera_roi_copy(
    tao_camera_roi* dest,
    const tao_camera_roi* src)
{
    *dest = *src;
    return dest;
}

tao_camera_roi* tao_camera_roi_define(
    tao_camera_roi* roi,
    long xbin,
    long ybin,
    long xoff,
    long yoff,
    long width,
    long height)
{
    roi->xbin   = xbin;
    roi->ybin   = ybin;
    roi->xoff   = xoff;
    roi->yoff   = yoff;
    roi->width  = width;
    roi->height = height;
    return roi;
}

tao_status tao_camera_roi_check(
    const tao_camera_roi* roi,
    long sensorwidth,
    long sensorheight)
{
    // Check all bounds to avoid integer overflows in the 2 last tests.
    if (roi->xbin < 1 || roi->xbin > sensorwidth ||
        roi->ybin < 1 || roi->ybin > sensorheight ||
        roi->xoff < 0 || roi->xoff >= sensorwidth ||
        roi->yoff < 0 || roi->yoff >= sensorheight ||
        roi->width < 1 || roi->width > sensorwidth ||
        roi->height < 1 || roi->height > sensorheight ||
        roi->xoff + roi->xbin*roi->width > sensorwidth ||
        roi->yoff + roi->ybin*roi->height > sensorheight) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

void tao_camera_roi_option_show(
    FILE* file,
    const tao_option* opt)
{
    tao_camera_roi* roi = (tao_camera_roi*)opt->ptr;
    fprintf(file, "%ld,%ld,%ld,%ld,%ld,%ld", roi->xbin, roi->ybin,
            roi->xoff, roi->yoff, roi->width, roi->height);
}

bool tao_camera_roi_option_parse(
    const tao_option* opt,
    char* args[])
{
    char c;
    tao_camera_roi* roi = (tao_camera_roi*)opt->ptr;
    if (sscanf(args[0], " %ld , %ld , %ld , %ld %c", &roi->xoff, &roi->yoff,
               &roi->width, &roi->height, &c) == 4) {
        roi->xbin = 1;
        roi->ybin = 1;
        return true;
    }
    if (sscanf(args[0], " %ld , %ld , %ld , %ld , %ld , %ld %c",
               &roi->xbin, &roi->ybin, &roi->xoff, &roi->yoff,
               &roi->width, &roi->height, &c) == 6) {
        roi->xbin = 1;
        roi->ybin = 1;
        return true;
    }
    return false;
}

//-----------------------------------------------------------------------------
// UNIFIED CAMERAS

static inline void get_monotonic_time(
    tao_time* t)
{
    if (tao_get_monotonic_time(t) != TAO_OK) {
        tao_panic();
    }
}

void tao_camera_info_initialize(
    tao_camera_info* info)
{
    if (info == NULL) {
        return;
    }
    memset(info, 0, sizeof(tao_camera_info));
    info->sensorwidth = 1;
    info->sensorheight = 1;
    info->config.roi.xbin = 1;
    info->config.roi.ybin = 1;
    info->config.roi.xoff = 0;
    info->config.roi.yoff = 0;
    info->config.roi.width = 1;
    info->config.roi.height = 1;
    info->config.pixeltype = TAO_FLOAT;
    info->config.sensorencoding = TAO_ENCODING_UNKNOWN;
    info->config.bufferencoding = TAO_ENCODING_UNKNOWN;
    info->config.nbufs = 4;
    info->config.framerate = 25.0; // 25 Hz
    info->config.exposuretime = 0.001; // 1 ms
    info->config.preprocessing = TAO_PREPROCESSING_NONE;
    info->state = TAO_STATE_INITIALIZING;
    info->temperature = NAN;
    get_monotonic_time(&info->origin);
    info->frames = 0;
    info->overruns = 0;
    info->lostframes = 0;
    info->overflows = 0;
    info->lostsyncs = 0;
    info->timeouts = 0;
}

static void invalid_runlevel(
    tao_camera* cam,
    const char* func)
{
    int code;
    switch (cam->runlevel) {
     case 0:
        code = TAO_NOT_READY;
        break;
     case 1:
        code = TAO_NOT_ACQUIRING;
        break;
     case 2:
        code = TAO_ACQUISITION_RUNNING;
        break;
     case 3:
        code = TAO_MUST_RESET;
        break;
    case 4:
        code = TAO_UNRECOVERABLE;
        break;
    default:
        code = TAO_CORRUPTED;
    }
    tao_store_error(func, code);
}

static tao_status manage_buffers(
    tao_camera* cam,
    int nbufs)
{
    if (cam->nbufs != nbufs || cam->bufs == NULL) {
        // Free existing buffers.
        tao_acquisition_buffer* bufs = cam->bufs;
        cam->bufs = NULL;
        cam->nbufs = 0;
        if (bufs != NULL) {
            free(bufs);
        }
    }
    if (nbufs > cam->nbufs) {
        // Allocate buffers and initialize private information.
        cam->bufs = TAO_NEW(nbufs, tao_acquisition_buffer);
        if (cam->bufs == NULL) {
            return TAO_ERROR;
        }
        cam->nbufs = nbufs;
    }
    return TAO_OK;
}

tao_camera* tao_camera_create(
    const tao_camera_ops* ops,
    void* ctx,
    size_t size)
{
    // Check that all virtual methods have a valid address.
    if (ops->initialize == NULL || ops->finalize == NULL ||
        ops->reset == NULL || ops->update_config == NULL ||
        ops->check_config == NULL || ops->set_config == NULL ||
        ops->start == NULL || ops->stop == NULL ||
        ops->wait_buffer == NULL || ops->release_buffer == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return NULL;
    }
    if (size < sizeof(tao_camera)) {
        size = sizeof(tao_camera);
    }
    tao_camera* cam = tao_malloc(size);
    if (cam == NULL) {
        return NULL;
    }
    memset(cam, 0, size);
    if (tao_mutex_initialize(&cam->mutex, false) != TAO_OK) {
        goto error1;
    }
    if (tao_condition_initialize(&cam->cond, false) != TAO_OK) {
       goto error2;
    }
    cam->ops = ops;
    cam->ctx = ctx;
    tao_camera_info_initialize(&cam->info);
    cam->runlevel = 0;
    cam->info.state = TAO_STATE_INITIALIZING;
    if (cam->ops->initialize(cam) != TAO_OK) {
        goto error3;
    }
    cam->runlevel = 1;
    cam->info.state = TAO_STATE_WAITING;
    return cam;

    // Branch here in case of errors.
 error3:
    pthread_cond_destroy(&cam->cond);
 error2:
    pthread_mutex_destroy(&cam->mutex);
 error1:
    free(cam);
    return NULL;
}

tao_status tao_camera_destroy(
    tao_camera* cam)
{
    if (cam != NULL) {
        // Lock the camera.
        tao_camera_lock(cam);

        // Stop acquisition if acquiring (ignoring errors).
        if (cam->runlevel == 2) {
            cam->info.state = TAO_STATE_STOPPING;
            cam->ops->stop(cam);
            cam->runlevel = 1;
            cam->info.state = TAO_STATE_WAITING;
        }

        // Call virtual "finalize" method.
        cam->ops->finalize(cam);
        cam->runlevel = 4;
        cam->info.state = TAO_STATE_KILLED;

        // Free acquisition buffers **after** calling the "finalize" virtual
        // method to make sure that these buffers are not in use somewhere.
        manage_buffers(cam, 0);

        // Destroy condition variable.
        pthread_cond_destroy(&cam->cond);

        // Unlock the camera and immediately destroy its mutex.
        tao_camera_unlock(cam);
        pthread_mutex_destroy(&cam->mutex);

        // Eventually release memory storing the instance.
        free((void*)cam);
    }
    return TAO_OK;
}

// We should not modify anything in the camera instance unless we own the
// camera lock.
//
// Any errors occuring while locking/unlocking are an indication of bugs and
// aborting the program is the best we can do.
//
// Note that pthread_cond_init, pthread_cond_signal, pthread_cond_broadcast,
// and pthread_cond_wait never return an error code.  The
// pthread_cond_timedwait function may return ETIMEDOUT is the condition
// variable was not signaled until the specified timeout or EINTR if the call
// was interrupted by a signal.

void tao_camera_lock(
    tao_camera* cam)
{
    int code = pthread_mutex_lock(&cam->mutex);
    if (code != 0) {
        // This is a fatal error.  FIXME: Really?
        fatal_error("pthread_mutex_lock", code);
    }
}

tao_status tao_camera_try_lock(
    tao_camera* cam)
{
    int code = pthread_mutex_trylock(&cam->mutex);
    if (code == 0) {
        return TAO_OK;
    } else if (code == EBUSY) {
        return TAO_TIMEOUT;
    } else {
        // This is a fatal error.  FIXME: Really?
        fatal_error("pthread_mutex_trylock", code);
        return TAO_ERROR;
    }
}

void tao_camera_unlock(
    tao_camera* cam)
{
    int code = pthread_mutex_unlock(&cam->mutex);
    if (code != 0) {
        // This is a fatal error.  FIXME: Really?
        fatal_error("pthread_mutex_unlock", code);
    }
}

void tao_camera_signal(
    tao_camera* cam)
{
    int code = pthread_cond_signal(&cam->cond);
    if (code != 0) {
        // This is a fatal error.  FIXME: Really?
        fatal_error("pthread_cond_signal", code);
    }
}

void tao_camera_broadcast(
    tao_camera* cam)
{
    int code = pthread_cond_broadcast(&cam->cond);
    if (code != 0) {
        // This is a fatal error.  FIXME: Really?
        fatal_error("pthread_cond_broadcast", code);
    }
}

tao_status tao_camera_wait(
    tao_camera* cam)
{
    return tao_condition_wait(&cam->cond, &cam->mutex);
}

tao_status tao_camera_abstimed_wait(
    tao_camera* cam,
    const tao_time* abstime)
{
    return tao_condition_abstimed_wait(&cam->cond, &cam->mutex, abstime);
}

tao_status tao_camera_timed_wait(
    tao_camera* cam,
    double secs)
{
    return tao_condition_timed_wait(&cam->cond, &cam->mutex, secs);
}

// This function may be called after an unsuccessful command to set the camera
// state according to the actual camera run-level.  If the camera run-level
// indicates a recoverable error, this functions attempts to reset the camera.
// This function returns TO_OK or TAO_ERROR if resetting the camera failed to
// revert to a "normal" run-level.
static tao_status fix_camera_state(
    tao_camera* cam)
{
    if (cam->runlevel == 3) {
        // Recoverable error. Attempt to reset the camera.
        if (cam->ops->reset(cam) != TAO_OK || cam->runlevel != 1) {
            cam->runlevel = 4;
            cam->info.state = TAO_STATE_KILLED;
            tao_store_error(__func__, TAO_UNRECOVERABLE);
            return TAO_ERROR;
        }
    }
    if (cam->runlevel <= 0) {
        cam->info.state = TAO_STATE_INITIALIZING;
    } else if (cam->runlevel == 1) {
        cam->info.state = TAO_STATE_WAITING;
    } else if (cam->runlevel == 2) {
        cam->info.state = TAO_STATE_WORKING;
    } else {
        // Unrecoverable error.
        cam->info.state = TAO_STATE_KILLED;
    }
    return TAO_OK;
}

tao_status tao_camera_start_acquisition(
    tao_camera* cam)
{
    switch (cam->runlevel) {
    case 1:
        // Allocate acquisition buffers (one more than requested to prevent
        // pending buffers to be overwritten by the frame-grabber), clear
        // events and manage to have next acquisition buffer information at
        // index 0.
        if (cam->info.config.nbufs < 1) {
            tao_store_error(__func__, TAO_BAD_BUFFERS);
            return TAO_ERROR;
        }
        if (manage_buffers(cam, cam->info.config.nbufs + 1) != TAO_OK) {
            return TAO_ERROR;
        }
        for (int i = 0; i < cam->nbufs; ++i) {
            // Reset the serial number and data address of all acquisition
            // buffers.
            cam->bufs[i].data = NULL;
            cam->bufs[i].serial = 0;
        }
        cam->events = 0;
        cam->pending = 0;
        cam->last = -1; // so that index of next buffer is 0

        // Call the "start" virtual method.  In case of success, update the
        // run-level and report success.  In case of errors, the run-level is
        // left unchanged but the virtual method is allowed to set it to a more
        // suitable value which we reflect in the camera state.
        cam->info.state = TAO_STATE_STARTING;
        if (cam->ops->start(cam) != TAO_OK) {
            fix_camera_state(cam);
            return TAO_ERROR;
        }
        cam->runlevel = 2;
        cam->info.state = TAO_STATE_WORKING;
        return TAO_OK;

    case 2:
        // Camera is acquiring, nothing to do.
        return TAO_OK;

    default:
        // All other cases are hopeless!
        invalid_runlevel(cam, __func__);
        return TAO_ERROR;
    }
}

// FIXME: stop() should release all buffers, we force that wait_buffer() and
// release_buffer() can only be called while acquisition is running, outside
// this state, the acquisition buffers must be assumed unavailable.  It may
// however be a good idea to make acquisition buffers available outside
// acquisition.  However I do not know what does the Phoenix with internal
// buffers so it is safer to keep the restriction for now.
tao_status tao_camera_stop_acquisition(
    tao_camera* cam)
{
    switch (cam->runlevel) {
    case 1:
        // Camera is not acquiring, nothing to do.
        return TAO_OK;

    case 2:
        // There are no more pending buffers.
        cam->pending = 0;

        // Call the "stop" virtual method.  In case of success, update the
        // run-level and report success.  In case of errors, the run-level is
        // left unchanged but the virtual method is allowed to set it to a more
        // suitable value which we reflect in the camera state.
        cam->info.state = TAO_STATE_STOPPING;
        if (cam->ops->stop(cam) != TAO_OK) {
            fix_camera_state(cam);
            return TAO_ERROR;
        }
        cam->runlevel = 1;
        cam->info.state = TAO_STATE_WAITING;
        return TAO_OK;

    default:
        // All other cases are hopeless!
        invalid_runlevel(cam, __func__);
        return TAO_ERROR;
    }
}

tao_status tao_camera_wait_acquisition_buffer(
    tao_camera* cam,
    const tao_acquisition_buffer** bufptr,
    double secs,
    int drop)
{
    // Set address so as to prevent caller to use buffer contents in case of
    // errors.
    *bufptr = NULL;

    // Check timeout value and whether we are acquiring (whether waiting for
    // images is implemented is checked later).
    if (isnan(secs) || secs < 0) {
        tao_store_error(__func__, TAO_OUT_OF_RANGE);
        return TAO_ERROR;
    }
    if (cam->runlevel != 2) {
        tao_store_error(__func__, TAO_NOT_ACQUIRING);
        return TAO_ERROR;
    }

    // Drop pending buffers in excess until there are no more than 1 (drop
    // = 1) or 0 (drop > 1) pending buffers.  Each dropped acquisition
    // buffer counts as an overrun.
    if (drop > 0) {
        int limit = (drop > 1 ? 0 : 1);
        while (cam->pending > limit) {
            ++cam->info.overruns;
            if (cam->ops->release_buffer(cam) != TAO_OK) {
                return TAO_ERROR;
            }
        }
    }

    // If there are no more pending buffers, call "wait_buffer" to wait for a
    // new acquired buffer.  This virtual method shall update `cam->pending`
    // `cam->last`, the information in `cam->bufs[cam->last]`,
    // `cam->info.timeouts` and `cam->info.frames`.
    if (cam->pending < 1) {
        tao_status status = cam->ops->wait_buffer(cam, secs);
        if (status != TAO_OK) {
            return status;
        }
    }

    // Return the first pending buffer.
    int index = TAO_FIRST_PENDING_ACQUISITION_BUFFER(cam);
    *bufptr = &cam->bufs[index];
    return TAO_OK;
}

tao_status tao_camera_release_acquisition_buffer(
    tao_camera* cam)
{
    // Check arguments, then release buffer from the internal camera list.
    if (cam->runlevel != 2) {
        // If we are not acquiring, it is an error to release a buffer.
        tao_store_error(__func__, TAO_NOT_ACQUIRING);
        return TAO_ERROR;
    }
    if (cam->pending < 1) {
        // Refuse to release more buffers than there could possibly be.
        tao_store_error(__func__, TAO_EXHAUSTED);
        return TAO_ERROR;
    }
    return cam->ops->release_buffer(cam);
}

long tao_camera_get_pending_acquisition_buffers(
    const tao_camera* cam)
{
    return (cam != NULL && cam->runlevel == 2 ? cam->pending : 0);
}

tao_status tao_camera_reset(
    tao_camera* cam)
{
    switch (cam->runlevel) {
    case 1:
        // Camera is not acquiring, nothing else to do.
        cam->info.state = TAO_STATE_WAITING;
        return TAO_OK;

    case 2:
        // Resetting is equivalent to stopping acquisition.
        return tao_camera_stop_acquisition(cam);

    case 3:
        // Recoverable error.  Attempt to reset the camera.
        if (cam->ops->reset(cam) != TAO_OK || cam->runlevel != 1) {
            cam->runlevel = 4;
            cam->info.state = TAO_STATE_KILLED;
            tao_store_error(__func__, TAO_UNRECOVERABLE);
            return TAO_ERROR;
        }
        cam->info.state = TAO_STATE_WAITING;
        return TAO_OK;

    default:
        // All other cases are hopeless!
        invalid_runlevel(cam, __func__);
        return TAO_ERROR;
    }
}

tao_status tao_camera_check_configuration(
    tao_camera* cam,
    const tao_camera_config* cfg)
{
    if (cam->ops->check_config(cam, cfg) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

// Update settings from hardware.
tao_status tao_camera_update_configuration(
    tao_camera* cam)
{
    if (cam == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    if (cam->runlevel != 1) {
        invalid_runlevel(cam, __func__);
        return TAO_ERROR;
    }
    if (cam->ops->update_config(cam) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

tao_status tao_camera_get_configuration(
    const tao_camera* cam,
    tao_camera_config* cfg)
{
    if (cam == NULL || cfg == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    *cfg = cam->info.config;
    return TAO_OK;
}

tao_status tao_camera_set_configuration(
    tao_camera* cam,
    const tao_camera_config* cfg)
{
    if (cam == NULL || cfg == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    if (cam->runlevel != 1) {
        invalid_runlevel(cam, __func__);
        return TAO_ERROR;
    }
    if (cam->ops->set_config(cam, cfg) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

tao_status tao_camera_reflect_configuration(
    tao_shared_camera* dest,
    const tao_camera* src)
{
    if (dest == NULL || src == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    dest->info = src->info;
    return TAO_OK;
}

tao_status tao_camera_set_origin_of_time(
    tao_camera* cam,
    const tao_time* orig)
{
    if (cam == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return TAO_ERROR;
    }
    tao_time now;
    if (orig == NULL) {
        get_monotonic_time(&now);
        orig = &now;
    }
    cam->info.origin = *orig;
    return TAO_OK;
}

const tao_time* tao_camera_get_origin_of_time(
    tao_time* orig,
    const tao_camera* cam)
{
    if (cam == NULL) {
        tao_store_error(__func__, TAO_BAD_ADDRESS);
        return NULL;
    }
    if (orig == NULL) {
        return &cam->info.origin;
    } else {
        *orig = cam->info.origin;
        return orig;
    }
}

#define ENCODE(w)                                               \
    double tao_get_camera_elapsed_##w(                          \
        const tao_camera* cam,                                  \
        const tao_time* t)                                      \
    {                                                           \
        tao_time dt;                                            \
        if (t == NULL) {                                        \
            get_monotonic_time(&dt);                            \
            t = &dt;                                            \
        }                                                       \
        return tao_time_to_##w(                                 \
            tao_subtract_times(&dt, t, &cam->info.origin));     \
    }
ENCODE(seconds)
ENCODE(milliseconds)
ENCODE(microseconds)
ENCODE(nanoseconds)
#undef ENCODE

tao_status tao_camera_config_print(
    FILE* out,
    const tao_camera_config* cfg)
{
    if (out == NULL) {
        out = stdout;
    }
    char buf[TAO_ENCODING_STRING_SIZE];
    tao_error* err = tao_get_last_error();
    if (fprintf(out, "Pixel binning: %ld×%ld\n",
                cfg->roi.xbin, cfg->roi.ybin) < 0 ||
        fprintf(out, "Region of interest: %ld×%ld at (%ld,%ld)\n",
                cfg->roi.width, cfg->roi.height,
                cfg->roi.xoff, cfg->roi.yoff) < 0) {
    fprintf_error:
        tao_store_system_error("fprintf");
        return TAO_ERROR;
    }
    if (tao_format_encoding(
            buf, tao_encoding_of_eltype(cfg->pixeltype)) != TAO_OK) {
        tao_clear_error(err);
        strcpy(buf, "IllegalType");
    }
    if (fprintf(out, "Pixel type: %s\n", buf) < 0) {
        goto fprintf_error;
    }
    if (tao_format_encoding(buf, cfg->sensorencoding) != TAO_OK) {
        tao_clear_error(err);
        strcpy(buf, "IllegalEncoding");
    }
    if (fprintf(out, "Sensor encoding: %s\n", buf) < 0) {
        goto fprintf_error;
    }
    if (tao_format_encoding(buf, cfg->bufferencoding) != TAO_OK) {
        tao_clear_error(err);
        strcpy(buf, "IllegalEncoding");
    }
    if (fprintf(out, "Buffer encoding: %s\n", buf) < 0 ||
        fprintf(out, "Number of acquisition buffers: %ld\n", cfg->nbufs) < 0 ||
        fprintf(out, "Frame rate: %g Hz\n", cfg->framerate) < 0 ||
        fprintf(out, "Exposure time: %.3f ms\n", 1E3*cfg->exposuretime) < 0) {
        goto fprintf_error;
    }
    return TAO_OK;
}

tao_status tao_camera_info_print(
    FILE* out,
    const tao_camera_info* inf)
{
    char buf[32];
    if (fprintf(out, "Sensor size: %ld × %ld pixels\n",
                inf->sensorwidth, inf->sensorheight) < 0) {
    fprintf_error:
        tao_store_system_error("fprintf");
        return TAO_ERROR;
    }
    if (tao_camera_config_print(out, &inf->config) != TAO_OK) {
        return TAO_ERROR;
    }
    if (fprintf(out, "Camera server state: %s\n",
                tao_state_get_name(inf->state)) < 0 ||
        fprintf(out, "Sensor temperature: %.1f°C\n",
                inf->temperature) < 0 ||
        fprintf(out, "Origin of time: %s s\n",
                tao_sprintf_time(buf, &inf->origin)) < 0 ||
        fprintf(out, "Number of acquired frames: %" PRId64 "\n",
                inf->frames) < 0 ||
        fprintf(out, "Number of overruns: %" PRId64 "\n",
                inf->overruns) < 0 ||
        fprintf(out, "Number of lost frames: %" PRId64 "\n",
                inf->lostframes) < 0 ||
        fprintf(out, "Number of overflows: %" PRId64 "\n",
                inf->overflows) < 0 ||
        fprintf(out, "Number of lost synchronizations: %" PRId64 "\n",
                inf->lostsyncs) < 0 ||
        fprintf(out, "Number of timeouts: %" PRId64 "\n",
                inf->timeouts) < 0) {
        goto fprintf_error;
    }
    return TAO_OK;
}
