// locks.c --
//
// Management of mutexes, condition variables and read/write locks in TAO
// library.
//
//-----------------------------------------------------------------------------
//
// This file is part of the TAO software (https://git-cral.univ-lyon1.fr/tao)
// licensed under the MIT license.
//
// Copyright (C) 2018-2021, Éric Thiébaut.

#include "tao-locks.h"
#include "tao-errors.h"

#include "config.h"

#include <errno.h>
#include <math.h>

#define NANOSECONDS_TO_SLEEP         5000 // 5µs
#define NANOSECONDS_PER_SECOND 1000000000 // 1E9

// Emulate operations like pthread_mutex_timedlock(),
// pthread_rwlock_timedrdlock() or pthread_rwlock_timedwrlock().
//
// The emulation is far from perfect.
//
// - The thread is not exactly suspended until the mutex can be locked or the
//   time limit is exceeded.  The waiting time is split in chunks of
//   NANOSECONDS_TO_SLEEP nanoseconds.
//
// - POSIX says that:
//
//     If a signal that causes a signal handler to be executed is delivered to
//     a thread blocked on a mutex or a r/w lock via a call to one of these
//     functions, upon return from the signal handler the thread shall resume
//     waiting for the lock as if it was not interrupted.
//
//   Here we return EINTR if nanosleep is interrupted by a non-blocked signal.
#define EMULATE_TIMEDWAIT(try, abstime)                                 \
    do {                                                                \
        struct timespec lim = {                                         \
            .tv_sec = abstime->sec,                                     \
            .tv_nsec = abstime->nsec,                                   \
        };                                                              \
        if (lim.tv_nsec < 0 || lim.tv_nsec >= NANOSECONDS_PER_SECOND) { \
            /* See POSIX standard. */                                   \
            return EINVAL;                                              \
        }                                                               \
        while (true) {                                                  \
            /* Try to acquire the lock. */                              \
            int code = (try);                                           \
            if (code == 0 || code != EBUSY) {                           \
                return code;                                            \
            }                                                           \
            /* Compute remaining time with a nonnegative number of      \
               nanoseconds (members of timespec may be unsigned). */    \
            struct timespec now;                                        \
            if (clock_gettime(CLOCK_REALTIME, &now) != 0) {             \
                return errno;                                           \
            }                                                           \
            int64_t rem_sec =                                           \
                (int64_t)lim.tv_sec - (int64_t)now.tv_sec;              \
            int64_t rem_nsec =                                          \
                (int64_t)lim.tv_nsec - (int64_t)now.tv_nsec;            \
            if (rem_nsec < 0) {                                         \
                rem_sec -=1;                                            \
                rem_nsec += NANOSECONDS_PER_SECOND;                     \
            }                                                           \
            if (rem_sec < 0 || (rem_sec == 0 && rem_nsec == 0)) {       \
                return ETIMEDOUT;                                       \
            }                                                           \
            /* Sleep for a short time. */                               \
            struct timespec slp;                                        \
            slp.tv_sec = 0;                                             \
            slp.tv_nsec = ((rem_sec == 0 &&                             \
                            rem_nsec <= NANOSECONDS_TO_SLEEP) ?         \
                           rem_nsec : NANOSECONDS_TO_SLEEP);            \
            if (nanosleep(&slp, NULL) != 0) {                           \
                return errno;                                           \
            }                                                           \
        }                                                               \
    } while (false)

#ifndef HAVE_PTHREAD_MUTEX_TIMEDLOCK
static int pthread_mutex_timedlock(
    tao_mutex *mutex,
    const tao_time* abstime)
{
    EMULATE_TIMEDWAIT(pthread_mutex_trylock(mutex), abstime);
}
#endif

#ifndef HAVE_PTHREAD_RWLOCK_TIMEDRDLOCK
static int pthread_rwlock_timedrdlock(
    tao_rwlock* rwlock,
    const tao_time* abstime)
{
    EMULATE_TIMEDWAIT(pthread_rwlock_tryrdlock(rwlock), abstime);
}
#endif

#ifndef HAVE_PTHREAD_RWLOCK_TIMEDWRLOCK
static int pthread_rwlock_timedwrlock(
    tao_rwlock* rwlock,
    const tao_time* abstime)
{
    EMULATE_TIMEDWAIT(pthread_rwlock_trywrlock(rwlock), abstime);
}
#endif

// In Linux POSIX Threads doc., it is written that pthread_cond_init(),
// pthread_cond_signal(), pthread_cond_broadcast(), and pthread_cond_wait()
// never return an error.  However, in the Open Group Specifications (Issue 7,
// 2018 edition IEEE Std 1003.1-2017, Revision of IEEE Std 1003.1-2008), it is
// written that, if successful, these functions shall return zero; otherwise,
// an error number shall be returned to indicate the error.
//
// Whatever the implementation, the following macros should work correctly for
// calling a function of the POSIX Threads Library.

#define CALL_PTHREAD_FUNC(func, ...)            \
    do {                                        \
        int code_ = func(__VA_ARGS__);          \
        if (code_ == 0) {                       \
            return TAO_OK;                      \
        } else {                                \
            tao_store_error(#func, code_);      \
            return TAO_ERROR;                   \
        }                                       \
    } while (false)

#define CALL_PTHREAD_TIMED_FUNC(tm, func, ...)  \
    do {                                        \
        int code_ = func(__VA_ARGS__);          \
        if (code_ == 0) {                       \
            return TAO_OK;                      \
        } else if (code_ == (tm)) {             \
            return TAO_TIMEOUT;                 \
        } else {                                \
            tao_store_error(#func, code_);      \
            return TAO_ERROR;                   \
        }                                       \
    } while (false)

//-----------------------------------------------------------------------------
// MUTEXES

// See https://stackoverflow.com/questions/20325146 for configuring mutexes and
// condition variables shared between processes.
tao_status tao_mutex_initialize(
    tao_mutex* mutex,
    bool shared)
{
    // Initialize attributes of mutex.
    pthread_mutexattr_t attr;
    int code = pthread_mutexattr_init(&attr);
    if (code != 0) {
        // This should never occur, but...
        tao_store_error("pthread_mutexattr_init", code);
        return TAO_ERROR;
    }
    int pshared = (shared ? TAO_PROCESS_SHARED : TAO_PROCESS_PRIVATE);
    code = pthread_mutexattr_setpshared(&attr, pshared);
    if (code != 0) {
        pthread_mutexattr_destroy(&attr);
        tao_store_error("pthread_mutexattr_setpshared", code);
        return TAO_ERROR;
    }

    // Initialize mutex.
    code = pthread_mutex_init(mutex, &attr);
    if (code != 0) {
        pthread_mutexattr_destroy(&attr);
        tao_store_error("pthread_mutex_init", code);
        return TAO_ERROR;
    }

    // Destroy mutex attributes.
    code = pthread_mutexattr_destroy(&attr);
    if (code != 0) {
        tao_store_error("pthread_mutexattr_destroy", code);
        return TAO_ERROR;
    }

    // Report success.
    return TAO_OK;
}

tao_status tao_mutex_lock(
    tao_mutex* mutex)
{
    CALL_PTHREAD_FUNC(pthread_mutex_lock, mutex);
}

tao_status tao_mutex_try_lock(
    tao_mutex* mutex)
{
    CALL_PTHREAD_TIMED_FUNC(EBUSY, pthread_mutex_trylock, mutex);
}

tao_status tao_mutex_abstimed_lock(
    tao_mutex* mutex,
    const tao_time* abstime)
{
    struct timespec ts = {
        .tv_sec = abstime->sec,
        .tv_nsec = abstime->nsec,
    };
    CALL_PTHREAD_TIMED_FUNC(ETIMEDOUT, pthread_mutex_timedlock, mutex, &ts);
}

tao_status tao_mutex_timed_lock(
    tao_mutex* mutex,
    double secs)
{
    tao_time abstime;
    switch (tao_get_absolute_timeout(&abstime, secs)) {
    case TAO_TIMEOUT_PAST:
        return TAO_TIMEOUT;
    case TAO_TIMEOUT_NOW:
        return tao_mutex_try_lock(mutex);
    case TAO_TIMEOUT_FUTURE:
        return tao_mutex_abstimed_lock(mutex, &abstime);
    case TAO_TIMEOUT_NEVER:
        return tao_mutex_lock(mutex);
    default:
        return TAO_ERROR;
    }
}

tao_status tao_mutex_unlock(
    tao_mutex* mutex)
{
    CALL_PTHREAD_FUNC(pthread_mutex_unlock, mutex);
}

// Attempt to destroy the mutex, possibly blocking until the mutex is
// unlocked (signaled by pthread_mutex_destroy returning EBUSY).
tao_status tao_mutex_destroy(
    tao_mutex* mutex,
    bool wait)
{
    // Attempt to destroy the mutex, possibly blocking until the mutex is
    // unlocked (signaled by a pthread_mutex_destroy returning EBUSY).  In
    // order to not consume CPU, if the mutex was locked, we wait to become the
    // owner of the lock before re-trying to destroy the mutex.
    while (true) {
        // Attempt to destroy the mutex.
        int code = pthread_mutex_destroy(mutex);
        if (code == 0) {
            // Operation was successful.
            return TAO_OK;
        }
        if (!wait || code != EBUSY) {
            tao_store_error("pthread_mutex_destroy", code);
            return TAO_ERROR;
        }

        // The mutex is currently locked.  Wait for the owner of the lock to
        // unlock before re-trying to destroy the mutex.
        code = pthread_mutex_lock(mutex);
        if (code != 0) {
            tao_store_error("pthread_mutex_lock", code);
            return TAO_ERROR;
        }
        code = pthread_mutex_unlock(mutex);
        if (code != 0) {
            tao_store_error("pthread_mutex_unlock", code);
            return TAO_ERROR;
        }
    }
}

//-----------------------------------------------------------------------------
// CONDITION VARIABLES

tao_status tao_condition_initialize(
    tao_cond* cond,
    bool shared)
{
    pthread_condattr_t attr;
    int code, pshared;

    // Initialize attributes of condition variable.
    code = pthread_condattr_init(&attr);
    if (code != 0) {
        tao_store_error("pthread_condattr_init", code);
        return TAO_ERROR;
    }
    pshared = (shared ? TAO_PROCESS_SHARED : TAO_PROCESS_PRIVATE);
    code = pthread_condattr_setpshared(&attr, pshared);
    if (code != 0) {
        pthread_condattr_destroy(&attr);
        tao_store_error("pthread_condattr_setpshared", code);
        return TAO_ERROR;
    }

    // Initialize condition variable.
    code = pthread_cond_init(cond, &attr);
    if (code != 0) {
        tao_store_error("pthread_cond_init", code);
        return TAO_ERROR;
    }

    // Destroy condition variable attributes.
    code = pthread_condattr_destroy(&attr);
    if (code != 0) {
        tao_store_error("pthread_condattr_destroy", code);
        return TAO_ERROR;
    }

    // Report success.
    return TAO_OK;
}

tao_status tao_condition_destroy(
    tao_cond* cond)
{
    CALL_PTHREAD_FUNC(pthread_cond_destroy, cond);
}

tao_status tao_condition_signal(
    tao_cond* cond)
{
    CALL_PTHREAD_FUNC(pthread_cond_signal, cond);
}

tao_status tao_condition_broadcast(
    tao_cond* cond)
{
    CALL_PTHREAD_FUNC(pthread_cond_broadcast, cond);
}

tao_status tao_condition_wait(
    tao_cond* cond,
    tao_mutex* mutex)
{
    CALL_PTHREAD_FUNC(pthread_cond_wait, cond, mutex);
}

tao_status tao_condition_abstimed_wait(
    tao_cond* cond,
    tao_mutex* mutex,
    const tao_time* abstime)
{
    struct timespec ts = {
        .tv_sec = abstime->sec,
        .tv_nsec = abstime->nsec,
    };
    CALL_PTHREAD_TIMED_FUNC(ETIMEDOUT,
                            pthread_cond_timedwait, cond, mutex, &ts);
}

tao_status tao_condition_timed_wait(
    tao_cond* cond,
    tao_mutex* mutex,
    double secs)
{
    tao_time abstime;
    switch(tao_get_absolute_timeout(&abstime, secs)) {
    case TAO_TIMEOUT_PAST:
    case TAO_TIMEOUT_NOW:
        return TAO_TIMEOUT;
    case TAO_TIMEOUT_FUTURE:
        return tao_condition_abstimed_wait(cond, mutex, &abstime);
    case TAO_TIMEOUT_NEVER:
        return tao_condition_wait(cond, mutex);
    default:
        return TAO_ERROR;
    }
}

//-----------------------------------------------------------------------------
// R/W LOCKS

tao_status tao_rwlock_initialize(
    tao_rwlock* lock,
    bool shared)
{
    // Initialize attributes of r/w lock.
    pthread_rwlockattr_t attr;
    int code = pthread_rwlockattr_init(&attr);
    if (code != 0) {
        tao_store_error("pthread_rwlockattr_init", code);
        return TAO_ERROR;
    }
    int pshared = (shared ? TAO_PROCESS_SHARED : TAO_PROCESS_PRIVATE);
    code = pthread_rwlockattr_setpshared(&attr, pshared);
    if (code != 0) {
        pthread_rwlockattr_destroy(&attr);
        tao_store_error("pthread_rwlockattr_setpshared", code);
        return TAO_ERROR;
    }

    // Initialize r/w lock.
    code = pthread_rwlock_init(lock, &attr);
    if (code != 0) {
        tao_store_error("pthread_rwlock_init", code);
        return TAO_ERROR;
    }

    // Destroy r/w lock attributes.
    code = pthread_rwlockattr_destroy(&attr);
    if (code != 0) {
        tao_store_error("pthread_rwlockattr_destroy", code);
        return TAO_ERROR;
    }

    // Report success.
    return TAO_OK;
}

tao_status tao_rwlock_destroy(
    tao_rwlock* lock)
{
    CALL_PTHREAD_FUNC(pthread_rwlock_destroy, lock);
}

tao_status tao_rwlock_rdlock(
    tao_rwlock* lock)
{
    CALL_PTHREAD_FUNC(pthread_rwlock_rdlock, lock);
}

tao_status tao_rwlock_wrlock(
    tao_rwlock* lock)
{
    CALL_PTHREAD_FUNC(pthread_rwlock_wrlock, lock);
}

tao_status tao_rwlock_try_rdlock(
    tao_rwlock* lock)
{
    CALL_PTHREAD_TIMED_FUNC(EBUSY, pthread_rwlock_tryrdlock, lock);
}

tao_status tao_rwlock_try_wrlock(
    tao_rwlock* lock)
{
    CALL_PTHREAD_TIMED_FUNC(EBUSY, pthread_rwlock_trywrlock, lock);
}

tao_status tao_rwlock_abstimed_rdlock(
    tao_rwlock* lock,
    const tao_time* abstime)
{
    struct timespec ts = {
        .tv_sec = abstime->sec,
        .tv_nsec = abstime->nsec,
    };
    CALL_PTHREAD_TIMED_FUNC(ETIMEDOUT, pthread_rwlock_timedrdlock, lock, &ts);
}

tao_status tao_rwlock_abstimed_wrlock(
    tao_rwlock* lock,
    const tao_time* abstime)
{
    struct timespec ts = {
        .tv_sec = abstime->sec,
        .tv_nsec = abstime->nsec,
    };
    CALL_PTHREAD_TIMED_FUNC(ETIMEDOUT, pthread_rwlock_timedwrlock, lock, &ts);
}

tao_status tao_rwlock_unlock(
    tao_rwlock* lock)
{
    CALL_PTHREAD_FUNC(pthread_rwlock_unlock, lock);
}

tao_status tao_rwlock_timed_rdlock(
    tao_rwlock* lock,
    double secs)
{
    tao_time abstime;
    switch (tao_get_absolute_timeout(&abstime, secs)) {
    case TAO_TIMEOUT_PAST:
        return TAO_TIMEOUT;

    case TAO_TIMEOUT_NOW:
        return tao_rwlock_try_rdlock(lock);

    case TAO_TIMEOUT_FUTURE:
        return tao_rwlock_abstimed_rdlock(lock, &abstime);

    case TAO_TIMEOUT_NEVER:
        return tao_rwlock_rdlock(lock);

    default:
        return TAO_ERROR;
    }
}

tao_status tao_rwlock_timed_wrlock(
    tao_rwlock* lock,
    double secs)
{
    tao_time abstime;
    switch (tao_get_absolute_timeout(&abstime, secs)) {
    case TAO_TIMEOUT_PAST:
        return TAO_TIMEOUT;

    case TAO_TIMEOUT_NOW:
        return tao_rwlock_try_wrlock(lock);

    case TAO_TIMEOUT_FUTURE:
        return tao_rwlock_abstimed_wrlock(lock, &abstime);

    case TAO_TIMEOUT_NEVER:
        return tao_rwlock_wrlock(lock);

    default:
        return TAO_ERROR;
    }
}
