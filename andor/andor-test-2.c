// andor-test-2.c -
//
// Simple tests for Andor cameras library: list features.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2021, Éric Thiébaut.

#include <string.h>
#include <tao-andor-features.h>

int main(
    int argc,
    char* argv[])
{
    if (argc != 1) {
        fprintf(stderr, "Usage: %s\n", argv[0]);
        return EXIT_FAILURE;
    }
    const andor_feature_definition* defs =
        andor_feature_get_known_definitions();
    for (int i = 0; defs[i].wide_name != NULL; ++i) {
        const char* typename;
        switch (defs[i].type) {
        case ANDOR_FEATURE_BOOLEAN:    typename = "Boolean";    break;
        case ANDOR_FEATURE_INTEGER:    typename = "Integer";    break;
        case ANDOR_FEATURE_FLOAT:      typename = "Float";      break;
        case ANDOR_FEATURE_ENUMERATED: typename = "Enumerated"; break;
        case ANDOR_FEATURE_STRING:     typename = "String";     break;
        case ANDOR_FEATURE_COMMAND:    typename = "Command";    break;
        default: typename = NULL;
        }
        if (typename != NULL) {
            fprintf(stdout, "%-30ls %s\n", defs[i].wide_name, typename);
        }
    }
    return EXIT_SUCCESS;
}
