// andor-test-5.c -
//
// Simple tests for Andor cameras library: open a camera, list its features
// and acquire a bunch of images.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2022, Éric Thiébaut.

#include <string.h>
#include <time.h>
#include <tao-fits.h>
#include <tao-andor-private.h>
#include <tao-errors.h>

static double elapsed_seconds(
    const tao_time* t1,
    const tao_time* t0)
{
    tao_time dt;
    return tao_time_to_seconds(tao_subtract_times(&dt, t1, t0));
}
int main(
    int argc,
    char* argv[])
{
    tao_array* arr;
    tao_camera* cam;
    tao_camera_config cfg;
    long dev, ndevices, nimgs, imgsiz;
    uint16_t* imgptr;
    tao_time t0, t1;

    // Get number of devices (and initialize internals).
    ndevices = andor_get_ndevices();
    if (ndevices < 0) {
        tao_report_error();
        return EXIT_FAILURE;
    }
    if (argc == 1) {
        dev = 0;
    } else if (argc == 2) {
        if (tao_parse_long(argv[1], &dev, 0) != TAO_OK ||
            dev < 0 || dev >= ndevices) {
            fprintf(stderr, "Invalid device number \"%s\"\n", argv[1]);
            return EXIT_FAILURE;
        }
    } else {
        fprintf(stderr, "Usage: %s [dev]\n", argv[0]);
        return EXIT_FAILURE;
    }
    cam = andor_open_camera(dev);
    if (cam == NULL) {
        tao_report_error();
        return EXIT_FAILURE;
    }
    tao_camera_info_print(stdout, &cam->info);
    andor_print_supported_encodings(stdout, andor_get_device(cam));

    // Measure time taken for updating all the configuration and compare to
    // time taken to just retrieve the configuration.  Locking the camera may
    // be unecessary at this stage (there is only one thread aware of the
    // camera) but is good practice.
    tao_get_monotonic_time(&t0);
    tao_camera_lock(cam);
    tao_camera_update_configuration(cam);
    tao_camera_get_configuration(cam, &cfg);
    tao_get_monotonic_time(&t1);
    fprintf(stdout, "Time to update configuration: %.3f µs\n",
            1E6*elapsed_seconds(&t1, &t0));
    tao_get_monotonic_time(&t0);
    tao_camera_get_configuration(cam, &cfg);
    tao_get_monotonic_time(&t1);
    fprintf(stdout, "Time to retrieve configuration: %.3f µs\n",
            1E6*elapsed_seconds(&t1, &t0));

    // Get current configuration and change some parameters.
    tao_camera_get_configuration(cam, &cfg);
    //cfg.roi.xbin = 1;
    //cfg.roi.ybin = 1;
    //cfg.roi.xoff = 300;
    //cfg.roi.yoff = 200;
    //cfg.roi.width = 640;
    //cfg.roi.height = 480;
    cfg.framerate = 40.0;
    cfg.exposuretime = 0.005;
    cfg.nbufs = 4;
    //cfg.pixelencoding = ANDOR_ENCODING_MONO12PACKED;
    if (tao_camera_set_configuration(cam, &cfg) != TAO_OK) {
        tao_report_error();
        return EXIT_FAILURE;
    }
    fprintf(stdout, "\n");
    tao_camera_info_print(stdout, &cam->info);
    tao_camera_get_configuration(cam, &cfg);

    // Allocate an array to save some images.
    nimgs = 5;
    arr = tao_create_3d_array(TAO_INT16, cfg.roi.width, cfg.roi.height, nimgs);
    imgsiz = cfg.roi.width*cfg.roi.height;
    imgptr = (uint16_t*)tao_get_array_data(arr);

    // Start acquisition.
    tao_get_monotonic_time(&t0);
    if (tao_camera_start_acquisition(cam) != TAO_OK) {
        tao_report_error();
        return EXIT_FAILURE;
    }
    tao_camera_unlock(cam);
    tao_get_monotonic_time(&t1);
    fprintf(stdout, "Time to start acquisition: %.3f ms\n",
            1E3*elapsed_seconds(&t1, &t0));

    // Acquire some images.
    tao_camera_lock(cam);
    int j = 0;
    for (int k = 0; k < 100; ++k) {
        const tao_acquisition_buffer* buf;
        t0 = t1;
        tao_status status = tao_camera_wait_acquisition_buffer(cam, &buf, 0.1, 0);
        tao_get_monotonic_time(&t1);
        if (status == TAO_OK) {
            fprintf(stdout, "%3d: %10.3f ms\n", k+1,
                    1E3*elapsed_seconds(&t1, &t0));
            if (j < nimgs) {
                // Convert pixel values.  This takes some time, so we unlock
                // the camera during this operation.
                tao_camera_unlock(cam);
                andor_convert_buffer(imgptr + j*imgsiz,
                                     ANDOR_ENCODING_MONO16,
                                     buf->data,
                                     buf->encoding,
                                     cfg.roi.width,
                                     cfg.roi.height,
                                     buf->stride);
                tao_camera_lock(cam);
                ++j;
            }
            if (tao_camera_release_acquisition_buffer(cam) != TAO_OK) {
                tao_report_error();
                (void)tao_camera_stop_acquisition(cam);
                return EXIT_FAILURE;
            }
        } else if (status == TAO_TIMEOUT) {
            fprintf(stderr, "%3d: Timeout!\n", k+1);
        } else {
            tao_report_error();
            (void)tao_camera_stop_acquisition(cam);
            return EXIT_FAILURE;
        }
    }

    if (tao_camera_stop_acquisition(cam) != TAO_OK) {
        tao_report_error();
        return EXIT_FAILURE;
    }
    tao_camera_unlock(cam);

    tao_save_array_to_fits_file(arr, "/tmp/test-andor5.fits", true);
    tao_unreference_array(arr);
    tao_camera_destroy(cam);
    return EXIT_SUCCESS;
}
