// tao-andor-private.h -
//
// Private definitions for Andor cameras in TAO framework.  This header is the
// only one depending on header file <atcore.h> which is part of the Andor SDK
// (Software Development Kit).
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2022, Éric Thiébaut.

#ifndef TAO_ANDOR_PRIVATE_H_
#define TAO_ANDOR_PRIVATE_H_ 1

#include <tao-basics.h>
#include <tao-cameras-private.h>
#include <tao-andor-features.h>

#include <atcore.h>

TAO_BEGIN_DECLS

/**
 * @defgroup AndorPrivate  Private Andor API
 *
 * @ingroup AndorCameras
 *
 * @brief Private API for handling Andor cameras.
 *
 * @{
 */

//-----------------------------------------------------------------------------
// ERROR MANAGEMENT

/**
 * Store Andor SDK error.
 *
 * This functions stores Andor error information as the last TAO error of the
 * calling thread.
 *
 * @param func   The name of the function in Andor SDK that returned an error.
 * @param code   The code returned by the function.
 */
extern void andor_store_error(
    const char* func,
    int code);

/**
 * Retrieve message associated Andor SDK error code.
 *
 * @param code   The code returned by a function of the Andor SDK function.
 */
extern const char* andor_error_get_reason(
    int code);

/**
 * Retrieve the human readable name of Andor SDK error code.
 *
 * @param code   The code returned by a function of the Andor SDK function.
 */
extern const char* andor_error_get_name(
    int code);

//-----------------------------------------------------------------------------
/**
 * Structure to store acquisition buffer for an Andor camera.
 */
typedef struct andor_buffer andor_buffer;
struct andor_buffer {
    AT_U8* data;
    int    size;
};

/**
 * Andor camera device.
 *
 * The Andor camera class extends the TAO camera class and the `base` member
 * must come first.  The other members are specific to all Andor cameras.
 * Getting the type definition of the handle member is the only reason to
 * include `<atcore.h>` here.  The array of acquisition buffers is too track
 * malloc'ed buffers.
 */
struct andor_device {
    tao_camera    base;///< Unified TAO camera.
    AT_H        handle;///< Camera handle.
    tao_encoding  encs[ANDOR_MAX_ENCODINGS];///< Supported pixel encodings.
    int          nencs;///< Number of supported pixel encodings.
    andor_buffer *bufs;///< Array of acquisition buffers.
    int          nbufs;///< Number of acquisition buffers.
};

// All known features (i.e. found in Andor SDK Documentation) are summarized in
// the following table stored in the macro `ANDOR_FEATURES_`.  The idea is to
// (re)define the macro `ANDOR_FEATURE_` to format the table or extract
// whatever field is needed.  The `ANDOR_FEATURE_` macro is supposed to have as
// many arguments at the number of columns of the table:
//
//     #define ANDOR_FEATURE_(key, sys, sim, zyl, neo)
//
// The first column is the symbolic feature name, 2nd column is for the system
// handle, 3rd column is for the SimCam, 4th column is for the Zyla, and 5th
// column is for the Neo.  Other cameras will be added later.  The letters in
// the 2nd and subsequent columns gives the type of the feature for the
// considered camera:
//
//     B = Boolean
//     C = Command
//     I = Integer
//     F = Floating-point
//     E = Enumerated
//     S = String
//     X = Not implemented

#define ANDOR_FEATURES_                                     \
    ANDOR_FEATURE_(AccumulateCount,             X, X, I, I) \
    ANDOR_FEATURE_(AcquiredCount,               X, X, X, X) \
    ANDOR_FEATURE_(AcquisitionStart,            X, C, C, C) \
    ANDOR_FEATURE_(AcquisitionStop,             X, C, C, C) \
    ANDOR_FEATURE_(AlternatingReadoutDirection, X, X, B, X) \
    ANDOR_FEATURE_(AOIBinning,                  X, X, E, E) \
    ANDOR_FEATURE_(AOIHBin,                     X, I, I, X) \
    ANDOR_FEATURE_(AOIHeight,                   X, I, I, I) \
    ANDOR_FEATURE_(AOILayout,                   X, X, E, X) \
    ANDOR_FEATURE_(AOILeft,                     X, I, I, I) \
    ANDOR_FEATURE_(AOIStride,                   X, I, I, I) \
    ANDOR_FEATURE_(AOITop,                      X, I, I, I) \
    ANDOR_FEATURE_(AOIVBin,                     X, I, I, X) \
    ANDOR_FEATURE_(AOIWidth,                    X, I, I, I) \
    ANDOR_FEATURE_(AuxiliaryOutSource,          X, X, E, E) \
    ANDOR_FEATURE_(AuxOutSourceTwo,             X, X, E, X) \
    ANDOR_FEATURE_(BackoffTemperatureOffset,    X, X, X, X) \
    ANDOR_FEATURE_(Baseline,                    X, X, I, I) \
    ANDOR_FEATURE_(BitDepth,                    X, X, E, E) \
    ANDOR_FEATURE_(BufferOverflowEvent,         X, X, I, I) \
    ANDOR_FEATURE_(BytesPerPixel,               X, X, F, F) \
    ANDOR_FEATURE_(CameraAcquiring,             X, B, B, B) \
    ANDOR_FEATURE_(CameraFamily,                X, X, X, X) \
    ANDOR_FEATURE_(CameraMemory,                X, X, X, X) \
    ANDOR_FEATURE_(CameraModel,                 X, S, S, S) \
    ANDOR_FEATURE_(CameraName,                  X, X, S, S) \
    ANDOR_FEATURE_(CameraPresent,               X, X, B, B) \
    ANDOR_FEATURE_(ColourFilter,                X, X, X, X) \
    ANDOR_FEATURE_(ControllerID,                X, X, S, S) \
    ANDOR_FEATURE_(CoolerPower,                 X, X, F, X) \
    ANDOR_FEATURE_(CycleMode,                   X, E, E, E) \
    ANDOR_FEATURE_(DDGIOCEnable,                X, X, X, X) \
    ANDOR_FEATURE_(DDGIOCNumberOfPulses,        X, X, X, X) \
    ANDOR_FEATURE_(DDGIOCPeriod,                X, X, X, X) \
    ANDOR_FEATURE_(DDGOpticalWidthEnable,       X, X, X, X) \
    ANDOR_FEATURE_(DDGOutputDelay,              X, X, X, X) \
    ANDOR_FEATURE_(DDGOutputEnable,             X, X, X, X) \
    ANDOR_FEATURE_(DDGOutputPolarity,           X, X, X, X) \
    ANDOR_FEATURE_(DDGOutputSelector,           X, X, X, X) \
    ANDOR_FEATURE_(DDGOutputStepEnable,         X, X, X, X) \
    ANDOR_FEATURE_(DDGOutputWidth,              X, X, X, X) \
    ANDOR_FEATURE_(DDGStepCount,                X, X, X, X) \
    ANDOR_FEATURE_(DDGStepDelayCoefficientA,    X, X, X, X) \
    ANDOR_FEATURE_(DDGStepDelayCoefficientB,    X, X, X, X) \
    ANDOR_FEATURE_(DDGStepDelayMode,            X, X, X, X) \
    ANDOR_FEATURE_(DDGStepEnabled,              X, X, X, X) \
    ANDOR_FEATURE_(DDGStepUploadModeValues,     X, X, X, X) \
    ANDOR_FEATURE_(DDGStepUploadProgress,       X, X, X, X) \
    ANDOR_FEATURE_(DDGStepUploadRequired,       X, X, X, X) \
    ANDOR_FEATURE_(DDGStepWidthCoefficientA,    X, X, X, X) \
    ANDOR_FEATURE_(DDGStepWidthCoefficientB,    X, X, X, X) \
    ANDOR_FEATURE_(DDGStepWidthMode,            X, X, X, X) \
    ANDOR_FEATURE_(DDR2Type,                    X, X, X, X) \
    ANDOR_FEATURE_(DeviceCount,                 I, X, X, X) \
    ANDOR_FEATURE_(DeviceVideoIndex,            X, X, I, I) \
    ANDOR_FEATURE_(DisableShutter,              X, X, X, X) \
    ANDOR_FEATURE_(DriverVersion,               X, X, X, X) \
    ANDOR_FEATURE_(ElectronicShutteringMode,    X, E, E, E) \
    ANDOR_FEATURE_(EventEnable,                 X, X, B, B) \
    ANDOR_FEATURE_(EventSelector,               X, X, E, E) \
    ANDOR_FEATURE_(EventsMissedEvent,           X, X, I, I) \
    ANDOR_FEATURE_(ExposedPixelHeight,          X, X, I, X) \
    ANDOR_FEATURE_(ExposureEndEvent,            X, X, I, I) \
    ANDOR_FEATURE_(ExposureStartEvent,          X, X, I, I) \
    ANDOR_FEATURE_(ExposureTime,                X, F, F, F) \
    ANDOR_FEATURE_(ExternalIOReadout,           X, X, X, X) \
    ANDOR_FEATURE_(ExternalTriggerDelay,        X, X, F, X) \
    ANDOR_FEATURE_(FanSpeed,                    X, E, E, E) \
    ANDOR_FEATURE_(FastAOIFrameRateEnable,      X, X, B, B) \
    ANDOR_FEATURE_(FirmwareVersion,             X, X, S, S) \
    ANDOR_FEATURE_(ForceShutterOpen,            X, X, X, X) \
    ANDOR_FEATURE_(FrameCount,                  X, I, I, I) \
    ANDOR_FEATURE_(FrameGenFixedPixelValue,     X, X, X, X) \
    ANDOR_FEATURE_(FrameGenMode,                X, X, X, X) \
    ANDOR_FEATURE_(FrameIntervalTiming,         X, X, X, X) \
    ANDOR_FEATURE_(FrameInterval,               X, X, X, X) \
    ANDOR_FEATURE_(FrameRate,                   X, F, F, F) \
    ANDOR_FEATURE_(FullAOIControl,              X, X, B, B) \
    ANDOR_FEATURE_(GateMode,                    X, X, X, X) \
    ANDOR_FEATURE_(HeatSinkTemperature,         X, X, X, X) \
    ANDOR_FEATURE_(I2CAddress,                  X, X, X, X) \
    ANDOR_FEATURE_(I2CByteCount,                X, X, X, X) \
    ANDOR_FEATURE_(I2CByteSelector,             X, X, X, X) \
    ANDOR_FEATURE_(I2CByte,                     X, X, X, X) \
    ANDOR_FEATURE_(I2CRead,                     X, X, X, X) \
    ANDOR_FEATURE_(I2CWrite,                    X, X, X, X) \
    ANDOR_FEATURE_(ImageSizeBytes,              X, I, I, I) \
    ANDOR_FEATURE_(InputVoltage,                X, X, X, X) \
    ANDOR_FEATURE_(InsertionDelay,              X, X, X, X) \
    ANDOR_FEATURE_(InterfaceType,               X, X, S, S) \
    ANDOR_FEATURE_(IOControl,                   X, X, X, X) \
    ANDOR_FEATURE_(IODirection,                 X, X, X, X) \
    ANDOR_FEATURE_(IOInvert,                    X, X, B, B) \
    ANDOR_FEATURE_(IOSelector,                  X, X, E, E) \
    ANDOR_FEATURE_(IOState,                     X, X, X, X) \
    ANDOR_FEATURE_(IRPreFlashEnable,            X, X, X, X) \
    ANDOR_FEATURE_(KeepCleanEnable,             X, X, X, X) \
    ANDOR_FEATURE_(KeepCleanPostExposureEnable, X, X, X, X) \
    ANDOR_FEATURE_(LineScanSpeed,               X, X, F, F) \
    ANDOR_FEATURE_(LogLevel,                    X, E, E, E) \
    ANDOR_FEATURE_(LUTIndex,                    X, X, I, X) \
    ANDOR_FEATURE_(LUTValue,                    X, X, I, X) \
    ANDOR_FEATURE_(MaxInterfaceTransferRate,    X, X, F, F) \
    ANDOR_FEATURE_(MCPGain,                     X, X, X, X) \
    ANDOR_FEATURE_(MCPIntelligate,              X, X, X, X) \
    ANDOR_FEATURE_(MCPVoltage,                  X, X, X, X) \
    ANDOR_FEATURE_(MetadataEnable,              X, X, B, B) \
    ANDOR_FEATURE_(MetadataFrame,               X, X, B, B) \
    ANDOR_FEATURE_(MetadataTimestamp,           X, X, B, B) \
    ANDOR_FEATURE_(MicrocodeVersion,            X, X, X, S) \
    ANDOR_FEATURE_(MultitrackBinned,            X, X, B, B) \
    ANDOR_FEATURE_(MultitrackCount,             X, X, I, I) \
    ANDOR_FEATURE_(MultitrackEnd,               X, X, I, I) \
    ANDOR_FEATURE_(MultitrackSelector,          X, X, I, I) \
    ANDOR_FEATURE_(MultitrackStart,             X, X, I, I) \
    ANDOR_FEATURE_(Overlap,                     X, X, B, B) \
    ANDOR_FEATURE_(PIVEnable,                   X, X, X, X) \
    ANDOR_FEATURE_(PixelCorrection,             X, E, X, X) \
    ANDOR_FEATURE_(PixelEncoding,               X, E, E, E) \
    ANDOR_FEATURE_(PixelHeight,                 X, F, F, F) \
    ANDOR_FEATURE_(PixelReadoutRate,            X, E, E, E) \
    ANDOR_FEATURE_(PixelWidth,                  X, F, F, F) \
    ANDOR_FEATURE_(PortSelector,                X, X, X, X) \
    ANDOR_FEATURE_(PreAmpGain,                  X, E, X, E) \
    ANDOR_FEATURE_(PreAmpGainChannel,           X, E, X, E) \
    ANDOR_FEATURE_(PreAmpGainControl,           X, X, X, E) \
    ANDOR_FEATURE_(PreAmpGainSelector,          X, E, X, E) \
    ANDOR_FEATURE_(PreAmpGainValue,             X, X, X, X) \
    ANDOR_FEATURE_(PreAmpOffsetValue,           X, X, X, X) \
    ANDOR_FEATURE_(PreTriggerEnable,            X, X, X, X) \
    ANDOR_FEATURE_(ReadoutTime,                 X, X, F, F) \
    ANDOR_FEATURE_(RollingShutterGlobalClear,   X, X, B, B) \
    ANDOR_FEATURE_(RowNExposureEndEvent,        X, X, I, I) \
    ANDOR_FEATURE_(RowNExposureStartEvent,      X, X, I, I) \
    ANDOR_FEATURE_(RowReadTime,                 X, X, F, X) \
    ANDOR_FEATURE_(ScanSpeedControlEnable,      X, X, B, X) \
    ANDOR_FEATURE_(SensorCooling,               X, B, B, B) \
    ANDOR_FEATURE_(SensorHeight,                X, I, I, I) \
    ANDOR_FEATURE_(SensorModel,                 X, X, X, X) \
    ANDOR_FEATURE_(SensorReadoutMode,           X, X, E, X) \
    ANDOR_FEATURE_(SensorTemperature,           X, F, F, F) \
    ANDOR_FEATURE_(SensorType,                  X, X, X, X) \
    ANDOR_FEATURE_(SensorWidth,                 X, I, I, I) \
    ANDOR_FEATURE_(SerialNumber,                X, S, S, S) \
    ANDOR_FEATURE_(ShutterAmpControl,           X, X, X, X) \
    ANDOR_FEATURE_(ShutterMode,                 X, X, E, X) \
    ANDOR_FEATURE_(ShutterOutputMode,           X, X, E, X) \
    ANDOR_FEATURE_(ShutterState,                X, X, X, X) \
    ANDOR_FEATURE_(ShutterStrobePeriod,         X, X, X, X) \
    ANDOR_FEATURE_(ShutterStrobePosition,       X, X, X, X) \
    ANDOR_FEATURE_(ShutterTransferTime,         X, X, F, X) \
    ANDOR_FEATURE_(SimplePreAmpGainControl,     X, X, E, E) \
    ANDOR_FEATURE_(SoftwareTrigger,             X, X, C, C) \
    ANDOR_FEATURE_(SoftwareVersion,             S, X, X, S) \
    ANDOR_FEATURE_(SpuriousNoiseFilter,         X, X, B, B) \
    ANDOR_FEATURE_(StaticBlemishCorrection,     X, X, B, B) \
    ANDOR_FEATURE_(SynchronousTriggering,       X, B, X, X) \
    ANDOR_FEATURE_(TargetSensorTemperature,     X, F, X, F) \
    ANDOR_FEATURE_(TemperatureControl,          X, X, E, E) \
    ANDOR_FEATURE_(TemperatureStatus,           X, X, E, E) \
    ANDOR_FEATURE_(TimestampClock,              X, X, I, I) \
    ANDOR_FEATURE_(TimestampClockFrequency,     X, X, I, I) \
    ANDOR_FEATURE_(TimestampClockReset,         X, X, C, C) \
    ANDOR_FEATURE_(TransmitFrames,              X, X, X, X) \
    ANDOR_FEATURE_(TriggerMode,                 X, E, E, E) \
    ANDOR_FEATURE_(UsbDeviceId,                 X, X, X, X) \
    ANDOR_FEATURE_(UsbProductId,                X, X, X, X) \
    ANDOR_FEATURE_(VerticallyCentreAOI,         X, X, B, E)

#undef ANDOR_FEATURE_
#define ANDOR_FEATURE_(key, sys, sim, zyl, neo) key,
typedef enum andor_feature andor_feature;
enum andor_feature {ANDOR_FEATURES_};

#define ANDOR_NFEATURES (1 + VerticallyCentreAOI)

extern andor_feature_type andor_get_device_feature_type(
    andor_device* dev,
    andor_feature key,
    unsigned int* mode);

extern andor_feature_type andor_get_handle_feature_type(
    AT_H handle,
    const AT_WC* key,
    unsigned int* mode);

// Low-level getters and setters to correctly report errors.

extern int andor_is_implemented(
    AT_H handle,
    andor_feature key,
    bool* ptr,
    const char* info);

extern int andor_is_readable(
    AT_H handle,
    andor_feature key,
    bool* ptr,
    const char* info);

extern int andor_is_writable(
    AT_H handle,
    andor_feature key,
    bool* ptr,
    const char* info);

extern int andor_is_readonly(
    AT_H handle,
    andor_feature key,
    bool* ptr,
    const char* info);

extern int andor_set_boolean(
    AT_H handle,
    andor_feature key,
    bool val,
    const char* info);

extern int andor_get_boolean(
    AT_H handle,
    andor_feature key,
    bool* ptr,
    const char* info);

extern int andor_set_integer(
    AT_H handle,
    andor_feature key,
    long val,
    const char* info);

extern int andor_get_integer(
    AT_H handle,
    andor_feature key,
    long* ptr,
    const char* info);

extern int andor_get_integer_min(
    AT_H handle,
    andor_feature key,
    long* ptr,
    const char* info);

extern int andor_get_integer_max(
    AT_H handle,
    andor_feature key,
    long* ptr,
    const char* info);

extern int andor_set_float(
    AT_H handle,
    andor_feature key,
    double val,
    const char* info);

extern int andor_get_float(
    AT_H handle,
    andor_feature key,
    double* ptr,
    const char* info);

extern int andor_get_float_min(
    AT_H handle,
    andor_feature key,
    double* ptr,
    const char* info);

extern int andor_get_float_max(
    AT_H handle,
    andor_feature key,
    double* ptr,
    const char* info);

extern int andor_set_enum_index(
    AT_H handle,
    andor_feature key,
    int val,
    const char* info);

extern int andor_set_enum_string(
    AT_H handle,
    andor_feature key,
    const AT_WC* val,
    const char* info);

extern int andor_get_enum_index(
    AT_H handle,
    andor_feature key,
    int* ptr,
    const char* info);

extern int andor_get_enum_count(
    AT_H handle,
    andor_feature key,
    int* ptr,
    const char* info);

extern int andor_is_enum_index_available(
    AT_H handle,
    andor_feature key,
    int idx,
    bool* ptr,
    const char* info);

extern int andor_is_enum_index_implemented(
    AT_H handle,
    andor_feature key,
    int idx,
    bool* ptr,
    const char* info);

extern int andor_set_string(
    AT_H handle,
    andor_feature key,
    const AT_WC* val,
    const char* info);

extern int andor_get_string(
    AT_H handle,
    andor_feature key,
    AT_WC* str,
    long len,
    const char* info);

extern int andor_get_string_max_length(
    AT_H handle,
    andor_feature key,
    long* ptr,
    const char* info);

TAO_END_DECLS

// The following macros are needed when no `andor_device` structure is
// available.

#define ANDOR_GET_BOOLEAN0(handle, key, ptr) \
    andor_get_boolean(handle, key, ptr, "AT_GetBool("#key")")

#define ANDOR_GET_INTEGER0(handle, key, ptr) \
    andor_get_integer(handle, key, ptr, "AT_GetInt("#key")")

#define ANDOR_GET_FLOAT0(handle, key, ptr) \
    andor_get_float(handle, key, ptr, "AT_GetFloat("#key")")

#define ANDOR_GET_STRING_MAX_LENGTH0(handle, key, ptr) \
    andor_get_string_max_length(handle, key, ptr, \
                                 "AT_GetStringMaxLength("#key")")

#define ANDOR_GET_STRING0(handle, key, ptr, len) \
    andor_get_string(handle, key, ptr, len, "AT_GetString("#key")")


// The following macros work with a `andor_device` structure.

#define ANDOR_IS_IMPLEMENTED(dev, key, ptr)                     \
    andor_is_implemented(                                       \
        (dev)->handle, key, ptr, "AT_IsImplemented("#key")")

#define ANDOR_IS_READABLE(dev, key, ptr)                        \
    andor_is_readable(                                          \
        (dev)->handle, key, ptr, "AT_IsReadable("#key")")

#define ANDOR_IS_WRITABLE(dev, key, ptr)                        \
    andor_is_writable(                                          \
        (dev)->handle, key, ptr, "AT_IsWritable("#key")")

#define ANDOR_IS_READONLY(dev, key, ptr)                        \
    andor_is_readonly(                                          \
        (dev)->handle, key, ptr, "AT_IsReadOnly("#key")")

#define ANDOR_SET_BOOLEAN(dev, key, val)                \
    andor_set_boolean(                                  \
        (dev)->handle, key, val, "AT_SetBool("#key")")

#define ANDOR_GET_BOOLEAN(dev, key, ptr)                \
    andor_get_boolean(                                  \
        (dev)->handle, key, ptr, "AT_GetBool("#key")")

#define ANDOR_SET_INTEGER(dev, key, val)                \
    andor_set_integer(                                  \
        (dev)->handle, key, val, "AT_SetInt("#key")")

#define ANDOR_GET_INTEGER(dev, key, ptr)                \
    andor_get_integer(                                  \
        (dev)->handle, key, ptr, "AT_GetInt("#key")")

#define ANDOR_GET_INTEGER_MIN(dev, key, ptr)                    \
    andor_get_integer_min(                                      \
        (dev)->handle, key, ptr, "AT_GetIntMin("#key")")

#define ANDOR_GET_INTEGER_MAX(dev, key, ptr)                    \
    andor_get_integer_max(                                      \
        (dev)->handle, key, ptr, "AT_GetIntMax("#key")")

#define ANDOR_SET_FLOAT(dev, key, val)                  \
    andor_set_float(                                    \
        (dev)->handle, key, val, "AT_SetFloat("#key")")

#define ANDOR_GET_FLOAT(dev, key, ptr)                  \
    andor_get_float(                                    \
        (dev)->handle, key, ptr, "AT_GetFloat("#key")")

#define ANDOR_GET_FLOAT_MIN(dev, key, ptr)                      \
    andor_get_float_min(                                        \
        (dev)->handle, key, ptr, "AT_GetFloatMin("#key")")

#define ANDOR_GET_FLOAT_MAX(dev, key, ptr)                      \
    andor_get_float_max(                                        \
        (dev)->handle, key, ptr, "AT_GetFloatMax("#key")")

#define ANDOR_SET_ENUM_INDEX(dev, key, val)                     \
    andor_set_enum_index(                                       \
        (dev)->handle, key, val, "AT_SetEnumIndex("#key")")

#define ANDOR_SET_ENUM_STRING(dev, key, val)                    \
    andor_set_enum_string(                                      \
        (dev)->handle, key, val, "AT_SetEnumString("#key")")

#define ANDOR_GET_ENUM_INDEX(dev, key, ptr)                     \
    andor_get_enum_index(                                       \
        (dev)->handle, key, ptr, "AT_GetEnumIndex("#key")")

#define ANDOR_GET_ENUM_COUNT(dev, key, ptr)                     \
    andor_get_enum_count(                                       \
        (dev)->handle, key, ptr, "AT_GetEnumCount("#key")")

#define ANDOR_IS_ENUM_INDEX_AVAILABLE(dev, key, idx, ptr)       \
    andor_is_enum_index_available(                              \
        (dev)->handle, key, idx, ptr,                           \
        "AT_IsEnumIndexAvailable("#key")")

#define ANDOR_IS_ENUM_INDEX_IMPLEMENTED(dev, key, idx, ptr)     \
    andor_is_enum_index_implemented(                            \
        (dev)->handle, key, idx, ptr,                           \
        "AT_IsEnumIndexImplemented("#key")")

#define ANDOR_SET_STRING(dev, key, ptr)                         \
    andor_set_string(                                           \
        (dev)->handle, key, ptr, len, "AT_SetString("#key")")

#define ANDOR_GET_STRING(dev, key, ptr, len)                    \
    andor_get_string(                                           \
        (dev)->handle, key, ptr, len, "AT_GetString("#key")")

#define ANDOR_GET_STRING_MAX_LENGTH(dev, key, ptr)      \
    andor_get_string_max_length(                        \
        (dev)->handle, key, ptr,                        \
        "AT_GetStringMaxLength("#key")")

/**
 * @}
 */

#endif // TAO_ANDOR_PRIVATE_H_
