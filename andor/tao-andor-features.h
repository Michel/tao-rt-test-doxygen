// tao-andor-feature.h -
//
// Intermediate level definitions for Andor cameras in TAO framework.  This
// header provides access to Andor features.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2021, Éric Thiébaut.

#ifndef TAO_ANDOR_FEATURES_H_
#define TAO_ANDOR_FEATURES_H_ 1

#include <tao-andor.h>

#define andor_camera     andor_device

TAO_BEGIN_DECLS

/**
 * Maximum length of a feature name (counting the final null).
 */
#define ANDOR_FEATURE_MAXLEN 64

/**
 * Maximum length of a feature string value (counting the final null).
 */
#define ANDOR_FEATURE_STRING_MAXLEN 64

/**
 * Possible types for a feature of an Andor camera.
 */
typedef enum andor_feature_type {
    ANDOR_FEATURE_NOT_IMPLEMENTED = 0,
    ANDOR_FEATURE_BOOLEAN,
    ANDOR_FEATURE_INTEGER,
    ANDOR_FEATURE_FLOAT,
    ANDOR_FEATURE_ENUMERATED,
    ANDOR_FEATURE_STRING,
    ANDOR_FEATURE_COMMAND
} andor_feature_type;

// R/W flags are shifted so that they can be combined with the feature
// type in a small integer value.
#define ANDOR_FEATURE_READABLE    (1<<5)
#define ANDOR_FEATURE_WRITABLE    (1<<6)
#define ANDOR_FEATURE_TYPE_MASK   (ANDOR_FEATURE_READABLE - 1)

/**
 * Feature information.
 *
 * This structure collects information that can be obtained from a readable
 * feature.  This structure is typically instanciated by
 * andor_wide_feature_get_value() or andor_feature_get_value().
 */
typedef struct andor_feature_info {
    union {
        int64_t i; // Integer value, enumeration index, or string length.
        double  f; // Floating-point value.
        int     b; // Boolean value.
    } value;
    andor_feature_type type;
    unsigned int mode;
} andor_feature_info;

/**
 * Query information about a feature.
 *
 * This function is to query information about a feature specified as a
 * null-terminated string of wide characters (of type `wchar_t`).  call
 * andor_feature_get_info() if the feature name is a string of `char`
 * characters.
 *
 * This structure is typically instanciated by andor_wide_feature_get_value() or
 * andor_feature_get_value() according to the `type` field:
 *
 * This function returns the feature type and, if `ptr` is not `NULL`,
 * instanciates the structure pointed by `ptr` according of the feature type
 * which is also stored at `ptr->type`:
 *
 * - `ANDOR_FEATURE_NOT_IMPLEMENTED`: the feature is not implemented (or not
 *   available or not readable), `ptr->mode` is set to 0, and `ptr->value` is
 *   not set.
 *
 * - `ANDOR_FEATURE_BOOLEAN`: the feature is a boolean, its current value is
 *   `ptr->value.b`, and its access mode is `ptr->mode`.
 *
 * - `ANDOR_FEATURE_INTEGER`: the feature is an integer, its current value is
 *   `ptr->value.i`, and its access mode is `ptr->mode`.
 *
 * - `ANDOR_FEATURE_FLOAT`: the feature is a floating-point, its current value
 *   `ptr->value.f`, and its access mode is `ptr->mode`.
 *
 * - `ANDOR_FEATURE_ENUMERATED`: the feature is an enumeration, its current
 *   index value is `ptr->value.i`, and its access mode is `ptr->mode`.
 *
 * - `ANDOR_FEATURE_STRING`: the feature is a string, its length (including the
 *   final null) is `ptr->value.i`, and its access mode is `ptr->mode`.
 *
 * - `ANDOR_FEATURE_COMMAND`: the feature is a command, its access mode is
 *   `ptr->mode`, and `ptr->value` is not set.
 *
 * Note that the access mode is a bitwise combination of @ref
 * ANDOR_FEATURE_READABLE and/or @ref ANDOR_FEATURE_WRITABLE.
 *
 * @param cam   The Andor camera.  If `NULL`, Andor system is assumed.
 *
 * @param name  Name of the feature.
 *
 * @param ptr   Address to store information or `NULL` to just query the
 *              feature type.
 *
 * @return The type of the feature or @ref ANDOR_FEATURE_NOT_IMPLEMENTED if
 *         the feature is not implemented for the camera (or the system).
 */
extern andor_feature_type andor_wide_feature_get_info(
    const andor_camera* cam,
    const wchar_t* name,
    andor_feature_info* ptr);

/**
 * Query information about a feature.
 *
 * This function is similar to andor_wide_feature_get_info() except that the
 * feature name is a regular C string (a null-terminated array of `char`).
 *
 * @param cam   The Andor camera.  If `NULL`, Andor system is assumed.
 *
 * @param name  Name of the feature.
 *
 * @param ptr   Address to store information or `NULL` to just query the
 *              feature type.
 *
 * @return The type of the feature or @ref ANDOR_FEATURE_NOT_IMPLEMENTED if
 *         the feature is not implemented for the camera (or the system).
 */
extern andor_feature_type andor_feature_get_info(
    const andor_camera* cam,
    const char* name,
    andor_feature_info* ptr);

/**
 * Query the type and access mode of a feature.
 *
 * This function is to query the type and access mode of a feature given as a
 * null-terminated string of wide characters (of type `wchar_t`).  call
 * andor_feature_get_type() if the feature name is a string of `char`
 * characters.
 *
 * @param cam   The Andor camera.  If `NULL`, Andor system is assumed.
 *
 * @param name  Name of the feature.
 *
 * @param mode Address to store access mode.  If not `NULL`, the pointed value
 *              is set to a bitwise combination of @ref ANDOR_FEATURE_READABLE
 *              and/or @ref ANDOR_FEATURE_WRITABLE.
 *
 * @return The type of the feature or @ref ANDOR_FEATURE_NOT_IMPLEMENTED if
 *         the feature is not implemented for the camera (or the system).
 *
 * @see andor_wide_feature_get_info(), andor_feature_get_type().
 */
extern andor_feature_type andor_wide_feature_get_type(
    const andor_camera* cam,
    const wchar_t* name,
    unsigned int* mode);

/**
 * Query the type and access mode of a feature.
 *
 * This function is to query the type and access mode of a feature given as a
 * null-terminated string of characters (of type `wchar_t`).  call
 * andor_wide_feature_get_type() if the feature name is a string of `wchar_t`
 * characters.
 *
 * @param cam   The Andor camera.  If `NULL`, Andor system is assumed.
 *
 * @param name  Name of the feature.
 *
 * @param mode Address to store access mode.  If not `NULL`, the pointed value
 *              is set to a bitwise combination of @ref ANDOR_FEATURE_READABLE
 *              and/or @ref ANDOR_FEATURE_WRITABLE.
 *
 * @return The type of the feature or @ref ANDOR_FEATURE_NOT_IMPLEMENTED if
 *         the feature is not implemented for the camera (or the system).
 *
 * @see andor_feature_get_info(), andor_wide_feature_get_type().
 */
extern andor_feature_type andor_feature_get_type(
    const andor_camera* cam,
    const char* name,
    unsigned int* mode);

/**
 * Definition of a known feature.
 */
typedef struct andor_feature_definition {
    const wchar_t* wide_name;
    andor_feature_type type;
} andor_feature_definition;

/**
 * Query a table of all known features.
 */
extern const andor_feature_definition* andor_feature_get_known_definitions(
    void);

/**
 * Query whether a feature is implemented.
 */
extern tao_status andor_wide_feature_is_implemented(
    andor_camera* cam,
    const wchar_t* name,
    int* ptr);

/**
 * Query whether a feature is implemented.
 */
extern tao_status andor_feature_is_implemented(
    andor_camera* cam,
    const char* name,
    int* ptr);

/**
 * Query whether a feature is read-only.
 */
extern tao_status andor_wide_feature_is_read_only(
    andor_camera* cam,
    const wchar_t* name,
    int* ptr);

/**
 * Query whether a feature is read-only.
 */
extern tao_status andor_feature_is_read_only(
    andor_camera* cam,
    const char* name,
    int* ptr);

/**
 * Query whether a feature is readable.
 */
extern tao_status andor_wide_feature_is_readable(
    andor_camera* cam,
    const wchar_t* name,
    int* ptr);

/**
 * Query whether a feature is readable.
 */
extern tao_status andor_feature_is_readable(
    andor_camera* cam,
    const char* name,
    int* ptr);

// INTEGER FEATURES

/**
 * Set the value of an integer-valued feature.
 */
extern tao_status andor_wide_feature_set_integer(
    andor_camera* cam,
    const wchar_t* name,
    int64_t val);

/**
 * Set the value of an integer-valued feature.
 */
extern tao_status andor_feature_set_integer(
    andor_camera* cam,
    const char* name,
    int64_t val);

/**
 * Retrieve the value of an integer-valued feature.
 */
extern tao_status andor_wide_feature_get_integer(
    andor_camera* cam,
    const wchar_t* name,
    int64_t* ptr);

/**
 * Retrieve the value of an integer-valued feature.
 */
extern tao_status andor_feature_get_integer(
    andor_camera* cam,
    const char* name,
    int64_t* ptr);

/**
 * Retrieve the minimum value of an integer-valued feature.
 */
extern tao_status andor_wide_feature_get_integer_min(
    andor_camera* cam,
    const wchar_t* name,
    int64_t* ptr);

/**
 * Retrieve the minimum value of an integer-valued feature.
 */
extern tao_status andor_feature_get_integer_min(
    andor_camera* cam,
    const char* name,
    int64_t* ptr);

/**
 * Retrieve the maximum value of an integer-valued feature.
 */
extern tao_status andor_wide_feature_get_integer_max(
    andor_camera* cam,
    const wchar_t* name,
    int64_t* ptr);

/**
 * Retrieve the maximum value of an integer-valued feature.
 */
extern tao_status andor_feature_get_integer_max(
    andor_camera* cam,
    const char* name,
    int64_t* ptr);

// FLOATING-POINT FEATURES

/**
 * Set the value of a floating-point feature.
 */
extern tao_status andor_wide_feature_set_float(
    andor_camera* cam,
    const wchar_t* name,
    double val);

/**
 * Set the value of a floating-point feature.
 */
extern tao_status andor_feature_set_float(
    andor_camera* cam,
    const char* name,
    double val);

/**
 * Retrieve the value of a floating-point feature.
 */
extern tao_status andor_wide_feature_get_float(
    andor_camera* cam,
    const wchar_t* name,
    double* ptr);

/**
 * Retrieve the value of a floating-point feature.
 */
extern tao_status andor_feature_get_float(
    andor_camera* cam,
    const char* name,
    double* ptr);

/**
 * Retrieve the minimum value of a floating-point feature.
 */
extern tao_status andor_wide_feature_get_float_min(
    andor_camera* cam,
    const wchar_t* name,
    double* ptr);

/**
 * Retrieve the minimum value of a floating-point feature.
 */
extern tao_status andor_feature_get_float_min(
    andor_camera* cam,
    const char* name,
    double* ptr);

/**
 * Retrieve the maximum value of a floating-point feature.
 */
extern tao_status andor_wide_feature_get_float_max(
    andor_camera* cam,
    const wchar_t* name,
    double* ptr);

/**
 * Retrieve the maximum value of a floating-point feature.
 */
extern tao_status andor_feature_get_float_max(
    andor_camera* cam,
    const char* name,
    double* ptr);

// BOOLEAN FEATURES

/**
 * Set the value of a boolean feature.
 */
extern tao_status andor_wide_feature_set_boolean(
    andor_camera* cam,
    const wchar_t* name,
    int val);

/**
 * Set the value of a boolean feature.
 */
extern tao_status andor_feature_set_boolean(
    andor_camera* cam,
    const char* name,
    int val);

/**
 * Retrieve the value of a boolean feature.
 */
extern tao_status andor_wide_feature_get_boolean(
    andor_camera* cam,
    const wchar_t* name,
    int* ptr);

/**
 * Retrieve the value of a boolean feature.
 */
extern tao_status andor_feature_get_boolean(
    andor_camera* cam,
    const char* name,
    int* ptr);

// ENUMERATION FEATURES

/**
 * Set the index value of an enumerated feature.
 */
extern tao_status andor_wide_feature_set_enum_index(
    andor_camera* cam,
    const wchar_t* name,
    long index);

/**
 * Set the index value of an enumerated feature.
 */
extern tao_status andor_feature_set_enum_index(
    andor_camera* cam,
    const char* name,
    long val);

/**
 * Retrieve the index value of an enumerated feature.
 */
extern tao_status andor_wide_feature_get_enum_index(
    andor_camera* cam,
    const wchar_t* name,
    long* ptr);

/**
 * Retrieve the index value of an enumerated feature.
 */
extern tao_status andor_feature_get_enum_index(
    andor_camera* cam,
    const char* name,
    long* ptr);

/**
 * Retrieve the number of possible values of an enumerated feature.
 */
extern tao_status andor_wide_feature_get_enum_count(
    andor_camera* cam,
    const wchar_t* name,
    long* ptr);

/**
 * Retrieve the number of possible values of an enumerated feature.
 */
extern tao_status andor_feature_get_enum_count(
    andor_camera* cam,
    const char* name,
    long* ptr);

/**
 * Set the string value of an enumerated feature.
 */
extern tao_status andor_wide_feature_set_enum_string(
    andor_camera* cam,
    const wchar_t* name,
    const wchar_t* val);

/**
 * Set the string value of an enumerated feature.
 */
extern tao_status andor_feature_set_enum_string(
    andor_camera* cam,
    const char* name,
    const char* val);

/**
 * Retrieve the string value of a given index of an enumerated feature.
 */
extern tao_status andor_wide_feature_get_enum_string_by_index(
    andor_camera* cam,
    const wchar_t* name,
    long index,
    wchar_t* val,
    long len);

/**
 * Retrieve the string value of a given index of an enumerated feature.
 */
extern tao_status andor_feature_get_enum_string_by_index(
    andor_camera* cam,
    const char* name,
    long index,
    char* val,
    long len);

// STRING FEATURES

/**
 * Set the value of a string feature.
 */
extern tao_status andor_wide_feature_set_string(
    andor_camera* cam,
    const wchar_t* name,
    const wchar_t* val);

/**
 * Set the value of a string feature.
 */
extern tao_status andor_feature_set_string(
    andor_camera* cam,
    const char* name,
    const char* val);

/**
 * Retrieve the value of a string feature.
 */
extern tao_status andor_wide_feature_get_string(
    andor_camera* cam,
    const wchar_t* name,
    wchar_t* val,
    long len);

/**
 * Retrieve the value of a string feature.
 */
extern tao_status andor_feature_get_string(
    andor_camera* cam,
    const char* name,
    char* val,
    long len);

/**
 * Retrieve the maximum length of a string feature.
 */
extern tao_status andor_wide_feature_get_string_length(
    andor_camera* cam,
    const wchar_t* name,
    long* len);

/**
 * Retrieve the maximum length of a string feature.
 */
extern tao_status andor_feature_get_string_length(
    andor_camera* cam,
    const char* name,
    long* len);

TAO_END_DECLS

#endif // TAO_ANDOR_FEATURES_H_
