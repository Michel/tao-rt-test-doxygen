// andor-server.c -
//
// Image server for Andor cameras.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2022, Éric Thiébaut.

#include "tao-andor.h"
#include "tao-config.h"
#include "tao-errors.h"
#include "tao-servers-private.h"
#include "tao-cameras-private.h"
#include "tao-shared-cameras-private.h"
#include "tao-shared-arrays-private.h"

#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdarg.h>
#include <assert.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>

#define JOINABLE_WORKER 1

#define ERROR_DEBUGLEVEL   -1 // debug-level for errors
#define COMMAND_DEBUGLEVEL  1 // debug-level for commands
#define CONTROL_DEBUGLEVEL  2 // debug-level for control
#define MONITOR_DEBUGLEVEL  3 // debug-level for monitor
#define TICKING_DEBUGLEVEL  4 // debug-level for checking whether application is alive

// Global variables shared between the server and the processor threads.  These
// resources are "private" to the threads of this process.  The shared camera
// in the virtual frame-grabber is said to be "public" as it is shared with
// other processes.  Access to the "private" resources (including the camera
// device `dev`) is controlled by locking/unlocking the camera device `dev`,
// access to the "public" resources is controlled by locking/unlocking the
// shared camera `shmcam = tao_framegrabber_get_shared_camera(vfg)`.
//
// FIXME: it is important to qualify as "volatile" the variables that may
// change.
static char group[60] = {0};
static tao_camera* dev = NULL;
static tao_framegrabber* vfg = NULL;
static tao_shared_camera* shcam = NULL;
static volatile int debuglevel = 0; // FIXME: should be part of the monitor
static volatile bool drop = true; // FIXME: should be part of the monitor
static volatile bool quitting = false; // FIXME: should be part of the monitor
static volatile bool suspended = false; // FIXME: should be part of the monitor
static tao_status (*on_processing)(const tao_acquisition_buffer*) = NULL;
static long errors = 0; // number of errors FIXME: useful?
static const char* progname = NULL;

static tao_status init_callback(
    tao_server* srv,
    void* ctx);

static tao_status process_frame(
    const tao_acquisition_buffer* buf);

static void change_state(
    tao_state state);

static void wrlock_shared_camera(
    void);

static void rdlock_shared_camera(
    void);

static void unlock_shared_camera(
    void);

#if 0 // FIXME: not used yet
static int try_rdlock_shared_camera(void);
static int try_wrlock_shared_camera(void);
static int timed_rdlock_shared_camera(double secs);
static int timed_wrlock_shared_camera(double secs);
#endif

// Print formatted debug/error message.
static void debug(
    int level,
    const char* format,
    ...) TAO_FORMAT_PRINTF(2,3);

// Below are the commands that can be scheduled.  The policy is as follows: the
// master must lock the private resources to schedule a new command, and waits
// that command goes back to CMD_NONE.  QUIT is irreversible, after a START,
// any of STOP, ABORT and QUIT are possible.
typedef enum command {
    CMD_NONE = 0, // No commands
    CMD_START,    // Start acquisition
    CMD_STOP ,    // Stop acquisition
    CMD_ABORT,    // Abort acquisition
    CMD_QUIT,     // Acquisition thread must quit
} command;

// Other "private" resources are used to monitor the camera state (without
// having to lock the camera) and to send commands ("start', "stop", "abort" or
// "quit") to the worker and to wait for their results.
//
// The "worker" thread always lock/unlock the monitored information while
// owning the camera lock.
//
// When the "server" thread schedule a command, it waits for monitor.command to
// becomes CMD_NONE.
//
// If camera state is "acquiring", the worker thread owns the cmaera device;
// otherwise the server owns the camera device.

static struct {
    tao_mutex mutex; // Mutex to lock this structure.
    tao_cond   cond; // Condition variable to wait for.
} monitor = {TAO_MUTEX_INITIALIZER, TAO_COND_INITIALIZER};
static volatile tao_state camera_state = TAO_STATE_INITIALIZING;
static volatile enum {
    INITIALIZING,
    SLEEPING,
    WORKING,
    DONE
} worker_state = INITIALIZING;
static void lock_monitor(void);
static void unlock_monitor(void);
static void wait_monitor(void);

// Global variables for the on_return() callback.
static tao_server* srv = NULL;
static pthread_t worker = 0;

static void* run_worker(void* arg);

// Set commands.
static tao_send_callback on_get_accesspoint;
static tao_send_callback on_get_bufferencoding;
static tao_send_callback on_get_debuglevel;
static tao_send_callback on_get_drop;
static tao_send_callback on_get_exposuretime;
static tao_send_callback on_get_framerate;
static tao_send_callback on_get_height;
static tao_send_callback on_get_lastshmid;
static tao_send_callback on_get_nbufs;
static tao_send_callback on_get_nextshmid;
static tao_send_callback on_get_ping;
static tao_send_callback on_get_pixeltype;
static tao_send_callback on_get_roi;
static tao_send_callback on_get_sensorencoding;
static tao_send_callback on_get_sensorheight;
static tao_send_callback on_get_sensorwidth;
static tao_send_callback on_get_serial;
static tao_send_callback on_get_shmid;
static tao_send_callback on_get_state;
static tao_send_callback on_get_suspended;
static tao_send_callback on_get_temperature;
static tao_send_callback on_get_width;
static tao_send_callback on_get_xbin;
static tao_send_callback on_get_xoff;
static tao_send_callback on_get_ybin;
static tao_send_callback on_get_yoff;
static tao_server_command get_commands[] = {
    {"accesspoint",    on_get_accesspoint,    NULL, NULL, NULL},
    {"bufferencoding", on_get_bufferencoding, NULL, NULL, NULL},
    {"debuglevel",     on_get_debuglevel,     NULL, NULL, NULL},
    {"drop",           on_get_drop,           NULL, NULL, NULL},
    {"exposuretime",   on_get_exposuretime,   NULL, NULL, NULL},
    {"framerate",      on_get_framerate,      NULL, NULL, NULL},
    {"height",         on_get_height,         NULL, NULL, NULL},
    {"lastshmid",      on_get_lastshmid,      NULL, NULL, NULL},
    {"nbufs",          on_get_nbufs,          NULL, NULL, NULL},
    {"nextshmid",      on_get_nextshmid,      NULL, NULL, NULL},
    {"ping",           on_get_ping,           NULL, NULL, NULL},
    {"pixeltype",      on_get_pixeltype,      NULL, NULL, NULL},
    {"roi",            on_get_roi,            NULL, NULL, NULL},
    {"sensorencoding", on_get_sensorencoding, NULL, NULL, NULL},
    {"sensorheight",   on_get_sensorheight,   NULL, NULL, NULL},
    {"sensorwidth",    on_get_sensorwidth,    NULL, NULL, NULL},
    {"serial",         on_get_serial,         NULL, NULL, NULL},
    {"shmid",          on_get_shmid,          NULL, NULL, NULL},
    {"state",          on_get_state,          NULL, NULL, NULL},
    {"suspended",      on_get_suspended,      NULL, NULL, NULL},
    {"temperature",    on_get_temperature,    NULL, NULL, NULL},
    {"width",          on_get_width,          NULL, NULL, NULL},
    {"xbin",           on_get_xbin,           NULL, NULL, NULL},
    {"xoff",           on_get_xoff,           NULL, NULL, NULL},
    {"ybin",           on_get_ybin,           NULL, NULL, NULL},
    {"yoff",           on_get_yoff,           NULL, NULL, NULL},
    {NULL,             NULL,                  NULL, NULL, NULL},
};

// Get commands.
static tao_recv_callback on_set_abort;
static tao_recv_callback on_set_config;
static tao_recv_callback on_set_debuglevel;
static tao_recv_callback on_set_drop;
static tao_recv_callback on_set_quit;
static tao_recv_callback on_set_start;
static tao_recv_callback on_set_stop;
static tao_recv_callback on_set_suspend_or_resume;
static tao_server_command set_commands[] = {
    {"abort",      NULL, NULL, on_set_abort,             NULL},
    {"config",     NULL, NULL, on_set_config,            NULL},
    {"debuglevel", NULL, NULL, on_set_debuglevel,        NULL},
    {"drop",       NULL, NULL, on_set_drop,              NULL},
    {"quit",       NULL, NULL, on_set_quit,              NULL},
    {"resume",     NULL, NULL, on_set_suspend_or_resume, NULL},
    {"start",      NULL, NULL, on_set_start,             NULL},
    {"stop",       NULL, NULL, on_set_stop,              NULL},
    {"suspend",    NULL, NULL, on_set_suspend_or_resume, NULL},
    {NULL,         NULL, NULL, NULL,                     NULL},
};

// Other callbacks.
static void on_signal(
    int);

static void on_return(
    void);

int main(
    int argc,
    char* argv[])
{
    char accesspoint[64];
    tao_camera_config cfg;
    int code;
    int nframes = 20;
    int perms = 0660;
    int shmid;

    // Get name of program.
    progname = tao_basename(argv[0]);

    // Install exit and signal handlers.
    if (signal(SIGQUIT, on_signal) == SIG_ERR) {
        tao_store_system_error("signal(SIGQUIT, ...)");
        tao_report_error();
        return EXIT_FAILURE;
    }
    if (atexit(on_return) != 0) {
        tao_store_system_error("atexit");
        tao_report_error();
        return EXIT_FAILURE;
    }

    // Get the number of Andor devices.  This also initializes the library.
    long ndevices = andor_get_ndevices();
    if (ndevices < 1) {
        tao_report_error();
        return EXIT_FAILURE;
    }

    // Parse arguments.
    long devnum = 0;
    int nargs = 0; // number of parsed positional arguments
    for (int i = 1; i < argc; ++i) {
        if (nargs == 0 && argv[i][0] == '-') {
            if (strcmp(argv[i], "-debug") == 0) {
                debuglevel = 2;
                continue;
            }
            if (strcmp(argv[i], "--") == 0) {
                continue;
            }
        }
        if (nargs == 0) {
            if (tao_parse_long(argv[i], &devnum, 0) != TAO_OK ||
                devnum < 0 || devnum >= ndevices) {
                fprintf(stderr, "%s: Invalid device number %s\n",
                        progname, argv[i]);
                return EXIT_FAILURE;
            }
            ++nargs;
        } else {
            fprintf(stderr, "Usage: %s [-debug] [--] [dev]\n", progname);
            return EXIT_FAILURE;
        }
    }

    // Define accesspoint.
    sprintf(group, "Andor%ld", devnum);
    sprintf(accesspoint, "TAO:%s", group);
    if (debuglevel > 0) {
        fprintf(stderr, "%s: group=\"%s\", accesspoint=\"%s\"\n",
                progname, group, accesspoint);
    }

    // Open the camera device.  The camera state is set to "initializing" until
    // the worker thread has started.
    dev = andor_open_camera(devnum);
    if (dev == NULL) {
        fprintf(stderr, "%s: failed to open Andor camera device %ld\n",
                progname, devnum);
        tao_report_error();
        return EXIT_FAILURE;
    }
    if (debuglevel > 0) {
        fprintf(stderr, "%s: camera device %ld now open\n", progname, devnum);
    }

    // Retrieve initial configuration.  Make sure to synchronize the actual
    // configuration after any changes in case some parameters are not exactly
    // the requested ones.
    if (tao_camera_update_configuration(dev) != TAO_OK ||
        tao_camera_get_configuration(dev, &cfg) != TAO_OK) {
        tao_report_error();
        return EXIT_FAILURE;
    }
    if (debuglevel > 0) {
        // Apply the current configuration.  Should not change anything (this
        // is just for debugging purposes).
        if (tao_camera_set_configuration(dev, &cfg) != TAO_OK) {
            tao_report_error();
            return EXIT_FAILURE;
        }
        if (tao_camera_update_configuration(dev) != TAO_OK ||
            tao_camera_get_configuration(dev, &cfg) != TAO_OK) {
            tao_report_error();
            return EXIT_FAILURE;
        }
        fprintf(stderr, "%s: camera configuration has been retrieved\n",
                progname);
    }

    // Create the virtual frame-grabber and reflect the camera settings in the
    // "shared" resources.  Copy camera information from the camera device to
    // the shared camera, except that the shared camera state is set to
    // "sleeping" as it is not usable by clients until the worker thread has
    // effectively started.
    vfg = tao_framegrabber_create(accesspoint, nframes, perms);
    if (vfg == NULL) {
        fprintf(stderr, "%s: failed to create the virtual frame-grabber\n",
                progname);
        tao_report_error();
        return EXIT_FAILURE;
    }
    if (debuglevel > 0) {
        fprintf(stderr, "%s: virtual frame grabber created with %d buffers\n",
                progname, nframes);
    }
    shcam = tao_framegrabber_get_shared_camera(vfg);
    wrlock_shared_camera();
    (void)tao_camera_reflect_configuration(shcam, dev);
    shcam->info.state = TAO_STATE_INITIALIZING;
    unlock_shared_camera();
    if (tao_any_errors(NULL)) {
        tao_report_error();
        return EXIT_FAILURE;
    }
    if (debuglevel > 0) {
        fprintf(stderr, "%s: shared camera information has been updated\n",
                progname);
    }

    // Publish shmid of shared camera.
    shmid = tao_shared_camera_get_shmid(shcam);
    if (tao_config_write_long(group, shmid) != TAO_OK) {
        fprintf(stderr, "%s: Failed to publish my shmid.\n", progname);
        return EXIT_FAILURE;
    }
    fprintf(stderr, "%s: server XPA accesspoint is \"%s\"\n",
            progname, accesspoint);
    fprintf(stderr, "%s: server shimd (%d) has been written in \"%s/%s/shmid\"\n",
            progname, (int)shmid, TAO_CONFIG_DIR, group);

    // Create the server.
    srv = tao_create_server(accesspoint, "help!",
                            init_callback, NULL,
                            tao_serve_send_command, get_commands,
                            tao_serve_recv_command, set_commands);
    if (srv == NULL) {
        fprintf(stderr, "%s: failed to start XPA server %s\n",
                progname, accesspoint);
        tao_report_error();
        return EXIT_FAILURE;
    }
    if (debuglevel > 0) {
        fprintf(stderr, "%s: XPA server created\n", progname);
    }

    // Start the image processing thread and wait until it starts.
    tao_status status = TAO_OK;
    lock_monitor();
    on_processing = process_frame;
    camera_state = TAO_STATE_INITIALIZING;
    worker_state = INITIALIZING;
    code = pthread_create(&worker, NULL, run_worker, NULL);
    if (code != 0) {
        tao_store_error("pthread_create", code);
        tao_report_error();
        return EXIT_FAILURE;
    }
#if ! JOINABLE_WORKER
    code = pthread_detach(worker);
    if (code != 0) {
        tao_store_error("pthread_detach", code);
        tao_report_error();
        return EXIT_FAILURE;
    }
#endif
    while (!quitting && worker_state == INITIALIZING) {
        if (debuglevel > 0) {
            debug(CONTROL_DEBUGLEVEL, "Server: State is \"%s\"...",
                  tao_state_get_name(camera_state));
        }
        wait_monitor();
    }
    if (quitting || worker_state != SLEEPING) {
        status = TAO_ERROR;
    }
    if (status == TAO_OK) {
        change_state(TAO_STATE_WAITING);
    }
    unlock_monitor();
    if (status != TAO_OK) {
        fprintf(stderr, "%s: failed to start worker thread\n", progname);
        tao_report_error();
        return EXIT_FAILURE;
    }

    // Finally run the server.
    if (debuglevel > 0) {
        fprintf(stderr, "%s: now starting XPA server...\n", progname);
    }
    srv->debuglevel = debuglevel;
    int tic = 1;
    while (! quitting && status == TAO_OK) {
        double secs = 3.0;//0.5;
        tao_poll_requests(secs, 1);
        debug(TICKING_DEBUGLEVEL, "Server: %s", (tic?"tic":"tac"));
        tic = !tic;
        if (tao_any_errors(NULL)) {
            tao_report_error();
            status = TAO_ERROR;
        }
    }
    debug(CONTROL_DEBUGLEVEL, "Server: %s...", "Normal quit");

    // Return status.  The "on_return" callback will take care of releasing
    // resources.
    return (status == TAO_OK && errors == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}

static void on_return(
    void)
{
    // Write invalid shmid.
    if (group != NULL) {
        tao_config_write_long(group, -1L);
    }

    // Free all resources (more or less in reverse order compared to creation).
    // This function should only be called once.  But, in case of, we mark
    // resources as released to avoid errors such as freeing more than once.
    debug(CONTROL_DEBUGLEVEL, "Freeing resources...");
    quitting = true;
    if (srv != NULL) {
        // First kill the server.
        debug(CONTROL_DEBUGLEVEL, "Abort server...");
        tao_abort_server(srv);
        debug(CONTROL_DEBUGLEVEL, "Destroy server...");
        tao_destroy_server(srv);
        srv = NULL;
    }
    if (worker != 0) {
        // Wait for worker thread to finish before releasing other resources.
        debug(CONTROL_DEBUGLEVEL, "Cancel worker thread...");
        pthread_cancel(worker);
#if JOINABLE_WORKER
        debug(CONTROL_DEBUGLEVEL, "Join worker thread...");
        pthread_join(worker, NULL);
#endif
        worker = 0;
    }
    if (vfg != NULL) {
        // Destroy shared ressources after having indicating that the camera is
        // no longer reachable.
        debug(CONTROL_DEBUGLEVEL, "Finalize frame-grabber...");
        wrlock_shared_camera();
        shcam->info.state = TAO_STATE_KILLED;
        unlock_shared_camera();
        (void)tao_framegrabber_destroy(vfg);
        vfg = NULL;
    }
    if (dev != NULL) {
        // Finally close camera device.
        debug(CONTROL_DEBUGLEVEL, "Close camera device...");
        tao_camera_destroy(dev);
        debug(CONTROL_DEBUGLEVEL, "Camera device closed");
        dev = NULL;
    }
}

static void on_signal(
    int sig)
{
    char buf[100];
    char* signam;
    switch (sig) {
    case SIGINT:
        signam = "SIGINT";
        break;
    default:
        sprintf(buf, "%d", sig);
        signam = buf;
    }
    fprintf(stderr, "Signal %s received.  Exiting...\n", signam);
    exit(EXIT_FAILURE);
}

// Print message to standard error output, if allowed by current debug-level.
// If `level` is negative, the message is considered as an error.
static void debug(
    int level,
    const char* format,
    ...)
{
    static tao_mutex mutex = TAO_MUTEX_INITIALIZER;
    if (level <= debuglevel) {
        pthread_mutex_lock(&mutex);
        va_list args;
        fputs((level < 0 ? "ERROR: " : "DEBUG: "), stderr);
        va_start(args, format);
        vfprintf(stderr, format, args);
        va_end(args);
        fputs("\n", stderr);
        fflush(stderr);
        pthread_mutex_unlock(&mutex);
    }
}

// Yields error message corresponding to an invalid state.
static const char* invalid_state(
    tao_state state)
{
    switch (state) {
    case TAO_STATE_INITIALIZING:
        return "Camera has not been initialized";
    case TAO_STATE_WAITING:
        return "Acquisition has not been started";
    case TAO_STATE_STARTING:
        return "Acquisition is about to start";
    case TAO_STATE_WORKING:
        return "Acquisition is running";
    case TAO_STATE_STOPPING:
        return "Acquisition is about to stop";
    case TAO_STATE_ABORTING:
        return "Acquisition is about to abort";
    case TAO_STATE_KILLED:
        return "Camera has been closed";
    default:
        return "Camera in improper state";
    }
}

// These are the tasks that are currently been executed.  The worker thread,
// accounts for a new scheduled task when sleeping and a notifification is
// signaled or after every acquired frame (or timeout while waiting for a new
// frame).  When a new scheduled task is taken into account by the worker
// thread, it changes the value of its `state` parameter and when the task is
// completed,it changes the value of its `command` parameter.
#define STATE_INITIALIZING TAO_STATE_INITIALIZING
#define STATE_SLEEPING     TAO_STATE_WAITING
#define STATE_STARTING     TAO_STATE_STARTING
#define STATE_ACQUIRING    TAO_STATE_WORKING
#define STATE_STOPPING     TAO_STATE_STOPPING
#define STATE_ABORTING     TAO_STATE_ABORTING
#define STATE_FINISHED     TAO_STATE_KILLED

// Callback for processing acquisition buffers.
typedef void processor(
    void* data,
    tao_time* t,
    const void* buf,
    long siz);

#define KILO       1000
#define MEGA    1000000
#define GIGA 1000000000

static void wrlock_shared_camera(
    void)
{
    if (tao_shared_camera_wrlock(shcam) != TAO_OK) {
        tao_panic();
    }
}

static void rdlock_shared_camera(
    void)
{
    if (tao_shared_camera_rdlock(shcam) != TAO_OK) {
        tao_panic();
    }
}

static void unlock_shared_camera(
    void)
{
    if (tao_shared_camera_unlock(shcam) != TAO_OK) {
        tao_panic();
    }
}

#if 0 // FIXME: not used yet
static int try_rdlock_shared_camera(
    void)
{
    tao_status = tao_shared_camera_try_rdlock(shcam);
    if (status == TAO_OK) {
        return true;
    }
    if (status != TAO_TIMEOUT) {
        tao_panic();
    }
    return false;
}

static int try_wrlock_shared_camera(
    void)
{
    tao_status = tao_shared_camera_try_wrlock(shcam);
    if (status == TAO_OK) {
        return true;
    }
    if (status != TAO_TIMEOUT) {
        tao_panic();
    }
    return false;
}

static int timed_rdlock_shared_camera(
    double secs)
{
    tao_status = tao_shared_camera_timed_rdlock(shcam, secs);
    if (status == TAO_OK) {
        return true;
    }
    if (status != TAO_TIMEOUT) {
        tao_panic();
    }
    return false;
}

static int timed_wrlock_shared_camera(
    double secs)
{
    tao_status = tao_shared_camera_timed_wrlock(shcam, secs);
    if (status == TAO_OK) {
        return true;
    }
    if (status != TAO_TIMEOUT) {
        tao_panic();
    }
    return false;
}
#endif

//
// Unlocking and waiting on the monitor automatically signals the condition in
// case others were waiting on the condition while you are about to unlock.
//
static void lock_monitor(
    void)
{
    if (tao_mutex_lock(&monitor.mutex) != TAO_OK) {
        tao_panic();
    }
}

static void unlock_monitor(
    void)
{
    if (tao_condition_signal(&monitor.cond) != TAO_OK ||
        tao_mutex_unlock(&monitor.mutex) != TAO_OK) {
        tao_panic();
    }
}

static void wait_monitor(
    void)
{
    if (tao_condition_signal(&monitor.cond) != TAO_OK ||
        tao_condition_wait(&monitor.cond, &monitor.mutex) != TAO_OK) {
        tao_panic();
    }
}

static tao_status process_frame(
    const tao_acquisition_buffer* buf)
{
    // It is guaranteed that the configuration will not change while processing
    // the buffer, so we do not have to lock the private data while processing.
    long width = buf->width;
    long height = buf->height;
    long stride = buf->stride;
    tao_encoding bufenc = buf->encoding;
    const void* data = (unsigned char*)buf->data + buf->offset;
    tao_shared_array* arr = tao_framegrabber_get_buffer(vfg);
    tao_encoding arrenc;
    switch (TAO_SHARED_ARRAY_ELTYPE(arr)) {
    case TAO_UINT8:
    case TAO_INT8:
        arrenc = ANDOR_ENCODING_MONO8;
        break;
    case TAO_UINT16:
    case TAO_INT16:
        arrenc = ANDOR_ENCODING_MONO16;
        break;
    case TAO_INT32:
    case TAO_UINT32:
        arrenc = ANDOR_ENCODING_MONO32;
        break;
    case TAO_FLOAT:
        arrenc = ANDOR_ENCODING_FLOAT;
        break;
    case TAO_DOUBLE:
        arrenc = ANDOR_ENCODING_DOUBLE;
        break;
    default:
        arrenc = ANDOR_ENCODING_UNKNOWN;
    }

    if (arrenc != ANDOR_ENCODING_UNKNOWN &&
        TAO_SHARED_ARRAY_NDIMS(arr) >= 2 &&
        TAO_SHARED_ARRAY_DIM(arr, 1) == width &&
        TAO_SHARED_ARRAY_DIM(arr, 2) == height) {
        andor_convert_buffer(TAO_SHARED_ARRAY_DATA(arr), arrenc,
                             data, bufenc, width, height, stride);
        arr->ts[0] = buf->frame_start;
        arr->ts[1] = buf->frame_end;
        arr->ts[2] = buf->buffer_ready;
        wrlock_shared_camera();
        (void)tao_framegrabber_post_buffer(vfg);
        unlock_shared_camera();
    }
    return TAO_OK;
}

// Function called by the "worker" to change the state of the acquisition task
// and reflect it in the shared resources.  It is assumed that the monitor has
// been locked by the caller but not the shared resources (the shared camera).
// The other thread is notified of the change.
static void change_state(
    tao_state state)
{
    if (debuglevel > 0) {
        debug(COMMAND_DEBUGLEVEL, "Changing state from \"%s\" to \"%s\"...",
              tao_state_get_name(camera_state),
              tao_state_get_name(state));
    }
    camera_state = state;
    wrlock_shared_camera();
    shcam->info.state = state;
    unlock_shared_camera();
}

// This is the main function executed by the "worker" thread.
static void* run_worker(
    void* arg)
{
    // Lock the monitor and signal that the worker is waiting.
    lock_monitor();
    while (true) {
        // Wait until quitting or starting acquisition.
        while (camera_state != TAO_STATE_WORKING && !quitting) {
            worker_state = SLEEPING;
            wait_monitor();
        }
        if (quitting)  {
            break;
        }
        worker_state = WORKING;

        // Acquisition is running, we have the ownership of the camera.  Unlock
        // monitor while waiting for a new frame.  Even when no processing has
        // to be done, we still have to acquire and release acquisition buffers
        // to always process the most recent frames.  Time-out errors can be
        // ignored here, the timeout value is set to some sensible value, not
        // too long to not block when quitting and not too short to not
        // overload the CPU.  This timeout is thus unrelated to the frame rate
        // and exposure time.
        int drop = 1; // drop old frames
        double timeout = 0.5; // seconds
        const tao_acquisition_buffer* buf;
        tao_status status;
        unlock_monitor();
        status = tao_camera_wait_acquisition_buffer(dev, &buf, timeout, drop);
        lock_monitor();
        if (status == TAO_OK) {
            // Process the acquisition buffer unless aborting or suspended.
            // During the processing, unlock the monitor but keep camera
            // ownership until the acquisition buffer has been released.  The
            // address of the processing callback must be recorded before
            // unlocking the monitor.
            if (camera_state != TAO_STATE_ABORTING && !suspended && !quitting) {
                unlock_monitor();
                status = on_processing(buf);
                lock_monitor();
            }
            tao_camera_release_acquisition_buffer(dev);
        }
        if (status == TAO_ERROR) {
            // Report errors while owning the camera.  FIXME: We probably
            // should quit or stop and restart acquisition.
            tao_report_error();
        }
    }
    worker_state = DONE;
    unlock_monitor();
    return NULL;
}

// SERVER ROUTINES ------------------------------------------------------------

// Shortcuts.
#define set_static_message tao_set_static_reply_message
#define print_message      tao_format_reply_message

static tao_status init_callback(
    tao_server* srv,
    void* ctx)
{
    // FIXME: wrlock_shared_camera();
    // FIXME: tao__set_shared_object_accesspoint(
    // FIXME:         &shcam->base, tao_get_server_accesspoint(srv));
    // FIXME: unlock_shared_camera();
    return TAO_OK;
}

//-----------------------------------------------------------------------------
// GET CALLBACK
//
// The retrieved information transmitted by the server shall be the same as the
// shared infomation.  So the server the information from the shared camera.

// Set an "usage" error message in reply to an xpaget command.
static void send_usage(
    tao_server* srv,
    const char* argv0,
    const char* args)
{
    // We assume that the usage message is short enough and the format
    // specification valid so that no errors occur below.  This should be the
    // case as the arguments are controlled by the caller.
    if (args == NULL) {
        tao_format_reply_message(srv, "Usage: xpaget %s", argv0);
    } else {
        tao_format_reply_message(srv, "Usage: xpaget %s %s", argv0, args);
    }
}

static tao_status set_string_result(
    tao_server* srv,
    const char* str,
    void** bufptr,
    size_t* sizptr)
{
    if (tao_print_to_server_buffer(srv, "%s\n", str) != TAO_OK ||
        tao_set_reply_data_from_buffer(srv, bufptr, sizptr, false) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

static tao_status on_get_accesspoint(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void** bufptr,
    size_t* sizptr)
{
    if (argc != 1) {
        send_usage(srv, argv[0], NULL);
        return TAO_ERROR;
    }
    return set_string_result(srv, tao_get_server_accesspoint(srv),
                             bufptr, sizptr);
}

// Retrieve simple parameter from the shared camera.
#define GETTER(type, format, name, memb)                                       \
    static tao_status on_get_##name(                                         \
        tao_server* srv,                                                     \
        void* ctx,                                                             \
        int argc,                                                              \
        const char* argv[],                                                    \
        void** bufptr,                                                         \
        size_t* sizptr)                                                        \
    {                                                                          \
        if (argc != 1) {                                                       \
            send_usage(srv, argv[0], NULL);                                    \
            return TAO_ERROR;                                                  \
        }                                                                      \
        rdlock_shared_camera();                                                \
        type val = shcam->memb;                                                \
        unlock_shared_camera();                                                \
        if (tao_print_to_server_buffer(srv, format"\n", val) != TAO_OK) {      \
            return TAO_ERROR;                                                  \
        }                                                                      \
        return tao_set_reply_data_from_buffer(srv, bufptr, sizptr, false);     \
    }
GETTER(long long, "%lld", serial,       serial)
GETTER(long,       "%ld", lastshmid,    lastframe)
GETTER(long,       "%ld", nextshmid,    nextframe)
GETTER(long,       "%ld", sensorwidth,  info.sensorwidth)
GETTER(long,       "%ld", sensorheight, info.sensorheight)
GETTER(long,       "%ld", nbufs,        info.config.nbufs)
GETTER(long,       "%ld", xbin,         info.config.roi.xbin)
GETTER(long,       "%ld", ybin,         info.config.roi.ybin)
GETTER(long,       "%ld", xoff,         info.config.roi.xoff)
GETTER(long,       "%ld", yoff,         info.config.roi.yoff)
GETTER(long,       "%ld", width,        info.config.roi.width)
GETTER(long,       "%ld", height,       info.config.roi.height)
GETTER(double,      "%g", exposuretime, info.config.exposuretime)
GETTER(double,      "%g", framerate,    info.config.framerate)
GETTER(double,      "%g", temperature,  info.temperature)
#undef GETTER

// FIXME: use the camera to lock private resources.

static tao_status on_get_debuglevel(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void** bufptr,
    size_t* sizptr)
{
    if (argc != 1) {
        send_usage(srv, argv[0], NULL);
        return TAO_ERROR;
    }
    lock_monitor();
    int value = debuglevel;
    unlock_monitor();
    if (tao_print_to_server_buffer(srv, "%d\n", value) != TAO_OK) {
        return TAO_ERROR;
    }
    return tao_set_reply_data_from_buffer(srv, bufptr, sizptr, false);
}

static tao_status on_get_drop(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void** bufptr,
    size_t* sizptr)
{
    if (argc != 1) {
        send_usage(srv, argv[0], NULL);
        return TAO_ERROR;
    }
    lock_monitor();
    const char* str = drop ? "yes\n" : "no\n";
    unlock_monitor();
    return set_string_result(srv, str, bufptr, sizptr);
}

static tao_status on_get_ping(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void** bufptr,
    size_t* sizptr)
{
    char str[32];
    tao_time t;
    if (argc != 1) {
        send_usage(srv, argv[0], NULL);
        return TAO_ERROR;
    }
    if (tao_get_monotonic_time(&t) != TAO_OK) {
        return TAO_ERROR;
    }
    tao_sprintf_time(str, &t);
    strcat(str, "\n");
    return set_string_result(srv, str, bufptr, sizptr);
}

static tao_status on_get_roi(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void** bufptr,
    size_t* sizptr)
{
    if (argc != 1) {
        send_usage(srv, argv[0], NULL);
        return TAO_ERROR;
    }
    rdlock_shared_camera();
    tao_camera_roi roi = shcam->info.config.roi;
    unlock_shared_camera();
    if (tao_print_to_server_buffer(srv, "%ld %ld %ld %ld %ld %ld\n",
                                   roi.xbin, roi.ybin, roi.xoff, roi.yoff,
                                   roi.width, roi.height) != TAO_OK) {
        return TAO_ERROR;
    }
    return tao_set_reply_data_from_buffer(srv, bufptr, sizptr, false);
}

static tao_status on_get_shmid(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void** bufptr,
    size_t* sizptr)
{
    // No needs to lock "public" resources to get shmid as its value is
    // immutable.
    if (argc != 1) {
        send_usage(srv, argv[0], NULL);
        return TAO_ERROR;
    }
    tao_shmid shmid = tao_shared_camera_get_shmid(shcam);
    if (tao_print_to_server_buffer(srv, "%ld\n", (long)shmid) != TAO_OK) {
        return TAO_ERROR;
    }
    return tao_set_reply_data_from_buffer(srv, bufptr, sizptr, false);
}

static tao_status on_get_state(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void** bufptr,
    size_t* sizptr)
{
    if (argc != 1) {
        send_usage(srv, argv[0], NULL);
        return TAO_ERROR;
    }
    rdlock_shared_camera();
    tao_state state = shcam->info.state;
    unlock_shared_camera();
    if (tao_print_to_server_buffer(
            srv, "%s\n", tao_state_get_name(state)) != TAO_OK) {
        return TAO_ERROR;
    }
    return tao_set_reply_data_from_buffer(srv, bufptr, sizptr, false);
}

static tao_status on_get_suspended(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void** bufptr,
    size_t* sizptr)
{
    if (argc != 1) {
        send_usage(srv, argv[0], NULL);
        return TAO_ERROR;
    }
    lock_monitor();
    const char* str = (suspended ? "yes\n" : "no\n");
    unlock_monitor();
    return set_string_result(srv, str, bufptr, sizptr);
}

static tao_status get_encoding(
    tao_server* srv,
    tao_encoding enc,
    void** bufptr,
    size_t* sizptr)
{
    char buf[TAO_ENCODING_STRING_SIZE];
    if (tao_format_encoding(buf, enc) != TAO_OK) {
        strcpy(buf, "Unknown");
    }
    if (tao_print_to_server_buffer(srv, "%s\n", buf) != TAO_OK) {
        return TAO_ERROR;
    }
    return tao_set_reply_data_from_buffer(srv, bufptr, sizptr, false);
}

static tao_status on_get_pixeltype(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void** bufptr,
    size_t* sizptr)
{
    if (argc != 1) {
        send_usage(srv, argv[0], NULL);
        return TAO_ERROR;
    }
    rdlock_shared_camera();
    tao_encoding enc = tao_encoding_of_eltype(shcam->info.config.pixeltype);
    unlock_shared_camera();
    return get_encoding(srv, enc, bufptr, sizptr);
}

static tao_status on_get_sensorencoding(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void** bufptr,
    size_t* sizptr)
{
    if (argc != 1) {
        send_usage(srv, argv[0], NULL);
        return TAO_ERROR;
    }
    rdlock_shared_camera();
    tao_encoding enc = shcam->info.config.sensorencoding;
    unlock_shared_camera();
    return get_encoding(srv, enc, bufptr, sizptr);
}

static tao_status on_get_bufferencoding(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void** bufptr,
    size_t* sizptr)
{
    if (argc != 1) {
        send_usage(srv, argv[0], NULL);
        return TAO_ERROR;
    }
    rdlock_shared_camera();
    tao_encoding enc = shcam->info.config.bufferencoding;
    unlock_shared_camera();
    return get_encoding(srv, enc, bufptr, sizptr);
}

// SET CALLBACK ---------------------------------------------------------------

// Set an "usage" error message in reply to an xpaset command.
static void recv_usage(
    tao_server* srv,
    const char* argv0,
    const char* args)
{
    // We assume that the usage message is short enough and the format
    // specification valid so that no errors occur below.  This should be the
    // case as the arguments are controlled by the caller.
    if (args == NULL) {
        tao_format_reply_message(srv, "Usage: xpaset %s", argv0);
    } else {
        tao_format_reply_message(srv, "Usage: xpaset %s %s", argv0, args);
    }
}

static void recv_takes_no_data(
    tao_server* srv, const char* name)
{
    tao_format_reply_message(srv, "Expecting no data for xpaset "
                             "command \"%s\"", name);
}

static tao_status check_recv_args(
    tao_server* srv,
                int argc, const char* argv[],
                void* buf, size_t siz)
{
    if (siz > 0) {
        recv_takes_no_data(srv, argv[0]);
        return TAO_ERROR;
    }
    if (argc != 1) {
        recv_usage(srv, argv[0], NULL);
        return TAO_ERROR;
    }
    return TAO_OK;
}

// Caller (server/worker) shall own the camera.
static tao_state get_stable_state(
    tao_camera* dev,
    int reset)
{
    while (dev->runlevel == 3 && reset > 0) {
        // Try to reset.
        if (tao_camera_reset(dev) != TAO_OK) {
            tao_report_error();
        }
        --reset;
    }
    if (dev->runlevel == 0) {
        return TAO_STATE_INITIALIZING;
    } else if (dev->runlevel == 1) {
        return TAO_STATE_WAITING;
    } else if (dev->runlevel == 2) {
        return TAO_STATE_WORKING;
    } else {
        return TAO_STATE_KILLED;
    }
}

// This function is used by the server to handle "start", "stop" or "abort"
// commands.  The argument if one of the possible transient camera state.  The
// caller (server) shall have locked the monitor.
static void trigger_acquisition(
    tao_state transient)
{
    // Are we starting acquisition?
    bool starting = (transient == TAO_STATE_STARTING);

    // Maybe nothing to do.
    if (quitting || camera_state == transient ||
        camera_state == (starting ? TAO_STATE_WORKING :
                         TAO_STATE_WAITING)) {
        return;
    }

    // We need the ownership on the camera device to call its start/stop
    // commands.  Set the transient state and wait for the worker to stop
    // working.
    change_state(transient);
    while (!quitting && worker_state == WORKING) {
        wait_monitor();
    }
    if (quitting || worker_state != SLEEPING) {
        return;
    }

    // Start or stop acquisition by the camera device.  Update the camera state
    // to a stable state.
    tao_status status;
    if (starting) {
        status = tao_camera_start_acquisition(dev);
    } else {
        status = tao_camera_stop_acquisition(dev);
    }
    if (status == TAO_OK) {
        if (starting) {
            status = tao_framegrabber_start_acquisition(vfg);
        } else {
            status = tao_framegrabber_stop_acquisition(vfg);
        }
    }
    change_state(get_stable_state(dev, 1));
    if (status != TAO_OK) {
        tao_report_error();
        quitting = true;
        return;
    }

    // Wait for the worker thread to  to reach its expected state.
    while (!quitting && worker_state == (starting ? SLEEPING : WORKING)) {
        wait_monitor();
    }
}

static tao_status on_set_debuglevel(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void* buf,
    size_t siz)
{
    int value;
    if (siz > 0) {
        recv_takes_no_data(srv, argv[0]);
        return TAO_ERROR;
    }
    if (argc != 2) {
        recv_usage(srv, argv[0], "level");
        return TAO_ERROR;
    }
    if (tao_parse_int(argv[1], &value, 0) != TAO_OK || value < 0) {
        tao_set_reply_message(srv, "Invalid debug-level", -1);
        return TAO_ERROR;
    }
    lock_monitor();
    debuglevel = value;
    unlock_monitor();
    tao_set_server_debuglevel(srv, value);
    return TAO_OK;
}

static tao_status on_set_drop(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void* buf,
    size_t siz)
{
    bool value;
    if (siz > 0) {
        recv_takes_no_data(srv, argv[0]);
        return TAO_ERROR;
    }
    if (argc == 2 && strcmp(argv[1], "yes") == 0) {
        value = true;
    } else if (argc == 2 && strcmp(argv[1], "no") == 0) {
        value = false;
    } else {
        recv_usage(srv, argv[0], "yes|no");
        return TAO_ERROR;
    }
    lock_monitor();
    drop = value;
    unlock_monitor();
    return TAO_OK;
}

static tao_status on_set_suspend_or_resume(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void* buf,
    size_t siz)
{
    if (siz > 0) {
        recv_takes_no_data(srv, argv[0]);
        return TAO_ERROR;
    }
    if (argc != 1) {
        recv_usage(srv, argv[0], NULL);
        return TAO_ERROR;
    }
    lock_monitor();
    suspended = (argv[0][0] == 's');
    unlock_monitor();
    return TAO_OK;
}

static tao_status on_set_start(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void* buf,
    size_t siz)
{
    tao_status status = check_recv_args(srv, argc, argv, buf, siz);
    if (status == TAO_OK) {
        lock_monitor();
        trigger_acquisition(TAO_STATE_STARTING);
        if (camera_state == TAO_STATE_WORKING) {
            // If successful, starting acquisition resume acquisition.
            suspended = false;
        } else {
            set_static_message(srv, (camera_state == TAO_STATE_KILLED ?
                                     "Camera has been closed" :
                                     "Starting acquisition has failed"));
            status = TAO_ERROR;
        }
        unlock_monitor();
    }
    return status;
}

static tao_status on_set_stop(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void* buf,
    size_t siz)
{
    tao_status status = check_recv_args(srv, argc, argv, buf, siz);
    if (status == TAO_OK) {
        lock_monitor();
        trigger_acquisition(TAO_STATE_STOPPING);
        if (camera_state != TAO_STATE_WAITING) {
            set_static_message(srv, (camera_state == TAO_STATE_KILLED ?
                                     "Camera has been closed" :
                                     "Stopping acquisition has failed"));
            status = TAO_ERROR;
        }
        unlock_monitor();
    }
    return status;
}

static tao_status on_set_abort(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void* buf,
    size_t siz)
{
    tao_status status = check_recv_args(srv, argc, argv, buf, siz);
    if (status == TAO_OK) {
        lock_monitor();
        trigger_acquisition(TAO_STATE_ABORTING);
        if (camera_state != TAO_STATE_WAITING) {
            set_static_message(srv, (camera_state == TAO_STATE_KILLED ?
                                     "Camera has been closed" :
                                     "Aborting acquisition has failed"));
            status = TAO_ERROR;
        }
        unlock_monitor();
    }
    return status;
}

static tao_status on_set_quit(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void* buf,
    size_t siz)
{
    if (check_recv_args(srv, argc, argv, buf, siz) != TAO_OK) {
        return TAO_ERROR;
    }
    lock_monitor();
    trigger_acquisition(TAO_STATE_ABORTING);
    change_state(TAO_STATE_KILLED);
    quitting = true;
    unlock_monitor();
    return TAO_OK;
}


// Define some macros to deal with configuration options.
#define MARKED(memb) (*(unsigned char*)&mrk.memb != 0)
#define GETOPT(name, memb, expr)                \
    do {                                        \
        if (strcmp(key, name) == 0) {           \
            if (MARKED(memb)) {                 \
                goto duplicate_key;             \
            } else if (!(expr)) {               \
                goto bad_value;                 \
            } else {                            \
                *(unsigned char*)&mrk.memb = 1; \
                goto next_key;                  \
            }                                   \
        }                                       \
    } while (false)
#define SETOPT(memb)                                    \
    do {                                                \
        if (MARKED(memb) && req.memb != cfg.memb) {     \
            cfg.memb = req.memb;                        \
            change = true;                              \
        }                                               \
    } while (false)

static tao_status on_set_config(
    tao_server* srv,
    void* ctx,
    int argc,
    const char* argv[],
    void* buf,
    size_t siz)
{
    const char* key;
    const char* val;
    tao_camera_config req; // to store requested settings
    tao_camera_config mrk; // to mark requested settings
    tao_camera_config cfg; // to store settings

    if (siz > 0) {
        recv_takes_no_data(srv, argv[0]);
        return TAO_ERROR;
    }

    // There must be an odd number of arguments (the command plus any number of
    // key-value pairs).
    if ((argc & 1) != 1) {
        recv_usage(srv, argv[0], "key1 val1 [key2 val2 ...]");
        return TAO_ERROR;
    }

    // Clear the dummy `mrk` configuration which is used to keep track of
    // configured parameters and avoid duplicates.
    memset(&mrk, 0, sizeof(mrk));

    // Parse all settings.
    for (int i = 1; i < argc - 1; i += 2) {
        key = argv[i];
        val = argv[i+1];
        if (tao_get_server_debuglevel(srv) > 0) {
            fprintf(stderr, "key=%s, val=%s\n", key, val);
        }
        int c = key[0];
        if (c == 'b') {
            GETOPT("bufferencoding", bufferencoding,
                   (req.bufferencoding = tao_parse_encoding(val)) !=
                   TAO_ENCODING_UNKNOWN);
            GETOPT("nbufs", nbufs,
                   tao_parse_long(val, &req.nbufs, 10) == TAO_OK
                   && req.nbufs >= 2);
        } else if (c == 'e') {
            GETOPT("exposuretime", exposuretime,
                   tao_parse_double(val, &req.exposuretime) == TAO_OK
                   && req.exposuretime >= 0);
        } else if (c == 'f') {
            GETOPT("framerate", framerate,
                   tao_parse_double(val, &req.framerate) == TAO_OK
                   && req.framerate > 0);
        } else if (c == 'h') {
            GETOPT("height", roi.height,
                   tao_parse_long(val, &req.roi.height, 10) == TAO_OK
                   && req.roi.height >= 1);
        } else if (c == 'p') {
            GETOPT("pixeltype", pixeltype,
                   (req.pixeltype = tao_parse_encoding(val)) !=
                   TAO_ENCODING_UNKNOWN);
        } else if (c == 's') {
            GETOPT("sensorencoding", sensorencoding,
                   (req.sensorencoding = tao_parse_encoding(val)) !=
                   TAO_ENCODING_UNKNOWN);
        } else if (c == 'w') {
            GETOPT("width", roi.width,
                   tao_parse_long(val, &req.roi.width, 10) == TAO_OK
                   && req.roi.width >= 1);
        } else if (c == 'x') {
            GETOPT("xbin", roi.xbin,
                   tao_parse_long(val, &req.roi.xbin, 10) == TAO_OK
                   && req.roi.xbin >= 1);
            GETOPT("xoff", roi.xoff,
                   tao_parse_long(val, &req.roi.xoff, 10) == TAO_OK
                   && req.roi.xoff >= 0);
        } else if (c == 'y') {
            GETOPT("ybin", roi.ybin,
                   tao_parse_long(val, &req.roi.ybin, 10) == TAO_OK
                   && req.roi.ybin >= 1);
            GETOPT("yoff", roi.yoff,
                   tao_parse_long(val, &req.roi.yoff, 10) == TAO_OK
                   && req.roi.yoff >= 0);
        }
        print_message(srv, "Unknown key `%s`", key);
        return TAO_ERROR;

    next_key:
        continue;

    duplicate_key:
        print_message(srv, "Duplicate key `%s`", key);
        return TAO_ERROR;

    bad_value:
        print_message(srv, "Invalid value for key `%s`", key);
        return TAO_ERROR;
    }

    // NOTE: bufferencoding and sensorencoding are the same setting on Andor
    // cameras.
    if (MARKED(bufferencoding) && !MARKED(sensorencoding)) {
        *(unsigned char*)&mrk.sensorencoding = 1;
        req.sensorencoding = req.bufferencoding;
    } else if (!MARKED(bufferencoding) && MARKED(sensorencoding)) {
        *(unsigned char*)&mrk.bufferencoding = 1;
        req.bufferencoding = req.sensorencoding;
    } else if (MARKED(bufferencoding) && MARKED(sensorencoding)) {
        if (req.bufferencoding != req.sensorencoding) {
            set_static_message(srv, "bufferencoding and sensorencoding must be "
                               "set identically on Andor cameras");
            return TAO_ERROR;
        }
    }

    // Try to apply the changes.
    tao_status status = TAO_OK;
    lock_monitor(); do { // This loop is to unlock the monitor on return.

        // Acquisition must not be running or about to start.
        if (camera_state == TAO_STATE_WORKING ||
            camera_state == TAO_STATE_STARTING) {
            set_static_message(srv, invalid_state(camera_state));
            status = TAO_ERROR;
            break;
        }

        // Worker must not be busy.
        while (!quitting && worker_state == WORKING) {
            wait_monitor();
        }
        if (quitting) {
            set_static_message(srv, "Camera server is about to quit");
            change_state(TAO_STATE_KILLED);
            status = TAO_ERROR;
            break;
        }

        // We have the ownership of the camera device.  Get current
        // configuration, apply the changes, if any, make sure the
        // configuration is up-to-date and whatever could have been done,
        // reflect the actual configuration into the shared camera data.
        // FIXME: Any errors registered by the camera will be extracted as an
        // error message.
        status = tao_camera_update_configuration(dev);
        if (status == TAO_OK) {
            status = tao_camera_get_configuration(dev, &cfg);
        }
        if (status == TAO_OK) {
            bool change = false;
            SETOPT(bufferencoding);
            SETOPT(nbufs);
            SETOPT(framerate);
            SETOPT(exposuretime);
            SETOPT(pixeltype);
            SETOPT(roi.height);
            SETOPT(roi.width);
            SETOPT(roi.xbin);
            SETOPT(roi.xoff);
            SETOPT(roi.ybin);
            SETOPT(roi.yoff);
            SETOPT(sensorencoding);
            if (change && tao_camera_set_configuration(dev, &cfg) != TAO_OK) {
                status = TAO_ERROR;
            }
            wrlock_shared_camera();
            if (tao_camera_reflect_configuration(shcam, dev) != TAO_OK) {
                status = TAO_ERROR;
            }
            unlock_shared_camera();
        }
    } while (false);
    unlock_monitor();
    return status;
}

#undef GETOPT
#undef SETOPT
#undef IS_MARKED
