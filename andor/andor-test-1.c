// andor-test-1.c -
//
// Simple tests for Andor cameras library: initialize library and query number
// of available devices and software version.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2022, Éric Thiébaut.

#include <tao-andor.h>
#include <tao-errors.h>

int main(
    int argc,
    char* argv[])
{
    long ndevices = andor_get_ndevices();
    if (ndevices < 0) {
        tao_report_error();
        return EXIT_FAILURE;
    }
    fprintf(stdout, "%ld device(s) found\n", ndevices);
    fprintf(stdout, "Andor SDK Version: %s\n", andor_get_software_version());
    return EXIT_SUCCESS;
}
