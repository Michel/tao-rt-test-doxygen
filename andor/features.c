// features.c -
//
// Management of features for Andor cameras in TAO framework.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2021, Éric Thiébaut.

#include "tao-andor.h"
#include "tao-andor-features.h"
#include "tao-andor-private.h"

static inline AT_H get_handle(const andor_camera* cam)
{
    return (cam == NULL ? AT_HANDLE_SYSTEM : cam->handle);
}

andor_feature_type andor_wide_feature_get_info(
    const andor_camera* cam,
    const wchar_t* name,
    andor_feature_info* ptr)
{
    // If a feature is implemented, it is always at least readable.  Thus
    // either `AT_IsImplemented` or `AT_IsReadable` can be called to determine
    // whether a feature is implemented and readable.
    AT_BOOL bool_val;
    AT_H handle = get_handle(cam);
    andor_feature_type type = ANDOR_FEATURE_NOT_IMPLEMENTED;
    int code = AT_IsReadable(handle, name, &bool_val);
    if (code != AT_SUCCESS || bool_val == AT_FALSE) {
        if (ptr != NULL) {
            ptr->type = type;
            ptr->mode = 0;
        }
        return type;
    }

    // Retrieve r/w access mode.
    if (ptr != NULL) {
        code = AT_IsWritable(handle, name, &bool_val);
        if (code == AT_SUCCESS && bool_val != AT_FALSE) {
            ptr->mode = ANDOR_FEATURE_READABLE|ANDOR_FEATURE_WRITABLE;
        } else {
            ptr->mode = ANDOR_FEATURE_READABLE;
        }
    }

    // Determine type by trying all possibilities until one is successful.  If
    // all fail, assume a command.
    AT_64 int_val;
    double float_val;
    int index, length;
    if (AT_GetInt(handle, name, &int_val) == AT_SUCCESS) {
        type = ANDOR_FEATURE_INTEGER;
        if (ptr != NULL) {
            ptr->type = type;
            ptr->value.i = int_val;
        }
    } else if (AT_GetBool(handle, name, &bool_val) == AT_SUCCESS) {
        type = ANDOR_FEATURE_BOOLEAN;
        if (ptr != NULL) {
            ptr->type = type;
            ptr->value.b = (bool_val != AT_FALSE);
        }
    } else if (AT_GetEnumIndex(handle, name, &index) == AT_SUCCESS) {
        type = ANDOR_FEATURE_ENUMERATED;
        if (ptr != NULL) {
            ptr->type = type;
            ptr->value.i = index;
        }
    } else if (AT_GetFloat(handle, name, &float_val) == AT_SUCCESS) {
        type = ANDOR_FEATURE_FLOAT;
        if (ptr != NULL) {
            ptr->type = type;
            ptr->value.f = float_val;
        }
    } else if (AT_GetStringMaxLength(handle, name, &length) == AT_SUCCESS) {
        type = ANDOR_FEATURE_STRING;
        if (ptr != NULL) {
            ptr->type = type;
            ptr->value.i = length;
        }
    } else {
        type = ANDOR_FEATURE_COMMAND;
        if (ptr != NULL) {
            ptr->type = type;
        }
    }
    return type;
}

andor_feature_type andor_feature_get_info(
    const andor_camera* cam,
    const char* name,
    andor_feature_info* ptr)
{
    wchar_t wide_name[ANDOR_FEATURE_MAXLEN];
    for (int i = 0; ; ++i) {
        if (i >= ANDOR_FEATURE_MAXLEN) {
            if (ptr != NULL) {
                ptr->type = ANDOR_FEATURE_NOT_IMPLEMENTED;
                ptr->mode = 0;
            }
            return ANDOR_FEATURE_NOT_IMPLEMENTED;
        }
        if ((wide_name[i] = name[i]) == 0) {
            break;
        }
    }
    return andor_wide_feature_get_info(cam, wide_name, ptr);
}

andor_feature_type andor_wide_feature_get_type(
    const andor_camera* cam,
    const wchar_t* name,
    unsigned int* mode)
{
    andor_feature_info info;
    andor_feature_type type = andor_wide_feature_get_info(
        cam, name, (mode == NULL ? NULL : &info));
    if (mode != NULL) {
        *mode = info.mode;
    }
    return type;
}

andor_feature_type andor_feature_get_type(
    const andor_camera* cam,
    const char* name,
    unsigned int* mode)
{
    andor_feature_info info;
    andor_feature_type type = andor_feature_get_info(
        cam, name, (mode == NULL ? NULL : &info));
    if (mode != NULL) {
        *mode = info.mode;
    }
    return type;
}

static andor_feature_definition known_features[] = {
#define ITEM(a,b) {L ## #a, ANDOR_FEATURE_##b}
    ITEM(AccumulateCount              , INTEGER),
    ITEM(AcquisitionStart             , COMMAND),
    ITEM(AcquisitionStop              , COMMAND),
    ITEM(AlternatingReadoutDirection  , BOOLEAN),
    ITEM(AOIBinning                   , ENUMERATED),
    ITEM(AOIHBin                      , INTEGER),
    ITEM(AOIHeight                    , INTEGER),
    ITEM(AOILayout                    , ENUMERATED),
    ITEM(AOILeft                      , INTEGER),
    ITEM(AOIStride                    , INTEGER),
    ITEM(AOITop                       , INTEGER),
    ITEM(AOIVBin                      , INTEGER),
    ITEM(AOIWidth                     , INTEGER),
    ITEM(AuxiliaryOutSource           , ENUMERATED),
    ITEM(AuxOutSourceTwo              , ENUMERATED),
    ITEM(Baseline                     , INTEGER),
    ITEM(BitDepth                     , ENUMERATED),
    ITEM(BufferOverflowEvent          , INTEGER),
    ITEM(BytesPerPixel                , FLOAT),
    ITEM(CameraAcquiring              , BOOLEAN),
    ITEM(CameraModel                  , STRING),
    ITEM(CameraName                   , STRING),
    ITEM(CameraPresent                , BOOLEAN),
    ITEM(ControllerID                 , STRING),
    ITEM(CoolerPower                  , FLOAT),
    ITEM(CycleMode                    , ENUMERATED),
    ITEM(DeviceCount                  , INTEGER),
    ITEM(DeviceVideoIndex             , INTEGER),
    ITEM(ElectronicShutteringMode     , ENUMERATED),
    ITEM(EventEnable                  , BOOLEAN),
    ITEM(EventSelector                , ENUMERATED),
    ITEM(EventsMissedEvent            , INTEGER),
    ITEM(ExposedPixelHeight           , INTEGER),
    ITEM(ExposureEndEvent             , INTEGER),
    ITEM(ExposureStartEvent           , INTEGER),
    ITEM(ExposureTime                 , FLOAT),
    ITEM(ExternalTriggerDelay         , FLOAT),
    ITEM(FanSpeed                     , ENUMERATED),
    ITEM(FastAOIFrameRateEnable       , BOOLEAN),
    ITEM(FirmwareVersion              , STRING),
    ITEM(FrameCount                   , INTEGER),
    ITEM(FrameRate                    , FLOAT),
    ITEM(FullAOIControl               , BOOLEAN),
    ITEM(ImageSizeBytes               , INTEGER),
    ITEM(InterfaceType                , STRING),
    ITEM(IOInvert                     , BOOLEAN),
    ITEM(IOSelector                   , ENUMERATED),
    ITEM(LineScanSpeed                , FLOAT),
    ITEM(LogLevel                     , ENUMERATED),
    ITEM(LUTIndex                     , INTEGER),
    ITEM(LUTValue                     , INTEGER),
    ITEM(MaxInterfaceTransferRate     , FLOAT),
    ITEM(MetadataEnable               , BOOLEAN),
    ITEM(MetadataFrame                , BOOLEAN),
    ITEM(MetadataTimestamp            , BOOLEAN),
    ITEM(MicrocodeVersion             , STRING),
    ITEM(MultitrackBinned             , BOOLEAN),
    ITEM(MultitrackCount              , INTEGER),
    ITEM(MultitrackEnd                , INTEGER),
    ITEM(MultitrackSelector           , INTEGER),
    ITEM(MultitrackStart              , INTEGER),
    ITEM(Overlap                      , BOOLEAN),
    ITEM(PixelCorrection              , ENUMERATED),
    ITEM(PixelEncoding                , ENUMERATED),
    ITEM(PixelHeight                  , FLOAT),
    ITEM(PixelReadoutRate             , ENUMERATED),
    ITEM(PixelWidth                   , FLOAT),
    ITEM(PreAmpGain                   , ENUMERATED),
    ITEM(PreAmpGainChannel            , ENUMERATED),
    ITEM(PreAmpGainControl            , ENUMERATED),
    ITEM(PreAmpGainSelector           , ENUMERATED),
    ITEM(ReadoutTime                  , FLOAT),
    ITEM(RollingShutterGlobalClear    , BOOLEAN),
    ITEM(RowNExposureEndEvent         , INTEGER),
    ITEM(RowNExposureStartEvent       , INTEGER),
    ITEM(RowReadTime                  , FLOAT),
    ITEM(ScanSpeedControlEnable       , BOOLEAN),
    ITEM(SensorCooling                , BOOLEAN),
    ITEM(SensorHeight                 , INTEGER),
    ITEM(SensorReadoutMode            , ENUMERATED),
    ITEM(SensorTemperature            , FLOAT),
    ITEM(SensorWidth                  , INTEGER),
    ITEM(SerialNumber                 , STRING),
    ITEM(ShutterMode                  , ENUMERATED),
    ITEM(ShutterOutputMode            , ENUMERATED),
    ITEM(ShutterTransferTime          , FLOAT),
    ITEM(SimplePreAmpGainControl      , ENUMERATED),
    ITEM(SoftwareTrigger              , COMMAND),
    ITEM(SoftwareVersion              , STRING),
    ITEM(SpuriousNoiseFilter          , BOOLEAN),
    ITEM(StaticBlemishCorrection      , BOOLEAN),
    ITEM(SynchronousTriggering        , BOOLEAN),
    ITEM(TargetSensorTemperature      , FLOAT),
    ITEM(TemperatureControl           , ENUMERATED),
    ITEM(TemperatureStatus            , ENUMERATED),
    ITEM(TimestampClock               , INTEGER),
    ITEM(TimestampClockFrequency      , INTEGER),
    ITEM(TimestampClockReset          , COMMAND),
    ITEM(TriggerMode                  , ENUMERATED),
    ITEM(VerticallyCentreAOI          , BOOLEAN),
    {NULL, 0},
#undef ITEM
};

const andor_feature_definition* andor_feature_get_known_definitions(
    void)
{
    return known_features;
}

#define ENCODE(Fa, Fb)                                                  \
    tao_status andor_wide_feature_##Fa(                                 \
        andor_camera* cam,                                              \
        const wchar_t* name,                                            \
        int* ptr)                                                       \
    {                                                                   \
        AT_BOOL val;                                                    \
        int code = Fb(get_handle(cam), name, &val);                     \
        if (code != AT_SUCCESS) {                                       \
            andor_store_error(#Fb, code);                               \
            return TAO_ERROR;                                           \
        }                                                               \
        *ptr = (val != AT_FALSE);                                       \
        return TAO_OK;                                                  \
    }                                                                   \
    tao_status andor_feature_##Fa(                                      \
        andor_camera* cam,                                              \
        const char* name,                                               \
        int* ptr)                                                       \
    {                                                                   \
        if (name == NULL) {                                             \
            andor_store_error(#Fb, AT_ERR_NULL_FEATURE);                \
            return TAO_ERROR;                                           \
        }                                                               \
        wchar_t wide_name[ANDOR_FEATURE_MAXLEN];                        \
        for (int i = 0; i < ANDOR_FEATURE_MAXLEN; ++i) {                \
            if ((wide_name[i] = name[i]) == 0) {                        \
                return andor_wide_feature_##Fa(cam, wide_name, ptr);    \
            }                                                           \
        }                                                               \
        *ptr = 0;                                                       \
        return TAO_OK;                                                  \
    }
ENCODE(is_implemented, AT_IsImplemented);
ENCODE(is_read_only,   AT_IsReadOnly);
ENCODE(is_readable,    AT_IsReadable);
ENCODE(is_writable,    AT_IsWritable);
#undef ENCODE

tao_status andor_wide_feature_get_boolean(
    andor_camera* cam,
    const wchar_t* name,
    int* ptr)
{
    AT_BOOL val;
    int code = AT_GetBool(get_handle(cam), name, &val);
    if (code != AT_SUCCESS) {
        andor_store_error("AT_GetBool", code);
        return TAO_ERROR;
    }
    *ptr = (val != AT_FALSE);
    return TAO_OK;
}

tao_status andor_feature_get_boolean(
    andor_camera* cam,
    const char* name,
    int* ptr)
{
    int code = AT_SUCCESS;
    if (name == NULL) {
        code = AT_ERR_NULL_FEATURE;
        goto error;
    }
    wchar_t wide_name[ANDOR_FEATURE_MAXLEN];
    for (int i = 0; i < ANDOR_FEATURE_MAXLEN; ++i) {
        if (i >= ANDOR_FEATURE_MAXLEN) {
            code = AT_ERR_NOTIMPLEMENTED;
            goto error;
        }
        if ((wide_name[i] = name[i]) == 0) {
            break;
        }
    }
    return andor_wide_feature_get_boolean(cam, wide_name, ptr);
error:
    andor_store_error("AT_GetBool", code);
    return TAO_ERROR;
}

tao_status andor_wide_feature_set_boolean(
    andor_camera* cam,
    const wchar_t* name,
    int val)
{
    int code = AT_SetBool(get_handle(cam), name, (val ? AT_TRUE : AT_FALSE));
    if (code != AT_SUCCESS) {
        andor_store_error("AT_SetBool", code);
        return TAO_ERROR;
    }
    return TAO_OK;
}

tao_status andor_feature_set_boolean(
    andor_camera* cam,
    const char* name,
    int val)
{
    int code = AT_SUCCESS;
    if (name == NULL) {
        code = AT_ERR_NULL_FEATURE;
        goto error;
    }
    wchar_t wide_name[ANDOR_FEATURE_MAXLEN];
    for (int i = 0; ; ++i) {
        if (i >= ANDOR_FEATURE_MAXLEN) {
            code = AT_ERR_NOTIMPLEMENTED;
            goto error;
        }
        if ((wide_name[i] = name[i]) == 0) {
            break;
        }
    }
    return andor_wide_feature_set_boolean(cam, wide_name, val);
error:
    andor_store_error("AT_SetBool", code);
    return TAO_ERROR;
}

#define ENCODE(Ta, Fa, Tb, Fb)                                  \
    tao_status andor_wide_feature_##Fa(                         \
        andor_camera* cam,                                      \
        const wchar_t* name,                                    \
        Ta* ptr)                                                \
    {                                                           \
        Tb val;                                                 \
        int code = Fb(get_handle(cam), name, &val);             \
        if (code != AT_SUCCESS) {                               \
            andor_store_error(#Fb, code);                       \
            return TAO_ERROR;                                   \
        }                                                       \
        *ptr = val;                                             \
        return TAO_OK;                                          \
    }                                                           \
    tao_status andor_feature_##Fa(                              \
        andor_camera* cam,                                      \
        const char* name,                                       \
        Ta* ptr)                                                \
    {                                                           \
        int code = AT_SUCCESS;                                  \
        if (name == NULL) {                                     \
            code = AT_ERR_NULL_FEATURE;                         \
            goto error;                                         \
        }                                                       \
        wchar_t wide_name[ANDOR_FEATURE_MAXLEN];                \
        for (int i = 0; ; ++i) {                                \
            if (i >= ANDOR_FEATURE_MAXLEN) {                    \
                code = AT_ERR_NOTIMPLEMENTED;                   \
                goto error;                                     \
            }                                                   \
            if ((wide_name[i] = name[i]) == 0) {                \
                break;                                          \
            }                                                   \
        }                                                       \
        return andor_wide_feature_##Fa(cam, wide_name, ptr);    \
    error:                                                      \
        andor_store_error(#Fb, code);                           \
        return TAO_ERROR;                                       \
    }
ENCODE(int64_t, get_integer,        AT_64,  AT_GetInt);
ENCODE(int64_t, get_integer_min,    AT_64,  AT_GetIntMin);
ENCODE(int64_t, get_integer_max,    AT_64,  AT_GetIntMax);
ENCODE(double,  get_float,          double, AT_GetFloat);
ENCODE(double,  get_float_min,      double, AT_GetFloatMin);
ENCODE(double,  get_float_max,      double, AT_GetFloatMax);
ENCODE(long,    get_enum_index,     int,    AT_GetEnumIndex);
ENCODE(long,    get_enum_count,     int,    AT_GetEnumCount);
ENCODE(long,    get_string_length,  int,    AT_GetStringMaxLength);
#undef ENCODE

#define ENCODE(Ta, Fa, Tb, Fb)                                  \
    tao_status andor_wide_feature_##Fa(                         \
        andor_camera* cam,                                      \
        const wchar_t* name,                                    \
        Ta val)                                                 \
    {                                                           \
        int code = Fb(get_handle(cam), name, val);              \
        if (code != AT_SUCCESS) {                               \
            andor_store_error(#Fb, code);                       \
            return TAO_ERROR;                                   \
        }                                                       \
        return TAO_OK;                                          \
    }                                                           \
    tao_status andor_feature_##Fa(                              \
        andor_camera* cam,                                      \
        const char* name,                                       \
        Ta val)                                                 \
    {                                                           \
        int code = AT_SUCCESS;                                  \
        if (name == NULL) {                                     \
            code = AT_ERR_NULL_FEATURE;                         \
            goto error;                                         \
        }                                                       \
        wchar_t wide_name[ANDOR_FEATURE_MAXLEN];                \
        for (int i = 0; ; ++i) {                                \
            if (i >= ANDOR_FEATURE_MAXLEN) {                    \
                code = AT_ERR_NOTIMPLEMENTED;                   \
                goto error;                                     \
            }                                                   \
            if ((wide_name[i] = name[i]) == 0) {                \
                break;                                          \
            }                                                   \
        }                                                       \
        return andor_wide_feature_##Fa(cam, wide_name, val);    \
    error:                                                      \
        andor_store_error(#Fb, code);                           \
        return TAO_ERROR;                                       \
    }
ENCODE(int64_t, set_integer,    AT_64,  AT_SetInt);
ENCODE(double,  set_float,      double, AT_SetFloat);
ENCODE(long,    set_enum_index, int,    AT_SetEnumIndex);
#undef ENCODE

#define ENCODE(Fa, Fb)                                                  \
    tao_status andor_wide_feature_##Fa(                                 \
        andor_camera* cam,                                              \
        const wchar_t* name,                                            \
        const wchar_t* val)                                             \
    {                                                                   \
        int code = Fb(get_handle(cam), name, val);                      \
        if (code != AT_SUCCESS) {                                       \
            andor_store_error(#Fb, code);                               \
            return TAO_ERROR;                                           \
        }                                                               \
        return TAO_OK;                                                  \
    }                                                                   \
    tao_status andor_feature_##Fa(                                      \
        andor_camera* cam,                                              \
        const char* name,                                               \
        const char* val)                                                \
    {                                                                   \
        int code = AT_SUCCESS;                                          \
        if (name == NULL) {                                             \
            code = AT_ERR_NULL_FEATURE;                                 \
            goto error;                                                 \
        }                                                               \
        if (val == NULL) {                                              \
            code = AT_ERR_NULL_VALUE;                                   \
            goto error;                                                 \
        }                                                               \
        wchar_t wide_name[ANDOR_FEATURE_MAXLEN];                        \
        for (int i = 0; ; ++i) {                                        \
            if (i >= ANDOR_FEATURE_MAXLEN) {                            \
                code = AT_ERR_NOTIMPLEMENTED;                           \
                goto error;                                             \
            }                                                           \
            if ((wide_name[i] = name[i]) == 0) {                        \
                break;                                                  \
            }                                                           \
        }                                                               \
        wchar_t wide_val[ANDOR_FEATURE_STRING_MAXLEN];                  \
        for (int i = 0; ; ++i) {                                        \
            if (i >= ANDOR_FEATURE_STRING_MAXLEN) {                     \
                code = AT_ERR_STRINGNOTAVAILABLE;                       \
                goto error;                                             \
            }                                                           \
            if ((wide_val[i] = val[i]) == 0) {                          \
                break;                                                  \
            }                                                           \
        }                                                               \
        return andor_wide_feature_##Fa(cam, wide_name, wide_val);       \
    error:                                                              \
        andor_store_error(#Fb, code);                                   \
        return TAO_ERROR;                                               \
    }
ENCODE(set_string, AT_SetString);
ENCODE(set_enum_string, AT_SetEnumString);
#undef ENCODE

tao_status andor_wide_feature_get_string(
    andor_camera* cam,
    const wchar_t* name,
    wchar_t* val,
    long len)
{
    int code = AT_GetString(get_handle(cam), name, val, len);
    if (code != AT_SUCCESS) {
        andor_store_error("AT_GetString", code);
        return TAO_ERROR;
    }
    return TAO_OK;
}

tao_status andor_feature_get_string(
    andor_camera* cam,
    const char* name,
    char* val,
    long len)
{
    int code = AT_SUCCESS;
    if (name == NULL) {
        code = AT_ERR_NULL_FEATURE;
        goto error;
    }
    if (val == NULL || len < 1) {
        code = AT_ERR_NULL_VALUE;
        goto error;
    }
    wchar_t wide_name[ANDOR_FEATURE_MAXLEN];
    for (int i = 0; ; ++i) {
        if (i >= ANDOR_FEATURE_MAXLEN) {
            code = AT_ERR_NOTIMPLEMENTED;
            goto error;
        }
        if ((wide_name[i] = name[i]) == 0) {
            break;
        }
    }
    wchar_t wide_val[ANDOR_FEATURE_STRING_MAXLEN];
    tao_status status = andor_wide_feature_get_string(
        cam, wide_name, wide_val, ANDOR_FEATURE_STRING_MAXLEN);
    if (status != TAO_OK) {
        return TAO_ERROR;
    }
    wide_val[ANDOR_FEATURE_STRING_MAXLEN - 1] = 0;
    for (int i = 0; ; ++i) {
        if (i >= len) {
            val[len - 1] = 0;
            break;
        }
        if ((val[i] = (char)wide_val[i]) == 0) {
            break;
        }
    }
    return TAO_OK;
error:
    andor_store_error("AT_GetString", code);
    return TAO_ERROR;
}

#define ENCODE(Fa, Fb)                                                  \
    tao_status andor_wide_feature_##Fa(                                 \
        andor_camera* cam,                                              \
        const wchar_t* name,                                            \
        long index,                                                     \
        int* ptr)                                                       \
    {                                                                   \
        AT_BOOL val;                                                    \
        int code = Fb(get_handle(cam), name, index, &val);              \
        if (code != AT_SUCCESS) {                                       \
            andor_store_error(#Fb, code);                               \
            return TAO_ERROR;                                           \
        }                                                               \
        *ptr = (val != AT_FALSE);                                       \
        return TAO_OK;                                                  \
    }                                                                   \
    tao_status andor_feature_##Fa(                                      \
        andor_camera* cam,                                              \
        const char* name,                                               \
        long index,                                                     \
        int* ptr)                                                       \
    {                                                                   \
        int code = AT_SUCCESS;                                          \
        if (name == NULL) {                                             \
            andor_store_error(#Fb, code);                               \
            return TAO_ERROR;                                           \
        }                                                               \
        wchar_t wide_name[ANDOR_FEATURE_MAXLEN];                        \
        for (int i = 0; ; ++i) {                                        \
            if (i >= ANDOR_FEATURE_MAXLEN) {                            \
                *ptr = 0;                                               \
                return TAO_OK;                                          \
            }                                                           \
            if ((wide_name[i] = name[i]) == 0) {                        \
                break;                                                  \
            }                                                           \
        }                                                               \
        return andor_wide_feature_##Fa(cam, wide_name, index, ptr);     \
    }
ENCODE(is_enum_index_available, AT_IsEnumIndexAvailable);
ENCODE(is_enum_index_implemented, AT_IsEnumIndexImplemented);
#undef ENCODE

tao_status andor_wide_feature_get_enum_string_by_index(
    andor_camera* cam,
    const wchar_t* name,
    long index,
    wchar_t* val,
    long len)
{
    int code = AT_GetEnumStringByIndex(
        get_handle(cam), name, index, val, len);
    if (code != AT_SUCCESS) {
        andor_store_error("AT_GetEnumStringByIndex", code);
        return TAO_ERROR;
    }
    return TAO_OK;
}

tao_status andor_feature_get_enum_string_by_index(
    andor_camera* cam,
    const char* name,
    long index,
    char* val,
    long len)
{
    int code = AT_SUCCESS;
    if (name == NULL) {
        code = AT_ERR_NULL_FEATURE;
        goto error;
    }
    if (val == NULL || len < 1) {
        code = AT_ERR_NULL_VALUE;
        goto error;
    }
    wchar_t wide_name[ANDOR_FEATURE_MAXLEN];
    for (int i = 0; ; ++i) {
        if (i >= ANDOR_FEATURE_MAXLEN) {
            code = AT_ERR_NOTIMPLEMENTED;
            goto error;
        }
        if ((wide_name[i] = name[i]) == 0) {
            break;
        }
    }
    wchar_t wide_val[ANDOR_FEATURE_STRING_MAXLEN];
    tao_status status = andor_wide_feature_get_enum_string_by_index(
        cam, wide_name, index, wide_val, ANDOR_FEATURE_STRING_MAXLEN);
    if (status != TAO_OK) {
        return TAO_ERROR;
    }
    wide_val[ANDOR_FEATURE_STRING_MAXLEN - 1] = 0;
    for (int i = 0; ; ++i) {
        if (i >= len) {
            val[len - 1] = 0;
            break;
        }
        if ((val[i] = (char)wide_val[i]) == 0) {
            break;
        }
    }
    return TAO_OK;
error:
    andor_store_error("AT_GetEnumStringByIndex", code);
    return TAO_ERROR;
}
