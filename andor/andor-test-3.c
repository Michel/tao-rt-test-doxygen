// andor-test-3.c -
//
// Simple tests for Andor cameras library: open a camera and list its features.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2022, Éric Thiébaut.

#include <string.h>
#include <time.h>
#include <tao-andor-private.h>
#include <tao-errors.h>

static double elapsed_seconds(
    const tao_time* t1,
    const tao_time* t0)
{
    tao_time dt;
    return tao_time_to_seconds(tao_subtract_times(&dt, t1, t0));
}

int main(
    int argc,
    char* argv[])
{
    tao_camera* cam;
    tao_camera_config cfg;
    long dev, ndevices;
    char* end;
    tao_time t0, t1;

    ndevices = andor_get_ndevices();
    if (ndevices < 0) {
        tao_report_error();
        return EXIT_FAILURE;
    }
    if (argc == 1) {
        dev = 0;
    } else if (argc == 2) {
        dev = strtol(argv[1], &end, 0);
        if (end == argv[1] || *end != '\0' || dev < 0 || dev >= ndevices) {
            fprintf(stderr, "Invalid device number \"%s\"\n", argv[1]);
            return EXIT_FAILURE;
        }
    } else {
        fprintf(stderr, "Usage: %s [dev]\n", argv[0]);
        return EXIT_FAILURE;
    }
    cam = andor_open_camera(dev);
    if (cam == NULL) {
        tao_report_error();
        return EXIT_FAILURE;
    }
    tao_status status = TAO_OK;
    if (status == TAO_OK) {
        status = tao_camera_info_print(stdout, &cam->info);
    }
    if (status == TAO_OK) {
        status = andor_print_supported_encodings(stdout, andor_get_device(cam));
    }

    // Measure time taken for updating all the configuration and compare to
    // time taken to just retrieve the configuration.
    if (status == TAO_OK) {
        status = tao_get_monotonic_time(&t0);
    }
    if (status == TAO_OK) {
        status = tao_camera_update_configuration(cam);
    }
    if (status == TAO_OK) {
        status = tao_camera_get_configuration(cam, &cfg);
    }
    if (status == TAO_OK) {
        status = tao_get_monotonic_time(&t1);
    }
    if (status == TAO_OK) {
        fprintf(stdout, "Time to update configuration: %.3f µs\n",
                1E6*elapsed_seconds(&t1, &t0));
    }
    if (status == TAO_OK) {
        status = tao_get_monotonic_time(&t0);
    }
    if (status == TAO_OK) {
        status = tao_camera_get_configuration(cam, &cfg);
    }
    if (status == TAO_OK) {
        status = tao_get_monotonic_time(&t1);
    }
    if (status == TAO_OK) {
        fprintf(stdout, "Time to retrieve configuration: %.3f µs\n",
                1E6*elapsed_seconds(&t1, &t0));
    }
    if (tao_camera_destroy(cam) != TAO_OK) {
        status = TAO_ERROR;
    }
    if (status != TAO_OK) {
        tao_report_error();
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
