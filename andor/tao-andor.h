// tao-andor.h --
//
// Definitions for Andor cameras in TAO.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2021, Éric Thiébaut.

#ifndef TAO_ANDOR_H_
#define TAO_ANDOR_H_ 1

#include <tao-cameras.h>

#include <wchar.h>
#include <limits.h>

TAO_BEGIN_DECLS

/**
 * @defgroup AndorPublic Public Andor API
 *
 * @ingroup AndorCameras
 *
 * @brief Public API for handling Andor cameras.
 *
 * @{
 */

/**
 * Open an Andor camera.
 *
 * This function opens an Andor camera and returns a high level TAO camera
 * instance.
 *
 * @param dev    Device number to which the camera is connected.
 *
 * @return A valid address of a TAO camera on success, `NULL` on error.
 */
extern tao_camera* andor_open_camera(
    long dev);

/**
 * Initialize Andor cameras internals.
 *
 * This function initializes the Andor Core Library, retrieves the number of
 * available devices and manages to automatically finalize the Andor Core
 * Library at the exit of the program.
 *
 * Calling this function more than once has no effects and the functions
 * provided by the TAO interface to Andor cameras are able to automatically
 * call this function.
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure.
 */
extern tao_status andor_initialize(
    void);

/**
 * Get the number of available devices.
 *
 * This function yields the number of available Andor Camera devices.
 * If the Andor Core Library has not been initialized, andor_initialize()
 * is automatically called.
 *
 * @return The number of available devices, `-1L` on error.
 */
extern long andor_get_ndevices(
    void);

/**
 * Get the version of the Andor SDK.
 *
 * This function never fails, in case of errors, the returned version number is
 * `"0.0.0"` and error messages are printed (nothing is left as the last TAO
 * error of the calling thread).
 *
 * @return A string.
 */
const char* andor_get_software_version(
    void);

/**
 * Opaque structure to Andor camera device.
 *
 * A valid pointer to such a structure can always be re-cast as a
 * pointer to a @ref tao_camera structure.
 */
typedef struct andor_device andor_device;

/**
 * Get the Andor device associated with a TAO camera.
 *
 * @param cam   The address of a TAO camera.
 *
 * @return The address of the Andor device (a simple recast of @b cam) or
 *         `NULL` if @b cam is not an Andor camera.
 */
extern andor_device* andor_get_device(
    tao_camera* cam);

/**
 * Print the list of supported encodings by an Andor camera.
 *
 * @param out   The output file stream where to print.
 * @param dev   The address of the Andor device.
 *
 * @return @ref TAO_OK on success; @ref TAO_ERROR in case of failure.
 *
 * @see andor_get_device().
 */
extern tao_status andor_print_supported_encodings(
    FILE* out,
    const andor_device* dev);

/**
 * The various pixel encodings.
 */
#define ANDOR_ENCODING_UNKNOWN              TAO_ENCODING_UNKNOWN
#define ANDOR_ENCODING_MONO8                TAO_ENCODING_ANDOR_MONO8
#define ANDOR_ENCODING_MONO12               TAO_ENCODING_ANDOR_MONO12
#define ANDOR_ENCODING_MONO12CODED          TAO_ENCODING_ANDOR_MONO12CODED
#define ANDOR_ENCODING_MONO12CODEDPACKED    TAO_ENCODING_ANDOR_MONO12CODEDPACKED
#define ANDOR_ENCODING_MONO12PACKED         TAO_ENCODING_ANDOR_MONO12PACKED
#define ANDOR_ENCODING_MONO16               TAO_ENCODING_ANDOR_MONO16
#define ANDOR_ENCODING_MONO22PACKEDPARALLEL TAO_ENCODING_ANDOR_MONO22PACKEDPARALLEL
#define ANDOR_ENCODING_MONO22PARALLEL       TAO_ENCODING_ANDOR_MONO22PARALLEL
#define ANDOR_ENCODING_MONO32               TAO_ENCODING_ANDOR_MONO32
#define ANDOR_ENCODING_RGB8PACKED           TAO_ENCODING_ANDOR_RGB8PACKED
#define ANDOR_ENCODING_FLOAT                TAO_ENCODING_FLOAT(CHAR_BIT*sizeof(float))
#define ANDOR_ENCODING_DOUBLE               TAO_ENCODING_FLOAT(CHAR_BIT*sizeof(double))

/**
 * The maximum number of pixel encodings.
 */
#define ANDOR_MAX_ENCODINGS 10 // from ANDOR_ENCODING_MONO8 to
                               // ANDOR_ENCODING_RGB8PACKED

/**
 * Get the name of a given pixel encoding.
 *
 * @param encoding      The pixel encoding.
 *
 * @return The name of the encoding, `"Unknown"` if unknown.
 */
extern const char* andor_name_of_encoding(
    tao_encoding enc);

/**
 * Get the name of a given pixel encoding.
 *
 * @param encoding      The pixel encoding.
 *
 * @return The name of the encoding as a wide string (so that it can be
 * directly feed in Andor SDK functions), `L"Unknown"` if unknown.
 */
extern const wchar_t* andor_wide_name_of_encoding(
    tao_encoding enc);

/**
 * Get the identifier of a given pixel encoding name.
 *
 * @param name      The name of the pixel encoding.
 *
 * @return A pixel encoding identifier, `ANDOR_ENCODING_UNKNOWN` if unknown.
 */
extern tao_encoding andor_name_to_encoding(
    const char* name);

/**
 * Get the identifier of a given pixel encoding name.
 *
 * @param name      The name of the pixel encoding.
 *
 * @return A pixel encoding identifier, `ANDOR_ENCODING_UNKNOWN` if unknown.
 */
extern tao_encoding andor_wide_name_to_encoding(
    const wchar_t* name);

/**
 * @brief Convert raw pixels.
 *
 * This function converts the pixels from an acquisition buffer to a
 * destination array.  The destination array is assumed to have contiguous
 * pixels (no padding between lines).  The source buffer may have strided
 * lines.
 *
 * @param dst      Address of destination array.
 * @param dst_enc  Encoding in destination array.
 * @param src      Address of source buffer.
 * @param src_enc  Encoding in source buffer.
 * @param width    Number of pixels per line.
 * @param height   Number of lines.
 * @param stride   Stride in source buffer (in bytes).
 *
 * @return @ref TAO_OK on success, @ref TAO_ERROR in case of failure.
 */
extern tao_status andor_convert_buffer(
    void*       restrict dst,
    tao_encoding         dst_enc,
    const void* restrict src,
    tao_encoding         src_enc,
    long                 width,
    long                 height,
    long                 stride);

/**
 * @brief Andor camera model identifier.
 */
typedef enum andor_camera_model {
    ANDOR_MODEL_UNKNOWN,
    ANDOR_MODEL_APOGEE,
    ANDOR_MODEL_BALOR,
    ANDOR_MODEL_ISTAR,
    ANDOR_MODEL_MARANA,
    ANDOR_MODEL_NEO,
    ANDOR_MODEL_SIMCAM,
    ANDOR_MODEL_SONA,
    ANDOR_MODEL_SYSTEM,
    ANDOR_MODEL_ZYLA
} andor_camera_model;

/**
 * @}
 */

TAO_END_DECLS

#endif // TAO_ANDOR_H_
