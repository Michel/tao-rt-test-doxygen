// core.c -
//
// Core functions for Andor cameras.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2022, Éric Thiébaut.

#include "tao-andor-private.h"
#include "tao-errors.h"

#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <assert.h>

static struct {
    const char*    name;
    const wchar_t* wname;
    tao_encoding value;
} encoding_definitions[] = {
#define ENCDEF(val, str) { str, L##str, val }
    ENCDEF(ANDOR_ENCODING_MONO8, "Mono8"),
    ENCDEF(ANDOR_ENCODING_MONO12, "Mono12"),
    ENCDEF(ANDOR_ENCODING_MONO12CODED, "Mono12Coded"),
    ENCDEF(ANDOR_ENCODING_MONO12CODEDPACKED, "Mono12CodedPacked"),
    ENCDEF(ANDOR_ENCODING_MONO12PACKED, "Mono12Packed"),
    ENCDEF(ANDOR_ENCODING_MONO16, "Mono16"),
    ENCDEF(ANDOR_ENCODING_MONO22PACKEDPARALLEL, "Mono22PackedParallel"),
    ENCDEF(ANDOR_ENCODING_MONO22PARALLEL, "Mono22Parallel"),
    ENCDEF(ANDOR_ENCODING_MONO32, "Mono32"),
    ENCDEF(ANDOR_ENCODING_RGB8PACKED, "RGB8Packed"),
#undef ENCDEF
    {NULL, NULL, ANDOR_ENCODING_UNKNOWN}
};

// FIXME: shall we protect this by a mutex?
static long ndevices = -2;
#define SDK_VERSION_MAXLEN 32
static char sdk_version[SDK_VERSION_MAXLEN+1];

long andor_get_ndevices(
    void)
{
    if (ndevices < 0 && andor_initialize() != TAO_OK) {
        return -1L;
    }
    return ndevices;
}

const char* andor_get_software_version(
    void)
{
    if (sdk_version[0] == 0) {
        AT_WC version[SDK_VERSION_MAXLEN+1];
        int status;
        if (ndevices < 0) {
            if (andor_initialize() != TAO_OK) {
            error:
                tao_report_error();
                strcat(sdk_version, "0.0.0");
                return sdk_version;
            }
        }
        status = AT_GetString(AT_HANDLE_SYSTEM, L"SoftwareVersion",
                              version, SDK_VERSION_MAXLEN);
        if (status != AT_SUCCESS) {
            andor_store_error("AT_GetString(..., L\"SoftwareVersion\", ...)",
                              status);
            goto error;
        }
        for (int i = 0; i < SDK_VERSION_MAXLEN; ++i) {
            sdk_version[i] = (char)version[i];
            if (sdk_version[i] == 0) {
                break;
            }
        }
        sdk_version[SDK_VERSION_MAXLEN] = 0;
    }
    return sdk_version;
}

static const char* xmlfile = "/tmp/andor_pat_temp_CIS2051RO_AF.xml";
static mode_t logmode = (S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH);

static void finalize(
    void)
{
    // Finalize the library (if it has been initialized) and try to change
    // access permissions for the xmlfile to avoid crash for others.  Any
    // errors are ignored here.
    if (ndevices >= 0) {
        ndevices = -1;
        (void)AT_FinaliseLibrary();
    }
    (void)chmod(xmlfile, logmode);
}

tao_status andor_initialize(
    void)
{
    AT_64 ival;
    int status;

    if (ndevices == -2) {
        // Before initializing Andor Core library, we set r/w permissions for
        // all users to the xml file "/tmp/andor_pat_temp_CIS2051RO_AF.xml"
        // (otherwise initialization will crash for others).
        int fd = open(xmlfile, O_RDWR|O_CREAT, logmode);
        if (fd == -1) {
            fprintf(stderr,
                    "ERROR: xml file \"%s\" cannot be open/created for "
                    "r/w access.\n       You may either remove this file or "
                    "change its access permissions with:\n       "
                    "$ sudo chmod a+rw %s\n", xmlfile, xmlfile);
            tao_store_system_error("open");
            return TAO_ERROR;
        }
        (void)close(fd);

        // Initialize Andor Core library and schedule to perform finalization
        // at exit.
        status = AT_InitialiseLibrary();
        if (status != AT_SUCCESS) {
            andor_store_error("AT_InitialiseLibrary", status);
            return TAO_ERROR;
        }
        if (atexit(finalize) != 0) {
            tao_store_system_error("atexit");
            return TAO_ERROR;
        }
        ndevices = -1;
    }
    if (ndevices == -1) {
        status = AT_GetInt(AT_HANDLE_SYSTEM, L"DeviceCount", &ival);
        if (status != AT_SUCCESS) {
            andor_store_error("AT_GetInt(..., \"DeviceCount\", ...)", status);
            return TAO_ERROR;
        }
        if (ival < 0) {
            tao_store_error(__func__, TAO_ASSERTION_FAILED);
            return TAO_ERROR;
        }
        ndevices = ival;
    }
    return TAO_OK;
}

//-----------------------------------------------------------------------------
// TABLE OF CAMERA OPERATIONS

// Early declarations of all virtual methods to initialize table of operations.

static tao_status on_initialize(
    tao_camera* cam);

static void on_finalize(
    tao_camera* cam);

static tao_status on_reset(
    tao_camera* cam);

static tao_status on_update_config(
    tao_camera* cam);

static tao_status on_check_config(
    tao_camera* cam,
    const tao_camera_config* cfg);

static tao_status on_set_config(
    tao_camera* cam,
    const tao_camera_config* cfg);

static tao_status on_start(
    tao_camera* cam);

static tao_status on_stop(
    tao_camera* cam);

static tao_status on_release_buffer(
    tao_camera* cam);

static tao_status on_wait_buffer(
    tao_camera* cam,
    double secs);

// Table of operations of all Andor cameras.  Its address can be used to
// assert that a given TAO camera belongs to this family.
static const tao_camera_ops ops = {
    .name           = "Andor",
    .initialize     = on_initialize,
    .finalize       = on_finalize,
    .reset          = on_reset,
    .update_config  = on_update_config,
    .check_config   = on_check_config,
    .set_config     = on_set_config,
    .start          = on_start,
    .stop           = on_stop,
    .wait_buffer    = on_wait_buffer,
    .release_buffer = on_release_buffer,
};

andor_device* andor_get_device(
    tao_camera* cam)
{
    if (cam == NULL) {
        return NULL;
    }
    if (cam->ops != &ops) {
        tao_store_error("andor_get_device", TAO_BAD_DEVICE);
        return NULL;
    }
    return (andor_device*)cam;
}

//-----------------------------------------------------------------------------
// MANAGE ANDOR DEVICE INSTANCE

// Getters get_width() and get_height() must be called after setting
// sensorwidth and sensorheight in the device.
static tao_status get_sensorwidth(
    andor_device* dev,
    long* valptr);

static tao_status get_sensorheight(
    andor_device* dev,
    long* valptr);

static tao_status get_xbin(
    andor_device* dev,
    long* valptr);

static tao_status get_ybin(
    andor_device* dev,
    long* valptr);

static tao_status get_xoff(
    andor_device* dev,
    long* valptr);

static tao_status get_yoff(
    andor_device* dev,
    long* valptr);

static tao_status get_width(
    andor_device* dev,
    long* valptr);

static tao_status get_height(
    andor_device* dev,
    long* valptr);

static tao_status get_exposuretime(
    andor_device* dev,
    double* valptr);

static tao_status get_framerate(
    andor_device* dev,
    double* valptr);

static tao_status get_pixelencoding(
    andor_device* dev,
    tao_encoding* valptr);

static tao_status get_temperature(
    andor_device* dev,
    double* valptr);

static tao_status alloc_buffers(
    andor_device* dev,
    int nbufs,
    int size);

static void free_buffers(
    andor_device* dev);

static tao_status update_pixel_encodings(
    andor_device* dev);

static tao_status update_configuration(
    andor_device* dev,
    bool all);

static tao_status check_configuration(
    andor_device* dev,
    const tao_camera_config* cfg,
    const char* func);

tao_camera* andor_open_camera(
    long dev)
{
    // Check device number after initializing the library if needed.
    long ndevices = andor_get_ndevices();
    if (ndevices < 0) {
        return NULL;
    }
    if (dev < 0 || dev >= ndevices) {
        tao_store_error(__func__, TAO_BAD_DEVICE);
        return NULL;
    }

    // Allocate camera instance with the device number provided as contextual
    // data.  The remaining initialization will done by the `on_initialize`
    // virtual method.
    return tao_camera_create(&ops, &dev, sizeof(andor_device));
}

// In case of errors during initialization, we simply call the `on_finalize`
// virtual method.
static tao_status on_initialize(
    tao_camera* cam)
{
    // Fetch Andor device structure and device number.
    assert(cam->ops == &ops);
    andor_device* dev = (andor_device*)cam;
    long devnum = *(long*)cam->ctx;
    cam->ctx = NULL;

    // Open camera handle.
    int status = AT_Open(devnum, &dev->handle);
    if (status != AT_SUCCESS) {
        andor_store_error("AT_Open", status);
        dev->handle = AT_HANDLE_SYSTEM; // to not close
        return TAO_ERROR;
    }

    // Get list of supported pixel encodings.  (Must be done before updating
    // the configuration.)
    if (update_pixel_encodings(dev) != TAO_OK) {
        goto error;
    }

    // Disable centering of region of interest.
    status = AT_SetBool(dev->handle, L"VerticallyCentreAOI", AT_FALSE);
    if (status != AT_SUCCESS && status != AT_ERR_NOTIMPLEMENTED) {
        andor_store_error("AT_SetInt(..., L\"VerticallyCentreAOI\", ...)",
                          status);
        return TAO_ERROR;
    }

    // Update other parameters from the hardware.
    if (update_configuration(dev, true) != TAO_OK) {
        goto error;
    }
    return TAO_OK;

    // We branch here in case of errors.
error:
    on_finalize(cam);
    return TAO_ERROR;
}

// Finalize internal resources.  Manage to mark destroyed data as such to avoid
// freeing more than once.  This is to allow calling this method several times
// with no damages.  Acquisitions buffers are destroyed by the caller after
// calling this virtual method.
static void on_finalize(
    tao_camera* cam)
{
    assert(cam != NULL);
    assert(cam->ops == &ops);
    andor_device* dev = (andor_device*)cam;
    if (dev->handle != AT_HANDLE_SYSTEM) {
        free_buffers(dev);
        (void)AT_Close(dev->handle);
        dev->handle = AT_HANDLE_SYSTEM;
    }
}

static tao_status on_reset(
    tao_camera* cam)
{
    assert(cam->ops == &ops);
    assert(cam->runlevel == 3);
    // We shall revert to run-level 1 (sleeping) if possible.  FIXME: This is
    // not yet implemented, we could reset the USB device as done in Julia
    // code.
    tao_store_error("tao_camera_reset", TAO_UNSUPPORTED);
    return TAO_ERROR;
}

static tao_status on_update_config(
    tao_camera* cam)
{
    return update_configuration((andor_device*)cam, true);
}

static tao_status on_check_config(
    tao_camera* cam,
    const tao_camera_config* cfg)
{
    return check_configuration((andor_device*)cam, cfg,
                               "tao_camera_check_configuration");
}

static tao_status on_set_config(
    tao_camera* cam,
    const tao_camera_config* cfg)
{
    const char* func = "tao_camera_set_configuration";
    int status;
    bool changes = false;

    // Get Andor device.
    assert(cam->ops == &ops);
    andor_device* dev = (andor_device*)cam;

    // First check the configuration.
    if (check_configuration(dev, cfg, func) != TAO_OK) {
        return TAO_ERROR;
    }

    // If any changes in the ROI are requested, set the ROI parameters in the
    // order recommended in the Andor SDK doc.  We only set the parameters
    // which have changed.
    {
        tao_camera_roi* dst = &dev->base.info.config.roi;
        const tao_camera_roi* src = &cfg->roi;
        if (src->xbin  != dst->xbin  || src->ybin   != dst->ybin  ||
            src->xoff  != dst->xoff  || src->yoff   != dst->yoff  ||
            src->width != dst->width || src->height != dst->height) {
            if (src->xbin != dst->xbin) {
                status = AT_SetInt(dev->handle, L"AOIHBin", src->xbin);
                if (status != AT_SUCCESS) {
                    andor_store_error("AT_SetInt(AOIHBin)", status);
                    return TAO_ERROR;
                }
                dst->xbin = src->xbin;
                changes = true;
            }
            if (src->ybin != dst->ybin) {
                status = AT_SetInt(dev->handle, L"AOIVBin", src->ybin);
                if (status != AT_SUCCESS) {
                    andor_store_error("AT_SetInt(AOIVBin)", status);
                    return TAO_ERROR;
                }
                dst->ybin = src->ybin;
                changes = true;
            }
            if (src->width != dst->width) {
                status = AT_SetInt(dev->handle, L"AOIWidth", src->width);
                if (status != AT_SUCCESS) {
                    andor_store_error("AT_SetInt(AOIWidth)", status);
                    return TAO_ERROR;
                }
                dst->width = src->width;
                changes = true;
            }
            if (src->xoff != dst->xoff) {
                status = AT_SetInt(dev->handle, L"AOILeft", src->xoff + 1);
                if (status != AT_SUCCESS) {
                    andor_store_error("AT_SetInt(AOILeft)", status);
                    return TAO_ERROR;
                }
                dst->xoff = src->xoff;
                changes = true;
            }
            if (src->height != dst->height) {
                status = AT_SetInt(dev->handle, L"AOIHeight", src->height);
                if (status != AT_SUCCESS) {
                    andor_store_error("AT_SetInt(AOIHeight)", status);
                    return TAO_ERROR;
                }
                dst->height = src->height;
                changes = true;
            }
            if (src->yoff != dst->yoff) {
                status = AT_SetInt(dev->handle, L"AOITop", src->yoff + 1);
                if (status != AT_SUCCESS) {
                    andor_store_error("AT_SetInt(AOITop)", status);
                    return TAO_ERROR;
                }
                dst->yoff = src->yoff;
                changes = true;
            }
        }
    }

    // Change pixel encoding, pixel type and number of acquisition buffers
    // if requested so.
    {
        tao_camera_config* dst = &dev->base.info.config;
        const tao_camera_config* src = cfg;
        if (src->pixeltype != dst->pixeltype) {
            dst->pixeltype = src->pixeltype;
            changes = true;
        }
        if (src->nbufs != dst->nbufs) {
            dst->nbufs = src->nbufs;
            changes = true;
        }
        if (src->bufferencoding != dst->bufferencoding) {
            tao_encoding enc = src->bufferencoding;
            int idx = -1;
            for (int k = 0; k < dev->nencs; ++k) {
                if (dev->encs[k] == enc) {
                    idx = k;
                    break;
                }
            }
            if (idx == -1) {
                tao_store_error(func, TAO_BAD_ENCODING);
                return TAO_ERROR;
            }
            status = AT_SetEnumIndex(dev->handle, L"PixelEncoding", idx);
            if (status != AT_SUCCESS) {
                andor_store_error("AT_SetInt(PixelEncoding)", status);
                return TAO_ERROR;
            }
            dst->bufferencoding = enc;
            dst->sensorencoding = enc;
            changes = true;
        }
    }

    // Change frame rate and exposure time.  First reduce the frame rate if
    // requested, then change the exposure time if requested, finally augment
    // the frame rate if requested.
    for (int pass = 1; pass <= 2; ++pass) {
        tao_camera_config* dst = &dev->base.info.config;
        const tao_camera_config* src = cfg;
        if ((pass == 1 && src->framerate < dst->framerate) ||
            (pass == 2 && src->framerate > dst->framerate)) {
            status = AT_SetFloat(dev->handle, L"FrameRate", src->framerate);
            if (status != AT_SUCCESS) {
                andor_store_error("AT_SetFloat(FrameRate)", status);
                return TAO_ERROR;
            }
            dst->framerate = src->framerate;
            changes = true;
        }
        if (pass == 2) {
            break;
        }
        if (src->exposuretime != dst->exposuretime) {
            status = AT_SetFloat(dev->handle, L"ExposureTime",
                                 src->exposuretime );
            if (status != AT_SUCCESS) {
                andor_store_error("AT_SetFloat(ExposureTime)", status);
                return TAO_ERROR;
            }
            dst->exposuretime = src->exposuretime;
            changes = true;
        }
    }

    // If anything has changed, update the configuration in case of.
    if (changes && update_configuration(dev, true) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

static tao_status on_start(
    tao_camera* cam)
{
    // Name of the caller for reporting errors.
    const char* func = "tao_camera_start_acquisition";

    // Get Andor device and number of buffers (the high level interface
    // guarantees that `nbufs` is at least 2).
    assert(cam->ops == &ops);
    assert(cam->runlevel == 1);
    assert(cam->nbufs >= 2);
    andor_device* dev = (andor_device*)cam;

    // Make sure the camera does not use any old acquisition buffers.
    int status = AT_Flush(dev->handle);
    if (status != AT_SUCCESS) {
        andor_store_error("AT_Flush", status);
        return TAO_ERROR;
    }

    // Get the current pixel encoding.
    int idx;
    status = AT_GetEnumIndex(dev->handle, L"PixelEncoding", &idx);
    if (status != AT_SUCCESS) {
        andor_store_error("AT_GetInt(PixelEncoding)", status);
        return TAO_ERROR;
    }
    if (idx < 0 || idx >= ANDOR_MAX_ENCODINGS) {
        tao_store_error(func, TAO_OUT_OF_RANGE);
        return TAO_ERROR;

    }
    tao_encoding bufferencoding = dev->encs[idx];

    // Get size of acquisition buffers in bytes.
    AT_64 ival;
    status = AT_GetInt(dev->handle, L"ImageSizeBytes", &ival);
    if (status != AT_SUCCESS) {
        andor_store_error("AT_GetInt(ImageSizeBytes)", status);
        return TAO_ERROR;
    }
    long bufsiz = ival;

    // Get size of one row in the image in bytes.
    status = AT_GetInt(dev->handle, L"AOIStride", &ival);
    if (status != AT_SUCCESS) {
        andor_store_error("AT_GetInt(AOIStride)", status);
        return TAO_ERROR;
    }
    long stride = ival;

    // Get image size (even though it is written in the device configuration,
    // we read it again).
    long width, height;
    if (get_width(dev, &width) != TAO_OK ||
        get_height(dev, &height) != TAO_OK) {
        return TAO_ERROR;
    }

    // Instanciate camera acquisition buffers.  Buffer data and size
    // are instanciated by the on_wait_buffer method.
    tao_time unknown_time = TAO_UNKNOWN_TIME;
    for (int i = 0; i < cam->nbufs; ++i) {
        cam->bufs[i].data         = NULL;
        cam->bufs[i].size         = 0;
        cam->bufs[i].offset       = 0;
        cam->bufs[i].width        = width;
        cam->bufs[i].height       = height;
        cam->bufs[i].stride       = stride;
        cam->bufs[i].encoding     = bufferencoding;
        cam->bufs[i].serial       = 0;
        cam->bufs[i].frame_start  = unknown_time;
        cam->bufs[i].frame_end    = unknown_time;
        cam->bufs[i].buffer_ready = unknown_time;
    }

    // Allocate new device acquisition buffers as needed and queue them.
    // Beware that dev->bufs and cam->bufs (or dev->base.bufs) are two
    // different, but related, things.
    if (alloc_buffers(dev, cam->nbufs, bufsiz) != TAO_OK) {
        return TAO_ERROR;
    }
    for (int i = 0; i < dev->nbufs; ++i) {
        status = AT_QueueBuffer(dev->handle, dev->bufs[i].data,
                                dev->bufs[i].size);
        if (status != AT_SUCCESS) {
            andor_store_error("AT_QueueBuffer", status);
            return TAO_ERROR;
        }
    }

    // Set the camera to continuously acquires frames.
    AT_BOOL bval;
    status = AT_IsImplemented(dev->handle, L"CycleMode", &bval);
    if (status != AT_SUCCESS) {
        andor_store_error("AT_IsImplemented(CycleMode)", status);
        return TAO_ERROR;
    }
    if (bval == AT_TRUE) {
        status = AT_SetEnumString(dev->handle, L"CycleMode", L"Continuous");
        if (status != AT_SUCCESS) {
            andor_store_error("AT_SetEnumString(CycleMode,Continuous)", status);
            return TAO_ERROR;
        }
    }
    status = AT_IsImplemented(dev->handle, L"TriggerMode", &bval);
    if (status != AT_SUCCESS) {
        andor_store_error("AT_IsImplemented(TriggerMode)", status);
        return TAO_ERROR;
    }
    if (bval == AT_TRUE) {
        status = AT_SetEnumString(dev->handle, L"TriggerMode", L"Internal");
        if (status != AT_SUCCESS) {
            andor_store_error("AT_SetEnumString(TriggerMode,Internal)", status);
            return TAO_ERROR;
        }
    }

    // Start the acquisition.
    status = AT_Command(dev->handle, L"AcquisitionStart");
    if (status != AT_SUCCESS) {
        andor_store_error("AT_Command(AcquisitionStart)", status);
        return TAO_ERROR;
    }

    // Report success.
    return TAO_OK;
}

static tao_status on_stop(
    tao_camera* cam)
{
    // Get Andor device.
    assert(cam->ops == &ops);
    assert(cam->runlevel == 2);
    andor_device* dev = (andor_device*)cam;

    // Stop the acquisition.
    int status = AT_Command(dev->handle, L"AcquisitionStop");
    if (status != AT_SUCCESS) {
        andor_store_error("AT_Command(AcquisitionStop)", status);
        return TAO_ERROR;
    }

    // Make sure the camera no longer use any acquisition buffers.
    status = AT_Flush(dev->handle);
    if (status != AT_SUCCESS) {
        andor_store_error("AT_Flush", status);
        return TAO_ERROR;
    }

    // Report success.
    return TAO_OK;
}

static tao_status on_wait_buffer(
    tao_camera* cam,
    double secs)
{
    // Get Andor device.
    assert(cam->ops == &ops);
    assert(cam->runlevel == 2);
    assert(cam->pending >= 0);
    assert(cam->pending < cam->nbufs);
    andor_device* dev = (andor_device*)cam;

    // Convert timeout value to milliseconds.
    unsigned int tm = AT_INFINITE;
    double msecs = round(TAO_MILLISECONDS_PER_SECOND*secs);
    if (msecs < tm) {
        tm = msecs;
    }

    // Wait for a new image to be available.
    int siz;
    uint8_t* ptr;
    int status = AT_WaitBuffer(dev->handle, &ptr, &siz, tm);
    if (status != AT_SUCCESS) {
        if (status == AT_ERR_TIMEDOUT) {
            ++cam->info.timeouts;
            return TAO_TIMEOUT;
        } else {
            andor_store_error("AT_WaitBuffer", status);
            return TAO_ERROR;
        }
    }

    // Set acquisition buffer information.  FIXME: Use Andor meta-data
    // information.
    tao_time now;
    if (tao_get_monotonic_time(&now) != TAO_OK) {
        tao_panic();
    }

    // Update last acquisition buffer (we know that
    // cam->pending < cam->nbufs).
    int last = TAO_NEXT_ACQUISTION_BUFFER(cam);
    cam->bufs[last].data         = ptr;
    cam->bufs[last].size         = siz;
    cam->bufs[last].serial       = ++cam->info.frames;
    cam->bufs[last].frame_start  = now;
    cam->bufs[last].frame_end    = now;
    cam->bufs[last].buffer_ready = now;
    ++cam->pending;
    cam->last = last;
    return TAO_OK;
}

static tao_status on_release_buffer(
    tao_camera* cam)
{
    assert(cam->ops == &ops);
    assert(cam->runlevel == 2);
    assert(cam->pending > 0);
    andor_device* dev = (andor_device*)cam;
    int index = TAO_FIRST_PENDING_ACQUISITION_BUFFER(cam);
    int status = AT_QueueBuffer(dev->handle,
                                (AT_U8*)cam->bufs[index].data,
                                (   int)cam->bufs[index].size);
    if (status != AT_SUCCESS) {
        // FIXME: Not being able to re-queue the buffer probably require
        // more elaborated handling than just reporting an error: we should
        // stop acquisition and resume it or something similar.
        andor_store_error("AT_QueueBuffer", status);
        return TAO_ERROR;
    }
    --cam->pending;
    return TAO_OK;
}

const char * andor_name_of_encoding(
    tao_encoding enc)
{
    for (int i = 0; encoding_definitions[i].name != NULL; ++i) {
        if (encoding_definitions[i].value == enc) {
            return encoding_definitions[i].name;
        }
    }
    return "Unknown";
}

const wchar_t * andor_wide_name_of_encoding(
    tao_encoding enc)
{
    for (int i = 0; encoding_definitions[i].wname != NULL; ++i) {
        if (encoding_definitions[i].value == enc) {
            return encoding_definitions[i].wname;
        }
    }
    return L"Unknown";
}

tao_encoding andor_name_to_encoding(
    const char* name)
{
    for (int i = 0; encoding_definitions[i].name != NULL; ++i) {
        if (strcasecmp(encoding_definitions[i].name, name) == 0) {
            return encoding_definitions[i].value;
        }
    }
    return ANDOR_ENCODING_UNKNOWN;
}

tao_encoding andor_wide_name_to_encoding(
    const wchar_t* name)
{
    for (int i = 0; encoding_definitions[i].wname != NULL; ++i) {
        if (wcscasecmp(encoding_definitions[i].wname, name) == 0) {
            return encoding_definitions[i].value;
        }
    }
    return ANDOR_ENCODING_UNKNOWN;
}

tao_status andor_print_supported_encodings(
    FILE* out,
    const andor_device* dev)
{
    if (dev != NULL) {
        if (fprintf(out, "Supported pixel encodings: [") < 0) {
            goto fprintf_error;
        }
        for (int k = 0; k < dev->nencs; ++k) {
            if (k > 0) {
                if (fputs(", ", out) < 0) {
                    goto fputs_error;
                }
            }
            if (fprintf(out, "%s",
                        andor_name_of_encoding(dev->encs[k])) < 0) {
                goto fprintf_error;
            }
        }
        if (fputs("]\n", out) < 0) {
            goto fputs_error;
        }
    }
    return TAO_OK;
fprintf_error:
    tao_store_system_error("fprintf");
    return TAO_ERROR;
fputs_error:
    tao_store_system_error("fputs");
    return TAO_ERROR;
}

//-----------------------------------------------------------------------------
// GETTERS AND UTILITIES

static tao_status get_sensorwidth(
    andor_device* dev,
    long* valptr)
{
    AT_64 val;
    int status = AT_GetInt(dev->handle, L"SensorWidth", &val);
    if (status != AT_SUCCESS) {
        andor_store_error("AT_GetInt(Sensorwidth)", status);
        return TAO_ERROR;
    }
    *valptr = val;
    return TAO_OK;
}

static tao_status get_sensorheight(
    andor_device* dev, long* valptr)
{
    AT_64 val;
    int status = AT_GetInt(dev->handle, L"SensorHeight", &val);
    if (status != AT_SUCCESS) {
        andor_store_error("AT_GetInt(Sensorheight)", status);
        return TAO_ERROR;
    }
    *valptr = val;
    return TAO_OK;
}

static tao_status get_xbin(
    andor_device* dev,
    long* valptr)
{
    AT_64 val;
    int status = AT_GetInt(dev->handle, L"AOIHBin", &val);
    if (status != AT_SUCCESS) {
        if (status == AT_ERR_NOTIMPLEMENTED) {
            val = 1;
        } else {
            andor_store_error("AT_GetInt(AOIHBin)", status);
            return TAO_ERROR;
        }
    }
    *valptr = val;
    return TAO_OK;
}

static tao_status get_ybin(
    andor_device* dev,
    long* valptr)
{
    AT_64 val;
    int status = AT_GetInt(dev->handle, L"AOIVBin", &val);
    if (status != AT_SUCCESS) {
        if (status == AT_ERR_NOTIMPLEMENTED) {
            val = 1;
        } else {
            andor_store_error("AT_GetInt(AOIVBin)", status);
            return TAO_ERROR;
        }
    }
    *valptr = val;
    return TAO_OK;
}

static tao_status get_xoff(
    andor_device* dev,
    long* valptr)
{
    AT_64 val;
    int status = AT_GetInt(dev->handle, L"AOILeft", &val);
    if (status != AT_SUCCESS) {
        if (status == AT_ERR_NOTIMPLEMENTED) {
            val = 1;
        } else {
            andor_store_error("AT_GetInt(AOILeft)", status);
            return TAO_ERROR;
        }
    }
    *valptr = val - 1;
    return TAO_OK;
}

static tao_status get_yoff(
    andor_device* dev,
    long* valptr)
{
    AT_64 val;
    int status = AT_GetInt(dev->handle, L"AOITop", &val);
    if (status != AT_SUCCESS) {
        if (status == AT_ERR_NOTIMPLEMENTED) {
            val = 1;
        } else {
            andor_store_error("AT_GetInt(AOITop)", status);
            return TAO_ERROR;
        }
    }
    *valptr = val - 1;
    return TAO_OK;
}

// Must be called after setting sensorwidth.
static tao_status get_width(
    andor_device* dev,
    long* valptr)
{
    AT_64 val;
    int status = AT_GetInt(dev->handle, L"AOIWidth", &val);
    if (status != AT_SUCCESS) {
        if (status == AT_ERR_NOTIMPLEMENTED) {
            val = dev->base.info.sensorwidth;
        } else {
            andor_store_error("AT_GetInt(AOIWidth)", status);
            return TAO_ERROR;
        }
    }
    *valptr = val;
    return TAO_OK;
}

// Must be called after setting sensorheight.
static tao_status get_height(
    andor_device* dev,
    long* valptr)
{
    AT_64 val;
    int status = AT_GetInt(dev->handle, L"AOIHeight", &val);
    if (status != AT_SUCCESS) {
        if (status == AT_ERR_NOTIMPLEMENTED) {
            val = dev->base.info.sensorheight;
        } else {
            andor_store_error("AT_GetInt(AOIHeight)", status);
            return TAO_ERROR;
        }
    }
    *valptr = val;
    return TAO_OK;
}

static tao_status get_exposuretime(
    andor_device* dev,
    double* valptr)
{
    double val;
    int status = AT_GetFloat(dev->handle, L"ExposureTime", &val);
    if (status != AT_SUCCESS) {
        andor_store_error("AT_GetFloat(ExposureTime)", status);
        return TAO_ERROR;
    }
    *valptr = val;
    return TAO_OK;
}

static tao_status get_framerate(
    andor_device* dev,
    double* valptr)
{
    double val;
    int status = AT_GetFloat(dev->handle, L"FrameRate", &val);
    if (status != AT_SUCCESS) {
        andor_store_error("AT_GetFloat(FrameRate)", status);
        return TAO_ERROR;
    }
    *valptr = val;
    return TAO_OK;
}

static tao_status get_pixelencoding(
    andor_device* dev,
    tao_encoding* valptr)
{
    int idx;
    int status = AT_GetEnumIndex(dev->handle, L"PixelEncoding", &idx);
    if (status != AT_SUCCESS) {
        andor_store_error("AT_GetInt(PixelEncoding)", status);
        return TAO_ERROR;
    }
    if (idx < 0 || idx >= dev->nencs) {
        tao_store_error(__func__, TAO_OUT_OF_RANGE);
        return TAO_ERROR;
    }
    *valptr = dev->encs[idx];
    return TAO_OK;
}

static tao_status get_temperature(
    andor_device* dev,
    double* valptr)
{
    double val;
    int status = AT_GetFloat(dev->handle, L"SensorTemperature", &val);
    if (status != AT_SUCCESS) {
        andor_store_error("AT_GetFloat(SensorTemperature)", status);
        return TAO_ERROR;
    }
    *valptr = val;
    return TAO_OK;
}

static tao_status alloc_buffers(
    andor_device* dev,
    int nbufs,
    int size)
{
    // Check arguments.
    if (size < 0 || nbufs < 0 || (nbufs > 0 && size <= 0)) {
        tao_store_error(__func__, TAO_BAD_ARGUMENT);
        return TAO_ERROR;
    }

    // Reuse buffers if possible.
    if (dev->nbufs == nbufs) {
        bool keepbufs = true;
        for (int i = 0; i < nbufs; ++i) {
            if (dev->bufs[i].data == NULL || dev->bufs[i].size != size) {
                keepbufs = false;
                break;
            }
        }
        if (keepbufs) {
            return TAO_OK;
        }
    }

    // Explicitely free the buffers (even though there may be none).
    free_buffers(dev);

    // (Re)allocate buffers.
    dev->bufs = (andor_buffer*)tao_calloc(nbufs, sizeof(andor_buffer));
    if (dev->bufs == NULL) {
        dev->nbufs = 0;
        return TAO_ERROR;
    }
    dev->nbufs = nbufs;
    tao_status status = TAO_OK;
    for (int i = 0; i < nbufs; ++i) {
        if (status == TAO_OK) {
            dev->bufs[i].data = (AT_U8*)tao_malloc(size);
            if (dev->bufs[i].data == NULL) {
                dev->bufs[i].size = 0;
                status = TAO_ERROR;
            } else {
                dev->bufs[i].size = size;
            }
        } else {
            dev->bufs[i].data = NULL;
            dev->bufs[i].size = 0;
        }
    }
    if (status != TAO_OK) {
        free_buffers(dev);
    }
    return status;
}

static void free_buffers(
    andor_device* dev)
{
    // Make sure the frame-grabber no longer use any buffers before effectively
    // free the buffers.  The former pending buffers, if any, are gone.
    (void)AT_Flush(dev->handle);
    dev->base.pending = 0;
    andor_buffer* bufs = dev->bufs;
    int nbufs = dev->nbufs;
    dev->bufs = NULL;
    dev->nbufs = 0;
    if (bufs != NULL) {
        for (int i = 0; i < nbufs; ++i) {
            if (bufs[i].data != NULL) {
                free(bufs[i].data);
            }
        }
        free(bufs);
    }
}

static tao_status update_pixel_encodings(
    andor_device* dev)
{
    const int wstrlen = 32;
    AT_WC wstr[wstrlen];
    int cnt, status;

    // Get the number of supported current pixel encoding.
    status = AT_GetEnumCount(dev->handle, L"PixelEncoding", &cnt);
    if (status != AT_SUCCESS) {
        andor_store_error("AT_GetEnumCount(PixelEncoding)", status);
        return TAO_ERROR;
    }
    if (cnt > ANDOR_MAX_ENCODINGS) {
        tao_store_error(__func__, TAO_OUT_OF_RANGE);
        return TAO_ERROR;
    }
    for (int idx = 0; idx < cnt; ++idx) {
        status = AT_GetEnumStringByIndex(dev->handle, L"PixelEncoding",
                                         idx, wstr, wstrlen);
        if (status != AT_SUCCESS) {
            andor_store_error("AT_GetEnumStringByIndex(PixelEncoding)",
                              status);
            return TAO_ERROR;
        }
        dev->encs[idx] = andor_wide_name_to_encoding(wstr);
    }
    for (int idx = cnt; idx < ANDOR_MAX_ENCODINGS; ++idx) {
        dev->encs[idx] = TAO_ENCODING_UNKNOWN;
    }
    dev->nencs = cnt;
    return TAO_OK;
}

static tao_status update_configuration(
    andor_device* dev,
    bool all)
{
    tao_camera_info* info = &dev->base.info;
    if (all) {
        // First get the sensor size because some other getters rely on this
        // information.  FIXME: According to the doc., Apogee has no
        // ExposureTime feature.  Pixel encoding defines the encoding of the
        // pixel sent by the camera and of the acquisition buffers.
        tao_encoding enc;
        if (get_sensorwidth(  dev, &info->sensorwidth        ) != TAO_OK ||
            get_sensorheight( dev, &info->sensorheight       ) != TAO_OK ||
            get_xbin(         dev, &info->config.roi.xbin    ) != TAO_OK ||
            get_ybin(         dev, &info->config.roi.ybin    ) != TAO_OK ||
            get_xoff(         dev, &info->config.roi.xoff    ) != TAO_OK ||
            get_yoff(         dev, &info->config.roi.yoff    ) != TAO_OK ||
            get_width(        dev, &info->config.roi.width   ) != TAO_OK ||
            get_height(       dev, &info->config.roi.height  ) != TAO_OK ||
            get_exposuretime( dev, &info->config.exposuretime) != TAO_OK ||
            get_framerate(    dev, &info->config.framerate   ) != TAO_OK ||
            get_pixelencoding(dev, &enc                      ) != TAO_OK) {
            return TAO_ERROR;
        }
        info->config.sensorencoding = enc;
        info->config.bufferencoding = enc;
    }
    if (get_temperature(dev, &info->temperature) != TAO_OK) {
        return TAO_ERROR;
    }
    return TAO_OK;
}

static tao_status check_configuration(
    andor_device* dev,
    const tao_camera_config* cfg,
    const char* func)
{
    // Check region of interest.
    if (tao_camera_roi_check(&cfg->roi,
                             dev->base.info.sensorwidth,
                             dev->base.info.sensorheight) != TAO_OK) {
        tao_store_error(func, TAO_BAD_ROI);
        return TAO_ERROR;
    }

    // Check exposure time and frame rate.
    if (isnan(cfg->exposuretime) || isinf(cfg->exposuretime) ||
        cfg->exposuretime < 0) {
        tao_store_error(func, TAO_BAD_EXPOSURETIME);
        return TAO_ERROR;
    }
    if (isnan(cfg->framerate) || isinf(cfg->framerate) ||
        cfg->framerate <= 0) {
        tao_store_error(func, TAO_BAD_FRAMERATE);
        return TAO_ERROR;
    }

    // Check encoding.  With Andor cameras, it is assumed that the encoding of
    // the acquisition buffers and of the images sent by the camera must be the
    // same.
    int idx = -1;
    if (cfg->bufferencoding == cfg->sensorencoding) {
        tao_encoding enc = cfg->sensorencoding;
        for (int k = 0; k < dev->nencs; ++k) {
            if (dev->encs[k] == enc) {
                idx = k;
                break;
            }
        }
    }
    if (idx == -1) {
        goto bad_encoding;
    }

    // Check pre-processing level and pixel type.
    int nbits = 0;
    if (cfg->bufferencoding == TAO_ENCODING_MONO(8)) {
        nbits = 8;
    } else if (cfg->bufferencoding == TAO_ENCODING_MONO_PKT(12, 24)) {
        nbits = 12;
    } else if (cfg->bufferencoding == TAO_ENCODING_MONO(16)) {
        nbits = 16;
    } else if (cfg->bufferencoding == TAO_ENCODING_MONO(32)) {
        nbits = 32;
    } else {
        goto bad_encoding;
    }
    if (cfg->preprocessing == TAO_PREPROCESSING_NONE) {
        if ((cfg->pixeltype != TAO_UINT8  || nbits >  8) &&
            (cfg->pixeltype != TAO_UINT16 || nbits > 16) &&
            (cfg->pixeltype != TAO_UINT32 || nbits > 32) &&
            (cfg->pixeltype != TAO_FLOAT               ) &&
            (cfg->pixeltype != TAO_DOUBLE              )) {
            goto bad_pixeltype;
        }
    } else if (cfg->preprocessing == TAO_PREPROCESSING_AFFINE ||
               cfg->preprocessing == TAO_PREPROCESSING_FULL) {
        if (cfg->pixeltype != TAO_FLOAT && cfg->pixeltype != TAO_DOUBLE) {
            goto bad_pixeltype;
        }
    } else {
        goto bad_preprocessing;
    }

    // Check number of number of acquisition buffers.
    if (cfg->nbufs < 2) {
        tao_store_error(func, TAO_BAD_BUFFERS);
        return TAO_ERROR;
    }
    return TAO_OK;

    // Errors branch here.
bad_encoding:
    tao_store_error(func, TAO_BAD_ENCODING);
    return TAO_ERROR;
bad_pixeltype:
    tao_store_error(func, TAO_BAD_TYPE);
    return TAO_ERROR;
bad_preprocessing:
    tao_store_error(func, TAO_BAD_PREPROCESSING);
    return TAO_ERROR;
}
