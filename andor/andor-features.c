// andor-features.c -
//
// Utility program for Andor cameras: list all known features and their values.
//
//-----------------------------------------------------------------------------
//
// This file if part of TAO real-time software licensed under the MIT license
// (https://git-cral.univ-lyon1.fr/tao/tao-rt).
//
// Copyright (C) 2019-2022, Éric Thiébaut.

#include <wchar.h>
#include <string.h>
#include <tao-andor-private.h>
#include <tao-errors.h>

static void print_name(
    FILE* output,
    const wchar_t* name,
    long pad,
    int c)
{
    long len = (name != NULL ? wcslen(name) : 0);
    fprintf(output, "  %ls", (len > 0 ? name : L""));
    if (c == ':') {
        fputc(c, output);
        while (++len <= pad) {
            fputc(' ', output);
        }
    } else if (c == '>') {
        fputc(' ', output);
        while (++len <= pad) {
            fputc('-', output);
        }
        fputc(c, output);
        fputc(' ', output);
    } else if (c == '.') {
        fputc(' ', output);
        while (++len <= pad) {
            fputc('.', output);
        }
        fputc(' ', output);
    } else {
        while (++len <= pad) {
            fputc(' ', output);
        }
        fputc(' ', output);
    }
}

#define PRINT_NAME(file, name)  print_name(file, name, 30, '.')

#define PRINT_FEATURE_0(file, name, format)     \
    do {                                        \
        PRINT_NAME(file, name);                 \
        fprintf(file, format);                  \
    } while (false)

#define PRINT_FEATURE_1(file, name, format, arg1)       \
    do {                                                \
        PRINT_NAME(file, name);                         \
        fprintf(file, format, arg1);                    \
    } while (false)

#define PRINT_FEATURE_2(file, name, format, arg1, arg2) \
    do {                                                \
        PRINT_NAME(file, name);                         \
        fprintf(file, format, arg1, arg2);              \
    } while (false)

#define PRINT_FEATURE_3(file, name, format, arg1, arg2, arg3)   \
    do {                                                        \
        PRINT_NAME(file, name);                                 \
        fprintf(file, format, arg1, arg2, arg3);                \
    } while (false)

static const char* rwflags(
    unsigned int mode)
{
    unsigned int m = (mode & (ANDOR_FEATURE_READABLE|ANDOR_FEATURE_WRITABLE));
    return (m == (ANDOR_FEATURE_READABLE|ANDOR_FEATURE_WRITABLE) ? "rw" :
            (m == ANDOR_FEATURE_READABLE ? "r-" :
             ((m) == ANDOR_FEATURE_WRITABLE ? "-w" : "--")));
}

static void fatal_error(
    const wchar_t* name,
    int status,
    const char* file,
    int line)
{
    fprintf(stderr, "Unexpected error [%s] for \"%ls\" in %s, line %d\n",
            andor_error_get_name(status), name, file, line);
    exit(EXIT_FAILURE);
}

#define FATAL_ERROR(name, status) fatal_error(name, status, __FILE__, __LINE__)

static void ambiguous_type(
    const wchar_t* name,
    andor_feature_type type,
    const char* file,
    int line)
{
    fprintf(stderr, "Ambiguous type [%d] for \"%ls\" in %s, line %d\n",
            type, name, file, line);
    exit(EXIT_FAILURE);
}

#define AMBIGUOUS_TYPE(name, type) \
    ambiguous_type(name, type, __FILE__, __LINE__)

#define ASSERT(name, expr)                                              \
    do {                                                                \
        if (!(expr)) {                                                  \
            fprintf(                                                    \
                stderr,                                                 \
                "Assertion failed [%s] for \"%ls\" in %s, line %d\n",   \
                #expr, name, __FILE__, __LINE__);                       \
            /*exit(EXIT_FAILURE);*/                                     \
        }                                                               \
    } while (false)

// This version directly calls the functions of the Andor SDK to check our
// assumptions.
static void debug_feature(
    FILE* output,
    andor_camera* camera,
    const wchar_t* name,
    andor_feature_type known_type)
{
    AT_64 int_val;
    AT_BOOL bool_val;
    double fval;
    int idx;
    int code;
    unsigned int mode;
    const int wstrlen = 1024;
    wchar_t wstr[wstrlen];
    AT_H handle = (camera == NULL ? AT_HANDLE_SYSTEM : camera->handle);

    // Check that AT_IsImplemented, AT_IsReadable, AT_IsWritable, and
    // AT_IsReadOnly yield consistent results.
    code = AT_IsImplemented(handle, name, &bool_val);
    if (code != AT_SUCCESS) {
        fprintf(
            stderr, "AT_IsImplemented for \"%ls\" failed with error %s (%d)\n",
            name, andor_error_get_name(code), code);
        exit(EXIT_FAILURE);
    }
    bool implemented = (bool_val != AT_FALSE);

    code = AT_IsReadable(handle, name, &bool_val);
    if (code != (implemented ? AT_SUCCESS : AT_ERR_NOTIMPLEMENTED)) {
        fprintf(
            stderr,
            "AT_IsReadable for \"%ls\" yields unexpected result %s (%d)\n",
            name, andor_error_get_name(code), code);
        exit(EXIT_FAILURE);
    }
    bool readable = (bool_val != AT_FALSE);

    code = AT_IsWritable(handle, name, &bool_val);
    if (code != (implemented ? AT_SUCCESS : AT_ERR_NOTIMPLEMENTED)) {
        fprintf(
            stderr,
            "AT_IsWritable for \"%ls\" yields unexpected result %s (%d)\n",
            name, andor_error_get_name(code), code);
        exit(EXIT_FAILURE);
    }
    bool writable = (bool_val != AT_FALSE);

    code = AT_IsReadOnly(handle, name, &bool_val);
    if (code != (implemented ? AT_SUCCESS : AT_ERR_NOTIMPLEMENTED)) {
        fprintf(
            stderr,
            "AT_IsReadOnly for \"%ls\" yields unexpected result %s (%d)\n",
            name, andor_error_get_name(code), code);
        exit(EXIT_FAILURE);
    }
    bool readonly = (bool_val != AT_FALSE);

    ASSERT(name, implemented == readable);
    ASSERT(name, readable || ! writable);
    ASSERT(name, readonly == (readable && ! writable));
    mode = ((readable ? ANDOR_FEATURE_READABLE : 0)|
            (writable ? ANDOR_FEATURE_WRITABLE : 0));

    andor_feature_type type = ANDOR_FEATURE_NOT_IMPLEMENTED;

    code = AT_GetBool(handle, name, &bool_val);
    if (code == AT_SUCCESS &&
        implemented && type == ANDOR_FEATURE_NOT_IMPLEMENTED) {
        type = ANDOR_FEATURE_BOOLEAN;
        PRINT_FEATURE_2(
            output, name, "%s boolean = %s\n", rwflags(mode),
            (bool_val != AT_FALSE ? "true" : "false"));
    } else if (code == AT_SUCCESS &&
               type != ANDOR_FEATURE_NOT_IMPLEMENTED) {
        AMBIGUOUS_TYPE(name, type);
    } else if (code != AT_ERR_NOTIMPLEMENTED) {
        FATAL_ERROR(name, code);
    }

    code = AT_GetInt(handle, name, &int_val);
    if (code == AT_SUCCESS &&
        implemented && type == ANDOR_FEATURE_NOT_IMPLEMENTED) {
        type = ANDOR_FEATURE_INTEGER;
        PRINT_FEATURE_2(
            output, name, "%s integer = %ld\n", rwflags(mode), (long)int_val);
    } else if (code == AT_SUCCESS &&
               type != ANDOR_FEATURE_NOT_IMPLEMENTED) {
        AMBIGUOUS_TYPE(name, type);
    } else if (code != AT_ERR_NOTIMPLEMENTED) {
        FATAL_ERROR(name, code);
    }

    code = AT_GetFloat(handle, name, &fval);
    if (code == AT_SUCCESS &&
        implemented && type == ANDOR_FEATURE_NOT_IMPLEMENTED) {
        type = ANDOR_FEATURE_FLOAT;
        PRINT_FEATURE_2(
            output, name, "%s float = %g\n", rwflags(mode), fval);
    } else if (code == AT_SUCCESS &&
               type != ANDOR_FEATURE_NOT_IMPLEMENTED) {
        AMBIGUOUS_TYPE(name, type);
    } else if (code != AT_ERR_NOTIMPLEMENTED) {
        FATAL_ERROR(name, code);
    }

    code = AT_GetString(handle, name, wstr, wstrlen);
    if (code == AT_SUCCESS &&
        implemented && type == ANDOR_FEATURE_NOT_IMPLEMENTED) {
        type = ANDOR_FEATURE_STRING;
        PRINT_FEATURE_2(
            output, name, "%s string = \"%ls\"\n", rwflags(mode), wstr);
    } else if (code == AT_SUCCESS &&
               type != ANDOR_FEATURE_NOT_IMPLEMENTED) {
        AMBIGUOUS_TYPE(name, type);
    } else if (code != AT_ERR_NOTIMPLEMENTED) {
        FATAL_ERROR(name, code);
    }

    code = AT_GetEnumIndex(handle, name, &idx);
    if (code == AT_SUCCESS &&
        implemented && type == ANDOR_FEATURE_NOT_IMPLEMENTED) {
        code = AT_GetEnumStringByIndex(handle, name, idx, wstr, wstrlen);
        if (code != AT_SUCCESS) {
            FATAL_ERROR(name, code);
        }
        type = ANDOR_FEATURE_ENUMERATED;
        PRINT_FEATURE_3(
            output, name, "%s enumerated = %d => \"%ls\"\n",
            rwflags(mode), idx, wstr);
    } else if (code == AT_SUCCESS &&
               type != ANDOR_FEATURE_NOT_IMPLEMENTED) {
        AMBIGUOUS_TYPE(name, type);
    } else if (code != AT_ERR_NOTIMPLEMENTED) {
        FATAL_ERROR(name, code);
    }

    if (implemented && type == ANDOR_FEATURE_NOT_IMPLEMENTED) {
        type = ANDOR_FEATURE_COMMAND;
        PRINT_FEATURE_1(output, name, "%s command\n", rwflags(mode));
    }

    if (type == ANDOR_FEATURE_NOT_IMPLEMENTED) {
        PRINT_FEATURE_0(output, name, "--\n");
    }

    ASSERT(name, implemented == (type != ANDOR_FEATURE_NOT_IMPLEMENTED));
    if (type != ANDOR_FEATURE_NOT_IMPLEMENTED) {
        ASSERT(name, type == known_type);
    }
}

// This version calls the functions of the TAO/Andor library.
static void print_feature(
    FILE* output,
    andor_camera* camera,
    const wchar_t* name,
    andor_feature_type known_type)
{
    const int wstrlen = 1024;
    wchar_t wstr[wstrlen];
    int index;
    andor_feature_info info;
    andor_feature_type type = andor_wide_feature_get_info(
        camera, name, &info);

    ASSERT(name, type == info.type);
    if (type != ANDOR_FEATURE_NOT_IMPLEMENTED) {
        ASSERT(name, type == known_type);
    }

    // The following is not very efficient (because is amounts to query the
    // value of the feature at least twice) but it is meant to test the
    // library.
    switch (type) {
    case ANDOR_FEATURE_NOT_IMPLEMENTED:
        break;
    case ANDOR_FEATURE_BOOLEAN:
        PRINT_FEATURE_2(
            output, name, "%s boolean = %s\n", rwflags(info.mode),
            (info.value.b ? "true" : "false"));
        break;
    case ANDOR_FEATURE_INTEGER:
        PRINT_FEATURE_2(
            output, name, "%s integer = %ld\n", rwflags(info.mode),
            (long)info.value.i);
        break;
    case ANDOR_FEATURE_FLOAT:
        PRINT_FEATURE_2(
            output, name, "%s float = %g\n", rwflags(info.mode),
            info.value.f);
        break;
    case ANDOR_FEATURE_ENUMERATED:
        index = info.value.i;
        if (andor_wide_feature_get_enum_string_by_index(
                camera, name, index, wstr, wstrlen) != TAO_OK) {
            fprintf(stderr, "Unexpected error for \"%ls\" in %s, line %d\n",
                    name, __FILE__, __LINE__);
            tao_report_error();
            exit(EXIT_FAILURE);
        }
        PRINT_FEATURE_3(
            output, name, "%s enumerated = %d => \"%ls\"\n",
            rwflags(info.mode), index, wstr);
        break;
    case ANDOR_FEATURE_STRING:
        if (andor_wide_feature_get_string(
                camera, name, wstr, wstrlen) != TAO_OK) {
            fprintf(stderr, "Unexpected error for \"%ls\" in %s, line %d\n",
                    name,  __FILE__, __LINE__);
            tao_report_error();
            exit(EXIT_FAILURE);
        }
        PRINT_FEATURE_2(
            output, name, "%s string = \"%ls\"\n", rwflags(info.mode), wstr);
        break;
    case ANDOR_FEATURE_COMMAND:
        PRINT_FEATURE_1(output, name, "%s command\n", rwflags(info.mode));
        break;
    default:
        fprintf(stderr, "Unexpected type [%d] in %s, line %d\n",
                type, __FILE__, __LINE__);
        exit(EXIT_FAILURE);
    }
}

int main(
    int argc,
    char* argv[])
{
    // Initialize library and get number of attached devices.
    long ndevices = andor_get_ndevices();
    if (ndevices < 0) {
        tao_report_error();
        return EXIT_FAILURE;
    }

    // Parse arguments.
    FILE* output;
    const char* progname = tao_basename(argv[0]);
    bool debug = false;
    bool help = false;
    long devfirst = -1;
    long devlast = ndevices - 1;
    int nargs = 0; // number of positional arguments
    for (int i = 1; i < argc; ++i) {
        if (nargs == 0 && argv[i][0] == '-') {
            if (strcmp(argv[i], "-d") == 0 || strcmp(argv[i], "--debug") == 0) {
                debug = true;
                continue;
            }
            if (strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0) {
                help = true;
                goto usage;
            }
            if (strcmp(argv[i], "--") == 0) {
                continue;
            }
        }
        if (nargs == 0) {
            long devnum;
            if (tao_parse_long(argv[i], &devnum, 0) != TAO_OK ||
                devnum < -1 || devnum >= ndevices) {
                fprintf(stderr, "%s: Invalid device number %s\n",
                        progname, argv[i]);
                return EXIT_FAILURE;
            }
            devfirst = devlast = devnum;
            ++nargs;
        } else {
        usage:
            output = (help ? stdout : stderr);
            fprintf(
                output, "Usage: %s [-h|--help] [-d|--debug] [--] [dev]\n",
                progname);
            if (help) {
                fprintf(
                    output, "\n%s\n",
                    "List Andor features of connected devices or just of "
                    "device `dev` if\nspecified (set `dev` to -1 for the "
                    "system).\n"
                    "\n"
                    "Options:\n"
                    "  -d, --debug  Also check assumptions.\n"
                    "  -h, --help   Print this help.\n");
            }
            return (help ? EXIT_SUCCESS : EXIT_FAILURE);
        }
    }

    // List of known features.
    const andor_feature_definition* defs =
        andor_feature_get_known_definitions();

    // List features for devices.
    fprintf(stderr, "Found %ld connected Andor device(s).\n", ndevices);
    for (long devnum = devfirst; devnum <= devlast; ++devnum) {
        andor_camera* camera = NULL;
        if (devnum >= 0) {
            printf("\nFeatures of Andor device %ld:\n", devnum);
            camera = (andor_camera*)andor_open_camera(devnum);
            if (camera == NULL) {
                tao_report_error();
                return EXIT_FAILURE;
            }
        } else {
            printf("\nFeatures of Andor system:\n");
        }
        for (int i = 0; defs[i].wide_name != NULL; ++i) {
            if (debug) {
                debug_feature(stdout, camera, defs[i].wide_name, defs[i].type);
            } else {
                print_feature(stdout, camera, defs[i].wide_name, defs[i].type);
            }
        }
        if (camera != NULL) {
            tao_camera_destroy((tao_camera*)camera);
        }
        if (tao_any_errors(NULL)) {
            tao_report_error();
            return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;
}
