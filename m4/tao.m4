#                                                              -*- autoconf -*-
# tao.m4 -
#
# Macro definitions for configuring TAO software.
#

# Check for Alpao SDK availability.
AC_DEFUN([TAO_CHECK_ALPAO], [
    AC_ARG_ENABLE([alpao],
        AC_HELP_STRING([--enable-alpao[[=yes|no|check|DIR]]],
            [support for Alpao deformable mirrors @<:@default=check@:>@]),
        [enable_alpao=$enableval], [enable_alpao=check])
    if test "x$enable_alpao" = x; then
        AC_MSG_ERROR([empty argument for --enable-alpao is not allowed])
    fi
    AC_MSG_CHECKING([for Alpao SDK])
    ALPAO_INCDIR=
    ALPAO_LIBDIR=
    use_alpao=no
    # usage: search_alpao dir
    search_alpao() {
        if test $use_alpao = no -a \
                -f "[$]1/include/asdkWrapper.h" \
                -a \( -f "[$]1/lib64/libasdk.so" -o \
                     -f "[$]1/lib/libasdk.so" \); then
            ALPAO_INCDIR="[$]1/include"
            if test -f "[$]1/lib64/libasdk.so"; then
                ALPAO_LIBDIR="[$]1/lib64"
            else
                ALPAO_LIBDIR="[$]1/lib"
            fi
            AC_MSG_RESULT([found in $ALPAO_INCDIR and $ALPAO_LIBDIR])
            use_alpao=yes
        fi
    }
    case "${enable_alpao}" in
        no )
            AC_MSG_RESULT([no])
            ;;
        yes | check )
            # First search in EXEC_PREFIX, then in PREFIX, then in a list of
            # predefined directories.
            if test $use_alpao = no -a x"$exec_prefix" != xNONE; then
                search_alpao "$exec_prefix"
                search_alpao "$exec_prefix/alpao"
                search_alpao "$exec_prefix/libexec/alpao"
            fi
            if test $use_alpao = no -a x"$prefix" != xNONE; then
                search_alpao "$prefix"
                search_alpao "$prefix/alpao"
                search_alpao "$prefix/libexec/alpao"
            fi
            if test $use_alpao = no; then
                search_alpao /opt/alpao
                search_alpao /usr
                search_alpao /usr/local
                search_alpao /usr/local/alpao
                search_alpao /usr/local/libexec/alpao
                search_alpao /apps
                search_alpao /apps/alpao
                search_alpao /apps/libexec/alpao
            fi
            if test $use_alpao = no; then
                AC_MSG_RESULT([not found])
                if test $enable_alpao = yes; then
                    AC_MSG_ERROR([Alpao SDK not found])
                fi
            fi
            ;;
        * )
            dir="${enable_alpao}"
            search_alpao "$dir"
            if test $use_alpao = no; then
                AC_MSG_RESULT([not found in $dir])
                AC_MSG_ERROR([Alpao SDK not found, --enable-alpao argument should be "yes", "check", "no" or the directory containing "include/asdkWrapper.h" and "lib64/libasdk.so" or "lib/libasdk.so"])
            fi
            ;;
    esac
    if test "$use_alpao" = yes; then
        ALPAO_DEFS="-I'\$(ALPAO_INCDIR)'"
        ALPAO_LIBS="-L'\$(ALPAO_LIBDIR)' -lasdk"
    else
        ALPAO_DEFS=
        ALPAO_LIBS=
    fi
    AM_CONDITIONAL([USE_ALPAO], [test "$use_alpao" = yes])
    AC_SUBST([ALPAO_INCDIR], ["${ALPAO_INCDIR}"])
    AC_SUBST([ALPAO_LIBDIR], ["${ALPAO_LIBDIR}"])
    AC_SUBST([ALPAO_DEFS], ["${ALPAO_DEFS}"])
    AC_SUBST([ALPAO_LIBS], ["${ALPAO_LIBS}"])
])

# Check for Andor SDK availability.
AC_DEFUN([TAO_CHECK_ANDOR], [
    AC_ARG_ENABLE([andor],
        AC_HELP_STRING([--enable-andor[[=yes|no|check|DIR]]],
            [support for Andor cameras @<:@default=check@:>@]),
        [enable_andor=$enableval], [enable_andor=check])
    if test "x$enable_andor" = x; then
        AC_MSG_ERROR([empty argument for --enable-andor is not allowed])
    fi
    AC_MSG_CHECKING([for Andor SDK])
    ANDOR_INCDIR=
    ANDOR_LIBDIR=
    use_andor=no
    # usage: search_andor dir
    search_andor() {
        if test $use_andor = no -a \
                -f "[$]1/include/atcore.h" -a \
                -r "[$]1/lib/libatcore.so"; then
            ANDOR_INCDIR="[$]1/include"
            ANDOR_LIBDIR="[$]1/lib"
            AC_MSG_RESULT([found in $ANDOR_INCDIR and $ANDOR_LIBDIR])
            use_andor=yes
        fi
    }
    case "${enable_andor}" in
        no )
            AC_MSG_RESULT([no])
            ;;
        yes | check )
            # First search in EXEC_PREFIX, then in PREFIX, then in a list of
            # predefined directories.
            if test $use_andor = no -a x"$exec_prefix" != xNONE; then
                search_andor "$exec_prefix"
                search_andor "$exec_prefix/andor"
                search_andor "$exec_prefix/libexec/andor"
            fi
            if test $use_andor = no -a x"$prefix" != xNONE; then
                search_andor "$prefix"
                search_andor "$prefix/andor"
                search_andor "$prefix/libexec/andor"
            fi
            if test $use_andor = no; then
                search_andor /opt/andor
                search_andor /usr/local
                search_andor /usr/local/andor
                search_andor /usr/local/libexec/andor
                search_andor /apps
                search_andor /apps/andor
                search_andor /apps/libexec/andor
            fi
            if test $use_andor = no; then
                AC_MSG_RESULT([not found])
                if test $enable_andor = yes; then
                    AC_MSG_ERROR([Andor SDK not found])
                fi
            fi
            ;;
        * )
            dir="${enable_andor}"
            search_andor "$dir"
            if test $use_andor = no; then
                AC_MSG_RESULT([not found in $dir])
                AC_MSG_ERROR([Andor SDK not found, --enable-andor argument should be "yes", "check", "no" or the directory containing "include/atcore.h" and "lib/libatcore.so"])
            fi
            ;;
    esac
    if test "$use_andor" = yes; then
        ANDOR_DEFS="-I'\$(ANDOR_INCDIR)'"
        ANDOR_LIBS="-L'\$(ANDOR_LIBDIR)' -latcore"
    else
        ANDOR_DEFS=
        ANDOR_LIBS=
    fi
    AM_CONDITIONAL([USE_ANDOR], [test "$use_andor" = yes])
    AC_SUBST([ANDOR_INCDIR], ["${ANDOR_INCDIR}"])
    AC_SUBST([ANDOR_LIBDIR], ["${ANDOR_LIBDIR}"])
    AC_SUBST([ANDOR_DEFS], ["${ANDOR_DEFS}"])
    AC_SUBST([ANDOR_LIBS], ["${ANDOR_LIBS}"])
])

# Check for the availability of ActiveSilicon Phoenix libraries.
AC_DEFUN([TAO_CHECK_PHOENIX], [
    AC_ARG_ENABLE([phoenix],
        AC_HELP_STRING([--enable-phoenix[[=yes|no|check|DIR]]],
            [support for Phoenix cameras @<:@default=check@:>@]),
        [enable_phoenix=$enableval], [enable_phoenix=check])
    if test "x$enable_phoenix" = x; then
        AC_MSG_ERROR([empty argument for --enable-phoenix is not allowed])
    fi
    AC_MSG_CHECKING([for ActiveSilicon Phoenix libraries])
    PHOENIX_INCDIR=
    PHOENIX_LIBDIR=
    phoenix_dll=libphxapi-x86_64.so
    use_phoenix=no
    # usage: search_phoenix dir
    search_phoenix() {
        if test $use_phoenix = no -a -f "[$]1/include/phx_api.h"; then
            if test -r "[$]1/lib64/${phoenix_dll}"; then
                PHOENIX_LIBDIR="[$]1/lib64"
            elif test -r "[$]1/lib/${phoenix_dll}"; then
                PHOENIX_LIBDIR="[$]1/lib"
            else
                PHOENIX_LIBDIR=
            fi
            if test x"$PHOENIX_LIBDIR" != x; then
                PHOENIX_INCDIR="[$]1/include"
                AC_MSG_RESULT([found in $PHOENIX_INCDIR and $PHOENIX_LIBDIR])
                use_phoenix=yes
            fi
        fi
    }
    case "${enable_phoenix}" in
        no )
            AC_MSG_RESULT([no])
            ;;
        yes | check )
            # First search in EXEC_PREFIX, then in PREFIX, then in a list of
            # predefined directories.
            if test $use_phoenix = no -a x"$exec_prefix" != xNONE; then
                search_phoenix "$exec_prefix"
                search_phoenix "$exec_prefix/activesilicon"
                search_phoenix "$exec_prefix/libexec/activesilicon"
            fi
            if test $use_phoenix = no -a x"$prefix" != xNONE; then
                search_phoenix "$prefix"
                search_phoenix "$prefix/activesilicon"
                search_phoenix "$prefix/libexec/activesilicon"
            fi
            if test $use_phoenix = no; then
                search_phoenix /opt/activesilicon
                search_phoenix /usr/local
                search_phoenix /usr/local/activesilicon
                search_phoenix /usr/local/libexec/activesilicon
                search_phoenix /apps
                search_phoenix /apps/activesilicon
                search_phoenix /apps/libexec/activesilicon
            fi
            if test $use_phoenix = no; then
                AC_MSG_RESULT([not found])
                if test $enable_phoenix = yes; then
                    AC_MSG_ERROR([ActiveSilicon Phoenix libraries not found])
                fi
            fi
            ;;
        * )
            dir="${enable_phoenix}"
            search_phoenix "$dir"
            if test $use_phoenix = no; then
                AC_MSG_RESULT([not found in $dir])
                AC_MSG_ERROR([ActiveSilicon Phoenix libraries not found, --enable-phoenix argument should be "yes", "check", "no" or the directory containing "include/phx_api.h" and "lib64/${phoenix_dll}"])
            fi
            ;;
    esac
    if test "$use_phoenix" = yes; then
        PHOENIX_DEFS="-I'\$(PHOENIX_INCDIR)' -D_PHX_LINUX"
        PHOENIX_LIBS="-L'\$(PHOENIX_LIBDIR)' -lphxfb02-x86_64 -lphxapi-x86_64 -lphxbl-x86_64 -lphxil-x86_64 -lphxdl-x86_64"
        #PHOENIX_DLL="'\$(PHOENIX_LIBDIR)'/${phoenix_dll}"
        PHOENIX_DLL="${PHOENIX_LIBDIR}/${phoenix_dll}"
    else
        PHOENIX_DEFS=
        PHOENIX_LIBS=
        PHOENIX_DLL=
    fi
    AM_CONDITIONAL([USE_PHOENIX], [test "$use_phoenix" = yes])
    AC_SUBST([PHOENIX_INCDIR], ["${PHOENIX_INCDIR}"])
    AC_SUBST([PHOENIX_LIBDIR], ["${PHOENIX_LIBDIR}"])
    AC_SUBST([PHOENIX_DEFS], ["${PHOENIX_DEFS}"])
    AC_SUBST([PHOENIX_LIBS], ["${PHOENIX_LIBS}"])
    AC_SUBST([PHOENIX_DLL], ["${PHOENIX_DLL}"])
])

# Enable XPA message system.
AC_DEFUN([TAO_CHECK_XPA], [
    AC_ARG_WITH([xpa-defs],
        AC_HELP_STRING([--with-xpa-defs=FLAGS],
            [Pre-processor flags to compile with XPA headers @<:@default=@:>@]),
            [with_xpa_defs=$withval], [with_xpa_defs=])
    AC_ARG_WITH([xpa-libs],
        AC_HELP_STRING([--with-xpa-libs=FLAGS],
            [Linker flags to link with XPA library @<:@default=-lxpa@:>@]),
            [with_xpa_libs=$withval], [with_xpa_libs=])
    AC_ARG_ENABLE([xpa],
        AC_HELP_STRING([--enable-xpa[[=yes|no|check|force|DIR]]],
            [support for XPA messaging system @<:@default=check@:>@]),
        [enable_xpa=$enableval], [enable_xpa=check])
    # Make sure options are coherent.
    if test "x$enable_xpa" = x; then
        AC_MSG_ERROR([empty argument for --enable-xpa is not allowed])
    fi
    XPA_DEFS="$with_xpa_defs"
    XPA_LIBS="$with_xpa_libs"
    case "${enable_xpa}" in
        no )
            use_xpa=disabled
            ;;
        yes | check )
            use_xpa=try
            ;;
        force )
            use_xpa=yes
            ;;
        * )
            use_xpa=try
            if test x"$with_xpa_defs" = x -a x"$with_xpa_libs" = x; then
                XPA_DEFS="-I${enable_xpa}/include"
                XPA_LIBS="-L${enable_xpa}/lib -lxpa"
            else
                AC_MSG_WARN([argument "$enable_xpa" of option --enable-xpa is ignored when options --with-xpa-defs and/or --with-xpa-libs are specified])
            fi
            ;;
    esac
    AC_MSG_CHECKING([for XPA libraries])
    if test x"$XPA_LIBS" = x; then
        XPA_LIBS="-lxpa"
    fi
    if test $use_xpa = try; then
        ac_save_CFLAGS="$CFLAGS"
        ac_save_CPPFLAGS="$CPPFLAGS"
        ac_save_LIBS="$LIBS"
        CFLAGS="$CFLAGS $XPA_DEFS"
        LIBS="$XPA_LIBS"
        AC_LINK_IFELSE([AC_LANG_PROGRAM([[
#include <xpa.h>
int send_callback(void *send_data, void *serv_data,
                  char *paramlist, char **buf, size_t *len)
{
    return 0;
}

int recv_callback(void *recv_data, void *serv_data,
                  char *paramlist, char *buf, size_t len)
{
    return 0;
}
        ]],[[
    XPA xpa = XPANew("class", "name", "help",
                     send_callback, NULL, "",
                     recv_callback, NULL, "");
    if (xpa != NULL) {
        XPAFree(xpa);
    }
        ]])], use_xpa=yes, use_xpa=no)
        CFLAGS="$ac_save_CFLAGS"
        CPPFLAGS="$ac_save_CPPFLAGS"
        LIBS="$ac_save_LIBS"
    fi
    AC_MSG_RESULT([$use_xpa])
    if test $use_xpa = yes; then
        AC_DEFINE([TAO_USE_XPA], [1],
            [Indicate whether TAO uses XPA messaging system.])
    else
        if test $enable_xpa != no -a \
                $enable_xpa != check; then
            AC_MSG_FAILURE([failed to link with XPA library])
        fi
        XPA_DEFS=
        XPA_LIBS=
    fi
    AM_CONDITIONAL([USE_XPA], [test "$use_xpa" = yes])
    AC_SUBST([XPA_DEFS], ["${XPA_DEFS}"])
    AC_SUBST([XPA_LIBS], ["${XPA_LIBS}"])
])

# Enable FITS support via CFITSIO library.
AC_DEFUN([TAO_CHECK_CFITSIO], [
    AC_ARG_WITH([cfitsio-defs],
        AC_HELP_STRING([--with-cfitsio-defs=FLAGS],
            [Pre-processor flags to compile with CFITSIO headers @<:@default=@:>@]),
            [with_cfitsio_defs=$withval], [with_cfitsio_defs=])
    AC_ARG_WITH([cfitsio-libs],
        AC_HELP_STRING([--with-cfitsio-libs=FLAGS],
            [Linker flags to link with CFITSIO library @<:@default=-lcfitsio@:>@]),
            [with_cfitsio_libs=$withval], [with_cfitsio_libs=])
    AC_ARG_ENABLE([cfitsio],
        AC_HELP_STRING([--enable-cfitsio[[=yes|no|check|force|DIR]]],
            [support for FITS files via CFITSIO library @<:@default=check@:>@]),
        [enable_cfitsio=$enableval], [enable_cfitsio=check])
    # Make sure options are coherent.
    if test "x$enable_cfitsio" = x; then
        AC_MSG_ERROR([empty argument for --enable-cfitsio is not allowed])
    fi
    CFITSIO_DEFS="$with_cfitsio_defs"
    CFITSIO_LIBS="$with_cfitsio_libs"
    case "${enable_cfitsio}" in
        no )
            use_cfitsio=disabled
            ;;
        yes | check )
            use_cfitsio=try
            ;;
        force )
            use_cfitsio=yes
            ;;
        * )
            use_cfitsio=try
            if test x"$with_cfitsio_defs" = x -a x"$with_cfitsio_libs" = x; then
                CFITSIO_DEFS="-I${enable_cfitsio}/include"
                CFITSIO_LIBS="-L${enable_cfitsio}/lib -lcfitsio"
            else
                AC_MSG_WARN([argument "$enable_cfitsio" of option --enable-cfitsio is ignored when options --with-cfitsio-defs and/or --with-cfitsio-libs are specified])
            fi
            ;;
    esac
    AC_MSG_CHECKING([for CFITSIO library])
    if test x"$CFITSIO_LIBS" = x; then
        CFITSIO_LIBS="-lcfitsio"
    fi
    if test $use_cfitsio = try; then
        ac_save_CFLAGS="$CFLAGS"
        ac_save_CPPFLAGS="$CPPFLAGS"
        ac_save_LIBS="$LIBS"
        CFLAGS="$CFLAGS $CFITSIO_DEFS"
        LIBS="$CFITSIO_LIBS"
        AC_LINK_IFELSE([AC_LANG_PROGRAM([[
#include <unistd.h>
#include <fitsio2.h>
        ]],[[
    int status = 0;
    char* filename = "conftest.fits";
    fitsfile* fptr;

    if (fits_create_file(&fptr, filename, &status) != 0) {
        return 1;
    }
    if (fits_close_file(fptr, &status) != 0) {
        return 1;
    }
    unlink(filename);
        ]])], use_cfitsio=yes, use_cfitsio=no)
        CFLAGS="$ac_save_CFLAGS"
        CPPFLAGS="$ac_save_CPPFLAGS"
        LIBS="$ac_save_LIBS"
    fi
    AC_MSG_RESULT([$use_cfitsio])
    if test $use_cfitsio = yes; then
        AC_DEFINE([TAO_USE_CFITSIO], [1],
            [Indicate whether TAO implements FITS support via the CFITSIO library.])
    else
        if test $enable_cfitsio != no -a \
                $enable_cfitsio != check; then
            AC_MSG_FAILURE([failed to link with CFITSIO library])
        fi
        CFITSIO_DEFS=
        CFITSIO_LIBS=
    fi

    AM_CONDITIONAL([USE_CFITSIO], [test "$use_cfitsio" = yes])
    AC_SUBST([CFITSIO_DEFS], ["${CFITSIO_DEFS}"])
    AC_SUBST([CFITSIO_LIBS], ["${CFITSIO_LIBS}"])
])

AC_DEFUN([TAO_CHECK_YORICK], [
    AC_ARG_ENABLE([yorick],
        AC_HELP_STRING([--enable-yorick[[=yes|no|check|PATH]]],
            [support for Yorick @<:@default=check@:>@]),
        [enable_yorick=$enableval], [enable_yorick=check])
    if test "x$enable_yorick" = x; then
        AC_MSG_ERROR([empty argument for --enable-yorick is not allowed])
    fi
    yorick_exe=yorick
    case "${enable_yorick}" in
        no )
            use_yorick=disabled
            ;;
        yes | check )
            use_yorick=try
            ;;
        * )
            use_yorick=try
            yorick_exe="${enable_yorick}"
            ;;
    esac
    AC_MSG_CHECKING([for Yorick executable])
    if test ${use_yorick} = try; then
        use_yorick=no
        echo >conftest.i "nl = strchar(char(0x0a)); write, format=\"Y_HOME=%s\"+nl+\"Y_SITE=%s\"+nl+\"Y_EXE=%s\"+nl, Y_HOME, Y_SITE, get_argv()(1); quit;"
        if "$yorick_exe" -batch conftest.i >conftest.out 2>/dev/null; then
            YORICK_HOME=$(sed <conftest.out -e '/^Y_HOME=/!d;s/^Y_HOME=//')
            YORICK_SITE=$(sed <conftest.out -e '/^Y_SITE=/!d;s/^Y_SITE=//')
            YORICK_EXE=$(sed <conftest.out -e '/^Y_EXE=/!d;s/^Y_EXE=//')
            if test -d "$YORICK_HOME" -a -d "$YORICK_SITE" -a \
                    -x "$YORICK_EXE"; then
                use_yorick=yes
            fi
        fi
        rm -f conftest.i conftest.out
    fi
    AC_MSG_RESULT([$use_yorick])
    if test $use_yorick != yes -a \
            $enable_yorick != no -a \
            $enable_yorick != check; then
       AC_MSG_FAILURE([failed to execute simple Yorick script with "$yorick_exe"])
    fi
    AM_CONDITIONAL([USE_YORICK], [test "$use_yorick" = yes])
    AC_SUBST([YORICK_EXE], [$YORICK_EXE])
    AC_SUBST([YORICK_MAKEDIR], [$YORICK_HOME])
    AC_SUBST([YORICK_HOME], [$YORICK_HOME])
    AC_SUBST([YORICK_SITE], [$YORICK_SITE])
])

# Check for functions in POSIX threads library.
AC_DEFUN([TAO_CHECK_PTHREAD_FUNCS],[
    AC_CACHE_CHECK(
        [whether pthread_mutex_timedlock is provided by POSIX Threads Library],
        [tao_cv_have_pthread_mutex_timedlock],
        [_TAO_CHECK_PTHREAD_MUTEX_TIMEDLOCK(
            [tao_cv_have_pthread_mutex_timedlock=yes],
            [tao_cv_have_pthread_mutex_timedlock=no])])
    if test x"$tao_cv_have_pthread_mutex_timedlock" = xyes; then
        AC_DEFINE([HAVE_PTHREAD_MUTEX_TIMEDLOCK], 1,
            [Define if pthread_mutex_timedlock is provided by POSIX Threads Library.])
    fi
    AC_CACHE_CHECK(
        [whether pthread_rwlock_timedrdlock is provided by POSIX Threads Library],
        [tao_cv_have_pthread_rwlock_timedrdlock],
        [_TAO_CHECK_PTHREAD_RWLOCK_TIMEDRDLOCK(
            [tao_cv_have_pthread_rwlock_timedrdlock=yes],
            [tao_cv_have_pthread_rwlock_timedrdlock=no])])
    if test x"$tao_cv_have_pthread_rwlock_timedrdlock" = xyes; then
        AC_DEFINE([HAVE_PTHREAD_RWLOCK_TIMEDRDLOCK], 1,
            [Define if pthread_rwlock_timedrdlock is provided by POSIX Threads Library.])
    fi
    AC_CACHE_CHECK(
        [whether pthread_rwlock_timedwrlock is provided by POSIX Threads Library],
        [tao_cv_have_pthread_rwlock_timedwrlock],
        [_TAO_CHECK_PTHREAD_RWLOCK_TIMEDWRLOCK(
            [tao_cv_have_pthread_rwlock_timedwrlock=yes],
            [tao_cv_have_pthread_rwlock_timedwrlock=no])])
    if test x"$tao_cv_have_pthread_rwlock_timedwrlock" = xyes; then
        AC_DEFINE([HAVE_PTHREAD_RWLOCK_TIMEDWRLOCK], 1,
            [Define if pthread_rwlock_timedwrlock is provided by POSIX Threads Library.])
    fi
    ]) # TAO_CHECK_PTHREAD_FUNCS

AC_DEFUN([_TAO_CHECK_PTHREAD_MUTEX_TIMEDLOCK], [
    tao_save_CFLAGS="$CFLAGS"
    tao_save_CPPFLAGS="$CPPFLAGS"
    tao_save_LIBS="$LIBS"
    CFLAGS="$CFLAGS"
    LIBS="$PTHREADLIB"
    AC_LANG_PUSH([C])
    AC_LINK_IFELSE([AC_LANG_PROGRAM([[
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
        ]],[[
    pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
    struct timespec abstime;
    if (clock_gettime(CLOCK_REALTIME, &abstime) == 0) {
        abstime.tv_sec += 1;
        if (pthread_mutex_timedlock(&mutex, &abstime) == 0) {
            if (pthread_mutex_unlock(&mutex) == 0) {
                return EXIT_SUCCESS;
            }
        }
    }
    return EXIT_SUCCESS;
    ]])],[$1],[$2])
    AC_LANG_POP([C])
    CFLAGS="$tao_save_CFLAGS"
    CPPFLAGS="$tao_save_CPPFLAGS"
    LIBS="$tao_save_LIBS"])

AC_DEFUN([_TAO_CHECK_PTHREAD_RWLOCK_TIMEDRDLOCK], [
    tao_save_CFLAGS="$CFLAGS"
    tao_save_CPPFLAGS="$CPPFLAGS"
    tao_save_LIBS="$LIBS"
    CFLAGS="$CFLAGS"
    LIBS="$PTHREADLIB"
    AC_LANG_PUSH([C])
    AC_LINK_IFELSE([AC_LANG_PROGRAM([[
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
        ]],[[
    pthread_rwlock_t rwlock = PTHREAD_RWLOCK_INITIALIZER;
    struct timespec abstime;
    if (clock_gettime(CLOCK_REALTIME, &abstime) == 0) {
        abstime.tv_sec += 1;
        if (pthread_rwlock_timedrdlock(&rwlock, &abstime) == 0) {
            if (pthread_rwlock_unlock(&rwlock) == 0) {
                return EXIT_SUCCESS;
            }
        }
    }
    return EXIT_SUCCESS;
    ]])],[$1],[$2])
    AC_LANG_POP([C])
    CFLAGS="$tao_save_CFLAGS"
    CPPFLAGS="$tao_save_CPPFLAGS"
    LIBS="$tao_save_LIBS"])

AC_DEFUN([_TAO_CHECK_PTHREAD_RWLOCK_TIMEDWRLOCK], [
    tao_save_CFLAGS="$CFLAGS"
    tao_save_CPPFLAGS="$CPPFLAGS"
    tao_save_LIBS="$LIBS"
    CFLAGS="$CFLAGS"
    LIBS="$PTHREADLIB"
    AC_LANG_PUSH([C])
    AC_LINK_IFELSE([AC_LANG_PROGRAM([[
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
        ]],[[
    pthread_rwlock_t rwlock = PTHREAD_RWLOCK_INITIALIZER;
    struct timespec abstime;
    if (clock_gettime(CLOCK_REALTIME, &abstime) == 0) {
        abstime.tv_sec += 1;
        if (pthread_rwlock_timedwrlock(&rwlock, &abstime) == 0) {
            if (pthread_rwlock_unlock(&rwlock) == 0) {
                return EXIT_SUCCESS;
            }
        }
    }
    return EXIT_SUCCESS;
    ]])],[$1],[$2])
    AC_LANG_POP([C])
    CFLAGS="$tao_save_CFLAGS"
    CPPFLAGS="$tao_save_CPPFLAGS"
    LIBS="$tao_save_LIBS"])
